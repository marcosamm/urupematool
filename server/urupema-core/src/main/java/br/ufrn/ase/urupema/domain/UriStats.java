package br.ufrn.ase.urupema.domain;

import java.util.Collection;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(of = "uri")
public class UriStats implements Comparable<UriStats>{
	private String uri;
	private Long count;
	private Long usersCount;
	private Set<UserStats> usersStats;
	private Date firstDate;
	private Date lastDate;
	
	public UriStats(String uri, Long count, Long usersCount, Date firstDate, Date lastDate) {
		this.uri = uri;
		this.count = count;
		this.usersCount = usersCount;
		this.usersStats = new TreeSet<UserStats>();
		this.firstDate = firstDate;
		this.lastDate = lastDate;
	}
	
	public void addCount(Long count){
		this.count += count;
	}
	
	public void addUsersCount(Long usersCount) {
		this.usersCount += usersCount;
	}
	
	public void addUserStats(UserStats userStats) {
		UserStats exist = this.usersStats.stream()
				.filter(element -> element.equals(userStats))
				.findAny()
				.orElse(null);
		
		if(exist == null) {
			this.usersStats.add(userStats);
		} else {
			exist.addCount(userStats.getCount());
		}
	}
	
	public void addUsersStats(Collection<UserStats> usersStats) {
		for (UserStats userStats : usersStats) {
			addUserStats(userStats);
		}
	}
	
	@Override
	public int compareTo(UriStats o) {
		int ret = o.getCount().compareTo(this.count);
		
		if(ret == 0) {
			ret = o.getUri().compareTo(this.uri);
		}
		
		return ret;
	}
	
	public void join(UriStats uriStats) {
		this.addCount(uriStats.getCount());
		this.addUsersCount(uriStats.getUsersCount());
		this.addUsersStats(uriStats.getUsersStats());
		this.firstDate = this.firstDate.before(uriStats.firstDate) ? this.firstDate : uriStats.firstDate;
		this.lastDate = this.lastDate.after(uriStats.lastDate) ? this.lastDate : uriStats.lastDate;
	}
}
