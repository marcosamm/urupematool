package br.ufrn.ase.urupema.parser;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;

import br.ufrn.ase.urupema.domain.SimpleWebExecutionTrace;
import br.ufrn.ase.urupema.domain.TraceElement;
import br.ufrn.ase.urupema.domain.TraceElementImpl;
import br.ufrn.ase.urupema.util.UrlUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StackTraceParser {
	private final static String ERROR = "Error";
	private final static String EXCEPTION = "Exception";
	private final static String FAULT = "Fault";
	private final static String FAILURE = "Failure";
	private final static String JAVA_EXCEPTION_CLASS = "([\\w<>\\$_]+\\.)+[\\w<>\\$_]*("+ERROR+"|"+EXCEPTION+"|"+FAULT+"|"+FAILURE+"){1}";
	private final static String JAVA_EXCEPTION_CLASS_GROUP = "("+JAVA_EXCEPTION_CLASS+")";
	private final static String JAVA_EXCEPTION_CLASS_NAMED_GROUP = "(?<exception>("+JAVA_EXCEPTION_CLASS+"))";
	private final static String JAVA_EXCEPTION_LINE_GROUP = "("+JAVA_EXCEPTION_CLASS_GROUP+".*?)";
	//--- Não entram na composição da stack trace, mas são usados em outros lugares ---
	private final static String AT_LINE_NUMBER = "at line \\d+";
	private final static String AT_LINE_NUMBER_NAMED_GROUP = "(?<atLineNumber>"+AT_LINE_NUMBER+")";
	private final static String LINE_NUMBER = "\\d+:{1}";
	private final static String LINE_NUMBER_GROUP_NAME = "lineNumber";
	private final static String LINE_NUMBER_NAMED_GROUP = " (?<"+LINE_NUMBER_GROUP_NAME+">"+LINE_NUMBER+")";
	//------
	private final static String XHTMLSOURCE = "\\s*\\d+:{1}\\s.*?\\s*";
	private final static String XHTMLSOURCE_GROUP_NAME = "xhtmlSource";
	private final static String XHTMLSOURCE_GROUP = "("+XHTMLSOURCE+")";
	private final static String XHTMLSOURCE_NAMED_GROUP = "(?<"+XHTMLSOURCE_GROUP_NAME+">"+XHTMLSOURCE+")";
	private final static String STACKTRACE_WORD = "Stacktrace:";
	private final static String STACKTRACE_WORD_GROUP = "("+STACKTRACE_WORD+")";
	private final static String CAUSEDBY_WORD = "Caused by: ";
	private final static String CAUSEDBY_EXCEPTION_LINE_GROUP = "("+CAUSEDBY_WORD+JAVA_EXCEPTION_CLASS_GROUP+".*?)";
	private final static String AT_LINE_FULL_CLASS_NAME = "\\S+";
	private final static String AT_LINE_FULL_CLASS_NAME_GROUP = "("+AT_LINE_FULL_CLASS_NAME+")";
	private final static String AT_LINE_FULL_CLASS_NAME_GROUP_NAME = "fullClassName";
	private final static String AT_LINE_FULL_CLASS_NAME_NAMED_GROUP = "(?<"+AT_LINE_FULL_CLASS_NAME_GROUP_NAME+">"+AT_LINE_FULL_CLASS_NAME+")";
	private final static String AT_LINE_METHOD_NAME = "[^\\.]*";
	private final static String AT_LINE_METHOD_NAME_GROUP = "("+AT_LINE_METHOD_NAME+")";
	private final static String AT_LINE_METHOD_NAME_GROUP_NAME = "methodName";
	private final static String AT_LINE_METHOD_NAME_NAMED_GROUP = "(?<"+AT_LINE_METHOD_NAME_GROUP_NAME+">"+AT_LINE_METHOD_NAME+")";
	private final static String AT_LINE_FILE_NAME = "[<]{0,1}\\w+[\\s|\\.]{0,1}\\w+[>]{0,1}";
	private final static String AT_LINE_FILE_NAME_GROUP = "("+AT_LINE_FILE_NAME+")";
	private final static String AT_LINE_FILE_NAME_GROUP_NAME = "fileName";
	private final static String AT_LINE_FILE_NAME_NAMED_GROUP = "(?<"+AT_LINE_FILE_NAME_GROUP_NAME+">"+AT_LINE_FILE_NAME+")";
	private final static String AT_LINE_FILE_LINE = "\\d+";
	private final static String AT_LINE_FILE_LINE_GROUP = "("+AT_LINE_FILE_LINE+")";
	private final static String AT_LINE_FILE_LINE_GROUP_NAME = "fileLine";
	private final static String AT_LINE_FILE_LINE_NAMED_GROUP = "(?<"+AT_LINE_FILE_LINE_GROUP_NAME+">"+AT_LINE_FILE_LINE+")";
	private final static String AT_LINE_SIGNATURE  = AT_LINE_FULL_CLASS_NAME_GROUP+"\\."+AT_LINE_METHOD_NAME_GROUP;
	private final static String AT_LINE_SIGNATURE_GROUP = "("+AT_LINE_SIGNATURE+")";
	private final static String AT_LINE_SIGNATURE_SUBGROUP_NAMED  = AT_LINE_FULL_CLASS_NAME_NAMED_GROUP+"\\."+AT_LINE_METHOD_NAME_NAMED_GROUP;
	private final static String AT_LINE_SIGNATURE_GROUP_NAME  = "signature";
	private final static String AT_LINE_SIGNATURE_NAMED_GROUP = "(?<"+AT_LINE_SIGNATURE_GROUP_NAME+">"+AT_LINE_SIGNATURE_SUBGROUP_NAMED+")";
	private final static String AT_LINE_GROUP = "(at\\s"+AT_LINE_SIGNATURE_GROUP+"\\("+AT_LINE_FILE_NAME_GROUP+"{0,1}:{0,1}"+AT_LINE_FILE_LINE_GROUP+"{0,1}\\))";
	private final static String AT_LINE_GROUP_NAME  = "atLine";
	private final static String AT_LINE_NAMED_GROUP = "(?<"+AT_LINE_GROUP_NAME+">at\\s"+AT_LINE_SIGNATURE_NAMED_GROUP+"\\("+AT_LINE_FILE_NAME_NAMED_GROUP+"{0,1}:{0,1}"+AT_LINE_FILE_LINE_NAMED_GROUP+"{0,1}\\))";
	private final static String MORE_LINE  = "\\.\\.\\. \\d* more";
	private final static String MORE_LINE_GROUP  = "("+MORE_LINE+")";
	private final static String MORE_LINE_GROUP_NAME = "moreLine";
	private final static String MORE_LINE_NAMED_GROUP  = "(?<"+MORE_LINE_GROUP_NAME+">"+MORE_LINE+")";
	private final static String JAVA_HIGHLEVEL_EXCEPTION = 
			"(" +
					JAVA_EXCEPTION_LINE_GROUP + "{1}" +
					XHTMLSOURCE_GROUP + "*" +
					STACKTRACE_WORD_GROUP+ "{0,1}\\s*" +
					"("+AT_LINE_GROUP+"\\s*){1,}" +
					MORE_LINE_GROUP+"{0,1}\\s*"+
			")";
	private final static String JAVA_CAUSEDBY_EXCEPTION = 
			"(?<causedBy>" +
					CAUSEDBY_EXCEPTION_LINE_GROUP + "\\s*" +
					"("+AT_LINE_GROUP+"\\s*){1,}" +
					MORE_LINE_GROUP+"{0,1}\\s*"+
			")";
	private final static String JAVA_STACKTRACE  = 
			"(" +
				JAVA_HIGHLEVEL_EXCEPTION +
				"(?<causedBys>(" + JAVA_CAUSEDBY_EXCEPTION + "\\s*)*)" +
			")";
	private final static Pattern PATTERN_JAVA_STACKTRACE = Pattern.compile(JAVA_STACKTRACE);
	private final static Pattern PATTERN_AT_LINE = Pattern.compile(AT_LINE_NAMED_GROUP);
	private final static Pattern PATTERN_MORE_LINE = Pattern.compile(MORE_LINE_NAMED_GROUP);
	
	private final static String FILE_NAME_GROUP_NAME = "fileName";
	private final static Pattern PATTERN_FILE_NAME = Pattern.compile("\\s(?<"+FILE_NAME_GROUP_NAME+">(\\/\\w+)+\\.\\w*)\\s");
	private final static String URI = "/[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
	private final static Pattern PATTERN_URI = Pattern.compile("\\s"+URI);
	private final static Pattern PATTERN_URL = Pattern.compile("\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
	
	private final static String LINES_WITH_POSSIBLE_MESSAGES_GROUP_NAME = "linesWithoutAtOrMore";
	private final static Pattern PATTERN_LINES_WITH_POSSIBLE_MESSAGES = Pattern.compile("(?<"+LINES_WITH_POSSIBLE_MESSAGES_GROUP_NAME+">^((?!(^\\s*at |^\\s*"+MORE_LINE+")).)*$)", Pattern.MULTILINE);
	private final static String ONLY_EXCEPTIONS_IN_LINE_WHITH_POSSIBLE_MESSAGES_GROUP_NAME = "onlyExceptionsInLine";
	private final static Pattern PATTERN_ONLY_EXCEPTIONS_IN_LINE_WHITH_POSSIBLE_MESSAGES = Pattern.compile("(?<"+ONLY_EXCEPTIONS_IN_LINE_WHITH_POSSIBLE_MESSAGES_GROUP_NAME+">("+CAUSEDBY_WORD+"){0,1}(?<exceptions>(?<class>(\\s*[#{\\w]+\\.){1,}[\\w}]+)[:]{0,1})+)", Pattern.MULTILINE);
	private final static String JSF_EXPRESSION_GROUP_NAME = "jsfExpression";
	private final static Pattern PATTERN_JSF_EXPRESSION = Pattern.compile("(?<"+JSF_EXPRESSION_GROUP_NAME+">[$#]\\{\\s*.*?\\})");
	
	private String regexSignature;
	private String regexIgnoreSignature;
	
	public StackTraceParser() {
		
	}
	
	public StackTraceParser(String regexSignature) {
		this.regexSignature = regexSignature;
	}
	
	public StackTraceParser(String regexSignature, String regexIgnoreSignature) {
		this.regexSignature = regexSignature;
		this.regexIgnoreSignature = regexIgnoreSignature;
	}
	
	public List<StackTraceElement> parse(String stringStackTrace){
		List<StackTraceElement> elements = new ArrayList<StackTraceElement>();
		
		String formatedStackTrace = formatStackTrace(stringStackTrace);
		
		String javaStackTrace = null;
		if(!formatedStackTrace.contains(STACKTRACE_WORD)) {
			javaStackTrace = formatedStackTrace;
		}else {
			String[] s2 = formatedStackTrace.split(STACKTRACE_WORD);
			if(s2.length > 1) {
				javaStackTrace = STACKTRACE_WORD + "\n"+s2[1];
			}else {
				javaStackTrace = "";
			}
		}
		
		String[] s = javaStackTrace.split(CAUSEDBY_WORD);
		
		for(int i=s.length-1; i >= 0; i--){
			Matcher atLineMatcher = PATTERN_AT_LINE.matcher(s[i]);
			while(atLineMatcher.find()) {
				String classAndMethod = atLineMatcher.group("signature");
				if(classAndMethod != null && regexSignature == null || (regexSignature != null && classAndMethod.matches(regexSignature))){
					log.debug(classAndMethod + "matches with regexSignature (" + regexSignature + ")");
					if(regexIgnoreSignature == null || !classAndMethod.matches(regexIgnoreSignature)){
						log.debug(classAndMethod + "not matches with regexIgnoreSignature (" + regexIgnoreSignature + "): Including");
						String declaringClass = atLineMatcher.group(AT_LINE_FULL_CLASS_NAME_GROUP_NAME);
						String methodName = atLineMatcher.group(AT_LINE_METHOD_NAME_GROUP_NAME);
						String fileName = atLineMatcher.group(AT_LINE_FILE_NAME_GROUP_NAME);
						int lineNumber = atLineMatcher.group(AT_LINE_FILE_LINE_GROUP_NAME)!=null?Integer.parseInt(atLineMatcher.group(AT_LINE_FILE_LINE_GROUP_NAME)):-1;
						elements.add(new StackTraceElement(declaringClass, methodName, fileName, lineNumber));
					}else {
						log.debug(classAndMethod + " matches with regexIgnoreSignature (" + regexIgnoreSignature + "): Excluding");
					}
				}
		    }
		}
		
		return elements;
	}
	
	public SimpleWebExecutionTrace toSimpleError(String stackTrace){
		SimpleWebExecutionTrace se = new SimpleWebExecutionTrace();
		se.setStackTrace(stackTrace);
		TraceElement tElements = TraceElementImpl.toTraceElement(parse(stackTrace));
		if(tElements != null) {
			se.setTraceElements(Arrays.asList(tElements));
		} else {
			se.setTraceElements(Arrays.asList());
		}
		return se;
	}
	
	public String getTopFrameFile(String stackTrace) {
		String topFrameStr = null;
		
		SimpleWebExecutionTrace representantStacktraceGroup = toSimpleError(stackTrace);
		//Extrai o método do topo da pilha
		if(representantStacktraceGroup.getTraceElements().isEmpty()) {
			//Não há stacktrace, tenta pegar o nome do arquivo na mensagem de erro
			Matcher fileNameMatcher = PATTERN_FILE_NAME.matcher(stackTrace);
			if(fileNameMatcher.find()) {
				topFrameStr = fileNameMatcher.group(FILE_NAME_GROUP_NAME);
			} else {
				Matcher urlMatcher = PATTERN_URL.matcher(stackTrace);
				Matcher uriMatcher = PATTERN_URI.matcher(stackTrace);
				if(urlMatcher.find()) {
					topFrameStr = UrlUtil.getUri(urlMatcher.group());
				}else if(uriMatcher.find()) {
					topFrameStr = uriMatcher.group().trim();
				} else {
					log.debug("Qualified file name not found:" + stackTrace);
					topFrameStr = "Undefined";
				}
			}
		} else {
			TraceElement topFrame = representantStacktraceGroup.getTraceElements().get(0);
			while(!topFrame.getChildren().isEmpty()) {
				topFrame = topFrame.getChildren().get(0);
			}
			topFrameStr = topFrame.getQualifiedFileName();
		}
		
		return topFrameStr;
	}
	
	public static String htmlToText(String html) {
		final String INIT_METHOD = "<init>", INIT_METHOD_TMP_VAR = "_TMP_VAR_INIT_";
		final String CINIT_METHOD = "<cinit>", CINIT_METHOD_TMP_VAR = "_TMP_VAR_CINIT_";
		final String GENERATED_FILENAME = "<generated>", GENERATED_FILENAME_TMP_VAR = "_TMP_VAR_GENERATED_";
		String preservWords = html.trim()
				.replaceAll(INIT_METHOD, INIT_METHOD_TMP_VAR)
				.replaceAll(CINIT_METHOD, CINIT_METHOD_TMP_VAR)
				.replaceAll(GENERATED_FILENAME, GENERATED_FILENAME_TMP_VAR);
		String noHtml = Jsoup.parse(preservWords).text().trim();
		String plainText = noHtml
				.replaceAll(INIT_METHOD_TMP_VAR, INIT_METHOD)
				.replaceAll(CINIT_METHOD_TMP_VAR, CINIT_METHOD)
				.replaceAll(GENERATED_FILENAME_TMP_VAR, GENERATED_FILENAME);
		return plainText;
	}
	
	private static String extractFormattedXhtmlSource(String stackTrace) {
		String originalXhtmlSource = null;
		Matcher atLineNumberMatcher = Pattern.compile(AT_LINE_NUMBER_NAMED_GROUP).matcher(stackTrace);
		if(atLineNumberMatcher.find()) {
			String atLineNumber = atLineNumberMatcher.group();
			int startIndex = stackTrace.indexOf(atLineNumber) + atLineNumber.length();
			int endIndex = stackTrace.indexOf(STACKTRACE_WORD);
			if(startIndex < endIndex) {
				originalXhtmlSource = stackTrace.substring(startIndex, endIndex);
				if(originalXhtmlSource.split("\n").length == 1) {
					originalXhtmlSource = originalXhtmlSource.replaceAll(LINE_NUMBER_NAMED_GROUP, "\n${"+LINE_NUMBER_GROUP_NAME+"}");
					originalXhtmlSource = "\n\n" + originalXhtmlSource.trim() + "\n\n\n";
				}
			}
		}
		return originalXhtmlSource;
	}
	
	private static String extractTextSourceInSingleLineStackTrace(String singleLineStackTrace) {
		String textSource = null;
		if(singleLineStackTrace.contains(" at line ") && singleLineStackTrace.contains(STACKTRACE_WORD)) {
			Matcher atLineNumberMatcher = Pattern.compile(AT_LINE_NUMBER_NAMED_GROUP).matcher(singleLineStackTrace);
			atLineNumberMatcher.find();
			String atLine = atLineNumberMatcher.group();
			int indexStartXhtmlSource = singleLineStackTrace.indexOf(atLine) + atLine.length();
			int indexEndXhtmlSource = singleLineStackTrace.indexOf(STACKTRACE_WORD);
			if(indexStartXhtmlSource < indexEndXhtmlSource) {
				textSource = singleLineStackTrace.substring(indexStartXhtmlSource, indexEndXhtmlSource);
			}
		}
		return textSource;
	}
	
	public static String formatStackTrace(String stackTrace) {
		boolean isValid = false;
		
		String originalXhtmlSource = extractFormattedXhtmlSource(stackTrace);
		String unformated = htmlToText(stackTrace);
		String st = unformated;
		Matcher stackTraceMatcher = PATTERN_JAVA_STACKTRACE.matcher(st);
		try {
		if(stackTraceMatcher.lookingAt()) {
			String s = stackTraceMatcher.group();
			st = st.replaceAll("[\\s+|\\n+|\\t+]*"+AT_LINE_NAMED_GROUP, "\n\t${"+AT_LINE_GROUP_NAME+"}");
			st = st.replaceAll("[\\s+|\\n+|\\t+]*" + MORE_LINE_NAMED_GROUP, "\n\t${"+MORE_LINE_GROUP_NAME+"}");
			st = st.replaceAll("[\\s+|\\n+|\\t+]*"+CAUSEDBY_WORD, "\n"+CAUSEDBY_WORD);
			st = st.replaceAll("[\\s+|\\n+|\\t+]*"+STACKTRACE_WORD, "\n"+STACKTRACE_WORD);
			if(originalXhtmlSource != null) {
				String textXhtmlSource = extractTextSourceInSingleLineStackTrace(st);
				st = st.replace(textXhtmlSource, originalXhtmlSource);
			} else {
				st = st.replaceAll(XHTMLSOURCE_NAMED_GROUP, "\n${"+XHTMLSOURCE_GROUP_NAME+"}");
			}
			if(unformated.equals(s)) {
				isValid = true;
			} else {
				log.warn("---------------------------- INVALID STACK TRACE ---------------------------");
				log.warn(stackTrace);
				log.warn("---------------------------- FORMATED GROUP ---------------------------------");
				log.warn(st);
				log.warn("-----------------------------------------------------------------------------");
			}
		}
		} catch (Error | Exception e) {
			st = stackTrace; //Em caso de erro, mantem o original
			log.warn("---------------------------- ERROR - formatSackTrace (" + e.getMessage() + ") ---------------------------");
			log.warn(stackTrace);
			log.warn("---------------------------- ERROR - formatSackTrace ---------------------------");
		}
		
		if(!isValid){
			//throw new IllegalArgumentException("Invalid stack trace");
		}
		return st.trim();
	}
	
	/**
	 * Replace $Proxy377 -> $Proxy000
	 * Replace $12 -> $000
	 * @param stackTraces
	 * @return
	 */
	public static String equivalentStackTrace(String stackTrace) {
		String str = replaceProxy(stackTrace);
		str = replaceGeneratedMethodAccessor(str);
		str = replaceEnhancerByCGLIB(str);
		str = replaceFastClassByCGLIB(str);
		str = formatStackTrace(str);
		return str;
	}
	
	
	/**
	 * Replace $Proxy377 -> $Proxy000
	 * Replace $12 -> $000
	 * @param stackTraces
	 * @return
	 */
	private static String replaceProxy(String stackTrace) {
		String str = stackTrace.replaceAll("(\\$Proxy[0-9]*)", "\\$Proxy000");
		str = str.replaceAll("(\\$[0-9]{1,})", "\\$000");
		return str;
	}
	
	/**
	 * Replace .GeneratedMethodAccessor544. -> .GeneratedMethodAccessor000.
	 * @param stackTraces
	 * @return
	 */
	private static String replaceGeneratedMethodAccessor(String stackTrace) {
		return stackTrace.replaceAll("(GeneratedMethodAccessor[0-9]*)", "GeneratedMethodAccessor000");
	}
	
	/**
	 * Replace .GeneratedMethodAccessor544. -> .GeneratedMethodAccessorXXX.
	 * @param stackTraces
	 * @return
	 */
	private static String replaceEnhancerByCGLIB(String stackTrace) {
		return stackTrace.replaceAll("(\\$\\$EnhancerByCGLIB\\$\\$[0-9,a-z,A-Z]*)", "\\$\\$EnhancerByCGLIB\\$\\$000");
	}
	
	/**
	 * Replace .GeneratedMethodAccessor544. -> .GeneratedMethodAccessorXXX.
	 * @param stackTraces
	 * @return
	 */
	private static String replaceFastClassByCGLIB(String stackTrace) {
		return stackTrace.replaceAll("(\\$\\$FastClassByCGLIB\\$\\$[0-9,a-z,A-Z]*)", "\\$\\$FastClassByCGLIB\\$\\$000");
	}
	
	public static String stackTraceWithoutMessages(String stackTrace) {
		String st = stackTrace;
		Matcher linesWithMessagesMatcher = PATTERN_LINES_WITH_POSSIBLE_MESSAGES.matcher(st);
		while(linesWithMessagesMatcher.find()) {
			String linesWithMessages = linesWithMessagesMatcher.group(LINES_WITH_POSSIBLE_MESSAGES_GROUP_NAME);
			Matcher exceptionsLineMatcher = PATTERN_ONLY_EXCEPTIONS_IN_LINE_WHITH_POSSIBLE_MESSAGES.matcher(linesWithMessages);
			if(exceptionsLineMatcher.find()) {
				String lineWithExceptions = exceptionsLineMatcher.group(ONLY_EXCEPTIONS_IN_LINE_WHITH_POSSIBLE_MESSAGES_GROUP_NAME);
				if(!linesWithMessages.equals(lineWithExceptions)) {
					st = st.replace(linesWithMessages, lineWithExceptions);
				}
			} else {
				st = st.replace(linesWithMessages, "");
			}
		}
		
		return st;
	}
	
	public static Set<String> getLinesWithExceptions(String stackTrace) {
		Set<String> linesWithException = new TreeSet<>();
		Matcher linesWithMessagesMatcher = PATTERN_LINES_WITH_POSSIBLE_MESSAGES.matcher(stackTrace);
		while(linesWithMessagesMatcher.find()) {
			String linesWithMessages = linesWithMessagesMatcher.group(LINES_WITH_POSSIBLE_MESSAGES_GROUP_NAME);
			Matcher exceptionsLineMatcher = PATTERN_ONLY_EXCEPTIONS_IN_LINE_WHITH_POSSIBLE_MESSAGES.matcher(linesWithMessages);
			if(exceptionsLineMatcher.find()) {
				String lineWithExceptions = exceptionsLineMatcher.group(ONLY_EXCEPTIONS_IN_LINE_WHITH_POSSIBLE_MESSAGES_GROUP_NAME);
				linesWithException.add(lineWithExceptions);
			}
		}
		
		return linesWithException;
	}
	
	public static Set<String> getLinesWithMore(String stackTrace) {
		Set<String> linesWithMore = new TreeSet<>();
		Matcher linesWithMoreMatcher = PATTERN_MORE_LINE.matcher(stackTrace);
		while(linesWithMoreMatcher.find()) {
			String lineWithMore = linesWithMoreMatcher.group();
			linesWithMore.add(lineWithMore);
		}
		
		return linesWithMore;
	}
	
	public static Set<String> getJsfExpresions(String stackTrace) {
		Set<String> jsfExpressions = new TreeSet<>();
		Matcher jsfExpressionMatcher = PATTERN_JSF_EXPRESSION.matcher(stackTrace);
		while(jsfExpressionMatcher.find()) {
			String jsfExpression = jsfExpressionMatcher.group(JSF_EXPRESSION_GROUP_NAME);
			jsfExpressions.add(jsfExpression);
		}
		return jsfExpressions;
	}
	
	public static Set<String> extractStackTraces(String text) {
		String noHtmlText = htmlToText(text);
		Set<String> stackTraces = new TreeSet<>();
		//Verifica primeiro os mais rápidos
		if((noHtmlText.contains(ERROR) || noHtmlText.contains(EXCEPTION) || noHtmlText.contains(FAULT) || noHtmlText.contains(FAILURE)) && noHtmlText.contains("at ")) {
			Matcher stackTraceMatcher = PATTERN_JAVA_STACKTRACE.matcher(noHtmlText);
			while(stackTraceMatcher.find()) {
				String unformattedStackTrace = stackTraceMatcher.group();
				String formattedStackTrace = formatStackTrace(unformattedStackTrace.replace("\n", " ")); //To single line);
				Matcher atLineNumberMatcher = Pattern.compile(AT_LINE_NUMBER_NAMED_GROUP).matcher(formattedStackTrace);
				if(atLineNumberMatcher.find()) {
					//Tem codigo xhtml - preservar o original e formatar
					String unformattedTextSource = extractTextSourceInSingleLineStackTrace(unformattedStackTrace).trim();
					if(unformattedTextSource != null) {
						String formattedTextSource = unformattedTextSource.replaceAll(LINE_NUMBER_NAMED_GROUP, "\n${"+LINE_NUMBER_GROUP_NAME+"}");
						String originalXhtmlSource = null;
						Matcher atLineNumberOriginalMatcher = Pattern.compile(AT_LINE_NUMBER_NAMED_GROUP).matcher(text);
						if(atLineNumberOriginalMatcher.find()) {
							String firstLineFormattedStackTrace = formattedStackTrace.split("\n")[0];
							int indexStartOrignalXhtmlSource = text.indexOf(firstLineFormattedStackTrace) + firstLineFormattedStackTrace.length();
							int indexEndOriginalXhtmlSource = text.indexOf(STACKTRACE_WORD, indexStartOrignalXhtmlSource);
							if (indexStartOrignalXhtmlSource < indexEndOriginalXhtmlSource) {
								originalXhtmlSource = text.substring(indexStartOrignalXhtmlSource, indexEndOriginalXhtmlSource);
								Matcher lineNumberMatcher = Pattern.compile(LINE_NUMBER_NAMED_GROUP).matcher(originalXhtmlSource);
								int lineBreaks = originalXhtmlSource.split("\n").length;
								int groupCount = 0;
								while(lineNumberMatcher.find()) {
									groupCount++;
								}
								if(lineBreaks < groupCount) {
									originalXhtmlSource = originalXhtmlSource.replaceAll("\n", " ");
									originalXhtmlSource = originalXhtmlSource.replaceAll(LINE_NUMBER_NAMED_GROUP, "\n${"+LINE_NUMBER_GROUP_NAME+"}");
								}
							}
						}
						if(originalXhtmlSource != null) {
							formattedStackTrace = formattedStackTrace.replace(formattedTextSource.trim(), originalXhtmlSource.trim());
						}
					}
				}
				/*
				 * Remove parte repetida da primeira exception para remover "lixo" de stack trace registrado no GAS, visto que
				 * é comum registrarem primeiro a expressão jsf com erro, seguida da exception e mensagem de erro. Essa parte 
				 * está presente na stack trace após javax.servlet.ServletException:. Com a remoção de conteúdo HTML fica tudo
				 * e acaba poluindo a stack trace extraída. 
				 * 
				 * Ex.:
				 * #{matriculaExtraordinaria.confirmar}: br.ufrn.arq.erros.ArqException: Erro de execução
				 * 
				 * javax.servlet.ServletException: #{matriculaExtraordinaria.confirmar}: br.ufrn.arq.erros.ArqException: Erro de execução
				 *     at ...
				 * 
				 * Fica poluído com:
				 * br.ufrn.arq.erros.ArqException: Erro de execução javax.servlet.ServletException: #{matriculaExtraordinaria.confirmar}: br.ufrn.arq.erros.ArqException: Erro de execução at ...
				 * 
				 * Deveria ficar assim:
				 * javax.servlet.ServletException: #{matriculaExtraordinaria.confirmar}: br.ufrn.arq.erros.ArqException: Erro de execução at ...
				 */
				String firstLine = formattedStackTrace.split("\n")[0];
				
				Matcher firstExceptionInFirstLineMatcher =  Pattern.compile(JAVA_EXCEPTION_CLASS_NAMED_GROUP).matcher(firstLine);
				if(firstExceptionInFirstLineMatcher.lookingAt()) {
					String firstException = firstExceptionInFirstLineMatcher.group();
					
					int lastIndexOf = firstLine.lastIndexOf(firstException);
					if(lastIndexOf > 0) {
						String lastExceptionWithMessages = firstLine.substring(lastIndexOf);
						String cleanFirstLine = firstLine.substring(lastExceptionWithMessages.length());
						formattedStackTrace = formattedStackTrace.replace(firstLine, cleanFirstLine).trim();
					}
				}
				stackTraces.add(formattedStackTrace);
			}
		}
		
		return stackTraces;
	}
}