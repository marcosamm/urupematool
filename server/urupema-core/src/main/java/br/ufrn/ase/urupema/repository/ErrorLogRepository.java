/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ElasticScenarioExtractor.java 
 * 4 de nov de 2017
 */
package br.ufrn.ase.urupema.repository;

import java.io.IOException;
import java.util.Optional;

import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.parser.StackTraceParser;

public interface ErrorLogRepository extends WebExecutionRepository {
	
	Optional<WebExecutionTrace> findById(String id, StackTraceParser stackTraceParser) throws IOException;
}
