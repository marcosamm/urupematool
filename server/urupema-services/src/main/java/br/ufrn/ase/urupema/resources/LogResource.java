/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.Log;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.UriStats;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.dtos.LogDTO;
import br.ufrn.ase.urupema.process.LogProcess;
import br.ufrn.ase.urupema.process.SourceConnectionProcess;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class LogResource {
	@Autowired
	SourceConnectionProcess sourceConnectionProcess;
	
	@Autowired
	LogProcess logProcess;
	
	@GetMapping("/sourceconnection/log/count")
	public Set<UriStats> countGroupedByUri(@RequestParam("idSourceConnection") Long idSourceConnection) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		SourceConnection sc = sourceConnectionProcess.findById(idSourceConnection);
		return logProcess.countLogsGroupedByUri(sc);
	}
	
	@GetMapping("/sourceconnection/log/detail")
	public LogDTO getLog(@RequestParam("idSourceConnection") Long idSourceConnection, @RequestParam("id") String id) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		SourceConnection sc = sourceConnectionProcess.findById(idSourceConnection);
		return toLogDTO(logProcess.getLog(sc, id));
	}
	
	private LogDTO toLogDTO(Log log) throws IOException {
		LogDTO dto = new LogDTO();
		dto.setIdentifier(log.getIdentifier());
		dto.setDate(log.getDate());
		dto.setRequestURL(log.getRequestURL());
		dto.setRequestURI(log.getRequestURI());
		dto.setLogType(log.getLogType());
		if(log.getLogType() == LogType.ERROR || log.getLogType() == LogType.EXECUTION) {
			dto.setTrace(((WebExecutionTrace)log).getTraceElementsAsString());
			dto.setExceptionMessage(((WebExecutionTrace)log).getExceptionMessage());
		}
		dto.setMoreInformation(log.getMoreInformation());
		return dto;
	}
	
	@SuppressWarnings("unchecked")
	@PostMapping("/sourceconnection/log/agroup")
	public void agroup(
			@RequestBody Map<String, Object> body) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException{
		log.debug("Start agroup");
		Long idSourceConnection = Long.valueOf(body.get("idSourceConnection").toString());
		logProcess.agroup(idSourceConnection, (List<String>) body.get("uris"));
		log.debug("End agroup");
	}
	
	@GetMapping("/sourceconnection/log/idsbyproperty")
	public List<String> logGroupIdsByProperty(@RequestParam("idSourceConnection") Long idSourceConnection, @RequestParam("propertyName") String propertyName, @RequestParam("propertyValue") String propertyValue) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		SourceConnection sc = sourceConnectionProcess.findById(idSourceConnection);
		return logProcess.getLogIdsByProperty(sc, propertyName, propertyValue);
	}
	
	@GetMapping("/sourceconnection/log/affectedurisbystacktrace")
	public Set<String> affectedUrisByEquivalentStackTracesAndDateBetween(
			@RequestParam("idSourceConnection") Long idSourceConnection, 
			@RequestParam("stackTrace") String stackTrace, 
			@RequestParam("uriFragment") String uriFragment
	) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		SourceConnection sc = sourceConnectionProcess.findById(idSourceConnection);
		return logProcess.getAffectedUrisByEquivalentStackTracesAndDateBetween(sc, stackTrace, uriFragment);
	}
}
