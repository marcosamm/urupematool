import { Component, OnInit } from '@angular/core';
import { GroupAggregatorTypeService } from '../service';
import { GroupAggregatorType } from '../model';

@Component({
  selector: 'app-groupaggregatortype',
  templateUrl: './groupaggregatortype.component.html',
  styleUrls: ['./groupaggregatortype.component.css']
})
export class GroupAggregatorTypeComponent implements OnInit {
  groupAggregatorTypes: GroupAggregatorType[];

  constructor(private groupAggregatorTypeService: GroupAggregatorTypeService) { }

  ngOnInit() {
    this.groupAggregatorTypeService.getGroupAggregatorTypes().subscribe(
      groupAggregatorTypes => this.groupAggregatorTypes = groupAggregatorTypes
    );
  }
}
