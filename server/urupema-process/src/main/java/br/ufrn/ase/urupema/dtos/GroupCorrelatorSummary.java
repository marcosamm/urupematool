package br.ufrn.ase.urupema.dtos;

import java.util.Set;
import java.util.TreeSet;

import br.ufrn.ase.urupema.domain.GroupRelationship;
import br.ufrn.ase.urupema.domain.LogGroup;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupCorrelatorSummary implements Comparable<GroupCorrelatorSummary>{
	String uri;
	Long idGroupCorrelator;
	Set<String> idsLogsOrigin = new TreeSet<>();
	Set<Long> idsGroupsOrigin = new TreeSet<>();
	Set<String> idsLogsDestiny = new TreeSet<>();
	Set<Long> idsGroupsDestiny = new TreeSet<>();
	
	public GroupCorrelatorSummary(String uri, Long idGroupCorrelator) {
		this.uri = uri;
		this.idGroupCorrelator = idGroupCorrelator;
	}
	
	public GroupCorrelatorSummary(GroupRelationship gr) {
		uri = gr.getGroup().getSuperGroup().getLabel();
		idGroupCorrelator = gr.getGroupCorrelator().getId();
		idsGroupsOrigin.add(gr.getGroup().getId());
		idsLogsOrigin.addAll(gr.getGroup().getElementsId());
		for(LogGroup lg : gr.getRelateds()) {
			idsGroupsDestiny.add(lg.getId());
			idsLogsDestiny.addAll(lg.getElementsId());
		}
	}
	
	public void join(GroupRelationship gr) {
		if(uri != gr.getGroup().getSuperGroup().getLabel()) {
			throw new IllegalArgumentException("different uris");
		}
		idsGroupsOrigin.add(gr.getGroup().getId());
		idsLogsOrigin.addAll(gr.getGroup().getElementsId());
		for(LogGroup lg : gr.getRelateds()) {
			idsGroupsDestiny.add(lg.getId());
			idsLogsDestiny.addAll(lg.getElementsId());
		}
	}
	
	@Override
	public int compareTo(GroupCorrelatorSummary o) {
		int ret = uri.compareTo(o.getUri());
		if(ret == 0) {
			ret = idGroupCorrelator.compareTo(o.getIdGroupCorrelator());
		}
		return ret;
	}
}
