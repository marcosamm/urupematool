package br.ufrn.ase.urupema.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@SequenceGenerator(name = "ID_SOURCECONNECTION_SEQUENCE", sequenceName = "sourceconnection_id_sequence", allocationSize = 1)
@Getter
@Setter
public class SourceConnection implements Serializable, Comparable<SourceConnection> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_SOURCECONNECTION_SEQUENCE")
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	private String regexSignature;
	
	private String regexIgnoreSignature;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY, optional=false)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_sourceconnection_study_id"))
	private Study study;
	
	@ManyToOne(fetch = FetchType.EAGER, optional=false)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_sourceconnection_source_id"))
	private Source source;
	
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private LogType logType;
	
	@Column(nullable = false)
	private String groupAggregatorTypeId;
	
	@Transient
	private GroupAggregatorType groupAggregatorType;
	
	@JsonIgnore
	@OneToMany(mappedBy = "sourceConnection", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
	private Set<LogGroup> logGroups = new TreeSet<>();
	
	@Column(nullable = true)
	private String suspiciousFilesFinderTypeId;
	
	@Transient
	private SuspiciousFilesFinderType suspiciousFilesFinderType;
	
	private String suspiciousFilesFilter;
	
	public void setLogType(LogType logType) {
		//TODO O repositório do soruce deveria injetar o sourceType
		/*if(!source.getSourceType().getLogTypes().contains(logType)) {
			throw new IllegalArgumentException("logType ("+logType.getLabel()+") incompatible with source ("+source.getName()+")");
		}*/
		this.logType = logType;
	}
	
	public void setGroupAggregatorType(GroupAggregatorType groupAggregatorType) {
		if(groupAggregatorType != null && !groupAggregatorType.getLogTypes().contains(logType)) {
			throw new IllegalArgumentException("groupAggregatorType ("+groupAggregatorType.getId()+") incompatible with logType ("+logType.getLabel()+")");
		}
		this.groupAggregatorType = groupAggregatorType;
		if(groupAggregatorType != null) {
			this.groupAggregatorTypeId = groupAggregatorType.getId();
		}
	}
	
	public void setSuspiciousFilesFinderTypeId(String suspiciousFilesFinderTypeId) {
		if(suspiciousFilesFinderTypeId != null && !suspiciousFilesFinderTypeId.trim().equals("")) {
			this.suspiciousFilesFinderTypeId = suspiciousFilesFinderTypeId;
		} else {
			this.suspiciousFilesFinderType = null;
		}
	}
	
	public void setSuspiciousFilesFinderType(SuspiciousFilesFinderType suspiciousFilesFinderType) {
		this.suspiciousFilesFinderType = suspiciousFilesFinderType;
		if(suspiciousFilesFinderType != null) {
			this.suspiciousFilesFinderTypeId = suspiciousFilesFinderType.getId();
		} else {
			this.suspiciousFilesFinderType = null;
		}
	}

	@Override
	public int compareTo(SourceConnection o) {
		int ret = study.compareTo(o.getStudy());
		if(ret == 0) {
			ret = source.compareTo(o.getSource());
		}
		if(ret == 0) {
			ret = groupAggregatorTypeId.compareTo(o.getGroupAggregatorTypeId());
		}
		if(ret == 0) {
			if(suspiciousFilesFinderTypeId != null && o.getSuspiciousFilesFinderTypeId() != null) {
				ret = suspiciousFilesFinderTypeId.compareTo(o.getSuspiciousFilesFinderTypeId());
			} else if(suspiciousFilesFinderTypeId != null && o.getSuspiciousFilesFinderTypeId() == null) {
				ret = 1;
			} else if(suspiciousFilesFinderTypeId == null && o.getSuspiciousFilesFinderTypeId() != null) {
				ret = -1;
			}
		}
		if(ret == 0) {
			ret = startDate.compareTo(o.getStartDate());
		}
		if(ret == 0) {
			ret = endDate.compareTo(o.getEndDate());
		}
		if(ret == 0) {
			ret = regexSignature.compareTo(o.getRegexSignature());
		}
		if(ret == 0) {
			ret = regexIgnoreSignature.compareTo(o.getRegexIgnoreSignature());
		}
		return ret;
	}
}
