package br.ufrn.ase.urupema.extensible.aggregator;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.repository.LogRepository;
import br.ufrn.ase.urupema.repository.WebExecutionRepository;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupAggregatorByStacktraceAndUri implements GroupAggregatorTypePlugin {
	public static String ID = "urupema:uri_st";
	
	private LogRepository logRepository;
	private List<LogType> logTypes = Arrays.asList(LogType.ERROR, LogType.EXECUTION);
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getName() {
		return "URI and Stack Trace";
	}

	@Override
	public String getDescription() {
		return "Creates a set of logs grouped by stack trace and subgrouped by uri";
	}
	
	protected WebExecutionRepository getWebExecutionRepository() {
		return (WebExecutionRepository) logRepository;
	}
	
	@Override
	public List<LogGroup> agroup(SourceConnection sourceConnection, Map<String, Object> params, Map<String, WebExecutionTrace> cache) throws IOException {
		@SuppressWarnings("unchecked")
		Set<String> uris = (Set<String>) params.get("uris");
		List<LogGroup> groups = getWebExecutionRepository().agroupByStackTraceSubgroupByUri(uris, sourceConnection.getStartDate(), sourceConnection.getEndDate());
		
		for(LogGroup stackTraceGroup : groups) {
			if(sourceConnection.getLogType()!=LogType.ACCESS) {
				WebExecutionTrace representantStacktraceGroup = getWebExecutionRepository().findById(stackTraceGroup.getRepresentantId()).get();
				stackTraceGroup.getMoreInformation().put("numTraceElements", representantStacktraceGroup.getQtdTraceElements().toString());
			}
			stackTraceGroup.setGroupAggregatorTypeId(ID);
		}
		
		return groups;
	}

	@Override
	public int compareTo(GroupAggregatorType o) {
		return getId().compareTo(o.getId());
	}
}
