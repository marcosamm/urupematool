import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { IssueTrackerType } from '../model';

@Injectable()
export class IssueTrackerTypeService {
    urlGet = environment.apiUrl + '/issuetrackertype';

    constructor(private http: HttpClient) { }

    getIssueTrackerType(id): Observable<IssueTrackerType> {
        return this.http.get<IssueTrackerType>(this.urlGet + '/detail?id=' + id);
    }

    getIssueTrackerTypes(): Observable<IssueTrackerType[]> {
        return this.http.get<IssueTrackerType[]>(this.urlGet + '/list');
    }
}
