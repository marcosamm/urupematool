import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SourceConnectionService, LogGroupService } from '../../service';
import { SourceConnection } from '../../model/SourceConnection';
import { LogGroup } from '../../model/LogGroup';
import { SuspiciousFile } from '../../model/SuspiciousFile';
import { CalledMethodStats } from '../../model/CalledMethodStats';


@Component({
  selector: 'app-suspiciousfiles',
  templateUrl: './suspiciousfiles.component.html',
  styleUrls: ['./suspiciousfiles.component.css']
})
export class SuspiciousFilesComponent implements OnInit {
  sourceConnection: SourceConnection;
  logGroup: LogGroup;
  suspiciousFiles: SuspiciousFile [];
  calledFileMethods: Map<SuspiciousFile, CalledMethodStats []>;

  constructor(
    private route: ActivatedRoute,
    private logGroupService: LogGroupService,
    private sourceConnectionService: SourceConnectionService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.sourceConnectionService.getSourceConnection(params['idSourceConnection']).subscribe(
        sourceConnection => {
          this.sourceConnection = sourceConnection;
          this.logGroupService.getLogGroup(params['idLogGroup']).subscribe(
            logGroup => {
              this.logGroup = logGroup;
            }
          );
          this.logGroupService.getSuspiciousFilesByLogGroupId(params['idLogGroup']).subscribe(
            suspicousFiles => {
              this.suspiciousFiles = suspicousFiles;
              this.calledFileMethods = new Map();
              this.suspiciousFiles.forEach(element => {
                this.calledFileMethods.set(element, []);
              });
            }
          );
        }
      );
    });
  }

  calledFileMethodsByLogGroupIdAndFileName(suspiciousFile: SuspiciousFile) {
    return this.calledFileMethods.get(suspiciousFile);
  }

  fillCalledFileMethodsByLogGroupIdAndFileName(event) {
    const suspiciousFile: SuspiciousFile = event.data;
    const methodsCount = this.calledFileMethods.get(suspiciousFile);
    if (methodsCount.length === 0) {
      this.logGroupService.getCalledFileMethodsByLogGroupIdAndFileName(this.logGroup.id, suspiciousFile.name).subscribe(
        calledMethodsStats => {
          this.calledFileMethods.set(suspiciousFile, calledMethodsStats);
        }
      );
    }
  }
}
