import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SourceConnectionService, LogGroupService } from '../../service';
import { SourceConnection } from '../../model/SourceConnection';

@Component({
  selector: 'app-listloggroups',
  templateUrl: './listloggroups.component.html',
  styleUrls: ['./listloggroups.component.css']
})
export class ListLogGroupsComponent implements OnInit {
  sourceConnection: SourceConnection;
  totalElements: number;
  totalSubgroups: number;

  constructor(
    private route: ActivatedRoute,
    private sourceConnectionService: SourceConnectionService,
    private logGroupService: LogGroupService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.sourceConnectionService.getSourceConnection(params['idSourceConnection']).subscribe(
        sourceConnection => {
          this.sourceConnection = sourceConnection;
          this.logGroupService.getLogGroupsBySourceConnectionId(this.sourceConnection).subscribe(
            logGroups => {
              this.sourceConnection.logGroups = logGroups.sort((lg1, lg2) => lg2.elementsCount - lg1.elementsCount);
              this.totalElements = 0;
              this.totalSubgroups = 0;
              for (const lg of this.sourceConnection.logGroups) {
                this.totalElements += lg.elementsCount;
                this.totalSubgroups += lg.subGroupsCount;
              }
            }
          );
        }
      );
    });
  }
}
