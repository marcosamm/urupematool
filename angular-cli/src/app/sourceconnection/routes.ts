
import { Routes } from '@angular/router';
import { SourceConnectionCreateComponent } from './create/create.component';
import { LogDetailComponent } from './logdetail/logdetail.component';
import { LogGroupComponent } from './loggroup/loggroup.component';
import { LogCountComponent } from './logcount/logcount.component';
import { ListLogGroupsComponent } from './listloggroups/listloggroups.component';
import { LogDistByNumeTraceElementsComponent } from './logdistbynumtraceelements/dist.component';
import { FindLogsComponent } from './findlogs/findlogs.component';
import { SuspiciousFilesComponent } from './suspiciousfiles/suspiciousfiles.component';
import { LogGroupAffectedUrisAndUsersComponent } from './loggroupaffectedurisandusers/loggroupaffectedurisandusers.component';
import { LogsOverTimeComponent } from './logsovertime/logsovertime.component';
import { FCSFsLogGroupComponent } from './fcsfslogroup/fcsfsloggroup.component';
import { CommonFCSFsComponent } from './commonfcsfs/commonfcsfs.component';
import { SuspiciousFilesAllGroupsComponent } from './suspiciousfilesallgroups/suspiciousfilesallgroups.component';
import { LogGroupSuspiciousReportComponent } from './loggroupsuspiciousreport/loggroupsuspiciousreport.component';

/**
 * Define the routes
 */
export const routes: Routes = [
    { path: 'sourceconnection/listloggroups/:idSourceConnection', component: ListLogGroupsComponent },
    { path: 'sourceconnection/logcount/:idSourceConnection', component: LogCountComponent },
    { path: 'sourceconnection/loggroup/:idSourceConnection/:idLogGroup', component: LogGroupComponent },
    { path: 'sourceconnection/suspiciousfiles/:idSourceConnection/:idLogGroup', component: SuspiciousFilesComponent },
    { path: 'sourceconnection/affectedurisandusers/:idSourceConnection/:idLogGroup', component: LogGroupAffectedUrisAndUsersComponent },
    { path: 'sourceconnection/logsovertime/:idSourceConnection/:idLogGroup', component: LogsOverTimeComponent },
    { path: 'sourceconnection/log/detail/:idSourceConnection/:idLog', component : LogDetailComponent },
    { path: 'sourceconnection/create/:idStudy', component: SourceConnectionCreateComponent},
    { path: 'sourceconnection/create/:idStudy/:id', component: SourceConnectionCreateComponent},
    { path: 'sourceconnection/logdistbynumtraceelements/:idSourceConnection', component: LogDistByNumeTraceElementsComponent},
    { path: 'sourceconnection/findlogs/:idSourceConnection', component: FindLogsComponent},
    { path: 'sourceconnection/findlogs/:idSourceConnection/:propertyName/:propertyValue', component: FindLogsComponent},
    { path: 'sourceconnection/fcsfsloggroup/:idSourceConnection/:idLogGroup', component: FCSFsLogGroupComponent },
    { path: 'sourceconnection/commonfcsfs/:idSourceConnection/:idLogGroup', component: CommonFCSFsComponent },
    { path: 'sourceconnection/suspiciousfilesallgroups/:idSourceConnection', component: SuspiciousFilesAllGroupsComponent },
    { path: 'sourceconnection/loggroupsuspiciousreport/:idLogGroup', component: LogGroupSuspiciousReportComponent },
];
