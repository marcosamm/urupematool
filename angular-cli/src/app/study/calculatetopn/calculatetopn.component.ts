import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { StudyService, IssueTrackerConnectionService, LogGroupService } from '../../service';
import { Study, StudyResult, SuspiciousFile, StatsTopN } from '../../model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-calculatetopn',
  templateUrl: './calculatetopn.component.html',
  styleUrls: ['./calculatetopn.component.css']
})
export class CalculateTopnComponent implements OnInit {
  study: Study;
  form: FormGroup;
  studyResult: StudyResult;
  calledMethodsFindedFiles: any [];

  constructor(
    private route: ActivatedRoute,
    private studyService: StudyService,
    private issueTrackerConnectionService: IssueTrackerConnectionService,
    private logGroupService: LogGroupService,
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      issueTrackerConnectionId: ['', Validators.required],
      sourceConnectionId: ['', Validators.required],
      fileExtensions: ['.java', Validators.required],
      sourcePackage: ['br.ufrn', Validators.required],
      checkSourcePackageInStackTrace: [false, Validators.required],
      maxEnabledChangedFiles: [50, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.min(1), Validators.max(100)]]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studyService.getStudy(params['id']).subscribe(
        dados => {
          this.study = dados;
        }
      );
    });
  }

  calculate() {
    this.issueTrackerConnectionService.calculateTopNs(
      this.form.get('issueTrackerConnectionId').value,
      this.form.get('sourceConnectionId').value,
      this.form.get('fileExtensions').value,
      this.form.get('sourcePackage').value,
      this.form.get('checkSourcePackageInStackTrace').value,
      this.form.get('maxEnabledChangedFiles').value
    ).subscribe(
      studyResult => {
        this.studyResult = studyResult;
        const mapSuspFiles: Map<number, SuspiciousFile> = new Map();
        this.studyResult.statsTopNs.forEach(statsN => {
          statsN.top10.forEach(suspFile => {
            if (suspFile.name !== undefined) {
              mapSuspFiles.set(suspFile.id, suspFile);
            }
          });
          statsN.sourceOnlyTop10.forEach(suspFile => {
            if (suspFile.name !== undefined) {
              mapSuspFiles.set(suspFile.id, suspFile);
            }
          });
        });
        this.studyResult.statsTopNs.forEach(statsN => {
          statsN.top10.forEach(suspFile => {
            if (suspFile.name === undefined) {
              const indexOf: number = statsN.top10.indexOf(suspFile);
              const _id: number = suspFile as unknown as number;
              statsN.top10[indexOf] = mapSuspFiles.get(_id);
            }
          });
          statsN.sourceOnlyTop10.forEach(suspFile => {
            if (suspFile.name === undefined) {
              const indexOf: number = statsN.sourceOnlyTop10.indexOf(suspFile);
              const _id: number = suspFile as unknown as number;
              statsN.sourceOnlyTop10[indexOf] = mapSuspFiles.get(_id);
            }
          });
        });
      }
    );
  }

  changedFileInSuspiciousFile(changedFile: string, statsTopN: StatsTopN): boolean {
    let found = false;

    statsTopN.top10.forEach(suspiciousFile => {
      if (suspiciousFile.name === changedFile) {
        found = true;
      }
    });

    statsTopN.sourceOnlyTop10.forEach(suspiciousFile => {
      if (suspiciousFile.name === changedFile) {
        found = true;
      }
    });

    return found;
  }

  suspiciousFileFound(statsTopN: StatsTopN, suspFile: SuspiciousFile): boolean {
    let found = false;

    statsTopN.changedFiles.forEach(changedFile => {
      if (changedFile === suspFile.name) {
        found = true;
      }
    });

    return found;
  }

  countFindedMethods (calledMethods): number {
    let count = 0;

    calledMethods.forEach(calledMethod => {
      if (calledMethod.found) {
        count++;
      }
    });

    return count;
  }
}


