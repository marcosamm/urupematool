import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import {TableModule} from 'primeng/table';
import {CalendarModule} from 'primeng/calendar';
import {ChartModule} from 'primeng/chart';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
//import { library } from '@fortawesome/fontawesome-svg-core';
import { fas, faSearch, faBoxes, faChartBar, faBoxOpen, faBug } from '@fortawesome/free-solid-svg-icons';

//library.add(faSearch, faBoxes, faChartBar, faBoxOpen, faBug);

import { routes } from './routes';

import {
  IssueTrackerConnectionService,
  IssueService,
  AffectedUriService,
  ChangedFileService,
  IssueTypeService,
  RevisionService,
  FileChangeTypeService,
  ReportedStackTraceService } from '../service';
import { PipesModule } from '../pipes/pipes.module';
import { ListIssuesComponent } from './listissues/listissues.component';
import { IssueTrackerConnectionDetailComponent } from './detail/detail.component';
import { IssueTrackerConnectionCreateComponent } from './create/create.component';
import { AffectedUrisComponent } from './affecteduris/affecteduris.component';
import { IssueDetailComponent } from './issuedetail/issuedetail.component';
import { IssueCreateComponent } from './issuecreate/issuecreate.component';
import { ReportedStackTraceCreateComponent } from './reportedstacktracecreate/reportedstacktracecreate.component';
import { ChangedFileCreateComponent } from './changedfilecreate/changedfilecreate.component';
import { RevisionCreateComponent } from './revisioncreate/revisioncreate.component';
import { AffectedUriCreateComponent } from './affecteduricreate/affecteduricreate.component';
import { OverlayPanelModule } from 'primeng/overlaypanel';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    FontAwesomeModule,
    TableModule,
    CalendarModule,
    ChartModule,
    PipesModule,
    OverlayPanelModule
  ],
  declarations: [
    IssueTrackerConnectionCreateComponent,
    IssueTrackerConnectionDetailComponent,
    ListIssuesComponent,
    AffectedUrisComponent,
    IssueDetailComponent,
    IssueCreateComponent,
    ReportedStackTraceCreateComponent,
    ChangedFileCreateComponent,
    RevisionCreateComponent,
    AffectedUriCreateComponent
  ],
  providers: [
    IssueTrackerConnectionService,
    IssueService,
    AffectedUriService,
    ChangedFileService,
    IssueTypeService,
    ReportedStackTraceService,
    RevisionService,
    FileChangeTypeService
  ]
})
export class IssueTrackerConnectionModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
  }
}
