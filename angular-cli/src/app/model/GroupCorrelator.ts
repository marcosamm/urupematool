import { SourceConnection } from './SourceConnection';
import { Study } from './Study';
import { GroupCorrelatorType } from './GroupCorrelatorType';
import { GroupRelationship } from './GroupRelationship';

export interface GroupCorrelator {
    id: number;
    name: string;
    study: Study;
    groupCorrelatorTypeId: string;
    groupCorrelatorType: GroupCorrelatorType;
    origin: SourceConnection;
    destiny: SourceConnection;
    groupRelationships: GroupRelationship[];
}
