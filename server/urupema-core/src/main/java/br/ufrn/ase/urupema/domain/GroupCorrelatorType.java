package br.ufrn.ase.urupema.domain;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

public interface GroupCorrelatorType extends Comparable<GroupCorrelatorType>{
	public String getId();
	public String getName();
	public String getDescription();
	public Set<GroupAggregatorType> getCompatibleOriginAggregatorTypes();
	public Set<GroupAggregatorType> getCompatibleDestinyAggregatorTypes();
	public List<GroupRelationship> correlate(GroupCorrelator groupCorrelator) throws OperationNotSupportedException, IOException;
}
