package br.ufrn.ase.urupema.domain;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@SequenceGenerator(name = "ID_REPORTEDSTACKTRACE_SEQUENCE", sequenceName = "reportedstacktrace_id_sequence", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@JsonIgnoreProperties(value= {"affectedUris", "enabledAffectedUris"})
@Getter
@Setter
@ToString(of={"id", "stackTrace"})
@EqualsAndHashCode(of= {"issue", "stackTrace"})
public class ReportedStackTrace implements Serializable, Comparable<ReportedStackTrace> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_REPORTEDSTACKTRACE_SEQUENCE")
	private Long id;
	
	@Column(nullable = false, columnDefinition="text")
	private String stackTrace;
	
	@Column(nullable = false)
	private boolean enabled = true;
	
	@Column(columnDefinition="text")
	private String note;
	
	@JsonIgnore
	@OneToMany(mappedBy = "reportedStackTrace", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
	@OrderBy("uri asc")
	private Set<AffectedUri> affectedUris = new TreeSet<>();
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY, optional=false)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_reportedstacktrace_issue_id"))
	private Issue issue;
	
	public ReportedStackTrace() {
	}
	
	public ReportedStackTrace(String stackTrace, String note) {
		super();
		this.stackTrace = stackTrace;
		this.note = note;
	}
	
	public void addAffectedUri(AffectedUri affectedUri) {
		this.getAffectedUris().add(affectedUri);
		affectedUri.setReportedStackTrace(this);
	}
	
	public void removeAffectedUri(AffectedUri affectedUri) {
		this.getAffectedUris().remove(affectedUri);
		affectedUri.setReportedStackTrace(null);
	}
	
	public Set<AffectedUri> getEnabledAffectedUris(){
		Set<AffectedUri> affectedUris = new TreeSet<>();
		for(AffectedUri affectedUri : getAffectedUris()) {
			if(affectedUri.isEnabled()) {
				affectedUris.add(affectedUri);
			}
		}
		return affectedUris;
	}

	@Override
	public int compareTo(ReportedStackTrace arg0) {
		int ret = 0;
		if(issue != null && arg0.getIssue() != null) {
			ret = issue.compareTo(arg0.getIssue());
		} else if(issue != null) {
			ret = 1;
		} else if (arg0.getIssue() != null){
			ret = -1;
		}
		if(ret == 0) {
			ret = stackTrace.compareTo(arg0.getStackTrace());
		}
		return ret;
	}
}
