package br.ufrn.ase.urupema.extensible.suspiciousfilesfinder;

import br.ufrn.ase.urupema.domain.SuspiciousFilesFinderType;

public interface SuspiciousFilesFinderTypePlugin extends SuspiciousFilesFinderType {
}
