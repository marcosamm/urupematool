import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { GroupAggregatorType } from '../model';

@Injectable()
export class GroupAggregatorTypeService {
    urlGet = environment.apiUrl + '/groupaggregatortype';

    constructor(private http: HttpClient) { }

    getGroupAggregatorType(id): Observable<GroupAggregatorType> {
        return this.http.get<GroupAggregatorType>(this.urlGet + '/detail?id=' + id);
    }

    getGroupAggregatorTypes(): Observable<GroupAggregatorType[]> {
        return this.http.get<GroupAggregatorType[]>(this.urlGet + '/list');
    }
}
