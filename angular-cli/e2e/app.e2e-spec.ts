import { UrupemaWebPage } from './app.po';

describe('urupema-web App', () => {
  let page: UrupemaWebPage;

  beforeEach(() => {
    page = new UrupemaWebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
