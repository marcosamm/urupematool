import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { GroupCorrelator } from '../model/GroupCorrelator';
import { Observable } from 'rxjs';

@Injectable()
export class GroupCorrelatorService {
    urlGet = environment.apiUrl + '/groupcorrelator';

    constructor(private http: HttpClient) { }

    getGroupCorrelator(id: number): Observable<GroupCorrelator> {
        return this.http.get<GroupCorrelator>(this.urlGet + '?id=' + id);
    }

    relate(groupCorrelator: GroupCorrelator) {
        return this.http.get<any[]>(this.urlGet + '/relate?idGroupCorrelator=' + groupCorrelator.id);
    }

    createGroupCorrelator(
        studyId: number,
        name: string,
        groupCorrelatorTypeId: string,
        sourceConnectionOriginId: number,
        sourceConnectionDestinyId: number
    ) {
        const groupCorrelator = {
            study: { id: studyId },
            name: name,
            groupCorrelatorTypeId: groupCorrelatorTypeId,
            origin: { id: sourceConnectionOriginId },
            destiny: { id: sourceConnectionDestinyId }
        };
        return this.http.post(this.urlGet, groupCorrelator);
    }

    updateGroupCorrelator(
        id: number,
        studyId: number,
        name: string,
        groupCorrelatorTypeId: string,
        sourceConnectionOriginId: number,
        sourceConnectionDestinyId: number
    ) {
        const groupCorrelator = {
            id: id,
            study: { id: studyId },
            name: name,
            groupCorrelatorTypeId: groupCorrelatorTypeId,
            origin: { id: sourceConnectionOriginId },
            destiny: { id: sourceConnectionDestinyId }
        };
        return this.http.put(this.urlGet, groupCorrelator);
    }

    deleteGroupCorrelator(groupCorrelator: GroupCorrelator) {
        return this.http.delete(this.urlGet + '?id=' + groupCorrelator.id);
    }
}
