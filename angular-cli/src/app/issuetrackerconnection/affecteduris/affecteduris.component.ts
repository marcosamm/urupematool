import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Table } from 'primeng/table';
import { IssueTrackerConnectionService } from '../../service';
import { IssueTrackerConnection } from '../../model';


@Component({
  selector: 'app-affecteduris',
  templateUrl: './affecteduris.component.html',
  styleUrls: ['./affecteduris.component.css']
})
export class AffectedUrisComponent implements OnInit {
  @ViewChild('dt', {static: false}) tableRef: Table;
  issueTrackerConnection: IssueTrackerConnection;
  cols: any[];
  rows: any[];

  constructor(
    private route: ActivatedRoute,
    private issueTrackerConnectionService: IssueTrackerConnectionService
  ) { }

  ngOnInit() {
    this.cols = [
      { field: 'uri', header: 'URI' }
    ];
    this.route.params.subscribe(params => {
      this.issueTrackerConnectionService.getIssueTrackerConnection(params['idIssueTrackerConnection']).subscribe(
        issueTrackerConnection => {
          this.issueTrackerConnection = issueTrackerConnection;
          this.issueTrackerConnectionService.listAffectedUris(params['idIssueTrackerConnection']).subscribe(
            affectedUris => {
              this.rows = [];
              affectedUris.forEach( affectedUri => {
                const row = {
                  uri: affectedUri
                };
                this.rows.push(row);
              });
            }
          );
        }
      );
    });
  }
}
