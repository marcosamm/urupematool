/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.GroupCorrelator;
import br.ufrn.ase.urupema.domain.GroupRelationship;
import br.ufrn.ase.urupema.process.GroupCorrelatorProcess;


//@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class GroupCorrelatorResource {
	
	@Autowired
	GroupCorrelatorProcess groupCorrelatorProcess;
	
	@GetMapping("/groupcorrelator")
	public GroupCorrelator findById(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return groupCorrelatorProcess.findById(id);
	}
	
	@GetMapping("/groupcorrelator/list")
	public List<GroupCorrelator> findAll() throws IOException {
		return groupCorrelatorProcess.findAll();
	}
	
	@PostMapping("/groupcorrelator")
	public void put(@RequestBody GroupCorrelator groupCorrelator) throws IOException {
		groupCorrelatorProcess.save(groupCorrelator);
	}
	
	@PutMapping("/groupcorrelator")
	public void update(@RequestBody GroupCorrelator groupCorrelator) throws IOException, InstantiationException, IllegalAccessException {
		GroupCorrelator gc = findById(groupCorrelator.getId());
		BeanUtils.copyProperties(groupCorrelator, gc);
		groupCorrelatorProcess.save(gc);
	}
	
	@DeleteMapping("/groupcorrelator")
	public void delete(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		groupCorrelatorProcess.delete(id);
	}
	
	@GetMapping("/groupcorrelator/relate")
	public List<GroupRelationship> correlate(@RequestParam("idGroupCorrelator") Long id) throws IOException, InstantiationException, IllegalAccessException, OperationNotSupportedException {
		return groupCorrelatorProcess.correlate(id);
	}
}
