package br.ufrn.ase.urupema.elasticsearch;

import java.util.Arrays;

import javax.naming.OperationNotSupportedException;

import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.Source;
import br.ufrn.ase.urupema.elasticsearch.repository.ElasticSearchErrorLogRepository;
import br.ufrn.ase.urupema.extensible.sourcetype.AbstractSourceTypePlugin;
import br.ufrn.ase.urupema.extensible.sourcetype.SourceTypePlugin;
import br.ufrn.ase.urupema.repository.ErrorLogRepository;

public class ElasticsearchSourceTypePlugin extends AbstractSourceTypePlugin implements SourceTypePlugin {

	public ElasticsearchSourceTypePlugin() {
		super();
		logTypes.addAll(Arrays.asList(LogType.ERROR));
	}
	
	@Override
	public String getId() {
		return "elasticsearch:urupema";
	}

	@Override
	public String getName() {
		return "Urupema Elasticsearch Source Type";
	}

	@Override
	public ErrorLogRepository getErrorLogRepository(Source sourceConnection) throws OperationNotSupportedException {
		ElasticSearchErrorLogRepository er = new ElasticSearchErrorLogRepository();
		er.setUrl(sourceConnection.getUrlConnection());
		er.setUsername(sourceConnection.getUserName());
		er.setPassword(sourceConnection.getPassword());
		er.setIndexName(sourceConnection.getErrorIndexName());
		er.setDocumentType(sourceConnection.getErrorDocumentType());
		return er;
	}
}
