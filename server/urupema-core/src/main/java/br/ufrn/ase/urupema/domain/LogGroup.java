package br.ufrn.ase.urupema.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Formula;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@SequenceGenerator(name = "ID_LOGGROUP_SEQUENCE", sequenceName = "loggroup_id_sequence", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Getter
@Setter
@EqualsAndHashCode(of = {"firstOccurrenceDate", "lastOccurrenceDate", "label", "id"})
@ToString(of={"label", "elementsCount", "firstOccurrenceDate", "lastOccurrenceDate"})
public class LogGroup implements Serializable, Comparable<LogGroup>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_LOGGROUP_SEQUENCE")
	private Long id;
	
	@Column(nullable = false, columnDefinition = "text")
	private String label;
	
	private String groupAggregatorTypeId;
	
	private String typeDescription;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date firstOccurrenceDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastOccurrenceDate;
	
	private String representantId;
	
	@JsonIgnore
	@ElementCollection(fetch = FetchType.LAZY)
	private Set<String> elementsId = new TreeSet<String>();
	
	@JsonIgnore
	@ElementCollection(fetch= FetchType.LAZY)
    @CollectionTable(name="loggroup_fcsf")
    @MapKeyColumn(name="fcsf", columnDefinition = "text")
    @Column(name="relativeSupport")
	private Map<String, Float> mapFcsf;
	
	@Formula("(SELECT DISTINCT COUNT(fcsf.*) FROM loggroup_fcsf fcsf WHERE fcsf.loggroup_id = id)")
	private int mapFcsfsCount;
	
	private int elementsCount;
	
	@Formula("(SELECT DISTINCT COUNT(sg.id) FROM loggroup sg WHERE sg.supergroup_id = id)")
	private int subGroupsCount;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "superGroup_id", referencedColumnName="id", foreignKey = @ForeignKey(name = "fk_loggroup_id"))
	private LogGroup superGroup;
	
	@JsonIgnore
	@OneToMany(cascade=CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "superGroup_id", referencedColumnName = "id")
	@OrderBy("id asc")
	private Set<LogGroup> subGroups;
	
	@ElementCollection(fetch= FetchType.EAGER)
    @CollectionTable(name="loggroup_moreinformation")
    @MapKeyColumn(name="information")
    @Column(name="value")
	Map<String, String> moreInformation = new HashMap<>();
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_loggroup_sourceconnection_id"))
	private SourceConnection sourceConnection;
	
	@JsonIgnore
	@ManyToMany(mappedBy="logGroups")
	private Set<Issue> issues = new TreeSet<>();
	
	@JsonIgnore
	@OneToMany(mappedBy = "logGroup", cascade=CascadeType.ALL, orphanRemoval = true)
	private Set<SuspiciousFile> suspiciousFiles = new TreeSet<>();
	
	public LogGroup() {
	}
	
	public LogGroup(String label) {
		this.label = label;
	}
	
	public LogGroup(String label, SourceConnection sourceConnection) {
		this.label = label;
		this.sourceConnection = sourceConnection;
	}
	
	public void setSourceConnection(SourceConnection sourceConnection) {
		this.sourceConnection = sourceConnection;
	}
	
	public SourceConnection getSourceConnection() {
		if(sourceConnection == null && superGroup != null) {
			return superGroup.getSourceConnection();
		}
		return sourceConnection;
	}
	
	public Set<String> getElementsId() {
		return Collections.unmodifiableSet(elementsId);
	}
	
	public void addElementId(String elementId) {
		this.elementsId.add(elementId);
	}
	
	public void addElementsId(Set<String> elementsId) {
		this.elementsId.addAll(elementsId);
	}
	
	public void addSubGroup(LogGroup subGroup) {
		if(subGroups == null) {
			subGroups = new TreeSet<>();
		}
		subGroups.add(subGroup);
		elementsId.addAll(subGroup.getElementsId());
		subGroup.setSuperGroup(this);
	}
	
	public void removeSubGroup(LogGroup subGroup) {
		if(subGroups.contains(subGroup)) {
			subGroups.remove(subGroup);
			elementsId.removeAll(subGroup.getElementsId());
			subGroup.setSuperGroup(null);
		}
	}
	
	public void addAllSubGroups(Collection<LogGroup> subGroups) {
		for(LogGroup subGroup : subGroups) {
			addSubGroup(subGroup);
		}
	}
	
	public void updateElementsCount() {
		if(subGroups != null) {
			for(LogGroup subGroup : subGroups) {
				subGroup.updateElementsCount();
			}
		}
		
		if(elementsId != null) {
			elementsCount = elementsId.size();
			if(superGroup != null) {
				superGroup.addElementsId(elementsId);
			}
		} else {
			elementsCount = 0;
		}
	}
	
	public void updateOccurenceDates() {
		if(subGroups != null) {
			for(LogGroup subGroup : subGroups) {
				subGroup.updateOccurenceDates();
				if (firstOccurrenceDate == null || subGroup.getFirstOccurrenceDate().before(firstOccurrenceDate)) {
					firstOccurrenceDate = subGroup.getFirstOccurrenceDate();
				}
				
				if (lastOccurrenceDate == null || subGroup.getLastOccurrenceDate().after(lastOccurrenceDate)) {
					lastOccurrenceDate = subGroup.getLastOccurrenceDate();
				}
			}
		}
	}
	
	public LogGroup getSubGroup(String label) {
		LogGroup sg = null;
		for(LogGroup subGroup : this.subGroups) {
			if(subGroup.getLabel().equals(label)) {
				sg = subGroup;
			}
		}
		return sg;
	}

	@Override
	public int compareTo(LogGroup o) {
		int cmp = 0;
		if(cmp == 0) {
			if(firstOccurrenceDate != null && o.getFirstOccurrenceDate() != null) {
				cmp = firstOccurrenceDate.compareTo(o.getFirstOccurrenceDate());
			}else if(firstOccurrenceDate == null) {
				cmp = -1;
			}else if(o.getFirstOccurrenceDate() == null) {
				cmp = 1;
			}
		}
		if(cmp == 0) {
			if(lastOccurrenceDate != null && o.getLastOccurrenceDate() != null) {
				cmp = lastOccurrenceDate.compareTo(o.getLastOccurrenceDate());
			}else if(lastOccurrenceDate == null) {
				cmp = -1;
			}else if(o.getLastOccurrenceDate() == null) {
				cmp = 1;
			}
		}
		if(cmp == 0) {
			if(label != null && o.getLabel() != null) {
				cmp = label.compareTo(o.getLabel());
			}else if(label == null) {
				cmp = -1;
			}else if(o.getLabel() == null) {
				cmp = 1;
			}
		}
		return cmp;
	}
	
	public List<LogGroup> getSubGroupsWithGroupAggregatorTypeId(String groupAggregatorTypeId) {
		List<LogGroup> logGroups = new ArrayList<>();
		
		if(getSubGroups() != null && !getSubGroups().isEmpty()) {
			for(LogGroup subGroup : getSubGroups()) {
				if(subGroup.getGroupAggregatorTypeId().equals(groupAggregatorTypeId)){
					logGroups.add(subGroup);
				}else {
					logGroups.addAll(subGroup.getSubGroupsWithGroupAggregatorTypeId(groupAggregatorTypeId));
				}
			}
		}
		
		return logGroups;
	}
	
	public void addSuspiciousFile(SuspiciousFile suspiciousFile) {
		this.getSuspiciousFiles().add(suspiciousFile);
		suspiciousFile.setLogGroup(this);
	}
	
	public void addSuspiciousFiles(Set<SuspiciousFile> suspiciousFiles) {
		for (SuspiciousFile suspiciousFile : suspiciousFiles) {
			addSuspiciousFile(suspiciousFile);
		}
	}
	
	public void removeSuspiciousFile(SuspiciousFile suspiciousFile) {
		this.getSuspiciousFiles().remove(suspiciousFile);
		suspiciousFile.setLogGroup(null);
	}
}
