/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.Study;
import br.ufrn.ase.urupema.dtos.GroupCorrelatorSummary;
import br.ufrn.ase.urupema.dtos.SourceConnectionSummary;
import br.ufrn.ase.urupema.process.StudyProcess;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class StudyResource {
	@Autowired
	StudyProcess studyProcess;
	
	@PostMapping("/study")
	public void create(@RequestBody Study study) throws IOException {
		studyProcess.save(study);
	}
	
	@GetMapping("/study/list")
	public List<Study> findAll() throws IOException {
		return studyProcess.findAll();
	}
	
	@GetMapping("/study")
	public Study findById(@RequestParam("id") Long id) throws IOException {
		return studyProcess.findById(id).get();
	}
	
	@PutMapping("/study")
	public void update(@RequestBody Study study) throws IOException {
		Study s = studyProcess.findById(study.getId()).get();
		//Copy properties ignoring sourceConnections, 
		BeanUtils.copyProperties(study, s, "sourceConnections", "groupCorrelators", "issueTrackerConnections");
		studyProcess.save(s);
	}
	
	@DeleteMapping("/study")
	public void delete(@RequestParam("id") Long id) throws IOException {
		log.debug("Delete study: " + id);
		studyProcess.delete(studyProcess.findById(id).get());
	}
	
	@GetMapping("/study/sourceConnectionSummary")
	public Map<String, Set<SourceConnectionSummary>> getSourceConnectionSummary(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return studyProcess.getSourceConnectionSummary(id);
	}
	
	@GetMapping("/study/groupCorrelatorSummary")
	public Map<String, Set<GroupCorrelatorSummary>> getGroupCorrelatorSummary(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return studyProcess.getGroupCorrelatorSummary(id);
	}
}
