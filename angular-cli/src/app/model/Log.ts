import { LogType } from './LogType.enum';

export interface Log {
    identifier: string;
    requestURL: string;
    requestURI: string;
    date: Date;
    logType: LogType;
    moreInformation: Map<string, string>;
    trace: string;
    exceptionMessage: string;
}
