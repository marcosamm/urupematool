package br.ufrn.ase.urupema.extensible.aggregator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.parser.StackTraceParser;

import java.util.TreeSet;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class GroupAggregatorByTopFrameFileAndCrashTypeSignatureAndEquivalentSignatureAndStacktrace extends GroupAggregatorByCrashTypeSignatureAndEquivalentSignatureAndStacktrace implements GroupAggregatorTypePlugin {
	public static String ID = "urupema:st_eqv_r1_r2";
	
	private List<LogType> logTypes = Arrays.asList(LogType.ERROR);
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getName() {
		return "Stack Trace, Equivalent Signature, Crash Type Signature and Top Frame File";
	}

	@Override
	public String getDescription() {
		return "Creates a set of logs grouped by Top Frame File and subgrouped by Crash Type Signature, Equivalent Signature, and Stack Trace";
	}
	
	@Override
	public List<LogGroup> agroup(SourceConnection sourceConnection, Map<String, Object> params, Map<String, WebExecutionTrace> cache) throws IOException {
		Map<String, LogGroup> mapTopFrame = new HashMap<>();
		StackTraceParser stp = new StackTraceParser();
		
		for(LogGroup lg : sourceConnection.getLogGroups()) {
			mapTopFrame.put(lg.getLabel(), lg);
		}
		//Pega os grupos de uris subagrupados pela regra 1
		List<LogGroup> rule1Groups = super.agroup(sourceConnection, params, cache);
		
		//Percorre todos os grupos da regra 1
		for(LogGroup rule1Group : rule1Groups) {
			//Pega o representante do grupo
			String topFrameStr = stp.getTopFrameFile(rule1Group.getLabel());
			
			//Grupo formado por todos os logs cujos frames do topo da pilha tenham mesmo(s) nome(s) qualificado(s) de arquivo.
			LogGroup topFrameLogGroup = null;
			
			//Procura por todos os nomes de arquivos do topo da pilha encontrados no grupo da regra 1
			
			if(mapTopFrame.containsKey(topFrameStr)) {
				topFrameLogGroup = mapTopFrame.get(topFrameStr);
			}
			
			if(topFrameLogGroup == null){
				//Cria um grupo com o método do topo da pilha como label, pois ele ainda não existe
				topFrameLogGroup = new LogGroup(topFrameStr);
				topFrameLogGroup.setTypeDescription("Rule 2 (Top Frame Comparison)");
				topFrameLogGroup.setGroupAggregatorTypeId(ID);
				//Adiciona o grupo criado no map
				mapTopFrame.put(topFrameStr, topFrameLogGroup);
			} else {
				if(!topFrameLogGroup.getLabel().contains(topFrameStr)){
					topFrameLogGroup.setLabel(topFrameLogGroup.getLabel() + "\n" + topFrameStr);
				}
			}
			
			topFrameLogGroup.addSubGroup(rule1Group);
		}
		
		for(Entry<String, LogGroup> o : mapTopFrame.entrySet()) {
			o.getValue().updateOccurenceDates();
			o.getValue().updateElementsCount();
		}
		
		List<LogGroup> logGroups = new ArrayList<>(new TreeSet<>(mapTopFrame.values()));
		int qtdSubs = 0;
		for(LogGroup o : logGroups) {
			qtdSubs += o.getSubGroups().size();
		}
		
		log.debug("Rule 2 (Top Frame Compariton) groups: " + logGroups.size() + " - subGroups: " + qtdSubs);
		return logGroups;
	}

	@Override
	public int compareTo(GroupAggregatorType o) {
		return getId().compareTo(o.getId());
	}
}
