package br.ufrn.ase.urupema.extensible.aggregator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.repository.LogRepository;
import br.ufrn.ase.urupema.repository.WebExecutionRepository;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupAggregatorByUriAndStacktrace implements GroupAggregatorTypePlugin {
	public static String ID = "urupema:st_uri";
	
	private LogRepository logRepository;
	private List<LogType> logTypes = Arrays.asList(LogType.ERROR, LogType.EXECUTION);
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getName() {
		return "URI and Stack Trace Aggregator";
	}

	@Override
	public String getDescription() {
		return "Group by URI (superGroup) and subgroup (subGroups) by stack trace";
	}
	
	protected WebExecutionRepository getWebExecutionRepository() {
		return (WebExecutionRepository) logRepository;
	}
	
	protected LogGroup agroup(SourceConnection sourceConnection, String uri) throws IOException {
		LogGroup uriGroup = getWebExecutionRepository().agroupByUriSubgroupByStackTrace(uri, sourceConnection.getStartDate(), sourceConnection.getEndDate());
		
		
		for(LogGroup stackTraceGroup : uriGroup.getSubGroups()) {
			if(sourceConnection.getLogType()!=LogType.ACCESS) {
				WebExecutionTrace representantStacktraceGroup = getWebExecutionRepository().findById(stackTraceGroup.getRepresentantId()).get();
				stackTraceGroup.getMoreInformation().put("numTraceElements", representantStacktraceGroup.getQtdTraceElements().toString());
			}
			stackTraceGroup.setGroupAggregatorTypeId(ID);
		}
		
		//uriGroup.setSourceConnection(sourceConnection);
		return uriGroup;
	}
	
	@Override
	public List<LogGroup> agroup(SourceConnection sourceConnection, Map<String, Object> params, Map<String, WebExecutionTrace> cache) throws IOException {
		@SuppressWarnings("unchecked")
		Set<String> uris = (Set<String>) params.get("uris");
		List<LogGroup> groups = new ArrayList<>();
		for(String uri : uris) {
			groups.add(agroup(sourceConnection, uri));
		}
		return groups;
	}

	@Override
	public int compareTo(GroupAggregatorType o) {
		return getId().compareTo(o.getId());
	}
}
