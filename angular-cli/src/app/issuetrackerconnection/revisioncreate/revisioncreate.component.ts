import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IssueService, RevisionService } from '../../service';
import { Issue, Revision } from '../../model';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-revisioncreate',
  templateUrl: './revisioncreate.component.html',
  styleUrls: ['./revisioncreate.component.css']
})
export class RevisionCreateComponent implements OnInit {
  revisionForm: FormGroup;
  issue: Issue;
  revision: Revision;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private revisionService: RevisionService,
    private issueService: IssueService,
    private fb: FormBuilder
  ) {
    this.revisionForm = this.fb.group({
      identifier: ['', [Validators.required, Validators.minLength(5)]],
      date: ['', [Validators.required]],
      comment: [''],
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.issueService.getIssue(params['idIssue']).subscribe(
        issue => {
          this.issue = issue;
          this.issueService.getIssueTrackerConnectionByIssue(this.issue).subscribe(issueTrackerConnection => {
            this.issue.issueTrackerConnection = issueTrackerConnection;
          });
        }
      );
      if (params['idRevision'] !== undefined) {
        this.revisionService.getRevision(params['idRevision']).subscribe(
          revision => {
            this.revision = revision;
            this.revisionForm.get('identifier').setValue(this.revision.identifier);
            this.revisionForm.get('date').setValue(formatDate(this.revision.date, 'yyyy-MM-ddTHH:mm:ss', 'pt_BR', 'GMT-3'));
            this.revisionForm.get('comment').setValue(this.revision.comment);
          }
        );
      }
    });
  }

  create() {
    this.revisionService.createRevision(
      this.issue.id,
      this.revisionForm.get('identifier').value,
      new Date(this.revisionForm.get('date').value),
      this.revisionForm.get('comment').value
    ).subscribe(
      result => {
        this.router.navigate(['/issuetrackerconnection/issuedetail/' + + this.issue.issueTrackerConnection.id + '/' + this.issue.id]);
      }
    );
  }

  update() {
    this.revisionService.updateRevision(
      this.revision.id,
      this.revisionForm.get('identifier').value,
      new Date(this.revisionForm.get('date').value),
      this.revisionForm.get('comment').value
    ).subscribe(
      result => {
        this.router.navigate(['/issuetrackerconnection/issuedetail/' + + this.issue.issueTrackerConnection.id + '/' + this.issue.id]);
      }
    );
  }
}
