package br.ufrn.ase.urupema.process;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.FileChangeType;

@Component
public class FileChangeTypeProcess {
	public Collection<FileChangeType> findAll() {
		return Arrays.asList(FileChangeType.values());
	}
}
