import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Study } from '../../model';
import { StudyService } from '../../service';


@Component({
  selector: 'app-studylist',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class StudyListComponent implements OnInit {
  studies: Study[];

  constructor(private router: Router, private studyService: StudyService) { }

  ngOnInit() {
    this.studyService.getStudies().subscribe(res => this.studies = res);
  }

  deleteStudy(study: Study) {
    if (window.confirm('Are sure you want to delete this item ?')) {
      this.studyService.deleteStudy(study).subscribe(
        res => { this.ngOnInit(); }
      );
    }
  }
}
