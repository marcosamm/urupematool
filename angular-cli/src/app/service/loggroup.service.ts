import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { SourceConnection } from '../model/SourceConnection';
import { Observable } from 'rxjs';
import { LogGroup } from '../model/LogGroup';
import { SuspiciousFile } from '../model/SuspiciousFile';
import { UriStats, TimeStats, Issue, LogGroupSuspiciousReport } from '../model';
import { CalledMethodStats } from '../model/CalledMethodStats';

@Injectable()
export class LogGroupService {
  urlGet = environment.apiUrl + '/loggroup';

  constructor(private http: HttpClient) { }

  getLogGroup(id: number): Observable<LogGroup> {
    return this.http.get<LogGroup>(this.urlGet + '/detail?id=' + id);
  }

  getLogGroupsBySourceConnectionId(sourceConnection: SourceConnection): Observable<LogGroup[]> {
    return this.http.get<LogGroup[]>(
      this.urlGet + '/listbysourceconnection?idSourceConnection=' + sourceConnection.id
    );
  }

  getSuspiciousFilesByLogGroupId(logGroupId: number): Observable<SuspiciousFile[]> {
    return this.http.get<SuspiciousFile[]>(
      this.urlGet + '/suspiciousfilesbyloggroupid?idLogGroup=' + logGroupId
    );
  }

  getAffectedUrisByLogGroupId(logGroupId: number): Observable<UriStats[]> {
    return this.http.get<UriStats[]>(
      this.urlGet + '/affecteduris?idLogGroup=' + logGroupId
    );
  }

  getLogsOverTimeByLogGroupId(logGroupId: number): Observable<TimeStats[]> {
    return this.http.get<TimeStats[]>(
      this.urlGet + '/logsovertime?idLogGroup=' + logGroupId
    );
  }

  getCalledFileMethodsByLogGroupIdAndFileName(logGroupId: number, fileName: string): Observable<CalledMethodStats []> {
    return this.http.get<CalledMethodStats []>(
      this.urlGet + '/calledfilemethods?idLogGroup=' + logGroupId + '&fileName=' + fileName
    );
  }

  getSubGroupsByLogGroup(superGroup: LogGroup): Observable<LogGroup[]> {
    return this.http.get<LogGroup[]>(
      this.urlGet + '/listsubgroupsbyloggroup?idLogGroup=' + superGroup.id
    );
  }

  getMapFCSFsByLogGroup(superGroup: LogGroup): Observable<Map<string, number>> {
    return this.http.get<Map<string, number>>(
      this.urlGet + '/listmapfcsfsbyloggroup?idLogGroup=' + superGroup.id
    );
  }

  getSourceConnectionByLogGroup(logGroup: LogGroup): Observable<SourceConnection> {
    return this.http.get<SourceConnection>(
      this.urlGet + '/sourceconnection?idLogGroup=' + logGroup.id
    );
  }

  getIssues(logGroup: LogGroup): Observable<Issue[]> {
    return this.http.get<Issue[]>(
      this.urlGet + '/issues?idLogGroup=' + logGroup.id
    );
  }

  getLogGroupSuspiciousReport(id: number): Observable<LogGroupSuspiciousReport> {
    return this.http.get<LogGroupSuspiciousReport>(this.urlGet + '/summary?idLogGroup=' + id);
  }
}
