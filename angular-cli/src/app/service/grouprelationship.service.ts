import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { GroupCorrelator } from '../model/GroupCorrelator';
import { Observable, of } from 'rxjs';
import { GroupRelationship } from '../model/GroupRelationship';
import { LogGroupService } from './loggroup.service';
import { map } from 'rxjs/operators';
import { LogGroup } from '../model/LogGroup';

@Injectable()
export class GroupRelationshipService {
  urlGet = environment.apiUrl + '/grouprelationship';
  mapGroup: Map<number, LogGroup> = new Map();

  constructor(
    private http: HttpClient,
    private logGroupService: LogGroupService
  ) { }

  getGroupRelationship(id: number): Observable<GroupRelationship> {
    const groupRelationship: GroupRelationship = new GroupRelationship();

    this.http.get<any[]>(this.urlGet + '/detail?id=' + id).subscribe(
      async (g: any) => {
        groupRelationship.id = g['id'];
        if (!this.mapGroup.has(g['group'])) {
          this.mapGroup.set(g['group'], await this.logGroupService.getLogGroup(g['group']).toPromise());
        }
        groupRelationship.group = this.mapGroup.get(g['group']);
        groupRelationship.relateds = [];
        for (const idLogGroup of g.relateds) {
          if (!this.mapGroup.has(idLogGroup)) {
            this.mapGroup.set(idLogGroup, await this.logGroupService.getLogGroup(idLogGroup).toPromise());
          }
          groupRelationship.relateds.push(this.mapGroup.get(idLogGroup));
        }
      }
    );

    return of(groupRelationship);
  }

  getGroupRelationshipsByGroupCorrelator(groupCorrelator: GroupCorrelator): Observable<GroupRelationship[]> {
    //TODO Ver que erro é esse
    /*return this.http.get<any[]>(this.urlGet + '/listbygroupcorrelator?idGroupCorrelator=' + groupCorrelator.id)
    .pipe<GroupRelationship[]>(
      map(
        async (ret) => {
          const groupRelationships: GroupRelationship[] = [];
          for ( const g of ret) {
            const groupRelationship: GroupRelationship = await this.mapGroupRelationship(g);
            groupRelationships.push(groupRelationship);
          }
          return groupRelationships;
        }
      )
    );*/
    return null;
  }

  async mapGroupRelationship(g: any): Promise<GroupRelationship> {
    const groupRelationship: GroupRelationship = new GroupRelationship();
    groupRelationship.id = g['id'];
    if (!this.mapGroup.has(g['group'])) {
      this.mapGroup.set(g['group'], await this.logGroupService.getLogGroup(g['group']).toPromise());
    }
    groupRelationship.group = this.mapGroup.get(g['group']);
    groupRelationship.relateds = [];
    for (const idLogGroup of g.relateds) {
      if (!this.mapGroup.has(idLogGroup)) {
        this.mapGroup.set(idLogGroup, await this.logGroupService.getLogGroup(idLogGroup).toPromise());
      }
      groupRelationship.relateds.push(this.mapGroup.get(idLogGroup));
    }
    return groupRelationship;
  }
}
