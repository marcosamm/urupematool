import { LogType } from './LogType.enum';

export interface SourceType {
    id: number;
    name: string;
    logTypes: LogType[];
}
