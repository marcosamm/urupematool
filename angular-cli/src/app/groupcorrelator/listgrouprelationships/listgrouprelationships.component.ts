import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GroupCorrelatorService, GroupRelationshipService } from '../../service';
import { GroupCorrelator } from '../../model/GroupCorrelator';
import { Table } from 'primeng/table';
import { LogGroup } from '../../model/LogGroup';

@Component({
  selector: 'app-listgrouprelationships',
  templateUrl: './listgrouprelationships.component.html',
  styleUrls: ['./listgrouprelationships.component.css']
})
export class ListGroupRelationshipsComponent implements OnInit {
  @ViewChild('dt', {static: false}) tableRef: Table;
  groupCorrelator: GroupCorrelator;
  groupMap: Map<number, LogGroup>;

  cols: any[];
  rows: any[];
  rowTotal = {
    id: '',
    uri: 'TOTAL',
    groupOriginId: 0,
    groupOriginNumElements: 0,
    relateds: '',
    relatedsNumElements: '',
    numRelatedGroups: 0,
    numRelatedElements: 0
  };

  constructor(
    private route: ActivatedRoute,
    private groupCorrelatorService: GroupCorrelatorService,
    private groupRelationshipService: GroupRelationshipService
  ) { }

  ngOnInit() {
    this.cols = [
      { field: 'id', header: 'id' },
      { field: 'uri', header: 'URI' },
      { field: 'groupOriginId', header: 'Id' },
      { field: 'groupOriginNumElements', header: 'Nº Elements' },
      { field: 'relateds', header: 'Ids (nº elements)' },
      { field: 'numRelatedGroups', header: 'Nº Groups' },
      { field: 'numRelatedElements', header: 'Total Elements' }
    ];
    this.route.params.subscribe(params => {
      this.groupCorrelatorService.getGroupCorrelator(params['idGroupCorrelator']).subscribe(
        async (groupCorrelator) => {
        this.groupCorrelator = groupCorrelator;
        this.groupCorrelator.groupRelationships =
          await this.groupRelationshipService.getGroupRelationshipsByGroupCorrelator(groupCorrelator).toPromise();
        this.rows = [];
        this.groupMap = new Map<number, LogGroup>();
        this.groupCorrelator.groupRelationships.forEach(
          groupRelationship => {
            const row = {
              id: groupRelationship.id,
              uri: groupRelationship.group.superGroup.label,
              groupOriginId: groupRelationship.group.id,
              groupOriginLabel: groupRelationship.group.label,
              groupOriginNumElements: groupRelationship.group.elementsCount,
              relateds: [],
              relatedsNumElements: new Map(),
              numRelatedGroups: groupRelationship.relateds.length,
              numRelatedElements: groupRelationship.getRelatedsLogCount()
            };
            this.rowTotal.groupOriginId += 1;
            this.rowTotal.groupOriginNumElements += row.groupOriginNumElements;
            this.rowTotal.numRelatedGroups += row.numRelatedGroups;
            this.rowTotal.numRelatedElements += row.numRelatedElements;
            for (const rg of groupRelationship.relateds) {
              row.relateds.push(rg.id);
              if (!this.groupMap.has(rg.id)) {
                this.groupMap.set(rg.id, rg);
              }
            }
            this.rows.push(row);
          }
        );
      });
    });
  }
/*
  createFilters() {
    this.tableRef.filterConstraints['inRelateds'] = (value: any[], filter: any): boolean => {
      if (filter === undefined || filter === null) {
        return true;
      }
      if (value === undefined || value === null || !value.length) {
        return false;
      }
      const finded = value.find(x => x.id + '' === filter + '');
      return finded !== undefined;
    };
  }*/
}
