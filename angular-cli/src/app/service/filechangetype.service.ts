import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { FileChangeType } from '../model';

@Injectable()
export class FileChangeTypeService {
  urlGet = environment.apiUrl + '/filechangetype/list';

  constructor(private http: HttpClient) { }

  getFileChangeTypes(): Observable<FileChangeType[]> {
       return this.http.get<FileChangeType[]>(this.urlGet);
  }
}
