import { SuspiciousFile } from './SuspiciousFile';

export interface StatsTopN {
    issueIdentifier: string;
    logGroupId: number;
    logGroupElementsCount: number;
    sourcePackage: string;
    changedFiles: Set<string>;
    numChangedFilesInTop1: number;
    numChangedFilesInTop3: number;
    numChangedFilesInTop5: number;
    numChangedFilesInTop10: number;
    top10: SuspiciousFile [];
    numSourceOnlyChangedFilesInTop1: number;
    numSourceOnlyChangedFilesInTop3: number;
    numSourceOnlyChangedFilesInTop5: number;
    numSourceOnlyChangedFilesInTop10: number;
    sourceOnlyTop10: SuspiciousFile [];
}
