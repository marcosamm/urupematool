package br.ufrn.ase.urupema.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@SequenceGenerator(name = "ID_CHANGEDFILE_SEQUENCE", sequenceName = "changedfile_id_sequence", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Getter
@Setter
@ToString(of={"id", "name"})
public class ChangedFile implements Serializable, Comparable<ChangedFile> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_CHANGEDFILE_SEQUENCE")
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private FileChangeType fileChangeType;
	
	@ManyToOne(optional=false)
	private Revision revision;
	
	@Column(nullable = false)
	private boolean enabled = true;
	
	@Column(columnDefinition="text")
	private String note;
	
	public ChangedFile() {
	}
	
	public ChangedFile(String name, FileChangeType fileChangeType) {
		super();
		this.name = name;
		this.fileChangeType = fileChangeType;
	}
	
	public ChangedFile(String name, FileChangeType fileChangeType, boolean enabled, String note) {
		super();
		this.name = name;
		this.fileChangeType = fileChangeType;
		this.enabled = enabled;
		this.note = note;
	}

	@Override
	public int compareTo(ChangedFile arg0) {
		int ret = 0;
		if(revision != null && arg0.getRevision() != null) {
			ret = revision.compareTo(arg0.getRevision());
		} else if(revision != null) {
			ret = 1;
		} else if (arg0.getRevision() != null){
			ret = -1;
		}
		if(ret == 0) {
			ret = name.compareTo(arg0.getName());
		}
		return ret;
	}
}
