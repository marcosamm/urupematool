import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Study, AffectedUri, StudyResult } from '../model';
import { IssueTrackerConnection } from '../model/IssueTrackerConnection';

@Injectable()
export class IssueTrackerConnectionService {
  urlGet = environment.apiUrl + '/issuetrackerconnection';

  constructor(private http: HttpClient) { }

  getIssueTrackerConnection(id: number): Observable<IssueTrackerConnection> {
    return this.http.get<IssueTrackerConnection>(this.urlGet + '?id=' + id);
  }

  createIssueTrackerConnection(
    studyId: number,
    issueTrackerSourceId: string,
    name: string,
    startDate: Date,
    endDate: Date,
    queryProperties: Map<string, string>
  ) {
    const issueTrackerConnection = {
      study: { id: studyId },
      issueTrackerSource: {id: issueTrackerSourceId},
      name: name,
      startDate: startDate,
      endDate: endDate,
      queryProperties: this.convMap(queryProperties)
    };
    return this.http.post(this.urlGet, issueTrackerConnection);
  }

  updateIssueTrackerConnection(
    id: number,
    studyId: number,
    issueTrackerSourceId: string,
    name: string,
    startDate: Date,
    endDate: Date,
    queryProperties: Map<string, string>
  ) {
    const issueTrackerConnection = {
      id: id,
      study: { id: studyId },
      issueTrackerSource: {id: issueTrackerSourceId},
      name: name,
      startDate: startDate,
      endDate: endDate,
      queryProperties: this.convMap(queryProperties)
    };
    return this.http.put(this.urlGet, issueTrackerConnection);
  }

  deleteIssueTrackerConnection(issueTrackerConnection: IssueTrackerConnection) {
    return this.http.delete(this.urlGet + '?id=' + issueTrackerConnection.id);
  }

  getStudy(issueTrackerConnection: IssueTrackerConnection): Observable<Study> {
    return this.http.get<Study>(this.urlGet + '/study?idIssueTrackerConnection=' + issueTrackerConnection.id);
  }

  importIssues(issueTrackerConnection: IssueTrackerConnection) {
    return this.http.get<Study>(this.urlGet + '/importissues?idIssueTrackerConnection=' + issueTrackerConnection.id);
  }

  listAffectedUris(
    issueTrackerConnectionId: number
  ): Observable<string []> {
    return this.http.get<string []>(
      this.urlGet + '/listaffecteduris?idIssueTrackerConnection=' + issueTrackerConnectionId +
      '&issueTrackerConnectionId=' + issueTrackerConnectionId
    );
  }

  importAffectedUris(
    issueTrackerConnectionId: number,
    sourceConnectionId: number,
    uriFragment: string
  ) {
    return this.http.get(
      this.urlGet + '/importaffecteduris?idIssueTrackerConnection=' + issueTrackerConnectionId +
      '&idSourceConnection=' + sourceConnectionId +
      '&uriFragment=' + uriFragment
    );
  }

  agroupLogs(
    issueTrackerConnectionId: number,
    sourceConnectionId: number
  ) {
    return this.http.get(
      this.urlGet + '/agroup?idIssueTrackerConnection=' + issueTrackerConnectionId +
      '&idSourceConnection=' + sourceConnectionId
    );
  }

  matchIssueWithLogGroups(
    issueTrackerConnectionId: number,
    sourceConnectionId: number
  ) {
    return this.http.get(
      this.urlGet + '/match?idIssueTrackerConnection=' + issueTrackerConnectionId +
      '&idSourceConnection=' + sourceConnectionId
    );
  }

  calculateTopNs(
    issueTrackerConnectionId: number,
    sourceConnectionId: number,
    fileExtensions: string,
    sourcePackage: string,
    checkSourcePackageInStackTrace: boolean,
    maxEnabledChangedFiles: number
  ): Observable<StudyResult> {
    return this.http.get<StudyResult>(
      this.urlGet + '/calculateTopNs?idIssueTrackerConnection=' + issueTrackerConnectionId +
      '&idSourceConnection=' + sourceConnectionId +
      '&fileExtensions=' + fileExtensions +
      '&sourcePackage=' + sourcePackage +
      '&checkSourcePackageInStackTrace=' + checkSourcePackageInStackTrace +
      '&maxEnabledChangedFiles=' + maxEnabledChangedFiles
    );
  }

  convMap(map: Map<any, any>): {} {
    const convMap = {};
    map.forEach((val, key) => {
      convMap[key] = val;
    });
    return convMap;
  }
}
