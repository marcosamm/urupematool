package br.ufrn.ase.urupema.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

@Entity
@SequenceGenerator(name = "ID_GROUPRELATIONSHIP_SEQUENCE", sequenceName = "grouprelationship_id_sequence", allocationSize = 1)
@Getter
@Setter
public class GroupRelationship implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_GROUPRELATIONSHIP_SEQUENCE")
	private Long id;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY, optional=false)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_grouprelationship_groupcorrelator_id"))
	private GroupCorrelator groupCorrelator;
	
	@JsonIdentityReference(alwaysAsId = true)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = LogGroup.class)
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "loggroup_id", referencedColumnName="id", foreignKey = @ForeignKey(name = "fk_grouprelationship_loggroup_id"))
	LogGroup group;
	
	@JsonIdentityReference(alwaysAsId = true)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = LogGroup.class)
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="grouprelationship_related", 
		joinColumns={@JoinColumn(name="grouprelationship_id")}, 
		inverseJoinColumns={@JoinColumn(name="loggroup_id")}
	)
	List<LogGroup>  relateds = new ArrayList<>();
	
	public boolean existsRelashionship(LogGroup originGroup, LogGroup destinyGroup) {
		boolean related = false;
		
		if(group.getId().equals(originGroup.getId())) {
			for(LogGroup destiny : getRelateds()) {
				if(destiny.getId().equals(destinyGroup.getId())) {
					related = true;
					break;
				}
			}
		}
		
		return related;
	}
}
