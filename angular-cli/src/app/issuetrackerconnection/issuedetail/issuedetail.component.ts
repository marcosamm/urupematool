import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IssueService, IssueTrackerConnectionService, AffectedUriService, ChangedFileService, LogGroupService, RevisionService } from '../../service';
import { Issue, IssueTrackerConnection, ReportedStackTrace, Revision, ChangedFile, AffectedUri } from '../../model';
import { ReportedStackTraceService } from '../../service/reportedstacktrace.service';


@Component({
  selector: 'app-issuedetail',
  templateUrl: './issuedetail.component.html',
  styleUrls: ['./issuedetail.component.css']
})
export class IssueDetailComponent implements OnInit {
  issue: Issue;
  issueTrackerConnection: IssueTrackerConnection;

  constructor(
    private route: ActivatedRoute,
    private issueService: IssueService,
    private issueTrackerConnectionService: IssueTrackerConnectionService,
    private affectedUriService: AffectedUriService,
    private changedFileService: ChangedFileService,
    private logGroupService: LogGroupService,
    private reportedStackTraceService: ReportedStackTraceService,
    private revisionService: RevisionService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.issueTrackerConnectionService.getIssueTrackerConnection(params['idIssueTrackerConnection']).subscribe(
        issueTrackerConnection => {
          this.issueTrackerConnection = issueTrackerConnection;
        }
      );
      this.issueService.getIssue(params['idIssue']).subscribe(
        issue => {
          this.issue = issue;
          this.issueService.getReportedStackTracesByIssue(this.issue).subscribe(
            reportedStackTraces => {
              this.issue.reportedStackTraces = reportedStackTraces;
            }
          );
          this.issueService.getRevisionsByIssue(this.issue).subscribe(
            revisions => {
              this.issue.revisions = revisions;
            }
          );
          this.issueService.getLogGroupsByIssue(this.issue).subscribe(logGroups => {
            this.issue.logGroups = logGroups;
            this.issue.logGroups.forEach(logGroup => {
              this.logGroupService.getSourceConnectionByLogGroup(logGroup).subscribe(sourceConnection => {
                logGroup.sourceConnection = sourceConnection;
              });
            });
          });
        }
      );
    });
  }

  fillAffectedUrisByReportedStackTrace(event) {
    const reportedStackTrace: ReportedStackTrace = event.data;
    if (reportedStackTrace.affectedUris === undefined) {
      this.affectedUriService.getAffectedUrisByReportedStackTrace(reportedStackTrace).subscribe(
        affectedUris => {
          reportedStackTrace.affectedUris = affectedUris;
        }
      );
    }
  }

  fillChangedFilesByRevision(event) {
    const revision: Revision = event.data;
    if (revision.changedFiles === undefined) {
      this.changedFileService.getChangedFilesByRevision(revision).subscribe(
        changedFiles => {
          revision.changedFiles = changedFiles;
        }
      );
    }
  }

  deleteReportedStackTrace(reportedStackTrace: ReportedStackTrace) {
    if (window.confirm('Are sure you want to delete this item ?')) {
      this.reportedStackTraceService.deleteReportedStackTrace(reportedStackTrace).subscribe(
        res => { this.ngOnInit(); }
      );
    }
  }

  deleteChangedFile(changedFile: ChangedFile) {
    if (window.confirm('Are sure you want to delete this item ?')) {
      this.changedFileService.deleteChangedFile(changedFile).subscribe(
        res => { this.ngOnInit(); }
      );
    }
  }

  deleteRevision(revision: Revision) {
    if (window.confirm('Are sure you want to delete this item ?')) {
      this.revisionService.deleteRevision(this.issue, revision).subscribe(
        res => { this.ngOnInit(); }
      );
    }
  }

  deleteAffectedUri(affectedUri: AffectedUri) {
    if (window.confirm('Are sure you want to delete this item ?')) {
      this.affectedUriService.deleteAffectedUri(affectedUri).subscribe(
        res => { this.ngOnInit(); }
      );
    }
  }
}
