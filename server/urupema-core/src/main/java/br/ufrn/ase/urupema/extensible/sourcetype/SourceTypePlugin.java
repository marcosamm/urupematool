package br.ufrn.ase.urupema.extensible.sourcetype;

import javax.naming.OperationNotSupportedException;

import br.ufrn.ase.urupema.domain.Source;
import br.ufrn.ase.urupema.domain.SourceType;
import br.ufrn.ase.urupema.repository.AccessLogRepository;
import br.ufrn.ase.urupema.repository.ErrorLogRepository;
import br.ufrn.ase.urupema.repository.ScenarioRepository;

public interface SourceTypePlugin extends SourceType {
	public AccessLogRepository getAccessLogRepository(Source source) throws OperationNotSupportedException;
	public ErrorLogRepository getErrorLogRepository(Source source) throws OperationNotSupportedException;
	public ScenarioRepository getScenarioRepository(Source source) throws OperationNotSupportedException;
}
