export enum FileChangeType {
    ADDED = 'ADDED',
    MODIFIED = 'MODIFIED',
    DELETED = 'DELETED',
    RENAMED = 'RENAMED'
}
