export enum PropertyType {
    STRING,
    BOOLEAN,
	INTEGER,
	LONG,
	FLOAT,
	DOUBLE
}
