import { LogGroup } from './LogGroup';
import { GroupCorrelator } from './GroupCorrelator';

export class GroupRelationship {
    id: number;
    groupCorrelator: GroupCorrelator;
    group: LogGroup;
    relateds: LogGroup[];

    getRelatedsLogCount(): number {
        let cnt = 0;

        for (const logGroup of this.relateds) {
            cnt = cnt + logGroup.elementsCount;
        }

        return cnt;
    }
}
