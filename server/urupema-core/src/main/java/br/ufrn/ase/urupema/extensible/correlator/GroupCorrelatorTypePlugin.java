package br.ufrn.ase.urupema.extensible.correlator;

import br.ufrn.ase.urupema.domain.GroupCorrelatorType;
import br.ufrn.ase.urupema.repository.factory.LogRepositoryFactory;

public interface GroupCorrelatorTypePlugin extends GroupCorrelatorType {
	public void setLogRepositoryFactory(LogRepositoryFactory logRepositoryFactory);
	public LogRepositoryFactory getLogRepositoryFactory();
}
