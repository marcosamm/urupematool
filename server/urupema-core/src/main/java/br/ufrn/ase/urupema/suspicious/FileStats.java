package br.ufrn.ase.urupema.suspicious;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(of={"name"})
@ToString(of={"name"})
public class FileStats implements Serializable, Comparable<FileStats>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;
	private int numGroups;
	private int numElements;
	private int numFCSFs;
	private int numFreqFailFiles;
	
	private int totalFCSFs;
	private int totalGroups;
	private int totalElements;
	
	private Map<Integer, Integer> topFrameDists = new TreeMap<>();
	
	public FileStats() {
		
	}
	
	public FileStats(String name, int numGroups, int numElements) {
		this.name = name;
		this.topFrameDists = new TreeMap<>();
		this.numGroups = numGroups;
		this.numElements = numElements;
	}
	
	public void addTopFrameDists(Integer distance, Integer numElements) {
		if(!topFrameDists.containsKey(distance)) {
			topFrameDists.put(distance, numElements);
		}else {
			topFrameDists.put(distance, topFrameDists.get(distance)+numElements);
		}
	}
	
	public void addTopFrameDists(Map<Integer, Integer> topFrameDists) {
		for(Map.Entry<Integer, Integer> entry : topFrameDists.entrySet()) {
			addTopFrameDists(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public int compareTo(FileStats o) {
		int comp = this.name.compareTo(o.getName());
		if(comp == 0) {
			comp = Integer.valueOf(o.topFrameDists.size()).compareTo(this.topFrameDists.size());
		}
		return comp;
	}
	
	public int getCountFirst10Frames() {
		int i = 0;
		for(Map.Entry<Integer, Integer> entry : topFrameDists.entrySet()) {
			if(entry.getKey() <= 10) {
				i += entry.getValue();
			}
		}
		return i;
	}
	
	public Float getPercFirst10Frames(){
		return Float.valueOf(getCountFirst10Frames())/totalElements;
	}
	
	public Float getPercGroups(){
		return Float.valueOf(numGroups)/totalGroups;
	}
	
	public Float getPercElements(){
		return Float.valueOf(numElements)/totalElements;
	}
	
	public Float getPercFreqFailFiles(){
		return Float.valueOf(numFreqFailFiles)/totalGroups;
	}
	
	public Float getPercFCSFs(){
		Float tot = 0f;
		if(totalFCSFs != 0){
			tot = Float.valueOf(numFCSFs)/totalFCSFs;
		}
		return tot;
	}
	
	public Float getInverseAverageDistanceToCrashPoint() {
		int dist = 0;
		for(Map.Entry<Integer, Integer> entry : topFrameDists.entrySet()) {
			dist += entry.getKey()*entry.getValue();
		}
		return 1f / (1 + Float.valueOf(dist)/totalElements);
	}
	
	public Double getInverseBucketFrequency() {
		return Math.log1p(Float.valueOf(totalGroups)/numGroups);
	}
	
	public Float getFileFrequency() {
		return getPercElements();
	}
	
	public Double getScore() {
		return getInverseAverageDistanceToCrashPoint()*getInverseBucketFrequency()*getFileFrequency();
	}
	
	public Double getScore1() {
		return getScore() + getPercFCSFs();
	}
	
	public Double getScore2() {
		return getScore()*getPercFCSFs();
	}
}
