package br.ufrn.ase.urupema.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

@Entity
@SequenceGenerator(name = "ID_ISSUETRACKERCONNECTION_SEQUENCE", sequenceName = "issuetrackerconnection_id_sequence", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Getter
@Setter
public class IssueTrackerConnection implements Serializable, Comparable<IssueTrackerConnection> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_ISSUETRACKERCONNECTION_SEQUENCE")
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY, optional=false)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_issuetrackerconnection_study_id"))
	private Study study;
	
	@ManyToOne(fetch = FetchType.EAGER, optional=false)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_issuetrackerconnection_issuetrackersource_id"))
	private IssueTrackerSource issueTrackerSource;
	
	@ElementCollection(fetch= FetchType.EAGER)
    @CollectionTable(name="issuetrackerconnection_queryproperty")
    @MapKeyColumn(name="property")
    @Column(name="value")
	public Map<String, String> queryProperties;
	
	@JsonIgnore
	@OneToMany(mappedBy = "issueTrackerConnection", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
	@OrderBy("identifier ASC")
	private Set<Issue> issues = new TreeSet<>();

	@Override
	public int compareTo(IssueTrackerConnection o) {
		int ret = study.compareTo(o.getStudy());
		if(ret == 0) {
			ret = issueTrackerSource.compareTo(o.getIssueTrackerSource());
		}
		if(ret == 0) {
			ret = startDate.compareTo(o.getStartDate());
		}
		if(ret == 0) {
			ret = endDate.compareTo(o.getEndDate());
		}
		return ret;
	}
	
	public void addIssue(Issue issue) {
		this.issues.add(issue);
		issue.setIssueTrackerConnection(this);
	}
	
	public void addIssues(Set<Issue> issues) {
		for(Issue issue : issues) {
			addIssue(issue);
		}
	}
	
	public void removeIssue(Issue issue) {
		this.issues.remove(issue);
		issue.setIssueTrackerConnection(null);
	}
	
	public void removeIssues(Set<Issue> issues) {
		for(Issue issue : issues) {
			removeIssue(issue);
		}
	}
}
