package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.AffectedUri;
import br.ufrn.ase.urupema.domain.Issue;
import br.ufrn.ase.urupema.domain.IssueTrackerConnection;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.ReportedStackTrace;
import br.ufrn.ase.urupema.domain.Revision;
import br.ufrn.ase.urupema.process.IssueProcess;
import br.ufrn.ase.urupema.process.IssueTrackerConnectionProcess;


@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class IssueResource {	
	@Autowired
	IssueProcess process;
	
	@Autowired
	IssueTrackerConnectionProcess issueTrackerConnectionProcess;
	
	@GetMapping("/issue")
	public Issue findById(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findById(id);
	}
	
	@PostMapping("/issue")
	public void put(@RequestBody Issue issue) throws IOException {
		process.save(issue);
	}
	
	@PutMapping("/issue")
	public void update(@RequestBody Issue issue) throws IOException, InstantiationException, IllegalAccessException {
		Issue i = findById(issue.getId());
		//Copy properties ignoring logGroups
		BeanUtils.copyProperties(issue, i, "issueTrackerConnection", "revisions", "logGroups", "reportedStackTraces");
		process.save(i);
	}
	
	@DeleteMapping("/issue")
	public void delete(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		process.delete(id);
	}
	
	@GetMapping("/issue/listbyissuetrackerconnection")
	public List<Issue> findBySourceConnection(@RequestParam("idIssueTrackerConnection") Long id) throws InstantiationException, IllegalAccessException, IOException {
		return process.findByIssueTrackerConnection(issueTrackerConnectionProcess.findById(id));
	}
	
	@GetMapping("/issue/listreportedstacktraces")
	public Set<ReportedStackTrace> findReportedStackTracesByIssue(@RequestParam("idIssue") Long id) throws InstantiationException, IllegalAccessException, IOException {
		return process.getReportedStackTracesByIssue(id);
	}
	
	@GetMapping("/issue/listenabledaffecteduris")
	public Set<AffectedUri> findEnabledAffectedUrisByIssue(@RequestParam("idIssue") Long id) throws InstantiationException, IllegalAccessException, IOException {
		return process.getEnabledAffectedUrisByIssue(id);
	}
	
	@GetMapping("/issue/listrevisions")
	public Set<Revision> findRevisionsByIssue(@RequestParam("idIssue") Long id) throws InstantiationException, IllegalAccessException, IOException {
		return process.getRevisionsByIssue(id);
	}
	
	@GetMapping("/issue/listloggroups")
	public Set<LogGroup> findLogGroupsByIssue(@RequestParam("idIssue") Long id) throws InstantiationException, IllegalAccessException, IOException {
		return process.getLogGroups(id);
	}
	
	@GetMapping("/issue/issuetrackerconnection")
	public IssueTrackerConnection findIssueTrackerConnectionByIssue(@RequestParam("idIssue") Long id) throws InstantiationException, IllegalAccessException, IOException {
		return process.getIssueTrackerConnection(id);
	}
}
