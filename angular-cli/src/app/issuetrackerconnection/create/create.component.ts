import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StudyService, IssueTrackerConnectionService, IssueTrackerSourceService, } from '../../service';
import { Study, IssueTrackerSource, IssueTrackerType, IssueTrackerConnection } from '../../model';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-issuetrackerconnectioncreate',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class IssueTrackerConnectionCreateComponent implements OnInit {
  issueTrackerConnectionForm: FormGroup;
  issueTrackerConnection: IssueTrackerConnection;
  study: Study = undefined;
  issueTrackerSourceSelected: IssueTrackerSource;
  issueTrackerSources: IssueTrackerSource[] = [];
  issueTrackerSourcesMap: Map<number, IssueTrackerSource> = new Map();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private studyService: StudyService,
    private issueTrackerConnectionService: IssueTrackerConnectionService,
    private issueTrackerSourceService: IssueTrackerSourceService,
    private fb: FormBuilder
  ) {
    this.issueTrackerConnectionForm = this.fb.group({
      issueTrackerSourceId: ['', Validators.required],
      name: ['', [Validators.required, Validators.minLength(5)]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studyService.getStudy(params['idStudy']).subscribe(
        study => this.study = study
      );
      this.issueTrackerSourceService.getIssueTrackerSources().subscribe(
        issueTrackerSources => {
          this.issueTrackerSources = issueTrackerSources;
          for (const issueTrackerSource of issueTrackerSources) {
            this.issueTrackerSourcesMap.set(issueTrackerSource.id, issueTrackerSource);
          }
        }
      );
      if (params['id'] !== undefined) {
        this.issueTrackerConnectionService.getIssueTrackerConnection(params['id']).subscribe(
          issueTrackerConnection => {
            this.issueTrackerConnection = issueTrackerConnection;
            this.fillForm(issueTrackerConnection.issueTrackerSource.id);
          }
        );
      }
    });
  }

  fillForm(issueTrackerSourceId: number) {
    let issueTrackerSource: IssueTrackerSource;
    this.issueTrackerSources.forEach(its => {
      if (its.id + '' === issueTrackerSourceId + '') {
        issueTrackerSource = its;
      }
    });
    console.log(issueTrackerSource);

    let name = '';
    let startDate: Date  = new Date();
    let endDate: Date  = new Date();
    if (this.issueTrackerConnection !== undefined) {
      name = this.issueTrackerConnection.name;
      startDate = this.issueTrackerConnection.startDate;
      endDate = this.issueTrackerConnection.endDate;
    }

    const group: any = {
      issueTrackerSourceId: [issueTrackerSourceId, Validators.required],
      name: [name, [Validators.required, Validators.minLength(5)]],
      startDate: [formatDate(startDate, 'yyyy-MM-ddTHH:mm:ss', 'pt_BR', 'GMT-3'), Validators.required],
      endDate: [formatDate(endDate, 'yyyy-MM-ddTHH:mm:ss', 'pt_BR', 'GMT-3'), Validators.required]
    };

    issueTrackerSource.issueTrackerType.queryProperties.forEach(property => {
    let propertyValue = '';
    if (this.issueTrackerConnection !== undefined) {
      propertyValue = this.issueTrackerConnection.queryProperties[property.name] || '';
    }
    if (propertyValue === '' && property.defaultValue !== undefined) {
      propertyValue = property.defaultValue;
    }
    group[property.name] = property.required
      ? new FormControl(propertyValue, Validators.required)
      : new FormControl(propertyValue);
    });

    this.issueTrackerConnectionForm = this.fb.group(group);
    this.issueTrackerSourceSelected = issueTrackerSource;
  }

  create() {
    const queryProperties: Map<string, string> = new Map();
    this.issueTrackerSourceSelected.issueTrackerType.queryProperties.forEach(property => {
      queryProperties.set(property.name, this.issueTrackerConnectionForm.get(property.name).value);
    });
    this.issueTrackerConnectionService.createIssueTrackerConnection(
      this.study.id,
      this.issueTrackerConnectionForm.get('issueTrackerSourceId').value,
      this.issueTrackerConnectionForm.get('name').value,
      new Date(this.issueTrackerConnectionForm.get('startDate').value),
      new Date(this.issueTrackerConnectionForm.get('endDate').value),
      queryProperties
    ).subscribe(
      result => { this.router.navigate(['/study/detail/' + this.study.id]); }
    );
  }

  update() {
    const queryProperties: Map<string, string> = new Map();
    this.issueTrackerSourceSelected.issueTrackerType.queryProperties.forEach(property => {
      queryProperties.set(property.name, this.issueTrackerConnectionForm.get(property.name).value);
    });
    this.issueTrackerConnectionService.updateIssueTrackerConnection(
      this.issueTrackerConnection.id,
      this.study.id,
      this.issueTrackerConnectionForm.get('issueTrackerSourceId').value,
      this.issueTrackerConnectionForm.get('name').value,
      new Date(this.issueTrackerConnectionForm.get('startDate').value),
      new Date(this.issueTrackerConnectionForm.get('endDate').value),
      queryProperties
    ).subscribe(
      result => { this.router.navigate(['/study/detail/' + this.study.id]); }
    );
  }
}
