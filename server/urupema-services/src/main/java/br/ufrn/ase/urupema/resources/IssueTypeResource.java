/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.IssueType;
import br.ufrn.ase.urupema.process.IssueTypeProcess;


//@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class IssueTypeResource {
	@Autowired
	IssueTypeProcess issueTypeProcess;
	
	@GetMapping("/issuetype/list")
	public Collection<IssueType> findAll() {
		return issueTypeProcess.findAll();
	}
}
