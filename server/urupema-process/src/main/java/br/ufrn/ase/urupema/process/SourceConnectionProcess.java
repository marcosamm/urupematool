package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.naming.OperationNotSupportedException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.Source;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.Study;
import br.ufrn.ase.urupema.domain.SuspiciousFile;
import br.ufrn.ase.urupema.repository.SourceConnectionRepository;

@Component
@Transactional
public class SourceConnectionProcess {
	@Autowired
	private SourceConnectionRepository sourceConnectionRepository;
	
	@Autowired
	private GroupAggregatorTypeProcess groupAggregatorTypeProcess;
	
	public SourceConnection findById(Long id) throws IOException, InstantiationException, IllegalAccessException {
		SourceConnection sc = sourceConnectionRepository.findById(id.longValue()).get();
		if(sc != null) {
			sc.setGroupAggregatorType(groupAggregatorTypeProcess.findById(sc.getGroupAggregatorTypeId()));
		}
		return sc;
	}
	
	public void save(SourceConnection sourceConnection) {
		sourceConnectionRepository.save(sourceConnection);
	}
	
	public List<SourceConnection> findAll(){
		return sourceConnectionRepository.findAll();
	}
	
	public void delete(Long id) throws InstantiationException, IllegalAccessException, IOException {
		SourceConnection sc = findById(id);
		sc.getStudy().getSourceConnections().remove(sc);
		sourceConnectionRepository.delete(sc);
	}
	
	public Map<String, Map<Integer, Integer>> getDistrElementsByNumTraceElements (Long idSourceConnection) throws InstantiationException, IllegalAccessException, IOException{
		return getDistrElementsByInformation(idSourceConnection, "numTraceElements");
	}
	
	private Map<String, Map<Integer, Integer>> getDistrElementsByInformation (Long idSourceConnection, String infKey) throws InstantiationException, IllegalAccessException, IOException{
		//Map<URI, Map<qtdInfKey, qtdElementsOfGroup>>
		Map<String, Map<Integer, Integer>> urisMap = new HashMap<>();
		
		Set<Integer> list = new TreeSet<>();
		
		SourceConnection sc = findById(idSourceConnection);
		for(LogGroup lg : sc.getLogGroups()) {
			String label = lg.getLabel();
			if(lg.getSubGroups().size() > 0) {
				if(!urisMap.containsKey(label)) {
					urisMap.put(label, new TreeMap<>());
				}
				Map<Integer, Integer> uriStatsMap = urisMap.get(label);
				for(LogGroup subLg : lg.getSubGroups()) {
					String inf = subLg.getMoreInformation().get(infKey);
					if(inf != null) {
						Integer infI = Integer.parseInt(inf);
						list.add(infI);
						if(!uriStatsMap.containsKey(infI)) {
							uriStatsMap.put(infI, subLg.getElementsCount());
						}else {
							uriStatsMap.put(infI, uriStatsMap.get(infI) + subLg.getElementsCount());
						}
					}
				}
			}
		}
		
		for (Entry<String, Map<Integer, Integer>> entry : urisMap.entrySet()) {
			for(Integer infI : list) {
				if(!entry.getValue().containsKey(infI)) {
					entry.getValue().put(infI, 0);
				}
			}
		}
		
		return urisMap;
	}
	
	public void flush() {
		sourceConnectionRepository.flush();
	}
	
	public Study getStudy(Long idSourceConnection) throws InstantiationException, IllegalAccessException, IOException {
		SourceConnection sc = findById(idSourceConnection);
		sc.getStudy().getName();
		return sc.getStudy();
	}
	
	public Source getSource(Long idSourceConnection) throws InstantiationException, IllegalAccessException, IOException {
		SourceConnection sc = findById(idSourceConnection);
		sc.getSource().getSourceTypeId();
		return sc.getSource();
	}
	
	public List<SuspiciousFile> getSuspiciousFilesForAllLogGroups(Long idSourceConnection) throws IOException, InstantiationException, IllegalAccessException, OperationNotSupportedException {
		List<SuspiciousFile> suspiciousFiles = new ArrayList<>();
		
		SourceConnection sc = findById(idSourceConnection);
		for(LogGroup logGroup : sc.getLogGroups()) {
			if(!logGroup.getSuspiciousFiles().isEmpty()) {
				suspiciousFiles.addAll(logGroup.getSuspiciousFiles());
			}
		}
		
		return suspiciousFiles;
	}
}
