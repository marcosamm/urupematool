package br.ufrn.ase.urupema.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@SequenceGenerator(name = "ID_AFFECTEDURI_SEQUENCE", sequenceName = "affecteduri_id_sequence", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Getter
@Setter
@ToString(of={"id", "uri"})
@EqualsAndHashCode(of= {"reportedStackTrace", "uri"})
public class AffectedUri implements Serializable, Comparable<AffectedUri> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_AFFECTEDURI_SEQUENCE")
	private Long id;
	
	@Column(nullable = false)
	private String uri;
	
	@Column(nullable = false)
	private boolean enabled = true;
	
	@Column(columnDefinition="text")
	private String note;
	
	@JsonBackReference
	@ManyToOne(optional=false)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_affecteduri_reportedstacktrace_id"))
	private ReportedStackTrace reportedStackTrace;
	
	public AffectedUri() {
	}
	
	public AffectedUri(String uri, String note) {
		super();
		this.uri = uri;
		this.note = note;
	}

	@Override
	public int compareTo(AffectedUri arg0) {
		int ret = 0;
		if(reportedStackTrace != null && arg0.getReportedStackTrace() != null) {
			ret = reportedStackTrace.compareTo(arg0.getReportedStackTrace());
		} else if(reportedStackTrace != null) {
			ret = 1;
		} else if (arg0.getReportedStackTrace() != null){
			ret = -1;
		}
		if(ret == 0) {
			ret = uri.compareTo(arg0.getUri());
		}
		return ret;
	}
}
