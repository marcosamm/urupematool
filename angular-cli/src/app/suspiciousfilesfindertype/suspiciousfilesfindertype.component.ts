import { Component, OnInit } from '@angular/core';

import { SuspiciousFilesFinderTypeService } from '../service';
import { SuspiciousFilesFinderType } from '../model';

@Component({
  selector: 'app-suspiciousfilesfindertype',
  templateUrl: './suspiciousfilesfindertype.component.html',
  styleUrls: ['./suspiciousfilesfindertype.component.css']
})
export class SuspiciousFilesFinderTypeComponent implements OnInit {
  suspiciousFilesFinderTypes: SuspiciousFilesFinderType[];

  constructor(private suspiciousFilesFinderTypeService: SuspiciousFilesFinderTypeService) { }

  ngOnInit() {
    this.suspiciousFilesFinderTypeService.getSuspiciousFilesFinderTypes().subscribe(
      suspiciousFilesFinderTypes => this.suspiciousFilesFinderTypes = suspiciousFilesFinderTypes
    );
  }
}
