package br.ufrn.ase.urupema.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.Issue;
import br.ufrn.ase.urupema.domain.IssueTrackerConnection;

@Repository
public interface IssueRepository extends JpaRepository<Issue, Long>{
	public List<Issue> findByIssueTrackerConnection(IssueTrackerConnection issueTrackerConnection);
}
