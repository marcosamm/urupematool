import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Study } from '../model';
import { Observable } from 'rxjs';

@Injectable()
export class StudyService {
  urlGet = environment.apiUrl + '/study';

  constructor(private http: HttpClient) { }

  createStudy(name: string) {
    const study = {
      name: name
    };
    return this.http.post(this.urlGet, study);
  }

  updateStudy(id: number, name: string) {
    const study = {
      id: id,
      name: name
    };
    return this.http.put(this.urlGet, study);
  }

  getStudy(id: number): Observable<Study> {
    return this.http.get<Study>(this.urlGet + '?id=' + id);
  }

  getStudies(): Observable<Study[]> {
    return this.http.get<Study[]>(this.urlGet + '/list');
  }

  deleteStudy(study: Study) {
    return this.http.delete(this.urlGet + '?id=' + study.id);
  }

  getSourceConnectionSummary(study: Study) {
    return this.http.get<any[]>(this.urlGet + '/sourceConnectionSummary?id=' + study.id);
  }

  getGroupCorrelatorSummary(study: Study) {
    return this.http.get<any[]>(this.urlGet + '/groupCorrelatorSummary?id=' + study.id);
  }
}
