/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * Execution.java
 * 31 de out de 2017
 */
package br.ufrn.ase.urupema.perfminer.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString(doNotUseGetters=true)
public class Execution implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private Date date;

	@Column(name = "system_name")
	private String systemName;
	
	@Column(name = "system_version")
	private String systemVersion;
	
	@OneToMany(mappedBy = "execution", fetch = FetchType.LAZY)
	private List<Scenario> scenarios;
}
