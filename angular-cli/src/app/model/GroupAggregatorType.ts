import { LogType } from './LogType.enum';

export interface GroupAggregatorType {
    id: string;
    name: string;
    description: string;
    logTypes: LogType [];
}
