package br.ufrn.ase.urupema.extensible.suspiciousfilesfinder.repository;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.SuspiciousFilesFinderType;

@Repository
public class SuspiciousFilesFinderTypeRepository {
	
	Map<String, SuspiciousFilesFinderType> mapIssueTrackerTypes;
	
	public SuspiciousFilesFinderTypeRepository(@Value("${extensionPackages}") final String extensionPackages) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		mapIssueTrackerTypes = new TreeMap<>();
		
		ConfigurationBuilder builder = new ConfigurationBuilder();
		List<URL> urls = new ArrayList<>(ClasspathHelper.forPackage("br.ufrn.ase.urupema"));
		if(extensionPackages != null && !extensionPackages.isBlank()) {
			for(String pack : extensionPackages.split(",")) {
				if(!pack.isBlank()) {
					urls.addAll(ClasspathHelper.forPackage(pack.trim()));
				}
			}
		}
		builder.setUrls(urls);
		Reflections reflections = new Reflections(builder);
		Set<Class<? extends SuspiciousFilesFinderType>> classes = reflections.getSubTypesOf(SuspiciousFilesFinderType.class);
		for (Class<? extends SuspiciousFilesFinderType> class1 : classes) {
			if(!Modifier.isAbstract(class1.getModifiers()) && !class1.isInterface()) {
				SuspiciousFilesFinderType inst = class1.getConstructor().newInstance();
				if(!mapIssueTrackerTypes.containsKey(inst.getId())) {
					mapIssueTrackerTypes.put(inst.getId(), inst);
				} else {
					throw new InstantiationError("Duplicate ID found for SuspiciousFilesFinderType");
				}
			}
		}
	}
	
	public SuspiciousFilesFinderType findById(String id) throws InstantiationException, IllegalAccessException{
		return (SuspiciousFilesFinderType) mapIssueTrackerTypes.get(id);
	}
	
	public Collection<SuspiciousFilesFinderType> findAll() throws InstantiationException, IllegalAccessException{
		return mapIssueTrackerTypes.values();
	}
}
