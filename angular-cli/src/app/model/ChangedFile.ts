import { Issue } from './Issue';
import { Revision } from './Revision';
import { FileChangeType } from './FileChangeType.enum';

export interface ChangedFile {
    id: number;
    name: string;
    comment: string;
    enabled: boolean;
    note: string;
    fileChangeType: FileChangeType;
    revision: Revision;
}
