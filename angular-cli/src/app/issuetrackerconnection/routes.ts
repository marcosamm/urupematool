
import { Routes } from '@angular/router';
import { ListIssuesComponent } from './listissues/listissues.component';
import { IssueTrackerConnectionCreateComponent } from './create/create.component';
import { AffectedUrisComponent } from './affecteduris/affecteduris.component';
import { IssueDetailComponent } from './issuedetail/issuedetail.component';
import { IssueCreateComponent } from './issuecreate/issuecreate.component';
import { ReportedStackTraceCreateComponent } from './reportedstacktracecreate/reportedstacktracecreate.component';
import { ChangedFileCreateComponent } from './changedfilecreate/changedfilecreate.component';
import { RevisionCreateComponent } from './revisioncreate/revisioncreate.component';
import { AffectedUriCreateComponent } from './affecteduricreate/affecteduricreate.component';

/**
 * Define the routes
 */
export const routes: Routes = [
  { path: 'issuetrackerconnection/create/:idStudy', component: IssueTrackerConnectionCreateComponent },
  { path: 'issuetrackerconnection/create/:idStudy/:id', component: IssueTrackerConnectionCreateComponent },
  { path: 'issuetrackerconnection/listissues/:idStudy/:idIssueTrackerConnection', component: ListIssuesComponent },
  { path: 'issuetrackerconnection/affecteduris/:idIssueTrackerConnection', component: AffectedUrisComponent },
  { path: 'issuetrackerconnection/issuedetail/:idIssueTrackerConnection/:idIssue', component: IssueDetailComponent },
  { path: 'issuetrackerconnection/issuecreate/:idIssueTrackerConnection', component: IssueCreateComponent },
  { path: 'issuetrackerconnection/issuecreate/:idIssueTrackerConnection/:idIssue', component: IssueCreateComponent },
  { path: 'issuetrackerconnection/reportedstacktracecreate/:idIssue', component: ReportedStackTraceCreateComponent },
  { path: 'issuetrackerconnection/reportedstacktracecreate/:idIssue/:idReportedStackTrace', component: ReportedStackTraceCreateComponent },
  { path: 'issuetrackerconnection/affecteduricreate/:idIssue/:idReportedStackTrace', component: AffectedUriCreateComponent },
  { path: 'issuetrackerconnection/affecteduricreate/:idIssue/:idReportedStackTrace/:idAffectedUri', component: AffectedUriCreateComponent },
  { path: 'issuetrackerconnection/changedfilecreate/:idIssue/:idRevision', component: ChangedFileCreateComponent },
  { path: 'issuetrackerconnection/changedfilecreate/:idIssue/:idRevision/:idChangedFile', component: ChangedFileCreateComponent },
  { path: 'issuetrackerconnection/revisioncreate/:idIssue', component: RevisionCreateComponent },
  { path: 'issuetrackerconnection/revisioncreate/:idIssue/:idRevision', component: RevisionCreateComponent }
];
