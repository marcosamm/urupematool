
import {ModuleWithProviders } from '@angular/core';
import { Routes } from '@angular/router';

import { StudyDetailComponent } from './detail/detail.component';
import { StudyListComponent } from './list/list.component';
import { StudyCreateComponent } from './create/create.component';
import { SourceConnectionSummaryComponent } from './sourceconnectionsummary/summary.component';
import { GroupCorrelatorSummaryComponent } from './groupcorrelatorsummary/summary.component';
import { CalculateTopnComponent } from './calculatetopn/calculatetopn.component';

/**
 * Define the routes
 */
export const routes: Routes = [
    { path: 'study', component: StudyListComponent,
/*        children: [
            { path: 'listgrouprelationships/:idGroupCorrelator', component: ListGroupRelationshipsComponent},
        ]*/
    },
    { path: 'study/list', component: StudyListComponent},
    { path: 'study/detail/:id', component: StudyDetailComponent},
    { path: 'study/create', component: StudyCreateComponent},
    { path: 'study/create/:id', component: StudyCreateComponent},
    { path: 'study/sourceconnectionsummary/:id', component: SourceConnectionSummaryComponent},
    { path: 'study/groupcorrelatorsummary/:id', component: GroupCorrelatorSummaryComponent},
    { path: 'study/calculatetopn/:id', component: CalculateTopnComponent},
];
