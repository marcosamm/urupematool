package br.ufrn.ase.urupema.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

@Entity
@SequenceGenerator(name = "ID_SUSPICIOUSFILE_SEQUENCE", sequenceName = "suspiciousfile_id_sequence", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Getter
@Setter
public class SuspiciousFile implements Serializable, Comparable<SuspiciousFile> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_SUSPICIOUSFILE_SEQUENCE")
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private float inverseAverageDistanceToCrashPoint;
	
	@Column(nullable = false)
	private Double inverseBucketFrequency;
	
	@Column(nullable = false)
	private float fileFrequency;
	
	@JsonIgnore
	@ManyToOne(optional=false)
	private LogGroup logGroup;
	
	public SuspiciousFile() {
	}
	
	public SuspiciousFile(String name, float inverseAverageDistanceToCrashPoint, Double inverseBucketFrequency, float fileFrequency) {
		super();
		this.name = name;
		this.inverseAverageDistanceToCrashPoint = inverseAverageDistanceToCrashPoint;
		this.inverseBucketFrequency = inverseBucketFrequency;
		this.fileFrequency = fileFrequency;
	}
	
	public Double getScore() {
		return inverseAverageDistanceToCrashPoint*inverseBucketFrequency*fileFrequency;
	}

	@Override
	public int compareTo(SuspiciousFile arg0) {
		int ret = getScore().compareTo(arg0.getScore());
		if(ret == 0) {
			ret = name.compareTo(arg0.getName());
		}
		return ret;
	}

	@Override
	public String toString() {
		return "SuspiciousFile [id=" + id + ", name=" + name + ", getScore()=" + getScore() + "]";
	}
}
