/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioDTO.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.dtos;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import br.ufrn.ase.urupema.domain.LogType;
import lombok.Getter;
import lombok.Setter;

/**
 * DTO to export log information
 *
 */
@Getter
@Setter
public class LogDTO {
	String identifier;
	Date date;
	String requestURL;
	String requestURI;
	String trace;
	String exceptionMessage;
	LogType logType;
	Map<String, String> moreInformation = new TreeMap<>();
}
