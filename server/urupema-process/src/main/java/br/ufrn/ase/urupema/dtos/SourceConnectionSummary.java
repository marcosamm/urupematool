package br.ufrn.ase.urupema.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SourceConnectionSummary implements Comparable<SourceConnectionSummary>{
	Long idSourceConnection;
	int logsCount;
	int groupsCount;
	
	public SourceConnectionSummary(Long idSourceConnection, int logsCount, int groupsCount) {
		super();
		this.idSourceConnection = idSourceConnection;
		this.logsCount = logsCount;
		this.groupsCount = groupsCount;
	}

	@Override
	public int compareTo(SourceConnectionSummary o) {
		int ret = idSourceConnection.compareTo(o.getIdSourceConnection());
		if(ret == 0) {
			ret = logsCount - o.getLogsCount();
		}
		
		if(ret == 0) {
			ret = groupsCount - o.getGroupsCount();
		}
		return ret;
	}
}
