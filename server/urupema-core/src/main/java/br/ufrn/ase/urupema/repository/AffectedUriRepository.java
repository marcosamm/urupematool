package br.ufrn.ase.urupema.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.AffectedUri;
import br.ufrn.ase.urupema.domain.ReportedStackTrace;

@Repository
public interface AffectedUriRepository extends JpaRepository<AffectedUri, Long>{
	public List<AffectedUri> findByReportedStackTrace(ReportedStackTrace reportedStackTrace);
}
