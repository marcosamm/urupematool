package br.ufrn.ase.urupema.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.SourceConnection;

//@Repository
public interface LogGroupRepository extends JpaRepository<LogGroup, Long>, LogGroupRepositoryCustom{
	public List<LogGroup> findBySourceConnectionAndSuperGroupIsNull(SourceConnection sourceConnection);
	public List<LogGroup> findByLabel(String label);
}
