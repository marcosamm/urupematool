package br.ufrn.ase.urupema.domain;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@SequenceGenerator(name = "ID_STUDY_SEQUENCE", sequenceName = "study_id_sequence", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Getter
@Setter
@ToString(of={"id", "name"})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Study implements Serializable, Comparable<Study> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_STUDY_SEQUENCE")
	private Long id;
	
	@Column(nullable = false, unique=true)
	private String name;
	
	@OneToMany(mappedBy = "study", fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval = true)
	private Set<SourceConnection> sourceConnections = new TreeSet<>();
	
	@OneToMany(mappedBy = "study", fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval = true)
	private Set<GroupCorrelator> groupCorrelators = new TreeSet<>();
	
	@OneToMany(mappedBy = "study", fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval = true)
	private Set<IssueTrackerConnection> issueTrackerConnections = new TreeSet<>();

	@Override
	public int compareTo(Study o) {
		return name.compareTo(o.getName());
	}
}
