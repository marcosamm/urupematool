import { Property } from './';

export interface IssueTrackerType {
    id: string;
    name: string;
    connectionProperties: Property [];
    queryProperties: Property [];
}
