package br.ufrn.ase.urupema.domain;

import java.io.IOException;
import java.util.Set;

public interface SuspiciousFilesFinderType extends Comparable<SuspiciousFilesFinderType>{
	public String getId();
	public String getName();
	public String getDescription();
	public Set<SuspiciousFile> getSuspiciousFiles(LogGroup logGroup) throws IOException;
}
