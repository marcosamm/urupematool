import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import {TableModule} from 'primeng/table';

import { routes } from './routes';

import { StudyDetailComponent } from './detail/detail.component';
import { StudyListComponent } from './list/list.component';
import { StudyCreateComponent } from './create/create.component';
import { SourceConnectionSummaryComponent } from './sourceconnectionsummary/summary.component';
import { StudyService } from '../service';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
//import { library } from '@fortawesome/fontawesome-svg-core';
import { fas, faPlus, faPencilAlt, faSearch, faTrash, faChartBar, faProjectDiagram, faTable } from '@fortawesome/free-solid-svg-icons';
import { PipesModule } from '../pipes/pipes.module';
import { GroupCorrelatorSummaryComponent } from './groupcorrelatorsummary/summary.component';
import { CalculateTopnComponent } from './calculatetopn/calculatetopn.component';

//library.add(faPlus, faPencilAlt, faSearch, faTrash, faChartBar, faProjectDiagram, faTable);

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    FontAwesomeModule,
    TableModule,
    PipesModule
  ],
  declarations: [
    StudyDetailComponent,
    StudyListComponent,
    StudyCreateComponent,
    SourceConnectionSummaryComponent,
    GroupCorrelatorSummaryComponent,
    CalculateTopnComponent
  ],
  providers: [
    StudyService
  ]
})
export class StudyModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
  }
 }
