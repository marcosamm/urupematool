package br.ufrn.ase.urupema.extensible.aggregator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupAggregatorByUriAndSubtraceLeast3Frames extends GroupAggregatorByUriAndSubtrace implements GroupAggregatorTypePlugin {
	public static String ID = "urupema:uri_subst_least_3_frames";
	
	public GroupAggregatorByUriAndSubtraceLeast3Frames() {
		setMinFrames(3);
	}
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getName() {
		return "URI and SubTrace with at least 3 frames";
	}

	@Override
	public String getDescription() {
		return "Group by URI (superGroup) and subgroup (subGroups) by subset of stack trace with at least 3 frames";
	}
}
