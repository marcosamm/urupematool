/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioDTO.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.dtos;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogGroupSuspiciousReportDTO {
	String title;
	String description;
	String affectedURIs;
	String suspiciousFiles;
	List<String> stackTraceSamples;
	
	public LogGroupSuspiciousReportDTO(String title, String description, String affectedURIs, String suspiciousFiles,
			List<String> stackTraceSamples) {
		super();
		this.title = title;
		this.description = description;
		this.affectedURIs = affectedURIs;
		this.suspiciousFiles = suspiciousFiles;
		this.stackTraceSamples = stackTraceSamples;
	}
}
