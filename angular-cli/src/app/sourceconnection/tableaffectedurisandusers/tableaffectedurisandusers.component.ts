import { Component, OnInit, Input } from '@angular/core';
import { UriStats } from '../../model';


@Component({
  selector: 'app-tableaffectedurisandusers',
  templateUrl: './tableaffectedurisandusers.component.html',
  styleUrls: ['./tableaffectedurisandusers.component.css']
})
export class TableAffectedUrisAndUsersComponent implements OnInit {
  @Input() urisStats: UriStats[];

  constructor(
  ) { }

  ngOnInit() {
  }
}
