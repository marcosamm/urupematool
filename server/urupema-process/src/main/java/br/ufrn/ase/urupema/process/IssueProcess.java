package br.ufrn.ase.urupema.process;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.AffectedUri;
import br.ufrn.ase.urupema.domain.Issue;
import br.ufrn.ase.urupema.domain.IssueTrackerConnection;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.ReportedStackTrace;
import br.ufrn.ase.urupema.domain.Revision;
import br.ufrn.ase.urupema.repository.IssueRepository;

@Component
@Transactional
public class IssueProcess {
	@Autowired
	private IssueRepository repository;
	
	public Issue findById(Long id) {
		return repository.findById(id).get();
	}
	
	public List<Issue> findByIssueTrackerConnection(IssueTrackerConnection issueTrackerConnection){
		return repository.findByIssueTrackerConnection(issueTrackerConnection);
	}
	
	public void save(Issue issue) {
		repository.save(issue);
	}
	
	public void delete(Long id) {
		Issue i = findById(id);
		i.getIssueTrackerConnection().removeIssue(i);
		repository.delete(i);
	}
	
	public Set<ReportedStackTrace> getReportedStackTracesByIssue(Long id) {
		Issue i = findById(id);
		i.getReportedStackTraces().size();
		return i.getReportedStackTraces();
	}

	public Set<AffectedUri> getEnabledAffectedUrisByIssue(Long id) {
		Set<AffectedUri> affectedUris = new TreeSet<>();
		Issue i = findById(id);
		for(ReportedStackTrace reportedStackTrace : i.getEnabledReportedStackTraces(null)) {
			for(AffectedUri aUri : reportedStackTrace.getEnabledAffectedUris()) {
				affectedUris.add(aUri);
			}
		}
		return affectedUris;
	}
	
	public Set<Revision> getRevisionsByIssue(Long id) {
		Issue i = findById(id);
		i.getRevisions().size();
		return i.getRevisions();
	}
	
	public Set<LogGroup> getLogGroups(Long id) {
		Issue i = findById(id);
		i.getLogGroups().size();
		return i.getLogGroups();
	}
	
	public IssueTrackerConnection getIssueTrackerConnection(Long id) {
		Issue i = findById(id);
		i.getIssueTrackerConnection().getName();
		return i.getIssueTrackerConnection();
	}
}
