import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IssueTrackerSourceService, IssueTrackerTypeService } from '../../service';
import { IssueTrackerType, IssueTrackerSource } from '../../model';

@Component({
  selector: 'app-issuetrackersourcecreate',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class IssueTrackerSourceCreateComponent implements OnInit {
  form: FormGroup;
  issueTrackerSource: IssueTrackerSource;
  issueTrackerTypeSelected: IssueTrackerType;
  issueTrackerTypes: IssueTrackerType[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IssueTrackerSourceService,
    private issueTrackerTypeService: IssueTrackerTypeService,
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      issueTrackerTypeId: ['', Validators.required],
      name: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.issueTrackerTypeService.getIssueTrackerTypes().subscribe(issueTrackerTypes => this.issueTrackerTypes = issueTrackerTypes );
      if (params['id'] !== undefined) {
        this.service.getIssueTrackerSource(params['id']).subscribe(
          issueTrackerSource => {
            this.issueTrackerSource = issueTrackerSource;
            console.log(this.issueTrackerSource);
            this.fillForm(issueTrackerSource.issueTrackerTypeId);
          }
        );
      }
    });
  }

  fillForm(issueTrackerTypeId: string) {
    let issueTrackerType: IssueTrackerType;
    this.issueTrackerTypes.forEach(itt => {
      if (itt.id === issueTrackerTypeId) {
        issueTrackerType = itt;
      }
    });

    let name = '';
    if (this.issueTrackerSource !== undefined) {
      name = this.issueTrackerSource.name;
    }

    const group: any = {
      issueTrackerTypeId: [issueTrackerTypeId, Validators.required],
      name: [name, [Validators.required, Validators.minLength(5)]]
    };

    issueTrackerType.connectionProperties.forEach(property => {
      let propertyValue = '';
      if (this.issueTrackerSource !== undefined) {
        propertyValue = this.issueTrackerSource.connectionProperties[property.name] || '';
      }
      if (propertyValue === '' && property.defaultValue !== undefined) {
        propertyValue = property.defaultValue;
      }
      group[property.name] = property.required
        ? new FormControl(propertyValue, Validators.required)
        : new FormControl(propertyValue);
    });

    this.form = this.fb.group(group);
    this.issueTrackerTypeSelected = issueTrackerType;
  }

  create() {
    const connectionProperties: Map<string, string> = new Map();
    this.issueTrackerTypeSelected.connectionProperties.forEach(property => {
      connectionProperties.set(property.name, this.form.get(property.name).value);
    });
    this.service.createIssueTrackerSource(
      this.form.get('issueTrackerTypeId').value,
      this.form.get('name').value,
      connectionProperties
    ).subscribe(
      result => { this.router.navigate(['/issuetrackersource/list']); }
    );
  }

  update() {
    const connectionProperties: Map<string, string> = new Map();
    this.issueTrackerTypeSelected.connectionProperties.forEach(property => {
      connectionProperties.set(property.name, this.form.get(property.name).value);
    });
    this.service.updateIssueTrackerSource(
      this.issueTrackerSource.id,
      this.form.get('issueTrackerTypeId').value,
      this.form.get('name').value,
      connectionProperties
    ).subscribe(
      result => { this.router.navigate(['/issuetrackersource/list']); }
    );
  }
}
