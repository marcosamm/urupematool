package br.ufrn.ase.urupema.elasticsearch.parser;

import static javax.xml.bind.DatatypeConverter.parseDateTime;

import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import br.ufrn.ase.urupema.elasticsearch.domain.EsGenericLog;

public class EsGenericLogJsonParser {
	public static final String ID_FIELD_NAME = "_id";
	public static final String DATETIME_FIELD_NAME = "datetime";
	public static final String URL_FIELD_NAME = "url";
	public static final String USERLOGIN_FIELD_NAME = "user_login";
	
	public EsGenericLogJsonParser() {
	}
	
	public EsGenericLog parse (JsonNode node) {
		String id = node.get(ID_FIELD_NAME).asText();
		JsonNode src = node.get("_source");
		Date date = parseDateTime(src.get(DATETIME_FIELD_NAME).asText()).getTime();
		String url = src.get(URL_FIELD_NAME).asText();
		String userLogin = src.get(USERLOGIN_FIELD_NAME) != null ? src.get(USERLOGIN_FIELD_NAME).asText() : null;
		
		return new EsGenericLog(id, date, url, userLogin);
	}
	
	public EsGenericLog parse (Map<String, Object> node) {
		String id = node.get(ID_FIELD_NAME).toString();
		@SuppressWarnings("unchecked")
		Map<String, Object> src = (Map<String, Object>) node.get("_source"); 
		Date date = parseDateTime(src.get(DATETIME_FIELD_NAME).toString()).getTime();
		String url = src.get(URL_FIELD_NAME).toString();
		String userLogin = src.get(USERLOGIN_FIELD_NAME) !=null ? src.get(USERLOGIN_FIELD_NAME).toString() : null;
		
		return new EsGenericLog(id, date, url, userLogin);
	}
}