/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LogDistByNumeTraceElementsComponent } from './dist.component';

describe('LogDistByNumeTraceElementsComponent', () => {
  let component: LogDistByNumeTraceElementsComponent;
  let fixture: ComponentFixture<LogDistByNumeTraceElementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogDistByNumeTraceElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogDistByNumeTraceElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
