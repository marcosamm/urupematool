package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.GroupCorrelator;
import br.ufrn.ase.urupema.domain.GroupRelationship;
import br.ufrn.ase.urupema.repository.GroupRelationshipRepository;

@Component
@Transactional
public class GroupRelationshipProcess {
	@Autowired
	private GroupRelationshipRepository repository;
	
	public GroupRelationship findById(Long id) throws IOException {
		return repository.findById(id.longValue()).get();
	}
	
	public List<GroupRelationship> findByGroupCorrelator(GroupCorrelator groupCorrelator) throws IOException {
		return repository.findByGroupCorrelator(groupCorrelator);
	}
}
