import { SourceConnection } from './SourceConnection';
import { GroupCorrelator } from './GroupCorrelator';
import { IssueTrackerConnection } from './IssueTrackerConnection';

export interface Study {
    id: number;
    name: string;
    sourceConnections: SourceConnection [];
    groupCorrelators: GroupCorrelator [];
    issueTrackerConnections: IssueTrackerConnection [];
}
