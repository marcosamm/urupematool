/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * Node.java 
 * 30 de out de 2017
 */
package br.ufrn.ase.urupema.perfminer.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import br.ufrn.ase.urupema.domain.AbstractTraceElement;
import br.ufrn.ase.urupema.domain.TraceElement;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Node extends AbstractTraceElement implements TraceElement {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "member", columnDefinition = "text")
	private String memberSignature;

	@Column(name = "exception", columnDefinition = "text")
	private String exceptionMessage;

	@Column(name = "time")
	private long executionTime;

	@Column(name = "real_time")
	private long realExecutionTime;

	@Column(name = "constructor")
	private boolean isConstructor;

	//Não usamos annotation
//	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	@JoinTable(name = "node_annotation", joinColumns = @JoinColumn(name = "node_id"), inverseJoinColumns = @JoinColumn(name = "annotation_id"))
//	private Set<Annotation> annotations;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id", referencedColumnName = "id")
	private Node parent;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parent_id", referencedColumnName = "id")
	@OrderBy("id asc")
	private List<Node> children;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "node_scenario", joinColumns = @JoinColumn(name = "node_id"), inverseJoinColumns = @JoinColumn(name = "scenario_id"))
	@OrderBy("root asc")
	private List<Scenario> scenarios;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "node_id", referencedColumnName = "id")
	private List<Query> queries;

	@Override
	public String getSignature() {
		String signature = memberSignature;
		int index = memberSignature.indexOf('(');
		if(index > 0) {
			signature = memberSignature.substring(0, index);
		}
		return signature;
	}
	
	public String getExceptionIncludingChildren() {
		String exceptionMsg = "";
		if(exceptionMessage != null) {
			exceptionMsg = exceptionMessage;
		}else {
			for(Node child : children) {
				String msg = child.getExceptionIncludingChildren();
				if(msg != null && !msg.equals("")) {
					exceptionMsg = msg;
					break;
				}
			}
		}
		return exceptionMsg;
	}

	@Override
	public Integer getTopFrameDist() {
		// TODO Auto-generated method stub
		return null;
	}
}