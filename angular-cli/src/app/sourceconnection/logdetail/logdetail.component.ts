import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SourceConnectionService } from '../../service';
import { SourceConnection } from '../../model/SourceConnection';
import { Log } from '../../model/Log';
import { LogType } from '../../model/LogType.enum';


@Component({
  selector: 'app-logdetail',
  templateUrl: './logdetail.component.html',
  styleUrls: ['./logdetail.component.css']
})
export class LogDetailComponent implements OnInit {
  sourceConnection: SourceConnection;
  log: Log;
  sourceConnectionFindLogs: SourceConnection;

  constructor(
    private route: ActivatedRoute,
    private sourceConnectionService: SourceConnectionService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.sourceConnectionService.getSourceConnection(params['idSourceConnection']).subscribe(
        sourceConnection => {
          this.sourceConnection = sourceConnection;
          if (sourceConnection.logType === LogType.ACCESS) {
            this.sourceConnectionFindLogs = this.sourceConnection;
          } else {
            this.sourceConnectionService.getStudy(sourceConnection).subscribe(
              study => {
                for (const sc of study.sourceConnections) {
                  if (sc.logType === LogType.ACCESS) {
                    this.sourceConnectionFindLogs = sc;
                  }
                }
              }
            );
          }
          this.sourceConnectionService.getLog(sourceConnection.id, params['idLog']).subscribe(
            log => {
              this.log = log;
            }
          );
        }
      );
    });
  }
}
