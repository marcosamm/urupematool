/**
 * 
 */
package br.ufrn.ase.urupema.elasticsearch.repository;

import static javax.xml.bind.DatatypeConverter.parseDateTime;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import br.ufrn.ase.urupema.domain.TimeStats;
import br.ufrn.ase.urupema.domain.UriStats;
import br.ufrn.ase.urupema.domain.UserStats;
import br.ufrn.ase.urupema.elasticsearch.parser.EsGenericLogJsonParser;
import br.ufrn.ase.urupema.util.DateUtil;
import br.ufrn.ase.urupema.util.UrlUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractLogRepository {
	private static String AGG_BY_URIS_AND_USERS = "\"aggs\":{\"uris\":{\"terms\":{\"field\":\""+EsGenericLogJsonParser.URL_FIELD_NAME+".raw\",\"size\":65000,\"order\":{\"_count\":\"desc\"}}, \"aggs\":{\"first_date\":{\"min\":{\"field\":\""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\"}}, \"last_date\":{\"max\":{\"field\":\""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\"}}, \"users_count\": {\"cardinality\": {\"field\": \""+EsGenericLogJsonParser.USERLOGIN_FIELD_NAME+"\"}}, \"users\":{\"terms\":{\"field\":\""+EsGenericLogJsonParser.USERLOGIN_FIELD_NAME+"\",\"size\":5,\"order\":{\"_count\":\"desc\"}}}} } }";
	private static String AGG_BY_TIME_AND_URIS_AND_USERS = "\"aggs\":{\"logs_over_time\":{\"date_histogram\":{\"field\":\""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\",\"interval\":\"day\"}, "+AGG_BY_URIS_AND_USERS+"}}";
	
	/**
	 * To search in a specific index, the following convention is used:
	 * POST localhost:9200/index/_search
	 */
	public final static String TEMPLATE_URL_QUERY_ELASTICSEARCH = "%s/%s/" + "_search?pretty";
	/**
	 * To search in a specific index and type, the following convention is used:
	 * POST localhost:9200/index/type/_search
	 */
	public final static String TEMPLATE_URL_QUERY_ELASTICSEARCH_WITH_DOCUMENT_TYPE = "%s/%s/%s/" + "_search?pretty";
	
    String url;
    String username;
	String password;
	String indexName;
	String documentType;
	
	private String getUrlQuery() {
		String urlQuery = String.format(TEMPLATE_URL_QUERY_ELASTICSEARCH, this.url, this.indexName);
		
		if(this.documentType != null && !this.documentType.isEmpty()) {
			urlQuery = String.format(TEMPLATE_URL_QUERY_ELASTICSEARCH, this.url, this.indexName, this.documentType);
		}
		
		return urlQuery;
	}
	
	/**
	 * Curl is a computer software project providing a library and command-line tool for transferring data using various protocols
	 * @param query query
	 * @return the CURL command
	 */
	public String gerarateCurlCommmandElasticSearch(String urlQuery, String query) {
		StringBuilder curlCommand = new StringBuilder();
		curlCommand.append("\ncurl "); 
		curlCommand.append(" -H '").append(HttpHeaders.ACCEPT).append(": ").append(ContentType.APPLICATION_JSON.getMimeType()).append("' ");
		curlCommand.append(" -H '").append(HttpHeaders.CONTENT_TYPE).append(": ").append(ContentType.APPLICATION_JSON.getMimeType()).append("' ");
		if(username != null && !username.isEmpty()) {
			curlCommand.append(" -H '").append(HttpHeaders.AUTHORIZATION).append(": Basic ");
			curlCommand.append(Base64.encodeBase64String( (username + ":" + password).getBytes()));
			curlCommand.append("' ");
		}
		curlCommand.append(" -XGET '").append(urlQuery).append("' ");
		curlCommand.append(" -d'").append(query).append(" '");
		return curlCommand.toString();
	}
	
	protected String queryElasticSearch(String query) throws IOException{
		HttpClient httpClient = HttpClientBuilder.create().build();
		
		String urlRequisitadaElasticsearch = getUrlQuery();
		//String curl = gerarateCurlCommmandElasticSearch(urlRequisitadaElasticsearch, query);
		//System.out.println(curl);
		HttpPost httpPost = new HttpPost(urlRequisitadaElasticsearch);
		httpPost.setHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_JSON.getMimeType());
		httpPost.setHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
		
		if(username != null && !username.isEmpty()) {
			String encoding = Base64.encodeBase64String( (username + ":" + password).getBytes());
			httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + encoding);
		}
		
		StringEntity entityParametrosQueryJson = new StringEntity(query, Charsets.UTF_8);
		entityParametrosQueryJson.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType()));
		httpPost.setEntity(entityParametrosQueryJson);
		
		HttpResponse response = httpClient.execute(httpPost);
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("\nElasticsearch Error:"
					+ "\n HTTP de Error: " +	response.getStatusLine().getStatusCode()+" "+response.getStatusLine().getReasonPhrase()+" "
					+ "\n Details: \n" + response.getStatusLine().toString() );
		}
		return EntityUtils.toString(response.getEntity());
	}
	
	protected Set<TimeStats> aggregateByTime(JsonNode buckets) throws IOException {
		Set<TimeStats> counts = new TreeSet<>();
		if(buckets.isArray() && buckets.size() > 0){
			ArrayNode arrayTimesNode = (ArrayNode) buckets;
			Iterator<JsonNode> timeIterator = arrayTimesNode.elements();
			while( timeIterator.hasNext() ){
				JsonNode timeNode = timeIterator.next();

				LocalDate time = DateUtil.toLocalDate(parseDateTime(timeNode.get("key_as_string").asText()).getTime());
				Long timeCount = timeNode.get("doc_count").asLong();
				TimeStats timeStats = new TimeStats(time, timeCount, aggregateByUriAndUsers(timeNode.path("uris").path("buckets")));
				counts.add(timeStats);
			}
		}
		return counts;
	}
	
	protected Set<TimeStats> logsOverTime(Set<String> ids) throws IOException {
		String idsStrs = ids.stream().map(id -> "\"" + id.toString() + "\"").collect(Collectors.joining(", "));
		String query = "{\"size\":0,\"query\":{\"bool\":{\"must\":[{\"match_all\":{}},{\"ids\":{\"values\": ["+idsStrs+"]}}],\"must_not\":[]}},\"_source\":{\"excludes\":[]}, " +AGG_BY_TIME_AND_URIS_AND_USERS+ " }";
		String response = queryElasticSearch(query);
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"));
		JsonNode rootNode = mapper.readTree(response);
		
		JsonNode buckets = rootNode.path("aggregations").path("logs_over_time").path("buckets");

		return aggregateByTime(buckets);
	}
	
	protected Set<UriStats> aggregateByUriAndUsers(JsonNode buckets) throws IOException {
		Map<String, UriStats> counts = new LinkedHashMap<>();
		//TODO Verificar se é possível fazer a agregação por URI (no lugar de URL) diretamente no elasticsearch, evitando o laço abaixo 
		if(buckets.isArray() && buckets.size() > 0){
			ArrayNode arrayUrisNode = (ArrayNode) buckets;
			Iterator<JsonNode> uriIterator = arrayUrisNode.elements();
			while( uriIterator.hasNext() ){
				JsonNode uriNode = uriIterator.next();

				String url =  uriNode.get("key").asText();
				String uri = UrlUtil.getUri(url);
				Long uriCount = uriNode.get("doc_count").asLong();
				Long usersCount = uriNode.get("users_count").get("value").asLong();
				Date firstDate = parseDateTime(uriNode.get("first_date").get("value_as_string").asText()).getTime();
				Date lastDate = parseDateTime(uriNode.get("last_date").get("value_as_string").asText()).getTime();
				UriStats uriStats = new UriStats(uri, uriCount, usersCount, firstDate, lastDate);
				
				ArrayNode arrayUsersNode = (ArrayNode) uriNode.path("users").path("buckets");
				Iterator<JsonNode> userIterator = arrayUsersNode.elements();
				while( userIterator.hasNext() ){
					JsonNode userNode = userIterator.next();
					String identifier = userNode.get("key").asText();
					Long userCount = userNode.get("doc_count").asLong();
					UserStats userStats = new UserStats(identifier, userCount);
					uriStats.addUserStats(userStats);
					
				}
				
				UriStats exist = counts.get(uriStats.getUri());
				if(exist == null) {
					counts.put(uri, uriStats);
				} else {
					exist.join(uriStats);
				}
			}
		}
		return new TreeSet<UriStats>(counts.values());
	}
	
	protected Set<UriStats> getUrisCount(Date start, Date end) throws IOException {
		String query = "{\"size\":0,\"query\":{\"bool\":{\"must\":[{\"match_all\":{}},{\"range\":{\""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\":{\"gte\":"+start.getTime()+",\"lte\":"+end.getTime()+",\"format\":\"epoch_millis\"}}}],\"must_not\":[]}},\"_source\":{\"excludes\":[]}, "+AGG_BY_URIS_AND_USERS+" }";
		String response = queryElasticSearch(query);
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"));
		JsonNode rootNode = mapper.readTree(response);
		
		JsonNode shards = rootNode.path("_shards");
		if(shards.hasNonNull("failures")) {
			throw new IOException(rootNode.toString());
		}
		
		JsonNode buckets = rootNode.path("aggregations").path("uris").path("buckets");

		return aggregateByUriAndUsers(buckets);
	}
	
	protected Set<UriStats> getUrisCount(Set<String> ids) throws IOException {
		String idsStrs = ids.stream().map(id -> "\"" + id.toString() + "\"").collect(Collectors.joining(", "));
		String query = "{\"size\":0,\"query\":{\"bool\":{\"must\":[{\"match_all\":{}},{\"ids\":{\"values\": ["+idsStrs+"]}}],\"must_not\":[]}},\"_source\":{\"excludes\":[]}, "+AGG_BY_URIS_AND_USERS+" }";
		String response = queryElasticSearch(query);
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"));
		JsonNode rootNode = mapper.readTree(response);
		
		JsonNode buckets = rootNode.path("aggregations").path("uris").path("buckets");

		return aggregateByUriAndUsers(buckets);
	}
	
	protected List<String> findIds(String query) throws IOException {
		List<String> list = new ArrayList<>();
		
		String response = queryElasticSearch(query);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode = mapper.readTree(response);
		JsonNode hitsNode = rootNode.get("hits").get("hits"); 
		Iterator<JsonNode> i = hitsNode.elements();
		while(i.hasNext()) {
			list.add(i.next().get(EsGenericLogJsonParser.ID_FIELD_NAME).asText());
		}
		
		return list;
	}
	
	public List<String> findIdsByProperty(String propertyName, Object value) throws IOException {
		String query = "{\"size\": 10000, \"query\": {\"wildcard\" : {\""+propertyName+"\" : \""+value.toString()+"\"}}, \"_source\": [\""+EsGenericLogJsonParser.ID_FIELD_NAME+"\"]}";
		return findIds(query);
	}
}
