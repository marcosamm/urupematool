package br.ufrn.ase.urupema.domain;

import java.util.Date;
import java.util.Map;

public interface Log {
	public String getIdentifier();
	public String getRequestURL();
	public String getRequestURI();
	public Date getDate();
	public LogType getLogType();
	public Map<String, String> getMoreInformation();
}
