import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StudyService, GroupCorrelatorTypeService, GroupCorrelatorService } from '../../service';
import { Study, GroupCorrelatorType } from '../../model';
import { SourceConnection } from '../../model/SourceConnection';

@Component({
  selector: 'app-groupcorrelatorcreate',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class GroupCorrelatorCreateComponent implements OnInit {
  groupCorrelatorForm: FormGroup;
  id: number = undefined;
  study: Study = undefined;
  groupCorrelatorTypes: GroupCorrelatorType[] = [];
  groupCorrelatorTypesMap: Map<string, GroupCorrelatorType> = new Map();
  originSourceConnections: SourceConnection [] = [];
  destinySourceConnections: SourceConnection [] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private studyService: StudyService,
    private groupCorrelatorService: GroupCorrelatorService,
    private groupCorrelatorTypeService: GroupCorrelatorTypeService,
    private fb: FormBuilder
  ) {
    this.groupCorrelatorForm = this.fb.group({
      name: ['Error covered by manual perfminer', [Validators.required, Validators.minLength(5)]],
      groupCorrelatorTypeId: ['', [Validators.required]],
      sourceConnectionOriginId: ['', [Validators.required]],
      sourceConnectionDestinyId: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studyService.getStudy(params['idStudy']).subscribe(study => this.study = study);
      this.groupCorrelatorTypeService.getGroupCorrelatorTypes().subscribe(
        groupCorrelatorTypes => {
          this.groupCorrelatorTypes = groupCorrelatorTypes;
          for (const groupCorrelatorType of groupCorrelatorTypes) {
            this.groupCorrelatorTypesMap.set(groupCorrelatorType.id, groupCorrelatorType);
          }
        }
      );
      if (params['id'] !== undefined) {
        this.groupCorrelatorService.getGroupCorrelator(params['id']).subscribe(
          groupCorrelator => {
            this.id = groupCorrelator.id;
            this.groupCorrelatorForm.get('name').setValue(groupCorrelator.name);
            this.groupCorrelatorForm.get('groupCorrelatorTypeId').setValue(groupCorrelator.groupCorrelatorTypeId);
            this.groupCorrelatorForm.get('sourceConnectionOriginId').setValue(groupCorrelator.origin.id);
            this.groupCorrelatorForm.get('sourceConnectionDestinyId').setValue(groupCorrelator.destiny.id);

            this.fillSourceConnections(groupCorrelator.groupCorrelatorTypeId);
          }
        );
      }
    });
  }

  create() {
    this.groupCorrelatorService.createGroupCorrelator(
      this.study.id,
      this.groupCorrelatorForm.get('name').value,
      this.groupCorrelatorForm.get('groupCorrelatorTypeId').value,
      this.groupCorrelatorForm.get('sourceConnectionOriginId').value,
      this.groupCorrelatorForm.get('sourceConnectionDestinyId').value
    ).subscribe(
      result => { this.router.navigate(['/study/detail/' + this.study.id]); }
    );
  }

  update() {
    this.groupCorrelatorService.updateGroupCorrelator(
      this.id,
      this.study.id,
      this.groupCorrelatorForm.get('name').value,
      this.groupCorrelatorForm.get('groupCorrelatorTypeId').value,
      this.groupCorrelatorForm.get('sourceConnectionOriginId').value,
      this.groupCorrelatorForm.get('sourceConnectionDestinyId').value
    ).subscribe(
      result => { this.router.navigate(['/study/detail/' + this.study.id]); }
    );
  }

  fillSourceConnections(groupCorrelatorTypeId: string) {
    this.originSourceConnections = [];
    this.destinySourceConnections = [];
    const groupCorrelatorType = this.groupCorrelatorTypesMap.get(groupCorrelatorTypeId);
    this.study.sourceConnections.forEach(
      sourceConnection => {
        if (groupCorrelatorType.compatibleOriginAggregatorTypes.find(x => x.id === sourceConnection.groupAggregatorTypeId)) {
          this.originSourceConnections.push(sourceConnection);
        }
        if (groupCorrelatorType.compatibleDestinyAggregatorTypes.find(x => x.id === sourceConnection.groupAggregatorTypeId)) {
          this.destinySourceConnections.push(sourceConnection);
        }
      }
    );
  }
}
