import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { SourceConnectionService } from '../../service';
import { SourceConnection } from '../../model/SourceConnection';

@Component({
  selector: 'app-logsdistbynumtraceelements',
  templateUrl: './dist.component.html',
  styleUrls: ['./dist.component.css']
})
export class LogDistByNumeTraceElementsComponent implements OnInit {
  sourceConnection: SourceConnection;
  cols: any[];
  rows: any[] = [];
  rowTotal: any = {uri: 'TOTAL'};

  constructor(
    private route: ActivatedRoute,
    private sourceConnectionService: SourceConnectionService
  ) { }

  ngOnInit() {
    this.cols = [
      { field: 'uri', header: 'URI' }
    ];
    this.route.params.subscribe(params => {
      this.sourceConnectionService.getSourceConnection(params['idSourceConnection']).subscribe(
        sourceConnection => {
          this.sourceConnection = sourceConnection;

        }
      );
      this.findDist(params['idSourceConnection']);
    });
  }

  findDist(id: number) {
    this.sourceConnectionService.getLogDistByNumTraceElements(id).subscribe(
      dados => {
        Object.keys(dados).forEach(
          key => {
            const row = {uri: key};
            let sumLogsUri = 0;
            Object.keys(dados[key]).forEach(
              numTraceElements => {
                const nomField = numTraceElements + '_qtdElements';
                if (!this.cols.find(x => x.field === nomField)) {
                  this.cols.push({ field: nomField, header: numTraceElements });
                }
                row[nomField] = dados[key][numTraceElements];
                sumLogsUri = sumLogsUri + row[nomField];

                if (this.rowTotal[nomField] === undefined) {
                  this.rowTotal[nomField] = 0;
                }
                this.rowTotal[nomField] = this.rowTotal[nomField] + dados[key][numTraceElements];
              }
            );
            row['sum'] = sumLogsUri;
            this.rows.push(row);
          }
        );
        this.cols.push({ field: 'sum', header: 'Total' });
      }
    );
  }
}
