package br.ufrn.ase.urupema.extensible.aggregator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.parser.StackTraceParser;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class GroupAggregatorByEquivalentSignatureAndStacktrace extends GroupAggregatorByStacktrace implements GroupAggregatorTypePlugin {
	public static String ID = "urupema:st_eqv";
	
	private List<LogType> logTypes = Arrays.asList(LogType.ERROR);
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getName() {
		return "Equivalent Signatures and Stack Trace";
	}

	@Override
	public String getDescription() {
		return "Creates a set of logs grouped by equivalent signature and subsets of logs grouped by stack trace (subGroups)";
	}
	
	@Override
	public List<LogGroup> agroup(SourceConnection sourceConnection, Map<String, Object> params, Map<String, WebExecutionTrace> cache) throws IOException {
		List<LogGroup> superGroups = new ArrayList<>();
		
		//Pega os grupos de stacktrace
		List<LogGroup> stacktraceGroups = super.agroup(sourceConnection, params, cache);
		
		Map<String, LogGroup> mapEqvGroups = new HashMap<>();
		
		//Percorre todos os grupos de stacktrace
		for(LogGroup stacktraceGroup : stacktraceGroups) {
			String eqvStackTrace = null;
			try {
				eqvStackTrace = StackTraceParser.equivalentStackTrace(stacktraceGroup.getLabel());
			} catch (Exception e) {
				eqvStackTrace = stacktraceGroup.getLabel();
				//TODO Importante para pegar a stacktrace que provoca falha e criar teste para corrigir regex
				log.debug(eqvStackTrace);
			}
			
			if(mapEqvGroups.containsKey(eqvStackTrace)) {
				LogGroup superGroupDest = mapEqvGroups.get(eqvStackTrace);
				superGroupDest.addSubGroup(stacktraceGroup);
			} else {
				//Cria um supergrupo para stack trace equivalente, pois ele ainda não existe
				LogGroup superGroupDest = new LogGroup(eqvStackTrace);
				//Adiciona o grupo de equivalent stacktraces ao novo supergrupo 
				superGroupDest.addSubGroup(stacktraceGroup);
				superGroupDest.setTypeDescription("Equivalent Signature");
				superGroupDest.setGroupAggregatorTypeId(ID);
				superGroupDest.setFirstOccurrenceDate(stacktraceGroup.getFirstOccurrenceDate());
				superGroupDest.setLastOccurrenceDate(stacktraceGroup.getLastOccurrenceDate());
				//Adiciona o novo ao map
				mapEqvGroups.put(eqvStackTrace, superGroupDest);
				//Adiciona o novo grupo à lista de super grupos que será retornada
				superGroups.add(superGroupDest);
			}
		}
		
		int qtdSubs = 0;
		for(LogGroup superGroup : superGroups) {
			qtdSubs += superGroup.getSubGroups().size();
			superGroup.updateOccurenceDates();
			superGroup.updateElementsCount();
		}
		
		log.debug("Equivalente Signatures groups: " + superGroups.size()  + " - subGroups: " + qtdSubs);
		return superGroups;
	}

	@Override
	public int compareTo(GroupAggregatorType o) {
		return getId().compareTo(o.getId());
	}
}
