import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { ChangedFile, Revision, FileChangeType } from '../model';

@Injectable()
export class ChangedFileService {
  urlGet = environment.apiUrl + '/changedfile';

  constructor(private http: HttpClient) { }

  getChangedFile(id: number): Observable<ChangedFile> {
    return this.http.get<ChangedFile>(this.urlGet + '?id=' + id);
  }

  getChangedFilesByRevision(revision: Revision): Observable<ChangedFile[]> {
    return this.http.get<ChangedFile[]>(this.urlGet + '/listbyrevision?idRevision=' + revision.id);
  }

  createChangedFile(
    revisionId: number,
    name: string,
    note: string,
    enabled: boolean,
    fileChangeType: FileChangeType
  ) {
    const changedFile = {
      revision: { id: revisionId },
      name: name,
      note: note,
      enabled: enabled,
      fileChangeType: fileChangeType
    };
    return this.http.post(this.urlGet, changedFile);
  }

  updateChangedFile(
    id: number,
    revisionId: number,
    name: string,
    note: string,
    enabled: boolean,
    fileChangeType: FileChangeType
  ) {
    const changedFile = {
      id: id,
      revision: { id: revisionId },
      name: name,
      note: note,
      enabled: enabled,
      fileChangeType: fileChangeType
    };
    return this.http.put(this.urlGet, changedFile);
  }

  deleteChangedFile(changedFile: ChangedFile) {
    return this.http.delete(this.urlGet + '?id=' + changedFile.id);
  }
}

