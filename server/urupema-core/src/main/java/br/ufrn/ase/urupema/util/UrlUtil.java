package br.ufrn.ase.urupema.util;

public class UrlUtil {
	public static String getUri(String url) {
		int index1 = Math.max(0, url.indexOf("//"));
		int index2 = Math.max(0, url.indexOf("/", index1+2));
		int index3 = url.indexOf("?");
		if(index3 < 0) {
			index3 = url.length();
		}
		String uri = url.substring(index2, index3);
		return uri;
	}
}
