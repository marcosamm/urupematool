/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioRepository.java
 * 31 de out de 2017
 */
package br.ufrn.ase.urupema.repository;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.TimeStats;
import br.ufrn.ase.urupema.domain.UriStats;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;

/**
 * Represents a repository of web execution.
 *
 */
public interface WebExecutionRepository extends LogRepository{
	Set<TimeStats> logsOverTime(Set<String> ids) throws IOException;
	Optional<WebExecutionTrace> findById(String id) throws IOException;
	Set<UriStats> countGroupedByUri(Date start, Date end) throws IOException;
	Set<UriStats> countGroupedByUri(Set<String> ids) throws IOException;
	List<String> findIdsByDateBetween(Date start, Date end) throws IOException;
	List<String> findIdsByUriAndDateBetween(String uri, Date start, Date end) throws IOException;
	
	/**
	 * Retorna um agrupamento de logs com mesmas stack traces para uma determinada URI.
	 * @param uri A URI que se deseja agrupar os logs.
	 * @param start A data a partir da qual se quer procurar os logs
	 * @param end A data até a qual se quer obter os logs.
	 * @return Um grupo de logs agrupados por URI e subagrupados por stacktrace.
	 * @throws IOException IOException
	 */
	LogGroup agroupByUriSubgroupByStackTrace(String uri, Date start, Date end) throws IOException;
	
	/**
	 * Retorna agrupamentos de logs com mesmas stack traces.
	 * @param start A data a partir da qual se quer procurar os logs
	 * @param end A data até a qual se quer obter os logs.
	 * @return Logs agrupados por stacktrace.
	 * @throws IOException IOException
	 */
	List<LogGroup> agroupByStackTrace(Set<String> uris, Date start, Date end) throws IOException;
	
	/**
	 * Retorna agrupamentos de logs com mesmas stack traces, subagrupados por URIs.
	 * @param start A data a partir da qual se quer procurar os logs
	 * @param end A data até a qual se quer obter os logs.
	 * @return Logs agrupados por stacktrace.
	 * @throws IOException IOException
	 */
	List<LogGroup> agroupByStackTraceSubgroupByUri(Set<String> uris, Date start, Date end) throws IOException;
	
	/**
	 * Retorna URIs afetadas por uma stacktrace (ou stacktraces equivalentes a ela). 
	 * @param stackTrace A stacktraces cujas equivalentes devem procuradas.
	 * @param start A data a partir da qual se deseja procurara as stacktraces.
	 * @param end A data até a qual se deseja procurara as stacktraces.
	 * @return URIs dos logs de erros com stacktraces equivalentes à passada por parâmetro.
	 * @throws IOException
	 */
	Set<String> findAffectedUrisByEquivalentStackTracesAndDateBetween(String stackTrace, Date start, Date end, String uriFragment) throws IOException;
}
