/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LogCountComponent } from './logcount.component';

describe('LogcountComponent', () => {
  let component: LogCountComponent;
  let fixture: ComponentFixture<LogCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
