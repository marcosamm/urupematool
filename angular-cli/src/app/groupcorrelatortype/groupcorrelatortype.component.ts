import { Component, OnInit } from '@angular/core';

import { GroupCorrelatorTypeService } from '../service';
import { GroupCorrelatorType } from '../model';

@Component({
  selector: 'app-groupcorrelatortype',
  templateUrl: './groupcorrelatortype.component.html',
  styleUrls: ['./groupcorrelatortype.component.css']
})
export class GroupCorrelatorTypeComponent implements OnInit {
  groupCorrelatorTypes: GroupCorrelatorType[];

  constructor(private groupCorrelatorTypeService: GroupCorrelatorTypeService) { }

  ngOnInit() {
    this.groupCorrelatorTypeService.getGroupCorrelatorTypes().subscribe(
      groupCorrelatorTypes => this.groupCorrelatorTypes = groupCorrelatorTypes
    );
  }
}
