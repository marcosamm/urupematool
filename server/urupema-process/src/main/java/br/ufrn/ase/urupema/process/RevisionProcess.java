package br.ufrn.ase.urupema.process;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.Issue;
import br.ufrn.ase.urupema.domain.Revision;
import br.ufrn.ase.urupema.repository.RevisionRepository;

@Component
@Transactional
public class RevisionProcess {
	@Autowired
	private RevisionRepository repository;
	
	@Autowired
	private IssueProcess issueProcess;
	
	public Revision findById(Long id) {
		return repository.findById(id).get();
	}
	
	public void add(Revision revision, Long issueId) {
		Issue issue = issueProcess.findById(issueId);
		issue.addRevision(revision);
	}
	
	public void save(Revision revision) {
		repository.save(revision);
	}
	
	public void delete(Long idIssue, Long idRevision) {
		Revision r = findById(idRevision);
		for(Issue i : r.getIssues()) {
			if(i.getId().equals(idIssue)) {
				i.removeRevision(r);
			}
		}
		repository.save(r);
		if(r.getIssues().isEmpty()) {
			repository.delete(r);
		}
	}
}
