package br.ufrn.ase.urupema.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Property implements Serializable, Comparable<Property>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String description;
	private String defaultValue;
	private boolean required;
	private PropertyType type;
	
	public Property(String name, String description, String defaultValue, boolean required, PropertyType type) {
		super();
		this.name = name;
		this.description = description;
		this.defaultValue = defaultValue;
		this.required = required;
		this.type = type;
	}

	@Override
	public int compareTo(Property o) {
		return name.compareTo(o.getName());
	}
}
