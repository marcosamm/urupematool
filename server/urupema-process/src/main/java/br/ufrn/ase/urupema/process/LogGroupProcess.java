package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.naming.OperationNotSupportedException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.CalledMethodStats;
import br.ufrn.ase.urupema.domain.Issue;
import br.ufrn.ase.urupema.domain.LineNumberStats;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.SuspiciousFile;
import br.ufrn.ase.urupema.domain.SuspiciousFilesFinderType;
import br.ufrn.ase.urupema.domain.TimeStats;
import br.ufrn.ase.urupema.domain.UriStats;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.dtos.LogGroupSuspiciousReportDTO;
import br.ufrn.ase.urupema.extensible.aggregator.GroupAggregatorByCrashTypeSignatureAndEquivalentSignatureAndStacktrace;
import br.ufrn.ase.urupema.extensible.aggregator.GroupAggregatorByEquivalentSignatureAndStacktrace;
import br.ufrn.ase.urupema.extensible.aggregator.GroupAggregatorByStacktrace;
import br.ufrn.ase.urupema.extensible.suspiciousfilesfinder.repository.SuspiciousFilesFinderTypeRepository;
import br.ufrn.ase.urupema.parser.StackTraceParser;
import br.ufrn.ase.urupema.repository.ErrorLogRepository;
import br.ufrn.ase.urupema.repository.LogGroupRepository;
import br.ufrn.ase.urupema.repository.factory.LogRepositoryFactory;
import br.ufrn.ase.urupema.util.DateUtil;

@Component
@Transactional
public class LogGroupProcess {
	@Autowired
	private LogGroupRepository repository;
	
	@Autowired
	private LogRepositoryFactory logRepositoryFactory;
	
	@Autowired
	private SuspiciousFilesFinderTypeRepository suspiciousFilesFinderTypeRepository;
	
	public LogGroup findById(Long id) throws IOException {
		return repository.findById(id.longValue()).get();
	}
	
	public List<LogGroup> findBySourceConnection(SourceConnection sourceConnection) throws IOException {
		return repository.findBySourceConnectionAndSuperGroupIsNull(sourceConnection);
	}
	
	public Set<LogGroup> findSubGroupsByLogGroupId(Long id) throws IOException {
		LogGroup lg = findById(id);
		lg.getSubGroups().size();
		return lg.getSubGroups();
	}
	
	public Set<SuspiciousFile> findSuspiciousFilesByLogGroupId(Long id) throws IOException, InstantiationException, IllegalAccessException, OperationNotSupportedException {
		LogGroup logGroup = findById(id);
		if(logGroup.getSuspiciousFiles().isEmpty()) {
			SuspiciousFilesFinderType suspiciousFilesFinderType = suspiciousFilesFinderTypeRepository.findById(logGroup.getSourceConnection().getSuspiciousFilesFinderTypeId());
			logGroup.addSuspiciousFiles(suspiciousFilesFinderType.getSuspiciousFiles(logGroup));
			repository.save(logGroup);
		}
		return logGroup.getSuspiciousFiles();
	}

	public Set<String> findElementsIdByLogGroupId(Long id) throws IOException{
		LogGroup lg = findById(id);
		return lg.getElementsId();
	}
	
	public Set<UriStats> findAffectedUrisById(Long id) throws IOException, OperationNotSupportedException {
		LogGroup lg = findById(id);
		
		ErrorLogRepository lr = logRepositoryFactory.getErrorLogRepository(lg.getSourceConnection().getSource());
		Set<UriStats> uriStats = lr.countGroupedByUri(lg.getElementsId());
		
		return uriStats;
	}
	
	public Set<CalledMethodStats> findCalledFileMethods(Long id, String fileName) throws IOException, OperationNotSupportedException {
		Map<String, CalledMethodStats> mapMethodWithoutLine = new HashMap<>(); 
		
		List<LogGroup> gps = null;
		LogGroup lg = findById(id);
		if(lg.getGroupAggregatorTypeId().equals(GroupAggregatorByEquivalentSignatureAndStacktrace.ID) || lg.getGroupAggregatorTypeId().equals(GroupAggregatorByStacktrace.ID)) {
			gps = new ArrayList<>();
			gps.add(lg);
		}else {
			gps = lg.getSubGroupsWithGroupAggregatorTypeId(GroupAggregatorByEquivalentSignatureAndStacktrace.ID);
			
			if(gps.isEmpty()) {
				gps = lg.getSubGroupsWithGroupAggregatorTypeId(GroupAggregatorByStacktrace.ID);
			}
		}
		
		String className = fileName.replace(".java", "").replaceAll("/", ".");
		for(LogGroup g : gps) {
			String stackTrace = g.getLabel();
			
			int startIndex = stackTrace.indexOf(className);
			while(startIndex > 0){
				int endIndex = stackTrace.indexOf(")", startIndex + className.length())+1;
				int breakIndex = stackTrace.indexOf("\n", startIndex + className.length())+1;
				if(endIndex < breakIndex) {
					String methodWithFileAndLine = stackTrace.substring(startIndex, endIndex);
					String fileNameAndLine = methodWithFileAndLine.substring(methodWithFileAndLine.indexOf("(")+1, methodWithFileAndLine.indexOf(")"));
					String file = fileNameAndLine;
					Integer line = null;
					if(file.contains(":")) {
						file = file.substring(0, file.indexOf(":"));
						line = Integer.parseInt(fileNameAndLine.substring(fileNameAndLine.indexOf(":")+1));
					}
					LineNumberStats lineNumberStats = new LineNumberStats(file, line, 1l*g.getElementsCount());
					
					String methodWithoutFileAndLine = methodWithFileAndLine.substring(0, methodWithFileAndLine.indexOf("("));
					CalledMethodStats calledMethodStats = mapMethodWithoutLine.get(methodWithoutFileAndLine);
					if(calledMethodStats != null) {
						calledMethodStats.addLineNumberStats(lineNumberStats);
					} else {
						calledMethodStats = new CalledMethodStats(methodWithoutFileAndLine, 0l);
						calledMethodStats.addLineNumberStats(lineNumberStats);
						mapMethodWithoutLine.put(methodWithoutFileAndLine, calledMethodStats);
					}
				}
				startIndex = stackTrace.indexOf(className, endIndex);
			}
		}
		
		return new TreeSet<>(mapMethodWithoutLine.values());
	}
	
	public Set<TimeStats> findLogsOverTime(Long id) throws IOException, OperationNotSupportedException {
		LogGroup lg = findById(id);
		ErrorLogRepository lr = logRepositoryFactory.getErrorLogRepository(lg.getSourceConnection().getSource());
		return lr.logsOverTime(lg.getElementsId());
	}
	
	public List<LogGroup> findBySourceConnectionAndLabel(SourceConnection sourceConnection, String label) throws IOException {
		List<LogGroup> logGroups = new ArrayList<>();
		List<LogGroup> lgs = repository.findByLabel(label);
		for(LogGroup lg : lgs) {
			LogGroup superGroup = lg;
			while(superGroup.getSuperGroup() != null) {
				superGroup = superGroup.getSuperGroup();
			}
			if(superGroup.getSourceConnection().getId().equals(sourceConnection.getId())) {
				logGroups.add(lg);
			}
		}
		return logGroups;
	}
	
	public List<LogGroup> findBySourceConnectionAndStacktrace(SourceConnection sourceConnection, String stackTrace) throws IOException {
		List<LogGroup> logGroups = new ArrayList<>();
		
		String stLike = stackTrace.replace("%", "\\%");
		stLike = stLike.replaceAll("\\s{1,}", "%");// remove newlines, tabs, and spaces
		stLike = stLike.replace("000", "%"); // remove proxy, method, EnhancerByCGLIB and FastClassByCGLIB numbers
		stLike = stLike.replace("_", "\\_"); 
		List<LogGroup> lgs = repository.findByLabelLikeMultipleStrings("%"+stLike+"%");
		for(LogGroup lg : lgs) {
			LogGroup superGroup = lg;
			while(superGroup.getSuperGroup() != null) {
				superGroup = superGroup.getSuperGroup();
			}
			if(superGroup.getSourceConnection().getId().equals(sourceConnection.getId())) {
				logGroups.add(lg);
			}
		}
		return logGroups;
	}
	
	public List<LogGroup> findBySourceConnectionAndEquivalentStacktraceWithoutCauseByMessagesAndGeneratedNumbers(SourceConnection sourceConnection, String stackTrace) throws IOException {
		String st = StackTraceParser.stackTraceWithoutMessages(stackTrace);
		st = StackTraceParser.equivalentStackTrace(st);
		return findBySourceConnectionAndStacktrace(sourceConnection, st);
	}
	
	public Map<String, Float> listMapFCSFs(Long id) throws IOException {
		LogGroup lg = findById(id);
		lg.getMapFcsf().size();
		return lg.getMapFcsf();
	}
	
	public SourceConnection findSourceConnectionByLogGroup(Long id) throws IOException {
		LogGroup lg = findById(id);
		lg.getSourceConnection().getName();
		return lg.getSourceConnection();
	}
	
	public LogGroupSuspiciousReportDTO generateSummary(Long id) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		final String LINE_BREAK = "\n";
		LogGroup lg = findById(id);
		
		String title = "Issue relacionada ao grupo de logs " + lg.getId();
		StringBuilder description = new StringBuilder();
		description.append("--------------------------------------------").append(LINE_BREAK);
		description.append("Descrição:").append(LINE_BREAK);
		description.append("--------------------------------------------").append(LINE_BREAK);
		description.append(
				String.format("Logs analisados entre os dias %s e %s", 
					DateUtil.toBrDateString(lg.getSourceConnection().getStartDate()),
					DateUtil.toBrDateString(lg.getSourceConnection().getEndDate())
				)
		).append(LINE_BREAK);
		description.append(
				String.format("Forma de agrupamento: %s", lg.getTypeDescription())
		).append(LINE_BREAK);
		description.append(
				String.format("Número de logs agrupados: %d", lg.getElementsCount())
		).append(LINE_BREAK);
		description.append(
				String.format("Número de subgrupos: %d", lg.getSubGroupsCount())
		).append(LINE_BREAK);
		description.append(
				String.format("Data de ocorrência do primeiro log: %s", 
					DateUtil.toBrDateString(lg.getFirstOccurrenceDate())
				)
		).append(LINE_BREAK);
		description.append(
				String.format("Data de ocorrência do último log: %s", 
					DateUtil.toBrDateString(lg.getLastOccurrenceDate())
				)
		).append(LINE_BREAK);
		
		StringBuilder affectedURIs = new StringBuilder();
		affectedURIs.append("--------------------------------------------").append(LINE_BREAK);
		affectedURIs.append("URIs afetadas:").append(LINE_BREAK);
		affectedURIs.append("--------------------------------------------").append(LINE_BREAK);
		affectedURIs.append("URIS relacionadas aos logs do grupo: ").append(LINE_BREAK);
		Set<UriStats> urisStats = findAffectedUrisById(id);
		for (UriStats uriStats : urisStats) {
			affectedURIs.append(
					String.format("  * URI: %s", uriStats.getUri())
			).append(LINE_BREAK);
			affectedURIs.append(
					String.format("    - Número de logs: %d", uriStats.getCount())
			).append(LINE_BREAK);
			affectedURIs.append(
					String.format("    - Número de usuários afetados: %d", uriStats.getUsersCount())
			).append(LINE_BREAK);
			affectedURIs.append(
					"    - Logins mais afetados e número de ocorrências: "
			).append(LINE_BREAK);
			uriStats.getUsersStats().stream().limit(5).forEach(
					user -> {
						affectedURIs.append(
								String.format("      > %s: %d", user.getIdentifier(), user.getCount())
						).append(LINE_BREAK);
					}
			);
		}
		
		StringBuilder suspiciousFiles = new StringBuilder();
		suspiciousFiles.append("--------------------------------------------").append(LINE_BREAK);
		suspiciousFiles.append("Arquivos e métodos suspeitos:").append(LINE_BREAK);
		suspiciousFiles.append("--------------------------------------------").append(LINE_BREAK);
		suspiciousFiles.append("Arquivos suspeitos de conterem o bug: ").append(LINE_BREAK);
		Set<SuspiciousFile> suspFiles = findSuspiciousFilesByLogGroupId(id);
		List<SuspiciousFile> filtredSuspiciousFiles = suspFiles.stream().filter(suspFile -> suspFile.getName().contains(lg.getSourceConnection().getSuspiciousFilesFilter())).collect(Collectors.toList());
		//suspiciousFiles.append(
		//		String.format("Total de suspeitos: %d", filtredSuspiciousFiles.size())
		//).append(LINE_BREAK);
		//suspiciousFiles.append("Arquivos mais suspeitos: ").append(LINE_BREAK);
		List<SuspiciousFile> topFive = filtredSuspiciousFiles.stream().sorted(Comparator.reverseOrder()).limit(5).collect(Collectors.toList());
		for (SuspiciousFile suspiciousFile : topFive) {
			suspiciousFiles.append(
					//String.format("  * %s (score = %.3f):", suspiciousFile.getName(), suspiciousFile.getScore())
					String.format("  * %s:", suspiciousFile.getName(), suspiciousFile.getScore())
			).append(LINE_BREAK);
			
			Set<CalledMethodStats> calledMethods = findCalledFileMethods(id, suspiciousFile.getName());
			//suspiciousFiles.append(
			//		String.format("    - Número de métodos presentes nas stack traces do grupo de logs: %d", calledMethods.size())
			//).append(LINE_BREAK);
			//suspiciousFiles.append("    - Métodos mais presentes nas stack traces do grupo de logs: ").append(LINE_BREAK);
			suspiciousFiles.append("    - Métodos suspeitos de conterem o bug: ").append(LINE_BREAK);
			calledMethods.stream().forEach(
					calledMethod -> {
						suspiciousFiles.append(
								String.format("      > %s: chamado %d vezes", calledMethod.getQualifiedMethodName(), calledMethod.getCount())
						).append(LINE_BREAK);
						/*suspiciousFiles.append(
								String.format("        - Número de linhas distintas presentes no grupo de logs: %d", calledMethod.getLineNumbersStats().size())
						).append(LINE_BREAK);
						suspiciousFiles.append("        - Linhas mais presentes nas stack traces do grupo de logs: ").append(LINE_BREAK);
						calledMethod.getLineNumbersStats().stream().limit(5).forEach(
								lineNumberStats -> {
									suspiciousFiles.append(
											String.format("          + %s: %d", lineNumberStats.getLineNumber(), lineNumberStats.getCount())
									).append(LINE_BREAK);
								}
						);*/
					}
			);
		}
		
		List<String> stackTraceSamples = new ArrayList<>();
		List<LogGroup> equivSubGroups = null;
		if(lg.getGroupAggregatorTypeId().equals(GroupAggregatorByCrashTypeSignatureAndEquivalentSignatureAndStacktrace.ID) || lg.getGroupAggregatorTypeId().equals(GroupAggregatorByEquivalentSignatureAndStacktrace.ID) || lg.getGroupAggregatorTypeId().equals(GroupAggregatorByStacktrace.ID)) {
			equivSubGroups = new ArrayList<>();
			equivSubGroups.add(lg);
		}else {
			equivSubGroups = lg.getSubGroupsWithGroupAggregatorTypeId(GroupAggregatorByCrashTypeSignatureAndEquivalentSignatureAndStacktrace.ID);
			
			if(equivSubGroups.isEmpty()) {
				equivSubGroups = lg.getSubGroupsWithGroupAggregatorTypeId(GroupAggregatorByEquivalentSignatureAndStacktrace.ID);
				
				if(equivSubGroups.isEmpty()) {
					equivSubGroups = lg.getSubGroupsWithGroupAggregatorTypeId(GroupAggregatorByStacktrace.ID);
				}
			}
		}
		
		for (LogGroup equivSubGroup : equivSubGroups) {
			StringBuilder stSample = new StringBuilder();
			stSample.append("  * Alguns logs com stack trace semelhante: ").append(LINE_BREAK);
			equivSubGroup.getElementsId().stream().limit(5).forEach(
					element -> {
						WebExecutionTrace wet = null;
						try {
							wet = logRepositoryFactory.getErrorLogRepository(lg.getSourceConnection().getSource()).findById(element).get();
						} catch (OperationNotSupportedException | IOException e) {
							e.printStackTrace();
						}
						String idRegistroEntrada = "";
						if (wet != null) {
							idRegistroEntrada = wet.getMoreInformation().get("id_registro_entrada") != null ? wet.getMoreInformation().get("id_registro_entrada") : "";
						}
						stSample.append(
								String.format("    - %s (id_registro_entrada: %s)", element, idRegistroEntrada)
						).append(LINE_BREAK);
					}
			);
			stSample.append(LINE_BREAK);
			stSample.append("  * Stack trace: ").append(LINE_BREAK);
			stSample.append(equivSubGroup.getLabel()).append(LINE_BREAK);
			stackTraceSamples.add(stSample.toString());
		}
		
		LogGroupSuspiciousReportDTO lgsr = new LogGroupSuspiciousReportDTO(title, description.toString(), affectedURIs.toString(), suspiciousFiles.toString(), stackTraceSamples.stream().limit(5).collect(Collectors.toList()));
		return lgsr;
	}
	
	public Set<Issue> findIssuesByLogGroup(Long id) throws IOException {
		LogGroup lg = findById(id);
		lg.getIssues().size();//Forcando inicializacao para evitar erro de lazy
		return lg.getIssues();
	}
}
