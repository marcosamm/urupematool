import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import {TableModule} from 'primeng/table';
import {CalendarModule} from 'primeng/calendar';
import {ChartModule} from 'primeng/chart';
import {TreeModule} from 'primeng/tree';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
//import { library } from '@fortawesome/fontawesome-svg-core';
import { fas, faSearch, faBoxes, faChartBar, faBoxOpen, faBug } from '@fortawesome/free-solid-svg-icons';

//library.add(faSearch, faBoxes, faChartBar, faBoxOpen, faBug);

import { routes } from './routes';

import { SourceConnectionCreateComponent } from './create/create.component';
import { SourceConnectionService } from '../service';
import { SourceConnectionDetailComponent } from './detail/detail.component';
import { LogCountComponent } from './logcount/logcount.component';
import { ListLogGroupsComponent } from './listloggroups/listloggroups.component';
import { LogGroupComponent } from './loggroup/loggroup.component';
import { LogDetailComponent } from './logdetail/logdetail.component';
import { LogDistByNumeTraceElementsComponent } from './logdistbynumtraceelements/dist.component';
import { PipesModule } from '../pipes/pipes.module';
import { FindLogsComponent } from './findlogs/findlogs.component';
import { SuspiciousFilesComponent } from './suspiciousfiles/suspiciousfiles.component';
import { LogGroupAffectedUrisAndUsersComponent } from './loggroupaffectedurisandusers/loggroupaffectedurisandusers.component';
import { LogsOverTimeComponent } from './logsovertime/logsovertime.component';
import { FCSFsLogGroupComponent } from './fcsfslogroup/fcsfsloggroup.component';
import { CommonFCSFsComponent } from './commonfcsfs/commonfcsfs.component';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { AccordionModule } from 'primeng/accordion';
import { TableAffectedUrisAndUsersComponent } from './tableaffectedurisandusers/tableaffectedurisandusers.component';
import { SuspiciousFilesAllGroupsComponent } from './suspiciousfilesallgroups/suspiciousfilesallgroups.component';
import { LogGroupSuspiciousReportComponent } from './loggroupsuspiciousreport/loggroupsuspiciousreport.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    FontAwesomeModule,
    TableModule,
    CalendarModule,
    ChartModule,
    PipesModule,
    OverlayPanelModule,
    AccordionModule,
    TreeModule
  ],
  declarations: [
    SourceConnectionDetailComponent,
    LogCountComponent,
    ListLogGroupsComponent,
    LogGroupComponent,
    LogDetailComponent,
    SourceConnectionCreateComponent,
    LogDistByNumeTraceElementsComponent,
    FindLogsComponent,
    SuspiciousFilesComponent,
    TableAffectedUrisAndUsersComponent,
    LogGroupAffectedUrisAndUsersComponent,
    LogsOverTimeComponent,
    FCSFsLogGroupComponent,
    CommonFCSFsComponent,
    SuspiciousFilesAllGroupsComponent,
    LogGroupSuspiciousReportComponent
  ],
  providers: [
    SourceConnectionService
  ]
})
export class SourceConnectionModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
  }
}
