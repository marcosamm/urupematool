import { Issue } from './Issue';
import { ChangedFile } from './ChangedFile';

export interface Revision {
    id: number;
    identifier: string;
    comment: string;
    date: Date;
    issues: Issue [];
    changedFiles: ChangedFile [];
}
