import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Revision, Issue } from '../model';

@Injectable()
export class RevisionService {
  urlGet = environment.apiUrl + '/revision';

  constructor(private http: HttpClient) { }

  getRevision(id: number): Observable<Revision> {
    return this.http.get<Revision>(this.urlGet + '?id=' + id);
  }

  createRevision(
    issueId: number,
    identifier: string,
    date: Date,
    comment: string
  ) {
    const revision = {
      identifier: identifier,
      date: date,
      comment: comment
    };
    return this.http.post(this.urlGet + '/' + issueId, revision);
  }

  updateRevision(
    id: number,
    identifier: string,
    date: Date,
    comment: string
  ) {
    const revision = {
      id: id,
      identifier: identifier,
      date: date,
      comment: comment
    };
    return this.http.put(this.urlGet, revision);
  }

  deleteRevision(issue: Issue, revision: Revision) {
    return this.http.delete(this.urlGet + '?idIssue=' + issue.id + '&idRevision=' + revision.id);
  }
}

