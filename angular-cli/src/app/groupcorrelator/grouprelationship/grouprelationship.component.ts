import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GroupCorrelatorService, GroupRelationshipService } from '../../service';
import { GroupRelationship } from '../../model/GroupRelationship';

@Component({
  selector: 'app-grouprelationship',
  templateUrl: './grouprelationship.component.html',
  styleUrls: ['./grouprelationship.component.css']
})
export class GroupRelationshipComponent implements OnInit {
  groupRelationship: GroupRelationship;

  constructor(
    private route: ActivatedRoute,
    private groupCorrelatorService: GroupCorrelatorService,
    private groupRelationshipService: GroupRelationshipService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.groupRelationshipService.getGroupRelationship(params['id']).subscribe(
        dados => {
          this.groupRelationship = dados;
          this.groupCorrelatorService.getGroupCorrelator(params['idGroupCorrelator']).subscribe(
            groupCorrelator => this.groupRelationship.groupCorrelator = groupCorrelator
          );
        }
      );
    });
  }
}
