package br.ufrn.ase.urupema.repository;

import java.util.List;

import br.ufrn.ase.urupema.domain.LogGroup;

public interface LogGroupRepositoryCustom {
	public List<LogGroup> findByLabelLikeMultipleStrings(String label);
}
