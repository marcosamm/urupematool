/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.IssueTrackerSource;
import br.ufrn.ase.urupema.process.IssueTrackerSourceProcess;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class IssueTrackerSourceResource {
	@Autowired
	IssueTrackerSourceProcess issueTrackerSourceProcess;
	
	@PostMapping("/issuetrackersource")
	public void put(@RequestBody IssueTrackerSource issueTrackerSource) throws IOException, InstantiationException, IllegalAccessException {
		log.debug("IssueTrackerSource :" + issueTrackerSource);
		issueTrackerSourceProcess.save(issueTrackerSource);
	}
	
	@GetMapping("/issuetrackersource/list")
	public List<IssueTrackerSource> findAll() throws IOException, InstantiationException, IllegalAccessException {
		List<IssueTrackerSource> issueTrackerSources = issueTrackerSourceProcess.findAll();
		for(IssueTrackerSource issueTrackerSource : issueTrackerSources) {
			if(issueTrackerSource.getConnectionProperties().containsKey("key")) {
				issueTrackerSource.getConnectionProperties().put("key", "*****");
			}
			if(issueTrackerSource.getConnectionProperties().containsKey("password")) {
				issueTrackerSource.getConnectionProperties().put("password", "*****");
			}
		}
		return issueTrackerSources;
	}
	
	@GetMapping("/issuetrackersource")
	public IssueTrackerSource findById(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		IssueTrackerSource issueTrackerSource = issueTrackerSourceProcess.findById(id).get();
		if(issueTrackerSource.getConnectionProperties().containsKey("password")) {
			issueTrackerSource.getConnectionProperties().put("password", "*****");
		}
		return issueTrackerSource;
	}
	
	@PutMapping("/issuetrackersource")
	public void update(@RequestBody IssueTrackerSource source) throws IOException, InstantiationException, IllegalAccessException {
		IssueTrackerSource its = issueTrackerSourceProcess.findById(source.getId()).get();
		BeanUtils.copyProperties(source, its);
		issueTrackerSourceProcess.save(its);
	}
	
	@DeleteMapping("/issuetrackersource")
	public void delete(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		issueTrackerSourceProcess.delete(issueTrackerSourceProcess.findById(id).get());
	}
}
