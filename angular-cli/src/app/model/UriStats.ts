import { UserStats } from './UserStats';

export interface UriStats {
    uri: string;
    count: number;
    usersCount: number;
    usersStats: UserStats[];
    firstDate: Date;
    lastDate: Date;
}
