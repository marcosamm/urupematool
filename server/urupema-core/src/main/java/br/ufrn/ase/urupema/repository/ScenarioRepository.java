/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioRepository.java
 * 31 de out de 2017
 */
package br.ufrn.ase.urupema.repository;

import java.util.List;
import java.util.Optional;

/**
 * Represents a repository of scenarios
 *
 */
public interface ScenarioRepository extends WebExecutionRepository{
	Optional<Long> findNodeRootIdByScenarioId(String idScenario);
	List<String> findMinChildNodeIdByIdScenarioAndChildAncestorIdChildMemberSignature(Long ancestorId, String childMemberSignature);
}
