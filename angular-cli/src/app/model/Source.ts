import { SourceType } from './SourceType';

export interface Source {
    id: number;
    name: string;
    urlConnection: string;
    userName: string;
    password: string;
    accessIndexName: string;
    accessDocumentType: string;
    executionIndexName: string;
    executionDocumentType: string;
    errorIndexName: string;
    errorDocumentType: string;
    sourceTypeId: string;
    sourceType: SourceType;
}
