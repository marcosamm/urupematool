/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.Source;
import br.ufrn.ase.urupema.process.SourceProcess;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class SourceResource {
	@Autowired
	SourceProcess sourceProcess;
	
	@PostMapping("/source")
	public void put(@RequestBody Source source) throws IOException {
		log.debug("Source :" + source);
		sourceProcess.save(source);
	}
	
	@GetMapping("/source/list")
	public List<Source> findAll() throws IOException, InstantiationException, IllegalAccessException {
		List<Source> sources = sourceProcess.findAll();
		for(Source source : sources) {
			source.setPassword("*****");
		}
		return sources;
	}
	
	@GetMapping("/source")
	public Source findById(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		Source source = sourceProcess.findById(id).get();
		source.setPassword("*****");
		return source;
	}
	
	@PutMapping("/source")
	public void update(@RequestBody Source source) throws IOException, InstantiationException, IllegalAccessException {
		Source s = sourceProcess.findById(source.getId()).get();
		BeanUtils.copyProperties(source, s);
		sourceProcess.save(s);
	}
	
	@DeleteMapping("/source")
	public void delete(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		sourceProcess.delete(sourceProcess.findById(id).get());
	}
}
