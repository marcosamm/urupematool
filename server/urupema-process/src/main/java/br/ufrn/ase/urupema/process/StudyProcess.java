package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.GroupCorrelator;
import br.ufrn.ase.urupema.domain.GroupRelationship;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.Study;
import br.ufrn.ase.urupema.dtos.GroupCorrelatorSummary;
import br.ufrn.ase.urupema.dtos.SourceConnectionSummary;
import br.ufrn.ase.urupema.repository.StudyRepository;

@Component
@Transactional
public class StudyProcess {
	@Autowired
	StudyRepository studyRepository;
	
	public Optional<Study> findById(Long id) throws IOException {
		return studyRepository.findById(id);
	}
	
	public void save(Study study) {
		studyRepository.save(study);
	}
	
	public List<Study> findAll(){
		return studyRepository.findAll();
	}
	
	public void delete(Study study) {
		studyRepository.delete(study);
	}
	
	public Map<String, Set<SourceConnectionSummary>> getSourceConnectionSummary(Long id) throws IOException{
		Map<String, Set<SourceConnectionSummary>> map = new HashMap<>();
		Study s = findById(id).get();
		Set<Long> idsSourceConnection = new TreeSet<>();
		for(SourceConnection sc : s.getSourceConnections()) {
			idsSourceConnection.add(sc.getId());
			for(LogGroup lg : sc.getLogGroups()) {
				if(lg.getSubGroups().size() > 0) {
					if(!map.containsKey(lg.getLabel())) {
						map.put(lg.getLabel(), new TreeSet<>());
					}
					map.get(lg.getLabel()).add(new SourceConnectionSummary(sc.getId(), lg.getElementsCount(), lg.getSubGroups().size()));
				}
			}
		}
		
		for(Entry<String, Set<SourceConnectionSummary>> entry : map.entrySet()) {
			if(entry.getValue().size() < s.getSourceConnections().size()) {
				List<Long> l = new ArrayList<>(idsSourceConnection);
				for(SourceConnectionSummary scs : entry.getValue()) {
					l.remove(scs.getIdSourceConnection());
				}
				for(Long scr : l) {
					entry.getValue().add(new SourceConnectionSummary(scr, 0, 0));
				}
			}
		}
		
		return map;
	}
	
	public Map<String, Set<GroupCorrelatorSummary>> getGroupCorrelatorSummary(Long id) throws IOException{
		Map<String, Set<GroupCorrelatorSummary>> map = new HashMap<>();
		Study s = findById(id).get();
		Set<Long> idsGroupCorrelator = new TreeSet<>();
		for(GroupCorrelator gc : s.getGroupCorrelators()) {
			idsGroupCorrelator.add(gc.getId());
			for(GroupCorrelatorSummary gcs : getSummaries(gc)) {
				if(!map.containsKey(gcs.getUri())) {
					map.put(gcs.getUri(), new TreeSet<>());
				}
				map.get(gcs.getUri()).add(gcs);
			}
		}
		
		for(Entry<String, Set<GroupCorrelatorSummary>> entry : map.entrySet()) {
			if(entry.getValue().size() < s.getGroupCorrelators().size()) {
				List<Long> l = new ArrayList<>(idsGroupCorrelator);
				for(GroupCorrelatorSummary scs : entry.getValue()) {
					l.remove(scs.getIdGroupCorrelator());
				}
				for(Long gc : l) {
					entry.getValue().add(new GroupCorrelatorSummary(entry.getKey(), gc));
				}
			}
		}
		
		return map;
	}
	
	private Set<GroupCorrelatorSummary> getSummaries(GroupCorrelator gc){
		Map<String, GroupCorrelatorSummary> map = new TreeMap<>();
		
		for(GroupRelationship gr : gc.getGroupRelationships()) {
			String uri = gr.getGroup().getSuperGroup().getLabel();
			if(!map.containsKey(uri)) {
				map.put(uri, new GroupCorrelatorSummary(gr));
			}else {
				GroupCorrelatorSummary summ = map.get(uri);
				summ.join(gr);
			}
			
		}
		
		return new TreeSet<>(map.values());
	}
}
