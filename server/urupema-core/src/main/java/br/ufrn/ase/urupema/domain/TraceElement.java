package br.ufrn.ase.urupema.domain;

import java.util.List;
import java.util.Map;

public interface TraceElement {
	public String getQualifiedFileName();
	public String getFileName();
	public Integer getLineNumber();
	public String getSignature();
	public TraceElement getParent();
	public List<? extends TraceElement> getChildren();
	public List<TraceElement> getTraceElement(String signature);
	public boolean contains(TraceElement traceElement);
	String toString(String prefix);
	public Integer getHeight();
	public Integer getTopFrameDist();
	public Integer getQtdDescendents();
	public Map<String, Integer> getQualifiedFileNamesAndMinTopFrameDist();
	public List<String> getOrderedQualifiedFileNames();
}
