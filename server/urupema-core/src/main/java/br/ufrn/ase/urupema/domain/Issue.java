package br.ufrn.ase.urupema.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@SequenceGenerator(name = "ID_ISSUE_SEQUENCE", sequenceName = "issue_id_sequence", allocationSize = 1)
@Table(uniqueConstraints={
		@UniqueConstraint(name="issue_uc", columnNames = {"issuetrackerconnection_id", "identifier"})
})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Getter
@Setter
@JsonIgnoreProperties(value= {"reportedStackTraces", "enabledReportedStackTraces", "enabledChangedFiles", "changedFileNotes"})
@ToString(of={"id", "openDate", "closeDate", "title"})
public class Issue implements Serializable, Comparable<Issue> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_ISSUE_SEQUENCE")
	private Long id;
	
	@Column(nullable = false)
	private String identifier;
	
	@Column(nullable = false)
	private String title;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date openDate;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date closeDate;
	
	@Enumerated(EnumType.STRING)
	private IssueType issueType;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY, optional=false)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_issue_issuetrackerconnection_id"))
	private IssueTrackerConnection issueTrackerConnection;
	
	@JsonIgnore
	@JsonIdentityReference(alwaysAsId = true)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Revision.class)
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name="issue_revision", 
		joinColumns={@JoinColumn(name="issue_id")}, 
		inverseJoinColumns={@JoinColumn(name="revision_id")}
	)
	private Set<Revision> revisions = new TreeSet<>();
	
	@JsonIgnore
	@JsonIdentityReference(alwaysAsId = true)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = LogGroup.class)
	@ManyToMany()
	@JoinTable(name="issue_loggroup", 
		joinColumns={@JoinColumn(name="issue_id")}, 
		inverseJoinColumns={@JoinColumn(name="loggroup_id")}
	)
	private Set<LogGroup> logGroups = new TreeSet<>();
	
	@JsonIgnore
	@OneToMany(mappedBy = "issue", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
	@OrderBy("stackTrace asc")
	private Set<ReportedStackTrace> reportedStackTraces = new TreeSet<>();
	
	@Column(nullable = false)
	private boolean enabled;
	
	@Column(columnDefinition="text")
	private String note;
	
	public Issue() {
	}
	
	public Issue(String identifier, Date openDate, Date closeDate, String title, IssueType issueType, 
			boolean enabled, String note, IssueTrackerConnection issueTrackerConnection) {
		super();
		this.identifier = identifier;
		this.openDate = openDate;
		this.closeDate = closeDate;
		this.title = title;
		this.issueType = issueType;
		this.enabled = enabled;
		this.note = note;
		this.issueTrackerConnection = issueTrackerConnection;
	}

	@Override
	public int compareTo(Issue arg0) {
		int ret = openDate.compareTo(arg0.getOpenDate());
		if(ret == 0) {
			ret = closeDate.compareTo(arg0.getCloseDate());
		}
		if(ret == 0) {
			ret = identifier.compareTo(arg0.getIdentifier());
		}
		if(ret == 0) {
			ret = title.compareTo(arg0.getTitle());
		}
		return ret;
	}
	
	public void addLogGroup(LogGroup logGroup) {
		this.logGroups.add(logGroup);
		logGroup.getIssues().add(this);
	}
	
	public void addLogGroups(Set<LogGroup> logGroups) {
		for(LogGroup lg : logGroups) {
			addLogGroup(lg);
		}
	}
	
	public void removeLogGroup(LogGroup logGroup) {
		this.logGroups.remove(logGroup);
		logGroup.getIssues().remove(this);
	}
	
	public void removeLogGroups(Set<LogGroup> logGroups) {
		for(LogGroup lg : logGroups) {
			removeLogGroup(lg);
		}
	}
	
	public void addRevision(Revision revision) {
		this.revisions.add(revision);
		revision.getIssues().add(this);
	}
	
	public void addRevisions(Set<Revision> revisions) {
		for(Revision r : revisions) {
			addRevision(r);
		}
	}
	
	public void removeRevision(Revision revision) {
		this.revisions.remove(revision);
		revision.getIssues().remove(this);
	}
	
	public void removeRevisions(Set<Revision> revisions) {
		for(Revision r : revisions) {
			removeRevision(r);
		}
	}
	
	public Set<String> getEnabledChangedFiles(Collection<FileChangeType> fileChangeTypes, Collection<String> fileExtensions){
		Set<String> changedFiles = new TreeSet<>();
		for(Revision r : revisions) {
			for(ChangedFile cf : r.getChangedFiles()) {
				if(cf.isEnabled()) {
					String fileExtension = cf.getName().substring(cf.getName().lastIndexOf("."));
					boolean typeOk = false;
					if(fileChangeTypes == null || fileChangeTypes.isEmpty() || fileChangeTypes.contains(cf.getFileChangeType())) {
						typeOk = true;
					}
					boolean extensionOk = false;
					if(fileExtensions == null || fileExtensions.isEmpty() || fileExtensions.contains(fileExtension)) {
						extensionOk = true;
					}
					if(typeOk && extensionOk) {
						changedFiles.add(cf.getName());
					}
				}
			}
		}
		return changedFiles;
	}
	
	public void addReportedStackTrace(ReportedStackTrace reportedStackTrace) {
		this.getReportedStackTraces().add(reportedStackTrace);
		reportedStackTrace.setIssue(this);
	}
	
	public void removeReportedStackTrace(ReportedStackTrace reportedStackTrace) {
		this.getReportedStackTraces().remove(reportedStackTrace);
		reportedStackTrace.setIssue(null);
	}
	
	public Set<ReportedStackTrace> getEnabledReportedStackTraces(String pkg){
		Set<ReportedStackTrace> reportedStackTraces = new TreeSet<>();
		for(ReportedStackTrace reportedStackTrace : getReportedStackTraces()) {
			if(reportedStackTrace.isEnabled()) {
				if(pkg == null || reportedStackTrace.getStackTrace().contains(pkg)) {
					reportedStackTraces.add(reportedStackTrace);
				}
			}
		}
		return reportedStackTraces;
	}
	
	public List<String> getChangedFileNotes(String fileName){
		List<String> changedFileNotes = new ArrayList<>();
		for(Revision r : revisions) {
			for(ChangedFile cf : r.getChangedFiles()) {
				if(cf.getName().contains(fileName) && cf.getNote() != null && !cf.getNote().isEmpty()) {
					changedFileNotes.add(cf.getNote());
				}
			}
		}
		return changedFileNotes;
	}
}
