package br.ufrn.ase.urupema.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(of = {"fileName", "lineNumber"})
public class LineNumberStats implements Comparable<LineNumberStats>{
	private String fileName;
	private Integer lineNumber;
	private Long count;
	
	public LineNumberStats(String fileName, Integer lineNumber, Long count) {
		this.fileName = fileName;
		this.lineNumber = lineNumber;
		this.count = count;
	}
	
	public void addCount(Long count){
		this.count += count;
	}

	@Override
	public int compareTo(LineNumberStats o) {
		int ret = o.getCount().compareTo(this.count);
		
		if(ret == 0) {
			ret = o.getFileName().compareTo(this.fileName);
		}
		
		if(ret == 0 && o.getLineNumber() != null && this.lineNumber != null) {
			ret = o.getLineNumber().compareTo(this.lineNumber);
		}
		
		return ret;
	}
}
