package br.ufrn.ase.urupema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.IssueTrackerConnection;

@Repository
public interface IssueTrackerConnectionRepository extends JpaRepository<IssueTrackerConnection, Long>{
}
