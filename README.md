# Urupema

A crash report grouping approach to locate and rank buggy classes and methods.

## Overview

The approach is based on previous work (Wang et al. 2016, Wu et al. 2014) and consists of two steps: (a) to group crash reports related to the same software bug; and (b) to rank files suspected of causing crashes.

### Crash Report Grouping

We group crash reports using information available in the stack traces in five ordered and cumulative levels. The primary purpose is to aggregate strongly correlated crash reports and to reduce the time necessary for data processing.

![doc/images/grouping.png](doc/images/grouping.png)

### Ranking suspicious files and methods

We mine information of the files present in the stack traces of the crash report groups. We use this information to rank the files that are more likely to produce the crashes in a given crash report group. To this end, our approach leverages three discriminative factors proposed by Wu et al., though adapted to a coarse-grained level (files, instead of methods):

- Inverse Average Distance to Crash Point (IAD): If a file appears closer to the crash point, it is more likely to cause the crash.

- Inverse Bucket Frequency (IBF): If a file appears in stack traces caused by many different faults, it is less likely to be the cause of a specific fault.

- File Frequency (FF): If a file often appears in stack traces caused by a particular fault, then it is likely to be the cause of this fault.


#### Suggesting suspicious methods

For each of the ranked files, we identified the methods that appeared in the stack traces of the respective group of crash reports and suggest them to the developers, showing the most frequent ones first.


### Papers

Medeiros, M., Kulesza, U., Bonifacio, R., Adachi, E., and Coelho, R. Improving bug localization by mining crash reports: An industrial study. In 2020 IEEE International Conference on Software Maintenance and Evolution (ICSME), pages 766–775. IEEE.


### References

Wang, S., Khomh, F., and Zou, Y. (2016). Improving bug management using correlations in crash reports. Empirical Software Engineering, 21(2):337–367.

Wu, R., Zhang, H., Cheung, S.-C., and Kim, S. (2014). CrashLocator: locating crashing faults based on crash stacks. In Proceedings of the 2014 International Symposium on Software Testing and Analysis - ISSTA 2014, pages 204–214, New York, New York, USA. ACM Press.  

---   
  
## How to Use the Tool

### 1. Download and Install

- Download and install Java

- Download and unzip [urupema.zip](dist/urupema.zip)

- Create the database and the urupema\_user in PostgresSql (run or restore the backup using [urupema-0.1.0.sql](dist/urupema-0.1.0.sql))

- Change the HOST and PASS information in the [application.properties](dist/application.properties) file   

- Run the command: java -jar [urupema-0.1.0.jar](dist/urupema-0.1.0.jar)

- Open http://localhost:8080/ in a browser

![running.gif](doc/images/running.gif)


### 2. Add Log Source

![logsource.gif](doc/images/logsource.gif)

The urupema tool has a default implementation to collect data from crash reports stored in ElasticSearch. The index must have at least the following fields: datetime, user\_login, stacktrace and url. You can use the [index\_error\_log-2021\_analyzer.json](dist/index_error_log-2021_analyzer.json) and [index\_error\_log-2021\_mapping.json](dist/index_error_log-2021_mapping.json) files to create the indexes and then input your crash reports into ElasticSearch. Thus, the tool will be able to analyze your dataset.

You can also make your own implementation of the [SourceTypePlugin](server/urupema-core/src/main/java/br/ufrn/ase/urupema/extensible/sourcetype/SourceTypePlugin.java) interface.

### 3. Add Study and Log Source Connection
![study_and_logsourceconnection.gif](doc/images/study_and_logsourceconnection.gif)

### 4. Listing, grouping and detailing
![list_group_detail_subgroups.gif](doc/images/list_group_detail_subgroups.gif)

### 5. Ranking suspicious files
![suspicious_files.gif](doc/images/suspicious_files.gif)

### 6. Suspicious files report
![suspicious_report.gif](doc/images/suspicious_report.png)   

---

## How to Use in Development Mode
### Dependencies

- Download and Install Java (https://adoptopenjdk.net/)
- Download and Install Eclipse (https://www.eclipse.org/downloads/)
- Download and Install Gradle (https://gradle.org/)
- Install via Eclipse market Gradel BuildShip Eclipse Plugin (https://projects.eclipse.org/projects/tools.buildship)
- Download and Install Node.js, this install the npm tool (https://nodejs.org) 

### Install via npm tool

	sudo npm install –g typescript
	sudo npm install –g @angular/cli


### Open in Eclipse

Import urupema-server project **as a Gradle Project** in eclipse


### Run Back-end

Run the **main** method of urupema-services/br.ufrn.ase.urupema.UrupemaServicesApplication class. Navigate to `http://localhost:8080/`


### Run Front-end
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. That will show the web interface of the tool, make using angular.

	cd ./angular-cli
	ng serve --host 0.0.0.0


