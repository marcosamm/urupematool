package br.ufrn.ase.urupema.extensible.sourcetype.repository;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.SourceType;
import br.ufrn.ase.urupema.extensible.sourcetype.SourceTypePlugin;

@Repository
public class SourceTypeRepository {
	Map<String, SourceType> mapSourceTypes;
	
	public SourceTypeRepository(@Value("${extensionPackages}") final String extensionPackages) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		mapSourceTypes = new TreeMap<>();
		
		ConfigurationBuilder builder = new ConfigurationBuilder();
		List<URL> urls = new ArrayList<>(ClasspathHelper.forPackage("br.ufrn.ase.urupema"));
		if(extensionPackages != null && !extensionPackages.isBlank()) {
			for(String pack : extensionPackages.split(",")) {
				if(!pack.isBlank()) {
					urls.addAll(ClasspathHelper.forPackage(pack.trim()));
				}
			}
		}
		builder.setUrls(urls);
		Reflections reflections = new Reflections(builder);
		Set<Class<? extends SourceTypePlugin>> classes = reflections.getSubTypesOf(SourceTypePlugin.class);
		for (Class<? extends SourceTypePlugin> class1 : classes) {
			if(!Modifier.isAbstract(class1.getModifiers()) && !class1.isInterface()) {
				SourceType inst = class1.getConstructor().newInstance();
				mapSourceTypes.put(inst.getId(), inst);
			}
		}
	}
	
	public SourceTypePlugin findById(String id) throws InstantiationException, IllegalAccessException{
		return (SourceTypePlugin) mapSourceTypes.get(id);
	}
	
	public Collection<SourceType> findAll() throws InstantiationException, IllegalAccessException{
		return mapSourceTypes.values();
	}
}
