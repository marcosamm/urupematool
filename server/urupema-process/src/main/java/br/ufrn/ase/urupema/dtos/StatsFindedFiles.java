package br.ufrn.ase.urupema.dtos;

import java.util.List;

import lombok.Getter;

@Getter
public class StatsFindedFiles {
	private String issueIdentifier;
	private Long logGroupId;
	private String file;
	private String issueNotes;
	private List<CalledMethod> calledMethods;
	
	public StatsFindedFiles(String issueIdentifier, Long logGroupId, String file, String issueNotes,
			List<CalledMethod> calledMethods) {
		super();
		this.issueIdentifier = issueIdentifier;
		this.logGroupId = logGroupId;
		this.file = file;
		this.issueNotes = issueNotes;
		this.calledMethods = calledMethods;
	}
	
	public int getFindedMethodsCount() {
		int count = 0;
		for(CalledMethod calledMethod: calledMethods) {
			if(calledMethod.isFound()) {
				count++;
			}
		}
		return count;
	}
}
