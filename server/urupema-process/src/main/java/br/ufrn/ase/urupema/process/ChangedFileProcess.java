package br.ufrn.ase.urupema.process;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.ChangedFile;
import br.ufrn.ase.urupema.domain.Revision;
import br.ufrn.ase.urupema.repository.ChangedFileRepository;

@Component
@Transactional
public class ChangedFileProcess {
	@Autowired
	private ChangedFileRepository repository;
	
	public ChangedFile findById(Long id) {
		return repository.findById(id).get();
	}
	
	public void save(ChangedFile changedFile) {
		repository.save(changedFile);
	}
	
	public void delete(Long id) {
		ChangedFile c = findById(id);
		c.getRevision().removeChangedFile(c);
		repository.delete(c);
	}
	
	public List<ChangedFile> findByRevision(Revision revision){
		return repository.findByRevision(revision);
	}
}
