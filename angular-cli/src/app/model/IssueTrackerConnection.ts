import { Study } from './Study';
import { Issue } from './Issue';
import { IssueTrackerSource } from './IssueTrackerSource';

export interface IssueTrackerConnection {
    id: number;
    name: string;
    startDate: Date;
    endDate: Date;
    study: Study;
    issueTrackerSource: IssueTrackerSource;
    queryProperties: Map<string, string>;
    issues: Issue [];
}
