export enum LogType {
    ACCESS = 'ACCESS',
    ERROR = 'ERROR',
    EXECUTION = 'EXECUTION'
}
