import { ReportedStackTrace } from './ReportedStackTrace';

export interface AffectedUri {
    id: number;
    uri: string;
    enabled: boolean;
    note: string;
    reportedStackTrace: ReportedStackTrace;
}
