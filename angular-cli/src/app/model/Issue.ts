import { IssueType } from './IssueType.enum';
import { IssueTrackerConnection } from './IssueTrackerConnection';
import { LogGroup } from './LogGroup';
import { Revision } from './Revision';
import { ReportedStackTrace } from './ReportedStackTrace';

export interface Issue {
    id: number;
    identifier: string;
    title: string;
    openDate: Date;
    closeDate: Date;
    enabled: boolean;
    note: string;
    issueType: IssueType;
    issueTrackerConnection: IssueTrackerConnection;
    revisions: Revision [];
    logGroups: LogGroup [];
    reportedStackTraces: ReportedStackTrace [];
}
