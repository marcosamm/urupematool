import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { IssueType } from '../model';

@Injectable()
export class IssueTypeService {
  urlGet = environment.apiUrl + '/issuetype/list';

  constructor(private http: HttpClient) { }

  getIssueTypes(): Observable<IssueType[]> {
       return this.http.get<IssueType[]>(this.urlGet);
  }
}
