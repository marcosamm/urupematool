import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OverlayPanelModule } from 'primeng/overlaypanel';

import localept from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localept, 'pt');

import { HttpClientModule } from '@angular/common/http';

import { TableModule } from 'primeng/table';
import { ChartModule } from 'primeng/chart';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
//import { library } from '@fortawesome/fontawesome-svg-core';
import {
  fas, faSpinner, faEllipsisV, faCogs, faGraduationCap, faDatabase, faListOl, faLink, faBoxes, faBoxOpen,
  faSearch, faProjectDiagram, faCoins, faPlug, faDna
} from '@fortawesome/free-solid-svg-icons';

//library.add(faSpinner, faEllipsisV, faCogs, faGraduationCap,
//  faLink, faBoxes, faBoxOpen, faDatabase, faListOl, faSearch,
//  faProjectDiagram, faCoins, faPlug, faDna);

import {
  SourceService,
  SourceTypeService,
  StudyService,
  LogGroupService,
  GroupAggregatorTypeService,
  GroupCorrelatorTypeService,
  IssueTrackerConnectionService,
  IssueTrackerSourceService,
  IssueTrackerTypeService,
  SuspiciousFilesFinderTypeService,
} from './service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { SourceTypeComponent } from './sourcetype/sourcetype.component';

import { appRoutes } from './app.routing';
import { GroupCorrelatorTypeComponent } from './groupcorrelatortype/groupcorrelatortype.component';
import { GroupAggregatorTypeComponent } from './groupaggregatortype/groupaggregatortype.component';
import { GroupcorrelatorModule } from './groupcorrelator/groupcorrelator.module';
import { StudyModule } from './study/study.module';
import { SourceModule } from './source/source.module';
import { SourceConnectionModule } from './sourceconnection/sourceconnection.module';
import { PipesModule } from './pipes/pipes.module';

import { LoadingSpinnerModule } from './loadingspinner/loadingspinner.module';
import { IssueTrackerConnectionModule } from './issuetrackerconnection/issuetrackerconnection.module';
import { IssueTrackerTypeComponent } from './issuetrackertype/issuetrackertype.component';
import { IssueTrackerSourceModule } from './issuetrackersource/issuetrackersource.module';
import { SuspiciousFilesFinderTypeComponent } from './suspiciousfilesfindertype/suspiciousfilesfindertype.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    IssueTrackerTypeComponent,
    SourceTypeComponent,
    GroupCorrelatorTypeComponent,
    GroupAggregatorTypeComponent,
    SuspiciousFilesFinderTypeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,  // to user services and call the backend
    RouterModule.forRoot(appRoutes),
    FontAwesomeModule,
    PipesModule,
    TableModule,
    ChartModule,
    GroupcorrelatorModule,
    StudyModule,
    SourceModule,
    SourceConnectionModule,
    IssueTrackerConnectionModule,
    LoadingSpinnerModule,
    IssueTrackerSourceModule,
    OverlayPanelModule
  ],
  providers: [
    StudyService,
    SourceService,
    IssueTrackerTypeService,
    SourceTypeService,
    LogGroupService,
    GroupAggregatorTypeService,
    GroupCorrelatorTypeService,
    IssueTrackerConnectionService,
    IssueTrackerSourceService,
    SuspiciousFilesFinderTypeService,
    {provide: LOCALE_ID, useValue: 'pt'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
    library.addIcons(faSpinner, faEllipsisV, faCogs, faGraduationCap,
      faLink, faBoxes, faBoxOpen, faDatabase, faListOl, faSearch,
      faProjectDiagram, faCoins, faPlug, faDna);
  }
}
