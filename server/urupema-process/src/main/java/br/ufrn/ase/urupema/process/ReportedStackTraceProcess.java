package br.ufrn.ase.urupema.process;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.ReportedStackTrace;
import br.ufrn.ase.urupema.repository.ReportedStackTraceRepository;

@Component
@Transactional
public class ReportedStackTraceProcess {
	@Autowired
	private ReportedStackTraceRepository repository;
	
	public ReportedStackTrace findById(Long id) {
		return repository.findById(id).get();
	}
	
	public void save(ReportedStackTrace reportedStackTrace) {
		repository.save(reportedStackTrace);
	}
	
	public void delete(Long id) {
		ReportedStackTrace r = findById(id);
		r.getIssue().removeReportedStackTrace(r);
		repository.delete(r);
	}
}
