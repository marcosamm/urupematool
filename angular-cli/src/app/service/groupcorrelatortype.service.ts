import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { GroupCorrelatorType } from '../model/GroupCorrelatorType';

@Injectable()
export class GroupCorrelatorTypeService {
    urlGet = environment.apiUrl + '/groupcorrelatortype';

    constructor(private http: HttpClient) { }

    getGroupCorrelatorType(id): Observable<GroupCorrelatorType> {
        return this.http.get<GroupCorrelatorType>(this.urlGet + '/detail?id=' + id);
    }

    getGroupCorrelatorTypes(): Observable<GroupCorrelatorType[]> {
        return this.http.get<GroupCorrelatorType[]>(this.urlGet + '/list');
    }
}
