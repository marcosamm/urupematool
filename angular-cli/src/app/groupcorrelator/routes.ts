
import {ModuleWithProviders } from '@angular/core';
import { Routes } from '@angular/router';

import { ListGroupRelationshipsComponent } from './listgrouprelationships/listgrouprelationships.component';
import { GroupCorrelatorDetailComponent } from './detail/detail.component';
import { GroupCorrelatorCreateComponent } from './create/create.component';
import { GroupRelationshipComponent } from './grouprelationship/grouprelationship.component';

/**
 * Define the routes
 */
export const routes: Routes = [
    { path: 'groupcorrelator', component: ListGroupRelationshipsComponent,
/*        children: [
            { path: 'listgrouprelationships/:idGroupCorrelator', component: ListGroupRelationshipsComponent},
        ]*/
    },
    { path: 'groupcorrelator/listgrouprelationships/:idGroupCorrelator', component: ListGroupRelationshipsComponent},
    { path: 'groupcorrelator/create/:idStudy', component: GroupCorrelatorCreateComponent},
    { path: 'groupcorrelator/create/:idStudy/:id', component: GroupCorrelatorCreateComponent},
    { path: 'groupcorrelator/grouprelationship/:idGroupCorrelator/:id', component: GroupRelationshipComponent},
];
