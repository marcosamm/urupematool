
import { Routes } from '@angular/router';

import { SourceListComponent } from './list/list.component';
import { SourceCreateComponent } from './create/create.component';

/**
 * Define the routes
 */
export const routes: Routes = [
    { path: 'source', component: SourceListComponent},
    { path: 'source/list', component: SourceListComponent},
    { path: 'source/create', component: SourceCreateComponent},
    { path: 'source/create/:id', component: SourceCreateComponent},
];
