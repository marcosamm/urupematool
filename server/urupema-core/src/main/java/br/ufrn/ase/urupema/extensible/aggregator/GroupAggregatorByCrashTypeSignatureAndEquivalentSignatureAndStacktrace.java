package br.ufrn.ase.urupema.extensible.aggregator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.SimpleWebExecutionTrace;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.parser.StackTraceParser;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class GroupAggregatorByCrashTypeSignatureAndEquivalentSignatureAndStacktrace extends GroupAggregatorByEquivalentSignatureAndStacktrace implements GroupAggregatorTypePlugin {
	public static String ID = "urupema:st_eqv_r1";
	
	private List<LogType> logTypes = Arrays.asList(LogType.ERROR);
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getName() {
		return "Stack Trace, Equivalent Signature and Crash Type Signature";
	}

	@Override
	public String getDescription() {
		return "Creates a set of logs grouped by Crash Type Signature (A contains B or B contains A) and subgrouped by Equivalent Signature and Stack Trace";
	}
	
	@Override
	public List<LogGroup> agroup(SourceConnection sourceConnection, Map<String, Object> params, Map<String, WebExecutionTrace> cache) throws IOException {
		List<LogGroup> superGroups = new ArrayList<>();
		
		StackTraceParser stp = new StackTraceParser();
		
		//Pega os grupos de equivalente stacktrace
		List<LogGroup> eqvStacktraceGroups = super.agroup(sourceConnection, params, cache);
		
		Map<LogGroup, SimpleWebExecutionTrace> mapError = new HashMap<>();
		Map<LogGroup, List<StackTraceElement>> mapSTEs = new HashMap<>();
		for(LogGroup eqvStacktraceGroup : eqvStacktraceGroups) {
			mapError.put(eqvStacktraceGroup, stp.toSimpleError(eqvStacktraceGroup.getLabel()));
			mapSTEs.put(eqvStacktraceGroup, stp.parse(eqvStacktraceGroup.getLabel()));
		}
		
		//Ordenada os grupos da maior para a menor em quantidade de descentes
		Collections.sort(eqvStacktraceGroups, new Comparator<LogGroup>() {
			@Override
			public int compare(LogGroup arg0, LogGroup arg1) {
				List<StackTraceElement> l0 = mapSTEs.get(arg0);
				List<StackTraceElement> l1 = mapSTEs.get(arg1);
				return Integer.compare(l1.size(), l0.size());
			}
		});
		
		//Percorre todos os grupos de equivalent stacktrace
		for(LogGroup eqvStacktraceGroup : eqvStacktraceGroups) {
			boolean contains = false;
			
			//Pega o representante do grupo
			WebExecutionTrace representantEquivStacktraceGroup = mapError.get(eqvStacktraceGroup);
			
			//Percorre todos os supergrupos formados verificando se um contem o outro
			for(LogGroup superGroup : superGroups) {
				WebExecutionTrace representantSuperGroup = mapError.get(superGroup);
				List<StackTraceElement> lEqvStacktraceGroup = mapSTEs.get(eqvStacktraceGroup);
				List<StackTraceElement> lSuperGroup = mapSTEs.get(superGroup);
				
				if(lSuperGroup.size() >= lEqvStacktraceGroup.size() && lEqvStacktraceGroup.size() > 0 && lSuperGroup.containsAll(lEqvStacktraceGroup)) {
					if(!representantEquivStacktraceGroup.getTraceElements().isEmpty() && representantSuperGroup.contains(representantEquivStacktraceGroup.getTraceElements())) {
						contains = true;
						superGroup.addSubGroup(eqvStacktraceGroup);
						//Remove o subGroup (eqvStacktraceGroup) do map
						mapError.remove(eqvStacktraceGroup);
						break;
					}
				}/*
				  Com a ordenação pelo número de descentes do maior para o menor, nunca representantEquivStacktraceGroup poderá conter representantSuperGroup visto que o primeiro é sempre menor que o segundo
				  else if(!representantSuperGroup.getTraceElements().isEmpty() && representantEquivStacktraceGroup.contains(representantSuperGroup.getTraceElements())) {
					contains = true;
					superGroup.setLabel(eqvStacktraceGroup.getLabel());
					//Atualiza o map do superGroup
					mapError.put(superGroup, stp.toSimpleError(superGroup.getLabel()));
					//Remove o subGroup (eqvStacktraceGroup) do map
					mapError.remove(eqvStacktraceGroup);
					superGroup.addSubGroup(eqvStacktraceGroup);
					break;
				}*/
			}
			
			if(contains) {
				continue;
			} else {
				//Cria um supergrupo para a equivalent stacktrace, pois ele ainda não existe
				LogGroup superGroupDest = new LogGroup(eqvStacktraceGroup.getLabel());
				//Adiciona o grupo de equivalent stacktraces ao novo supergrupo 
				superGroupDest.addSubGroup(eqvStacktraceGroup);
				superGroupDest.setTypeDescription("Rule 1 (Crash Type Signature Comparison)");
				superGroupDest.setGroupAggregatorTypeId(ID);
				//Adiciona o novo ao map
				mapError.put(superGroupDest, stp.toSimpleError(superGroupDest.getLabel()));
				mapSTEs.put(superGroupDest, stp.parse(superGroupDest.getLabel()));
				//Remove o subGroup (eqvStacktraceGroup) do map
				mapError.remove(eqvStacktraceGroup);
				mapSTEs.remove(eqvStacktraceGroup);
				//Adiciona o novo grupo à lista de super grupos que será retornada
				superGroups.add(superGroupDest);
			}
		}
		
		int qtdSubs = 0;
		for(LogGroup superGroup : superGroups) {
			qtdSubs += superGroup.getSubGroups().size();
			superGroup.updateOccurenceDates();
			superGroup.updateElementsCount();
		}
		
		log.debug("Rule 1 (Crash Type Signature Comparison) groups: " + superGroups.size()  + " - subGroups: " + qtdSubs);
		return superGroups;
	}

	@Override
	public int compareTo(GroupAggregatorType o) {
		return getId().compareTo(o.getId());
	}
}
