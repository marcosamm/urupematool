package br.ufrn.ase.urupema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.GroupCorrelator;

@Repository
public interface GroupCorrelatorRepository extends JpaRepository<GroupCorrelator, Long>{
}
