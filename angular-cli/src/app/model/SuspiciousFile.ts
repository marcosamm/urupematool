export interface SuspiciousFile {
  id: number;
  name: string;
  inverseAverageDistanceToCrashPoint: number;
  inverseBucketFrequency: number;
  fileFrequency: number;
  score: number;
}
