import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapToIterablePipe } from '../pipes/maptoiterable.pipe';


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    MapToIterablePipe
  ],
  exports: [
    MapToIterablePipe
  ],
  providers: [
  ]
})
export class PipesModule { }
