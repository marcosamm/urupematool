import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { SourceType } from '../model';
import { Observable } from 'rxjs';

@Injectable()
export class SourceTypeService {
  urlGet = environment.apiUrl + '/sourcetype/list';

  constructor(private http: HttpClient) { }

  getSourceTypes(): Observable<SourceType[]> {
       return this.http.get<SourceType[]>(this.urlGet);
  }
}
