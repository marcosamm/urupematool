package br.ufrn.ase.urupema.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import lombok.Getter;

public abstract class AbstractWebExcecutionTrace implements WebExecutionTrace {
	private String traceElementsString = null;
	private Integer qtdDescendents = null;
	@Getter
	protected Map<String, String> moreInformation = new TreeMap<>();
	private List<String> orderedQualifiedFileNames = null;
	
	@Override
	public String getRequestURI () {
		int indexStart = getRequestURL().indexOf("/", 8);//Primeira / depois do http:// ou https://
		int indexEnd = getRequestURL().indexOf("?");
		if(indexEnd < 0) {
			indexEnd = getRequestURL().length();
		}
		return getRequestURL().substring(indexStart, indexEnd);
	}
	
	@Override
	public boolean contains(TraceElement traceElement) {
		boolean contains = false;
		
		for(TraceElement te : getTraceElements()) {
			if(te.contains(traceElement)) {
				contains = true;
				break;
			}
		}
		
		return contains;
	}
	
	@Override
	public boolean contains(List<TraceElement> traceElements) {
		if(traceElements.isEmpty()) {
			throw new IllegalArgumentException("traceElements is empty");
		}
		boolean contains = true;
		
		for(TraceElement te : traceElements) {
			if(!contains(te)) {
				contains = false;
				break;
			}
		}
		
		return contains;
	}
	
	@Override
	public boolean equivalent(WebExecutionTrace other) {
		if(this.getRequestURI().equals(other.getRequestURI()) && this.contains(other.getTraceElements()) && other.contains(this.getTraceElements())) {
			return true;
		}
		return false;
	}
	
	@Override
	public String getTraceElementsAsString() {
		if(traceElementsString == null) {
			StringBuilder sb = new StringBuilder();
			for(TraceElement te : getTraceElements()) {
				sb.append(te.toString());
			}
			traceElementsString = sb.toString().trim();
		}
		return traceElementsString;
	}
	
	@Override
	public Integer getQtdTraceElements() {
		if(qtdDescendents == null) {
			qtdDescendents = getTraceElements().size();
			for(TraceElement te : getTraceElements()) {
				qtdDescendents += te.getQtdDescendents(); 
			}
		}
		return qtdDescendents;
	}
	
	@Override
	public Map<String, Integer> getQualifiedFileNamesAndMinTopFrameDist() {
		Map<String, Integer> mapQfnMtfd = new HashMap<>();
		
		for(TraceElement c : getTraceElements()) {
			if(c == null) {
				System.out.println("Opa...");
			}
			for(Map.Entry<String, Integer> entry : c.getQualifiedFileNamesAndMinTopFrameDist().entrySet()) {
				if(mapQfnMtfd.containsKey(entry.getKey())) {
					if(mapQfnMtfd.get(entry.getKey()) > entry.getValue()) {
						mapQfnMtfd.put(entry.getKey(), entry.getValue());
					}
				}else {
					mapQfnMtfd.put(entry.getKey(), entry.getValue());
				}
			}
		}
		
		return mapQfnMtfd;
	}
	
	@Override
	public List<String> getOrderedQualifiedFileNames() {
		if(orderedQualifiedFileNames == null) {
			orderedQualifiedFileNames = new ArrayList<>();
			for(TraceElement te : getTraceElements()) {
				orderedQualifiedFileNames.addAll(te.getOrderedQualifiedFileNames());
			}
		}
		return orderedQualifiedFileNames;
	}
}
