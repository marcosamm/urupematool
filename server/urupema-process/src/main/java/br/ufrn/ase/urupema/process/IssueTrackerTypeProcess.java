package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.IssueTrackerType;
import br.ufrn.ase.urupema.extensible.issuetrackertype.repository.IssueTrackerTypeRepository;

@Component
public class IssueTrackerTypeProcess {
	@Autowired
	private IssueTrackerTypeRepository repository;
	
	public IssueTrackerType findById(String id) throws IOException, InstantiationException, IllegalAccessException {
		return repository.findById(id);
	}
	
	public Collection<IssueTrackerType> findAll() throws InstantiationException, IllegalAccessException{
		return repository.findAll();
	}
}
