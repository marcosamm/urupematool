package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.OperationNotSupportedException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.Log;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.UriStats;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.extensible.aggregator.GroupAggregatorTypeFactory;
import br.ufrn.ase.urupema.parser.StackTraceParser;
import br.ufrn.ase.urupema.repository.WebExecutionRepository;
import br.ufrn.ase.urupema.repository.factory.LogRepositoryFactory;

@Component
@Transactional
public class LogProcess {
	@Autowired
	private LogRepositoryFactory repositoryFactory;
	
	@Autowired
	private GroupAggregatorTypeFactory grouAggregatorFactory;
	
	@Autowired
	private SourceConnectionProcess sourceConnectionProcess;
	
	public Set<UriStats> countLogsGroupedByUri(SourceConnection sourceConnection) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		Set<UriStats> ret = repositoryFactory.getLogRepository(sourceConnection).countGroupedByUri(sourceConnection.getStartDate(), sourceConnection.getEndDate());
		return ret;
	}
	
	public Log getLog(SourceConnection sourceConnection, String id) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		if(sourceConnection.getLogType() == LogType.ERROR) {
			return repositoryFactory.getErrorLogRepository(sourceConnection.getSource()).findById(id, new StackTraceParser(sourceConnection.getRegexSignature(), sourceConnection.getRegexIgnoreSignature())).get();
		}
		return repositoryFactory.getLogRepository(sourceConnection).findById(id).get();
	}
	
	public void agroup(Long idSourceConnection, Collection<String> uris) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		SourceConnection sourceConnection = sourceConnectionProcess.findById(idSourceConnection);
		boolean alreadyGrouped = false;
		Set<String> noGroupeds = new HashSet<>(uris);
		for(LogGroup g : sourceConnection.getLogGroups()){
			noGroupeds.remove(g.getLabel());
			for(LogGroup subG : g.getSubGroups()){
				noGroupeds.remove(subG.getLabel());
			}
		}
		Map<String, Object> params = new HashMap<>();
		params.put("uris", noGroupeds);
		if(!alreadyGrouped){
			Map<String, WebExecutionTrace> cache = new LinkedHashMap<>();
			List<LogGroup> lgs = grouAggregatorFactory.getInstance(sourceConnection).agroup(sourceConnection, params, cache);
			cache.clear();
			for(LogGroup g : lgs) {
				g.updateElementsCount();
				g.setSourceConnection(sourceConnection);
			}
			sourceConnection.getLogGroups().addAll(lgs);
			sourceConnectionProcess.save(sourceConnection);
			sourceConnectionProcess.flush();
		}
	}
	
	public List<String> getLogIdsByProperty(SourceConnection sourceConnection, String propertyName, Object propertyValue) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		return repositoryFactory.getLogRepository(sourceConnection).findIdsByProperty(propertyName, propertyValue);
	}
	
	public Set<String> getAffectedUrisByEquivalentStackTracesAndDateBetween(SourceConnection sourceConnection, String stackTrace, String uriFragment) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		WebExecutionRepository weRepository = null;
		if(sourceConnection.getLogType() == LogType.EXECUTION || sourceConnection.getLogType() == LogType.ERROR) {
			weRepository = (WebExecutionRepository) repositoryFactory.getLogRepository(sourceConnection);
		}
		
		return weRepository.findAffectedUrisByEquivalentStackTracesAndDateBetween(stackTrace, sourceConnection.getStartDate(), sourceConnection.getEndDate(), uriFragment);
	}
}
