package br.ufrn.ase.urupema.extensible.correlator.repository;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.GroupCorrelatorType;
import br.ufrn.ase.urupema.extensible.correlator.GroupCorrelatorTypePlugin;
import br.ufrn.ase.urupema.repository.factory.LogRepositoryFactory;
import lombok.Getter;
import lombok.Setter;

@Repository
public class GroupCorrelatorTypeRepository {
	Map<String, GroupCorrelatorType> mapGroupCorrelator;
	
	@Autowired
	@Getter
	@Setter
	private LogRepositoryFactory logRepositoryFactory;
	
	public GroupCorrelatorTypeRepository(@Value("${extensionPackages}") final String extensionPackages) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		mapGroupCorrelator = new TreeMap<>();
		
		ConfigurationBuilder builder = new ConfigurationBuilder();
		List<URL> urls = new ArrayList<>(ClasspathHelper.forPackage("br.ufrn.ase.urupema"));
		if(extensionPackages != null && !extensionPackages.isBlank()) {
			for(String pack : extensionPackages.split(",")) {
				if(!pack.isBlank()) {
					urls.addAll(ClasspathHelper.forPackage(pack.trim()));
				}
			}
		}
		builder.setUrls(urls);
		Reflections reflections = new Reflections(builder);  
		Set<Class<? extends GroupCorrelatorTypePlugin>> classes = reflections.getSubTypesOf(GroupCorrelatorTypePlugin.class);
		for (Class<? extends GroupCorrelatorTypePlugin> class1 : classes) {
			if(!Modifier.isAbstract(class1.getModifiers()) && !class1.isInterface()) {
				GroupCorrelatorTypePlugin inst = class1.getConstructor().newInstance();
				mapGroupCorrelator.put(inst.getId(), inst);
			}
		}
	}
	
	public GroupCorrelatorTypePlugin findById(String id) throws InstantiationException, IllegalAccessException{
		GroupCorrelatorTypePlugin inst = (GroupCorrelatorTypePlugin) mapGroupCorrelator.get(id);
		if(inst.getLogRepositoryFactory() == null) {
			inst.setLogRepositoryFactory(logRepositoryFactory);
		}
		return inst;
	}
	
	public Collection<GroupCorrelatorType> findAll() throws InstantiationException, IllegalAccessException{
		return mapGroupCorrelator.values();
	}
}
