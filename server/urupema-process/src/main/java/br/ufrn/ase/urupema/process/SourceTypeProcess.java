package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.SourceType;
import br.ufrn.ase.urupema.extensible.sourcetype.repository.SourceTypeRepository;

@Component
public class SourceTypeProcess {
	@Autowired
	SourceTypeRepository sourceTypeRepository;
	
	public SourceType findById(String id) throws IOException, InstantiationException, IllegalAccessException {
		return sourceTypeRepository.findById(id);
	}
	
	public Collection<SourceType> findAll() throws InstantiationException, IllegalAccessException{
		return sourceTypeRepository.findAll();
	}
}
