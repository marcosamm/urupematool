package br.ufrn.ase.urupema.repository.factory;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.IssueTrackerSource;
import br.ufrn.ase.urupema.extensible.issuetrackertype.repository.IssueTrackerTypeRepository;
import br.ufrn.ase.urupema.repository.IssueTrackerRepository;

@Repository
public class IssueTrackerRepositoryFactory {
	@Autowired
	private IssueTrackerTypeRepository issueTrackerTypeRepository;
	
	public IssueTrackerRepository getIssueTrackerRepository(IssueTrackerSource issueTrackerSource) throws OperationNotSupportedException {
		try {
			return issueTrackerTypeRepository.findById(issueTrackerSource.getIssueTrackerTypeId()).getIssueTrackerRepository(issueTrackerSource.getConnectionProperties());
		} catch (IllegalAccessException | InstantiationException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
}
