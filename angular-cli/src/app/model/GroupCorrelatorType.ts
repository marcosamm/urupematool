import { GroupAggregatorType } from './GroupAggregatorType';

export interface GroupCorrelatorType {
    id: string;
    name: string;
    description: string;
    compatibleOriginAggregatorTypes: GroupAggregatorType [];
    compatibleDestinyAggregatorTypes: GroupAggregatorType [];
}
