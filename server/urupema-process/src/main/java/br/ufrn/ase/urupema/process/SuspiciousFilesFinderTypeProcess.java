package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.SuspiciousFilesFinderType;
import br.ufrn.ase.urupema.extensible.suspiciousfilesfinder.repository.SuspiciousFilesFinderTypeRepository;

@Component
public class SuspiciousFilesFinderTypeProcess {
	@Autowired
	private SuspiciousFilesFinderTypeRepository repository;
	
	public SuspiciousFilesFinderType findById(String id) throws IOException, InstantiationException, IllegalAccessException {
		return repository.findById(id);
	}
	
	public Collection<SuspiciousFilesFinderType> findAll() throws InstantiationException, IllegalAccessException{
		return repository.findAll();
	}
}
