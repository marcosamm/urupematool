package br.ufrn.ase.urupema.extensible.aggregator;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.repository.LogRepository;

public interface GroupAggregatorTypePlugin extends GroupAggregatorType {
	public void setLogRepository(LogRepository logRepository);
	public LogRepository getLogRepository();
}
