import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { SuspiciousFilesFinderType } from '../model/SuspiciousFilesFinderType';

@Injectable()
export class SuspiciousFilesFinderTypeService {
    urlGet = environment.apiUrl + '/suspiciousfilesfindertype';

    constructor(private http: HttpClient) { }

    getSuspiciousFilesFinderType(id): Observable<SuspiciousFilesFinderType> {
        return this.http.get<SuspiciousFilesFinderType>(this.urlGet + '/detail?id=' + id);
    }

    getSuspiciousFilesFinderTypes(): Observable<SuspiciousFilesFinderType[]> {
        return this.http.get<SuspiciousFilesFinderType[]>(this.urlGet + '/list');
    }
}
