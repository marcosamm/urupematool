import { Component, OnInit } from '@angular/core';

import { SourceService } from '../../service';
import { Source } from '../../model/Source';

@Component({
  selector: 'app-source',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class SourceListComponent implements OnInit {
  sources: Source[] = [];

  constructor(private sourceService: SourceService) { }

  ngOnInit() {
    this.sourceService.getSources().subscribe(sources => this.sources = sources);
  }

  deleteSource(source: Source) {
    if (window.confirm('Are sure you want to delete this item ?')) {
      this.sourceService.deleteSource(source).subscribe(
        res => { this.ngOnInit(); }
      );
    }
  }
}
