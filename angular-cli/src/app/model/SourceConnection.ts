import { Study } from './Study';
import { Source } from './Source';
import { GroupAggregatorType } from './GroupAggregatorType';
import { LogType } from './LogType.enum';
import { SuspiciousFilesFinderType } from './SuspiciousFilesFinderType';

export interface SourceConnection {
    id: number;
    name: string;
    startDate: Date;
    endDate: Date;
    regexSignature: string;
    regexIgnoreSignature: string;
    study: Study;
    source: Source;
    logType: LogType;
    groupAggregatorTypeId: string;
    groupAggregatorType: GroupAggregatorType;
    suspiciousFilesFinderTypeId: SuspiciousFilesFinderType;
    suspiciousFilesFilter: string;
    logGroups;
}
