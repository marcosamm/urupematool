--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Debian 13.2-1.pgdg100+1)
-- Dumped by pg_dump version 13.2 (Debian 13.2-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: affecteduri; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.affecteduri (
    id bigint NOT NULL,
    enabled boolean NOT NULL,
    note text,
    uri character varying(255) NOT NULL,
    reportedstacktrace_id bigint NOT NULL
);


ALTER TABLE public.affecteduri OWNER TO urupema_user;

--
-- Name: affecteduri_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.affecteduri_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.affecteduri_id_sequence OWNER TO urupema_user;

--
-- Name: changedfile; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.changedfile (
    id bigint NOT NULL,
    filechangetype character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    revision_id bigint NOT NULL,
    enabled boolean,
    note text
);


ALTER TABLE public.changedfile OWNER TO urupema_user;

--
-- Name: changedfile_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.changedfile_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.changedfile_id_sequence OWNER TO urupema_user;

--
-- Name: filestats_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.filestats_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filestats_id_sequence OWNER TO urupema_user;

--
-- Name: groupcorrelator; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.groupcorrelator (
    id bigint NOT NULL,
    groupcorrelatortypeid character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    destiny_id bigint NOT NULL,
    origin_id bigint NOT NULL,
    study_id bigint NOT NULL
);


ALTER TABLE public.groupcorrelator OWNER TO urupema_user;

--
-- Name: groupcorrelator_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.groupcorrelator_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groupcorrelator_id_sequence OWNER TO urupema_user;

--
-- Name: grouprelationship; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.grouprelationship (
    id bigint NOT NULL,
    loggroup_id bigint,
    groupcorrelator_id bigint NOT NULL
);


ALTER TABLE public.grouprelationship OWNER TO urupema_user;

--
-- Name: grouprelationship_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.grouprelationship_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grouprelationship_id_sequence OWNER TO urupema_user;

--
-- Name: grouprelationship_related; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.grouprelationship_related (
    grouprelationship_id bigint NOT NULL,
    loggroup_id bigint NOT NULL
);


ALTER TABLE public.grouprelationship_related OWNER TO urupema_user;

--
-- Name: issue; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.issue (
    id bigint NOT NULL,
    closedate timestamp without time zone NOT NULL,
    identifier character varying(255) NOT NULL,
    issuetype character varying(255),
    opendate timestamp without time zone NOT NULL,
    title character varying(255) NOT NULL,
    issuetrackerconnection_id bigint NOT NULL,
    enabled boolean,
    note text
);


ALTER TABLE public.issue OWNER TO urupema_user;

--
-- Name: issue_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.issue_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issue_id_sequence OWNER TO urupema_user;

--
-- Name: issue_loggroup; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.issue_loggroup (
    issue_id bigint NOT NULL,
    loggroup_id bigint NOT NULL
);


ALTER TABLE public.issue_loggroup OWNER TO urupema_user;

--
-- Name: issue_revision; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.issue_revision (
    issue_id bigint NOT NULL,
    revision_id bigint NOT NULL
);


ALTER TABLE public.issue_revision OWNER TO urupema_user;

--
-- Name: issuesource_connectionproperty; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.issuesource_connectionproperty (
    issuetrackersource_id bigint NOT NULL,
    value character varying(255),
    property character varying(255) NOT NULL
);


ALTER TABLE public.issuesource_connectionproperty OWNER TO urupema_user;

--
-- Name: issuesource_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.issuesource_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issuesource_id_sequence OWNER TO urupema_user;

--
-- Name: issuetrackerconnection; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.issuetrackerconnection (
    id bigint NOT NULL,
    enddate timestamp without time zone NOT NULL,
    name character varying(255) NOT NULL,
    startdate timestamp without time zone NOT NULL,
    study_id bigint NOT NULL,
    issuetrackersource_id bigint
);


ALTER TABLE public.issuetrackerconnection OWNER TO urupema_user;

--
-- Name: issuetrackerconnection_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.issuetrackerconnection_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issuetrackerconnection_id_sequence OWNER TO urupema_user;

--
-- Name: issuetrackerconnection_queryproperty; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.issuetrackerconnection_queryproperty (
    issuetrackerconnection_id bigint NOT NULL,
    value character varying(255),
    property character varying(255) NOT NULL
);


ALTER TABLE public.issuetrackerconnection_queryproperty OWNER TO urupema_user;

--
-- Name: issuetrackersource; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.issuetrackersource (
    id bigint NOT NULL,
    issuetrackertypeid character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.issuetrackersource OWNER TO urupema_user;

--
-- Name: loggroup; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.loggroup (
    id bigint NOT NULL,
    elementscount integer NOT NULL,
    label text NOT NULL,
    representantid character varying(255),
    sourceconnection_id bigint,
    supergroup_id bigint,
    firstoccurrencedate timestamp without time zone,
    lastoccurrencedate timestamp without time zone,
    typedescription character varying(255),
    groupaggregatortypeid character varying
);


ALTER TABLE public.loggroup OWNER TO urupema_user;

--
-- Name: loggroup_elementsid; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.loggroup_elementsid (
    loggroup_id bigint NOT NULL,
    elementsid character varying(255)
);


ALTER TABLE public.loggroup_elementsid OWNER TO urupema_user;

--
-- Name: loggroup_fcsf; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.loggroup_fcsf (
    loggroup_id bigint NOT NULL,
    relativesupport real,
    fcsf text NOT NULL
);


ALTER TABLE public.loggroup_fcsf OWNER TO urupema_user;

--
-- Name: loggroup_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.loggroup_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.loggroup_id_sequence OWNER TO urupema_user;

--
-- Name: loggroup_moreinformation; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.loggroup_moreinformation (
    loggroup_id bigint NOT NULL,
    value character varying(255),
    information character varying(255) NOT NULL
);


ALTER TABLE public.loggroup_moreinformation OWNER TO urupema_user;

--
-- Name: reportedstacktrace; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.reportedstacktrace (
    id bigint NOT NULL,
    enabled boolean NOT NULL,
    note text,
    stacktrace text NOT NULL,
    issue_id bigint NOT NULL
);


ALTER TABLE public.reportedstacktrace OWNER TO urupema_user;

--
-- Name: reportedstacktrace_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.reportedstacktrace_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reportedstacktrace_id_sequence OWNER TO urupema_user;

--
-- Name: revision; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.revision (
    id bigint NOT NULL,
    comment character varying(255),
    date timestamp without time zone,
    identifier character varying(255) NOT NULL
);


ALTER TABLE public.revision OWNER TO urupema_user;

--
-- Name: revision_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.revision_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.revision_id_sequence OWNER TO urupema_user;

--
-- Name: source; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.source (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    password character varying(255),
    sourcetypeid character varying(255),
    urlconnection character varying(255),
    username character varying(255),
    accessindexname character varying(255),
    accessdocumenttype character varying(255),
    executionindexname character varying(255),
    executiondocumenttype character varying(255),
    errorindexname character varying(255),
    errordocumenttype character varying(255)
);


ALTER TABLE public.source OWNER TO urupema_user;

--
-- Name: source_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.source_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.source_id_sequence OWNER TO urupema_user;

--
-- Name: sourceconnection; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.sourceconnection (
    id bigint NOT NULL,
    enddate timestamp without time zone NOT NULL,
    groupaggregatortypeid character varying(255) NOT NULL,
    logtype character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    regexignoresignature character varying(255),
    regexsignature character varying(255),
    startdate timestamp without time zone NOT NULL,
    source_id bigint NOT NULL,
    study_id bigint NOT NULL,
    suspiciousfilesfindertypeid character varying(255),
    suspiciousfilesfilter character varying(255)
);


ALTER TABLE public.sourceconnection OWNER TO urupema_user;

--
-- Name: sourceconnection_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.sourceconnection_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sourceconnection_id_sequence OWNER TO urupema_user;

--
-- Name: study; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.study (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.study OWNER TO urupema_user;

--
-- Name: study_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.study_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.study_id_sequence OWNER TO urupema_user;

--
-- Name: suspiciousfile; Type: TABLE; Schema: public; Owner: urupema_user
--

CREATE TABLE public.suspiciousfile (
    id bigint NOT NULL,
    filefrequency real NOT NULL,
    inverseaveragedistancetocrashpoint real NOT NULL,
    inversebucketfrequency real NOT NULL,
    name character varying(255) NOT NULL,
    loggroup_id bigint NOT NULL
);


ALTER TABLE public.suspiciousfile OWNER TO urupema_user;

--
-- Name: suspiciousfile_id_sequence; Type: SEQUENCE; Schema: public; Owner: urupema_user
--

CREATE SEQUENCE public.suspiciousfile_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.suspiciousfile_id_sequence OWNER TO urupema_user;

--
-- Name: affecteduri affecteduri_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.affecteduri
    ADD CONSTRAINT affecteduri_pkey PRIMARY KEY (id);


--
-- Name: changedfile changedfile_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.changedfile
    ADD CONSTRAINT changedfile_pkey PRIMARY KEY (id);


--
-- Name: groupcorrelator groupcorrelator_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.groupcorrelator
    ADD CONSTRAINT groupcorrelator_pkey PRIMARY KEY (id);


--
-- Name: grouprelationship grouprelationship_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.grouprelationship
    ADD CONSTRAINT grouprelationship_pkey PRIMARY KEY (id);


--
-- Name: issue_loggroup issue_loggroup_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issue_loggroup
    ADD CONSTRAINT issue_loggroup_pkey PRIMARY KEY (issue_id, loggroup_id);


--
-- Name: issue issue_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issue
    ADD CONSTRAINT issue_pkey PRIMARY KEY (id);


--
-- Name: issue_revision issue_revision_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issue_revision
    ADD CONSTRAINT issue_revision_pkey PRIMARY KEY (issue_id, revision_id);


--
-- Name: issue issue_uc; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issue
    ADD CONSTRAINT issue_uc UNIQUE (issuetrackerconnection_id, identifier);


--
-- Name: issuesource_connectionproperty issuesource_connectionproperty_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issuesource_connectionproperty
    ADD CONSTRAINT issuesource_connectionproperty_pkey PRIMARY KEY (issuetrackersource_id, property);


--
-- Name: issuetrackerconnection issuetrackerconnection_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issuetrackerconnection
    ADD CONSTRAINT issuetrackerconnection_pkey PRIMARY KEY (id);


--
-- Name: issuetrackerconnection_queryproperty issuetrackerconnection_queryproperty_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issuetrackerconnection_queryproperty
    ADD CONSTRAINT issuetrackerconnection_queryproperty_pkey PRIMARY KEY (issuetrackerconnection_id, property);


--
-- Name: issuetrackersource issuetrackersource_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issuetrackersource
    ADD CONSTRAINT issuetrackersource_pkey PRIMARY KEY (id);


--
-- Name: loggroup_fcsf loggroup_fcsf_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.loggroup_fcsf
    ADD CONSTRAINT loggroup_fcsf_pkey PRIMARY KEY (loggroup_id, fcsf);


--
-- Name: loggroup_moreinformation loggroup_moreinformation_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.loggroup_moreinformation
    ADD CONSTRAINT loggroup_moreinformation_pkey PRIMARY KEY (loggroup_id, information);


--
-- Name: loggroup loggroup_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.loggroup
    ADD CONSTRAINT loggroup_pkey PRIMARY KEY (id);


--
-- Name: reportedstacktrace reportedstacktrace_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.reportedstacktrace
    ADD CONSTRAINT reportedstacktrace_pkey PRIMARY KEY (id);


--
-- Name: revision revision_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.revision
    ADD CONSTRAINT revision_pkey PRIMARY KEY (id);


--
-- Name: source source_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.source
    ADD CONSTRAINT source_pkey PRIMARY KEY (id);


--
-- Name: sourceconnection sourceconnection_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.sourceconnection
    ADD CONSTRAINT sourceconnection_pkey PRIMARY KEY (id);


--
-- Name: study study_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.study
    ADD CONSTRAINT study_pkey PRIMARY KEY (id);


--
-- Name: suspiciousfile suspiciousfile_pkey; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.suspiciousfile
    ADD CONSTRAINT suspiciousfile_pkey PRIMARY KEY (id);


--
-- Name: source uk_aqjdobyi40nf4esak90f3e9sf; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.source
    ADD CONSTRAINT uk_aqjdobyi40nf4esak90f3e9sf UNIQUE (name);


--
-- Name: issuetrackersource uk_kal3ki2bs8pieg97e93vu5vso; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issuetrackersource
    ADD CONSTRAINT uk_kal3ki2bs8pieg97e93vu5vso UNIQUE (name);


--
-- Name: study uk_lp8cnuady8kkg5e5jjbwmfagw; Type: CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.study
    ADD CONSTRAINT uk_lp8cnuady8kkg5e5jjbwmfagw UNIQUE (name);


--
-- Name: issue_revision fk638h6mcfui952r3l0qvil6h16; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issue_revision
    ADD CONSTRAINT fk638h6mcfui952r3l0qvil6h16 FOREIGN KEY (issue_id) REFERENCES public.issue(id);


--
-- Name: suspiciousfile fk8nf1cpwng6teq6f1nc5uvpebg; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.suspiciousfile
    ADD CONSTRAINT fk8nf1cpwng6teq6f1nc5uvpebg FOREIGN KEY (loggroup_id) REFERENCES public.loggroup(id);


--
-- Name: affecteduri fk_affecteduri_reportedstacktrace_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.affecteduri
    ADD CONSTRAINT fk_affecteduri_reportedstacktrace_id FOREIGN KEY (reportedstacktrace_id) REFERENCES public.reportedstacktrace(id);


--
-- Name: groupcorrelator fk_groupcorrelator_sourceconnection_destiny_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.groupcorrelator
    ADD CONSTRAINT fk_groupcorrelator_sourceconnection_destiny_id FOREIGN KEY (destiny_id) REFERENCES public.sourceconnection(id);


--
-- Name: groupcorrelator fk_groupcorrelator_sourceconnection_origin_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.groupcorrelator
    ADD CONSTRAINT fk_groupcorrelator_sourceconnection_origin_id FOREIGN KEY (origin_id) REFERENCES public.sourceconnection(id);


--
-- Name: groupcorrelator fk_groupcorrelator_study_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.groupcorrelator
    ADD CONSTRAINT fk_groupcorrelator_study_id FOREIGN KEY (study_id) REFERENCES public.study(id);


--
-- Name: grouprelationship fk_grouprelationship_groupcorrelator_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.grouprelationship
    ADD CONSTRAINT fk_grouprelationship_groupcorrelator_id FOREIGN KEY (groupcorrelator_id) REFERENCES public.groupcorrelator(id);


--
-- Name: grouprelationship fk_grouprelationship_loggroup_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.grouprelationship
    ADD CONSTRAINT fk_grouprelationship_loggroup_id FOREIGN KEY (loggroup_id) REFERENCES public.loggroup(id);


--
-- Name: issue fk_issue_issuetrackerconnection_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issue
    ADD CONSTRAINT fk_issue_issuetrackerconnection_id FOREIGN KEY (issuetrackerconnection_id) REFERENCES public.issuetrackerconnection(id);


--
-- Name: issuetrackerconnection fk_issuetrackerconnection_issuetrackersource_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issuetrackerconnection
    ADD CONSTRAINT fk_issuetrackerconnection_issuetrackersource_id FOREIGN KEY (issuetrackersource_id) REFERENCES public.issuetrackersource(id);


--
-- Name: issuetrackerconnection fk_issuetrackerconnection_study_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issuetrackerconnection
    ADD CONSTRAINT fk_issuetrackerconnection_study_id FOREIGN KEY (study_id) REFERENCES public.study(id);


--
-- Name: loggroup fk_loggroup_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.loggroup
    ADD CONSTRAINT fk_loggroup_id FOREIGN KEY (supergroup_id) REFERENCES public.loggroup(id);


--
-- Name: loggroup fk_loggroup_sourceconnection_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.loggroup
    ADD CONSTRAINT fk_loggroup_sourceconnection_id FOREIGN KEY (sourceconnection_id) REFERENCES public.sourceconnection(id);


--
-- Name: reportedstacktrace fk_reportedstacktrace_issue_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.reportedstacktrace
    ADD CONSTRAINT fk_reportedstacktrace_issue_id FOREIGN KEY (issue_id) REFERENCES public.issue(id);


--
-- Name: sourceconnection fk_sourceconnection_source_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.sourceconnection
    ADD CONSTRAINT fk_sourceconnection_source_id FOREIGN KEY (source_id) REFERENCES public.source(id);


--
-- Name: sourceconnection fk_sourceconnection_study_id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.sourceconnection
    ADD CONSTRAINT fk_sourceconnection_study_id FOREIGN KEY (study_id) REFERENCES public.study(id);


--
-- Name: issue_loggroup fka7iqjaky92vn79803sxy0xhg; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issue_loggroup
    ADD CONSTRAINT fka7iqjaky92vn79803sxy0xhg FOREIGN KEY (loggroup_id) REFERENCES public.loggroup(id);


--
-- Name: grouprelationship_related fkaoa9f5r8oylo8a04a45ja1v6m; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.grouprelationship_related
    ADD CONSTRAINT fkaoa9f5r8oylo8a04a45ja1v6m FOREIGN KEY (loggroup_id) REFERENCES public.loggroup(id);


--
-- Name: loggroup_moreinformation fkbdsyrw0g4n3yfatw3ur50j0id; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.loggroup_moreinformation
    ADD CONSTRAINT fkbdsyrw0g4n3yfatw3ur50j0id FOREIGN KEY (loggroup_id) REFERENCES public.loggroup(id);


--
-- Name: issue_revision fkbn2t4mbj5j9j036kf3ww7a9sj; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issue_revision
    ADD CONSTRAINT fkbn2t4mbj5j9j036kf3ww7a9sj FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: issue_loggroup fkbq1nmrtqsi5ubxfbl10mtog8q; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issue_loggroup
    ADD CONSTRAINT fkbq1nmrtqsi5ubxfbl10mtog8q FOREIGN KEY (issue_id) REFERENCES public.issue(id);


--
-- Name: loggroup_elementsid fkeni30ayqx0h50vkcw2d5gq4f7; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.loggroup_elementsid
    ADD CONSTRAINT fkeni30ayqx0h50vkcw2d5gq4f7 FOREIGN KEY (loggroup_id) REFERENCES public.loggroup(id);


--
-- Name: issuetrackerconnection_queryproperty fkft6rnakyrh5nxx1d645xucwgq; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issuetrackerconnection_queryproperty
    ADD CONSTRAINT fkft6rnakyrh5nxx1d645xucwgq FOREIGN KEY (issuetrackerconnection_id) REFERENCES public.issuetrackerconnection(id);


--
-- Name: loggroup_fcsf fkfu0eqg7jxkhhth2j9ya1pe0cy; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.loggroup_fcsf
    ADD CONSTRAINT fkfu0eqg7jxkhhth2j9ya1pe0cy FOREIGN KEY (loggroup_id) REFERENCES public.loggroup(id);


--
-- Name: grouprelationship_related fklgrkycq0endoum7mfta3ll4w8; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.grouprelationship_related
    ADD CONSTRAINT fklgrkycq0endoum7mfta3ll4w8 FOREIGN KEY (grouprelationship_id) REFERENCES public.grouprelationship(id);


--
-- Name: changedfile fko7spnwllmdbpbeo25q8qaew5t; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.changedfile
    ADD CONSTRAINT fko7spnwllmdbpbeo25q8qaew5t FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: issuesource_connectionproperty fksq4wud9rb7t13vhai41dj3abk; Type: FK CONSTRAINT; Schema: public; Owner: urupema_user
--

ALTER TABLE ONLY public.issuesource_connectionproperty
    ADD CONSTRAINT fksq4wud9rb7t13vhai41dj3abk FOREIGN KEY (issuetrackersource_id) REFERENCES public.issuetrackersource(id);


--
-- PostgreSQL database dump complete
--

