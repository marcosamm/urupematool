import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLogGroupsComponent } from './listloggroups.component';

describe('LogGroupComponent', () => {
  let component: ListLogGroupsComponent;
  let fixture: ComponentFixture<ListLogGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListLogGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLogGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
