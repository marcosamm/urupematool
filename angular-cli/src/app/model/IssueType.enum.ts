export enum IssueType {
    BUG = 'BUG',
    SUPPORT = 'SUPPORT',
    FEATURE = 'FEATURE'
}
