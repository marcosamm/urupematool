package br.ufrn.ase.urupema.suspicious;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.SimpleWebExecutionTrace;
import br.ufrn.ase.urupema.domain.SuspiciousFile;
import br.ufrn.ase.urupema.domain.SuspiciousFilesFinderType;
import br.ufrn.ase.urupema.extensible.aggregator.GroupAggregatorByEquivalentSignatureAndStacktrace;
import br.ufrn.ase.urupema.extensible.suspiciousfilesfinder.SuspiciousFilesFinderTypePlugin;
import br.ufrn.ase.urupema.parser.StackTraceParser;

public class SuspiciousFilesFinder implements SuspiciousFilesFinderTypePlugin {
	private StackTraceParser stp = new StackTraceParser();
	
	public SuspiciousFilesFinder() {
	}
	
	@Override
	public String getId() {
		return "urupema:suspiciousfiles";
	}
	
	@Override
	public String getName() {
		return "Urupema Suspicious Files";
	}

	@Override
	public String getDescription() {
		return "Urupema approach to rank suspicious files";
	}

	@Override
	public int compareTo(SuspiciousFilesFinderType o) {
		return getId().compareTo(o.getId());
	}
	
	public Set<SuspiciousFile> getSuspiciousFiles(LogGroup logGroup) throws IOException{
		Set<LogGroup> brotherGroups = new HashSet<>();
		if(logGroup.getSuperGroup() != null){
			brotherGroups.addAll(logGroup.getSuperGroup().getSubGroups());
			brotherGroups.remove(logGroup);
		} else {
			brotherGroups.addAll(logGroup.getSourceConnection().getLogGroups());
			brotherGroups.remove(logGroup);
		}
		return getSuspiciousFiles(logGroup, brotherGroups);
	}
	
	private Set<SuspiciousFile> getSuspiciousFiles(LogGroup logGroup, Set<LogGroup> brotherGroups) throws IOException{
		if(brotherGroups.contains(logGroup)) {
			throw new IllegalArgumentException("logGroup in brotherGroups");
		}
		Set<FileStats> suspFiles = null;
		Map<LogGroup, Set<String>> mapLogGroupFiles = new HashMap<>();
		
		suspFiles = getFileStats(logGroup);
		mapLogGroupFiles.put(logGroup, suspFiles.stream().map(sf -> sf.getName()).collect(Collectors.toSet()));
		
		for (LogGroup brotherGroup : brotherGroups) {
			Set<FileStats> lgSuspFiles = getFileStats(brotherGroup);
			mapLogGroupFiles.put(brotherGroup, lgSuspFiles.stream().map(sf -> sf.getName()).collect(Collectors.toSet()));
		}
		
		Set<LogGroup> allGroups = new HashSet<>(brotherGroups);
		allGroups.add(logGroup);
		for (FileStats suspFile : suspFiles) {
			suspFile.setNumGroups(0);
			suspFile.setTotalElements(logGroup.getElementsCount());
			suspFile.setTotalGroups(brotherGroups.size() + 1);
			for (LogGroup group : allGroups) {
				//Incrementa o número de grupos que contem o arquivo pelo menos uma vez
				Set<String> lgFiles = mapLogGroupFiles.get(group);
				if(lgFiles.contains(suspFile.getName())){
					suspFile.setNumGroups(suspFile.getNumGroups() + 1);
				}
			}
		}
		System.out.println("##### " + logGroup.getId() + " #####");
		printSuspFiles(suspFiles);
		
		Set<SuspiciousFile> sfs = new TreeSet<>();
		for (FileStats suspFile : suspFiles) {
			SuspiciousFile sf = new SuspiciousFile(suspFile.getName(), suspFile.getInverseAverageDistanceToCrashPoint(), suspFile.getInverseBucketFrequency(), suspFile.getFileFrequency());
			sfs.add(sf);
		}
		
		return sfs;
	}
	
	private void printSuspFiles(Set<FileStats> suspFiles) {
		List<FileStats> listScore = new ArrayList<>(suspFiles);
		listScore.sort(new Comparator<FileStats>() {
			@Override
			public int compare(FileStats o1, FileStats o2) {
				return o2.getScore().compareTo(o1.getScore());
			}
		});
		
		List<FileStats> listScore1 = new ArrayList<>(suspFiles);
		listScore1.sort(new Comparator<FileStats>() {
			@Override
			public int compare(FileStats o1, FileStats o2) {
				return o2.getScore1().compareTo(o1.getScore1());
			}
		});
		
		System.out.print("\""); //---- SCORE ----
		listScore.stream().limit(10).forEach(fs -> System.out.println(fs.getName() + " (" + String.format("%02.3f", fs.getScore()) + ")"));
		System.out.print("\"\t");
		
		System.out.print("\""); //---- SCORE 1 ----
		listScore1.stream().limit(10).forEach(fs -> System.out.println(fs.getName() + " (" + String.format("%02.3f", fs.getScore1()) + ")"));
		System.out.print("\"\t");
		
		System.out.print("\""); //---- SCORE SÓ COM PACOTE BR ----
		listScore.stream().filter(file -> file.getName().startsWith("br")).limit(10).forEach(fs -> System.out.println(fs.getName() + " (" + String.format("%02.3f", fs.getScore()) + ")"));
		System.out.print("\"\t");
		
		System.out.print("\""); //---- SCORE 1 SÓ COM PACOTE BR ----
		listScore1.stream().filter(file -> file.getName().startsWith("br")).limit(10).forEach(fs -> System.out.println(fs.getName() + " (" + String.format("%02.3f", fs.getScore1()) + ")"));
		System.out.print("\"");
		
		System.out.println("\n\n");
	}

	/**
	 * Retorna o conjunto de arquivos contidos nas stack traces do grupo de log informado (incluindo seus subGroupos).
	 * @param logGroup O grupo para o qual se deseja recuperar a lista de arquivos presentes nas stack traces (incluindo as de seus subGrupos).
	 * @return O conjunto de arquivos (com dados estatísticos) que apareceram em alguma stack trace do grupo.
	 * @throws IOException
	 */
	private Set<FileStats> getFileStats(LogGroup logGroup) throws IOException{
		Set<FileStats> files = new TreeSet<>();
		if(!logGroup.getGroupAggregatorTypeId().equals(GroupAggregatorByEquivalentSignatureAndStacktrace.ID) && !logGroup.getSubGroups().isEmpty()) {
			for(LogGroup subGroup : logGroup.getSubGroups()) {
				for(FileStats fs: getFileStats(subGroup)) {
					Optional<FileStats> of = files.stream().filter(file -> file.getName().equals(fs.getName())).limit(1).findFirst();
					if(of.isPresent()) {
						FileStats file = of.get();
						file.addTopFrameDists(fs.getTopFrameDists());
						file.setNumElements(file.getNumElements() + fs.getNumElements());
						file.setNumGroups(file.getNumGroups() + fs.getNumGroups());
					}else {
						files.add(fs);
					}
				}
			}
		}else {
			SimpleWebExecutionTrace representant = stp.toSimpleError(logGroup.getLabel());
			for(Map.Entry<String, Integer> entry : representant.getQualifiedFileNamesAndMinTopFrameDist().entrySet()) {
				FileStats fs = new FileStats(entry.getKey(), 1, logGroup.getElementsCount());
				fs.addTopFrameDists(entry.getValue(), logGroup.getElementsCount());
				files.add(fs);
			}
		}
		return files;
	}

	
}
