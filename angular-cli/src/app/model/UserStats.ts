export interface UserStats {
    identifier: string;
    count: number;
}
