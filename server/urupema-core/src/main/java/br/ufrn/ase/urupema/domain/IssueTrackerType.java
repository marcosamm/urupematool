package br.ufrn.ase.urupema.domain;

import java.util.Map;
import java.util.Set;

public interface IssueTrackerType extends Comparable<IssueTrackerType>{
	public String getId();	
	public String getName();
	public Set<Property> getConnectionProperties();
	public Set<Property> getQueryProperties();
	public void validateConnectionProperties(Map<String, String> attributes);
	public void validateQueryProperties(Map<String, String> attributes);
}
