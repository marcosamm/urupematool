
import { Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { SourceTypeComponent } from './sourcetype/sourcetype.component';

import { GroupCorrelatorTypeComponent } from './groupcorrelatortype/groupcorrelatortype.component';
import { GroupAggregatorTypeComponent } from './groupaggregatortype/groupaggregatortype.component';
import { IssueTrackerTypeComponent } from './issuetrackertype/issuetrackertype.component';
import { SuspiciousFilesFinderTypeComponent } from './suspiciousfilesfindertype/suspiciousfilesfindertype.component';

/**
 * Define the routes
 */
export const appRoutes: Routes = [
  { path: '',          component: HomeComponent },
  { path: 'issuetrackertype', component: IssueTrackerTypeComponent },
  { path: 'sourcetype', component: SourceTypeComponent },
  { path: 'groupaggregatortype', component: GroupAggregatorTypeComponent },
  { path: 'groupcorrelatortype', component: GroupCorrelatorTypeComponent },
  { path: 'suspiciousfilesfindertype', component: SuspiciousFilesFinderTypeComponent }
];
