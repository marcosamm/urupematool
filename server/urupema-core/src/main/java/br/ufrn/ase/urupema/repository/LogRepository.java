/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ElasticScenarioExtractor.java 
 * 4 de nov de 2017
 */
package br.ufrn.ase.urupema.repository;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import br.ufrn.ase.urupema.domain.Log;
import br.ufrn.ase.urupema.domain.UriStats;

public interface LogRepository {
	Optional<? extends Log> findById(String id) throws IOException;
	Set<UriStats> countGroupedByUri(Date start, Date end) throws IOException;
	List<String> findIdsByDateBetween(Date start, Date end) throws IOException;
	List<String> findIdsByUriAndDateBetween(String uri, Date start, Date end) throws IOException;
	List<String> findIdsByProperty(String propertyName, Object value) throws IOException;
}
