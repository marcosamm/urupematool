import { IssueType } from './IssueType.enum';
import { IssueTrackerConnection } from './IssueTrackerConnection';
import { LogGroup } from './LogGroup';
import { Revision } from './Revision';
import { AffectedUri } from './AffectedUri';

export interface ReportedStackTrace {
    id: number;
    stackTrace: string;
    enabled: boolean;
    note: string;
    affectedUris: AffectedUri [];
}
