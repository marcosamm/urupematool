import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IssueService, IssueTrackerConnectionService, StudyService, AffectedUriService, ChangedFileService } from '../../service';
import { IssueTrackerConnection, Study, Issue } from '../../model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-listissues',
  templateUrl: './listissues.component.html',
  styleUrls: ['./listissues.component.css']
})
export class ListIssuesComponent implements OnInit {
  form: FormGroup;
  issueTrackerConnection: IssueTrackerConnection;
  study: Study;
  mapEnabledReportedStackTraceCount: Map<Issue, number> = new Map();
  mapEnabledChangedFilesCount: Map<Issue, number> = new Map();
  mapChangedFilesCount: Map<Issue, number> = new Map();
  mapEnabledAffectedUris: Map<Issue, Set<string>> = new Map();
  mapAffectedUris: Map<Issue, Set<string>> = new Map();

  totalEnabledChangedFiles: number;
  totalChangedFiles: number;
  totalEnabledStackTraces: number;
  totalStackTraces: number;
  affectedUris: Set<string> = new Set();
  enabledAffectedUris: Set<string> = new Set();

  constructor(
    private route: ActivatedRoute,
    private issueTrackerConnectionService: IssueTrackerConnectionService,
    private issueService: IssueService,
    private studyService: StudyService,
    private affectedUriService: AffectedUriService,
    private changedFileService: ChangedFileService,
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      sourceConnectionId: ['', [Validators.required]],
      uriFragment: ['', []],
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studyService.getStudy(params['idStudy']).subscribe(
        study => {
          this.study = study;
        }
      );
      this.issueTrackerConnectionService.getIssueTrackerConnection(params['idIssueTrackerConnection']).subscribe(
        issueTrackerConnection => {
          this.issueTrackerConnection = issueTrackerConnection;
          this.issueService.getIssuesByIssueTrackerConnectionId(this.issueTrackerConnection).subscribe(
            issues => {
              this.issueTrackerConnection.issues = issues.sort(
                (i1, i2) => i1.openDate > i2.openDate ? -1 : i1.openDate < i2.openDate ? 1 : 0
              );
              this.totalStackTraces = 0;
              for (const issue of this.issueTrackerConnection.issues) {
                this.issueService.getReportedStackTracesByIssue(issue).subscribe(
                  reportedStackTraces => {
                    issue.reportedStackTraces = reportedStackTraces;
                    reportedStackTraces.forEach(reportedStackTrace => {
                      this.affectedUriService.getAffectedUrisByReportedStackTrace(reportedStackTrace).subscribe(
                        affectedUris => {
                          reportedStackTrace.affectedUris = affectedUris;
                          affectedUris.forEach(affectedUri => {
                            this.affectedUris.add(affectedUri.uri);
                            if (issue.enabled && reportedStackTrace.enabled && affectedUri.enabled){
                              this.enabledAffectedUris.add(affectedUri.uri);
                            }
                          });
                        }
                      );
                    });
                    this.totalStackTraces += issue.reportedStackTraces.length;
                  }
                );
                this.issueService.getRevisionsByIssue(issue).subscribe(
                  revisions => {
                    issue.revisions = revisions;
                    issue.revisions.forEach(revision => {
                      this.changedFileService.getChangedFilesByRevision(revision).subscribe(
                        changedFiles => {
                          revision.changedFiles = changedFiles;
                        }
                      );
                    });
                  }
                );
              }
            }
          );
        }
      );
    });
  }

  enabledReportedStackTracesCount(issue: Issue): number {
    if (!this.mapEnabledReportedStackTraceCount.has(issue)) {
      let count = 0;
      issue.reportedStackTraces.forEach( reportedStackTrace => {
        if (reportedStackTrace.enabled) {
          count ++;
        }
      });
      this.mapEnabledReportedStackTraceCount.set(issue, count);
    }
    return this.mapEnabledReportedStackTraceCount.get(issue);
  }

  enabledAffectedUrisCount(issue: Issue): number {
    if (!this.mapEnabledAffectedUris.has(issue)) {
      const set: Set<string> = new Set();
      const setEnabled: Set<string> = new Set();
      issue.reportedStackTraces.forEach( reportedStackTrace => {
        reportedStackTrace.affectedUris.forEach(affectedUri => {
          set.add(affectedUri.uri);
          if (reportedStackTrace.enabled && affectedUri.enabled) {
            setEnabled.add(affectedUri.uri);
          }
        });
      });
      this.mapAffectedUris.set(issue, set);
      this.mapEnabledAffectedUris.set(issue, setEnabled);
    }
    return this.mapEnabledAffectedUris.get(issue).size;
  }

  affectedUrisCount(issue: Issue): number {
    if (!this.mapAffectedUris.has(issue)) {
      this.enabledAffectedUrisCount(issue);
    }
    return this.mapAffectedUris.get(issue).size;
  }

  enabledChangedFilesCount(issue: Issue): number {
    if (!this.mapEnabledChangedFilesCount.has(issue)) {
      const set: Set<string> = new Set();
      const setEnabled: Set<string> = new Set();
      issue.revisions.forEach( revision => {
        revision.changedFiles.forEach(changedFile => {
          set.add(changedFile.name);
          if (changedFile.enabled) {
            setEnabled.add(changedFile.name);
          }
        });
      });
      this.mapChangedFilesCount.set(issue, set.size);
      this.mapEnabledChangedFilesCount.set(issue, setEnabled.size);
    }
    return this.mapEnabledChangedFilesCount.get(issue);
  }

  changedFilesCount(issue: Issue): number {
    if (!this.mapChangedFilesCount.has(issue)) {
      this.enabledChangedFilesCount(issue);
    }
    return this.mapChangedFilesCount.get(issue);
  }

  totalCalculate() {
    this.totalEnabledStackTraces = 0;
    this.totalEnabledChangedFiles = 0;
    this.totalChangedFiles = 0;

    this.issueTrackerConnection.issues.forEach(issue => {
      this.totalEnabledStackTraces += this.enabledReportedStackTracesCount(issue);
      this.totalEnabledChangedFiles += this.enabledChangedFilesCount(issue);
      this.totalChangedFiles += this.changedFilesCount(issue);
    });
    return true;
  }

  importIssues() {
    this.issueTrackerConnectionService.importIssues(this.issueTrackerConnection).subscribe(result => {
      this.ngOnInit();
    });
  }

  importAffectedUris() {
    this.issueTrackerConnectionService.importAffectedUris(
      this.issueTrackerConnection.id,
      this.form.get('sourceConnectionId').value,
      this.form.get('uriFragment').value
    ).subscribe(result => {
      this.ngOnInit();
    });
  }

  agroupLogs() {
    this.issueTrackerConnectionService.agroupLogs(
      this.issueTrackerConnection.id,
      this.form.get('sourceConnectionId').value
    ).subscribe(result => {
      this.ngOnInit();
    });
  }

  matchIssueWithLogGroups() {
    this.issueTrackerConnectionService.matchIssueWithLogGroups(
      this.issueTrackerConnection.id,
      this.form.get('sourceConnectionId').value
    ).subscribe(result => {
      this.ngOnInit();
    });
  }
}
