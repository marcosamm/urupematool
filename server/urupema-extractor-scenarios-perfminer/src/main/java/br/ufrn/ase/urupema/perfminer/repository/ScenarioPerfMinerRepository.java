/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioRepository.java
 * 31 de out de 2017
 */
package br.ufrn.ase.urupema.perfminer.repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.UriStats;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.perfminer.domain.Scenario;
import br.ufrn.ase.urupema.repository.ScenarioRepository;
import lombok.Getter;
import lombok.Setter;

/**
 * Represents a repository of scenarios
 *
 */
public interface ScenarioPerfMinerRepository extends ScenarioRepository, JpaRepository<Scenario, Long> {
	@Getter
	@Setter
	class GroupStacktraceIds {
		String trace;
		String ids;
		
		public GroupStacktraceIds(String trace, String ids) {
			this.trace = trace;
			this.ids = ids;
		}
	}
	
	@Override
	List<String> findIdsByDateBetween(Date start, Date end) throws IOException;
	
	@Override
	@Query("select s.id from Scenario s where s.requestURL like '%' || ?1 || '%' and s.date between ?1 and ?2 order by s.id")
	List<String> findIdsByUriAndDateBetween(String uri, Date start, Date end) throws IOException;
	
	@Override
	@Query("select s from Scenario s inner join fetch s.root where s.id = CAST(?1 as long)")
	Optional<WebExecutionTrace> findById(String id);
	
	@Override
	@Query("select new br.ufrn.ase.urupema.domain.UriStats( CASE WHEN (LOCATE('?', s.requestURL) = 0) THEN SUBSTRING(s.requestURL, LOCATE('/', s.requestURL, 9)) ELSE SUBSTRING(s.requestURL, LOCATE('/', s.requestURL, 9), LOCATE('?', s.requestURL)) END AS URI, count(s)) from Scenario s where s.date between ?1 and ?2 group by CASE WHEN (LOCATE('?', s.requestURL) = 0) THEN SUBSTRING(s.requestURL, LOCATE('/', s.requestURL, 9)) ELSE SUBSTRING(s.requestURL, LOCATE('/', s.requestURL, 9), LOCATE('?', s.requestURL)) END")
	Set<UriStats> countGroupedByUri(Date start, Date end);
	
	//TODO Testar se isso funciona
	@Override
	@Query("select new br.ufrn.ase.urupema.domain.UriStats( CASE WHEN (LOCATE('?', s.requestURL) = 0) THEN SUBSTRING(s.requestURL, LOCATE('/', s.requestURL, 9)) ELSE SUBSTRING(s.requestURL, LOCATE('/', s.requestURL, 9), LOCATE('?', s.requestURL)) END AS URI, count(s)) from Scenario s where CAST (s.id as text) in ?1 group by CASE WHEN (LOCATE('?', s.requestURL) = 0) THEN SUBSTRING(s.requestURL, LOCATE('/', s.requestURL, 9)) ELSE SUBSTRING(s.requestURL, LOCATE('/', s.requestURL, 9), LOCATE('?', s.requestURL)) END")
	Set<UriStats> countGroupedByUri(Set<String> ids);

	@Query(nativeQuery=true, value=""+
			"SELECT " + 
			"    (WITH note_tree AS " + 
			"        (WITH RECURSIVE node_from_parents AS " + 
			"            ( " + 
			"                SELECT parent_id, id, '\t' AS prefix, member || '\n' AS stack " + 
			"                FROM node " + 
			"                WHERE id = s.root_id " + 
			"                UNION ALL " + 
			"                SELECT c.parent_id, c.id, CONCAT(prefix, '\t') AS prefix, CONCAT(prefix, c.member, '\n') AS stack " + 
			"                FROM node_from_parents p " + 
			"                JOIN node c " + 
			"                ON c.parent_id = p.id " + 
			"            ) " + 
			"            SELECT parent_id, stack, id, prefix " + 
			"            FROM node_from_parents " + 
			"            ORDER BY id " + 
			"        ) SELECT STRING_AGG(stack, '') FROM note_tree " + 
			"    ) AS executiontrace, " + 
			"    array_to_string(ARRAY_AGG (CAST(s.id AS text)),',') AS ids " + 
			"FROM " + 
			"    scenario s " + 
			"WHERE " + 
			"    s.date BETWEEN :start AND :end " + 
			"GROUP BY 1")
	List<Object> agroupIdsByStackTrace(@Param("start") Date start, @Param("end") Date end);
	
	@Query(nativeQuery=true, value=""+
			"SELECT " + 
			"    (WITH note_tree AS " + 
			"        (WITH RECURSIVE node_from_parents AS " + 
			"            ( " + 
			"                SELECT parent_id, id, '\t' AS prefix, member || '\n' AS stack " + 
			"                FROM node " + 
			"                WHERE id = s.root_id " + 
			"                UNION ALL " + 
			"                SELECT c.parent_id, c.id, CONCAT(prefix, '\t') AS prefix, CONCAT(prefix, c.member, '\n') AS stack " + 
			"                FROM node_from_parents p " + 
			"                JOIN node c " + 
			"                ON c.parent_id = p.id " + 
			"            ) " + 
			"            SELECT parent_id, stack, id, prefix " + 
			"            FROM node_from_parents " + 
			"            ORDER BY id " + 
			"        ) SELECT STRING_AGG(stack, '') FROM note_tree " + 
			"    ) AS executiontrace, " + 
			"    array_to_string(ARRAY_AGG (CAST(s.id AS text)),',') AS ids " + 
			"FROM " + 
			"    scenario s " + 
			"WHERE " +  
			"  s.request_url like '%' || :uri || '%' AND " + 
			"  s.date BETWEEN :start AND :end " + 
			"GROUP BY 1")
	List<Object> agroupIdsByUriSubgroupByStackTrace(@Param("uri") String uri, @Param("start") Date start, @Param("end") Date end);
	
	@Override
	default List<LogGroup> agroupByStackTrace(Set<String> uris, Date start, Date end) throws IOException {
		//TODO uris não esta sendo usada para nada
		List<LogGroup> subGroups = new ArrayList<>();
		
		List<Object> res = agroupIdsByStackTrace(start, end);
		for(Object g : res) {
			LogGroup subGroup = new LogGroup(((Object[])g)[0].toString());
			subGroup.setElementsId(new TreeSet<String>(Arrays.asList(((Object[])g)[1].toString().split(","))));
			subGroup.setRepresentantId(subGroup.getElementsId().stream().findFirst().get());
			subGroups.add(subGroup);
		}
		
		return subGroups;
	}
	
	@Override
	default LogGroup agroupByUriSubgroupByStackTrace(String uri, Date start, Date end) throws IOException {
		LogGroup uriGroup = new LogGroup(uri);
		Set<LogGroup> subGroups = new HashSet<>();
		
		List<Object> res = agroupIdsByUriSubgroupByStackTrace(uri, start, end);
		for(Object g : res) {
			LogGroup subGroup = new LogGroup(((Object[])g)[0].toString());
			subGroup.setElementsId(new TreeSet<String>(Arrays.asList(((Object[])g)[1].toString().split(","))));
			subGroup.setRepresentantId(subGroup.getElementsId().stream().findFirst().get());
			subGroups.add(subGroup);
			uriGroup.addElementsId(subGroup.getElementsId());
		}
		
		uriGroup.setSubGroups(subGroups);
		return uriGroup;
	}
	
	@Override
	@Query("select n.id from Node n, Scenario s where n.id = s.root.id and s.id = CAST(?1 as long)")
	Optional<Long> findNodeRootIdByScenarioId(String idScenario);
	
	@Query(nativeQuery=true, value=""+
			"WITH RECURSIVE node_tree (parent_id, id, member) AS " + 
			"( " + 
			"  SELECT f.parent_id, f.id, member " + 
			"  FROM node f " + 
			"  WHERE f.id = :ancestorId " + 
			"  UNION ALL " + 
			"  SELECT f2.parent_id, f2.id, f2.member " + 
			"  FROM node AS f2 " + 
			"  INNER JOIN node_tree AS f3 " + 
			"  ON f2.parent_id = f3.id " + 
			") " + 
			"SELECT DISTINCT cast(nt.id as text) " + 
			"FROM " + 
			"  node_tree nt " + 
			"WHERE  " + 
			"  nt.member LIKE :childMemberSignature ||'%'")
	List<String> findMinChildNodeIdByIdScenarioAndChildAncestorIdChildMemberSignature(@Param("ancestorId") Long ancestorId, @Param("childMemberSignature") String childMemberSignature);
	
	default List<String> findIdsByProperty(String propertyName, Object value) throws IOException {
		return new ArrayList<String>();
	}
}
