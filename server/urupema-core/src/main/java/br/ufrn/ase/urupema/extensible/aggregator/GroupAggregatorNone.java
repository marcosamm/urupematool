package br.ufrn.ase.urupema.extensible.aggregator;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.repository.LogRepository;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupAggregatorNone implements GroupAggregatorTypePlugin {
	public static String ID = "none";
	
	private LogRepository logRepository;
	private List<LogType> logTypes = Arrays.asList(LogType.ACCESS);
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getName() {
		return "None";
	}

	@Override
	public String getDescription() {
		return "Does not aggregate";
	}

	@Override
	public int compareTo(GroupAggregatorType o) {
		return getId().compareTo(o.getId());
	}

	@Override
	public List<LogGroup> agroup(SourceConnection sourceConnection, Map<String, Object> params, Map<String, WebExecutionTrace> cache) throws IOException {
		return null;
	}
}
