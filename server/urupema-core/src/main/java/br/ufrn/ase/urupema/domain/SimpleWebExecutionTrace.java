package br.ufrn.ase.urupema.domain;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimpleWebExecutionTrace extends AbstractWebExcecutionTrace implements WebExecutionTrace{
	private String stackTrace;
	private List<TraceElement> traceElements;

	@Override
	public String getIdentifier() {
		return null;
	}

	@Override
	public String getRequestURL() {
		return null;
	}

	@Override
	public Date getDate() {
		return null;
	}

	@Override
	public LogType getLogType() {
		return LogType.ERROR;
	}
	
	@Override
	public String getStackTrace() {
		return stackTrace;
	}

	@Override
	public List<TraceElement> getTraceElements() {
		return traceElements;
	}

	@Override
	public String getExceptionMessage() {
		return null;
	}
}
