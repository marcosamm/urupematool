import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { AffectedUri } from '../model/AffectedUri';
import { ReportedStackTrace } from '../model';

@Injectable()
export class AffectedUriService {
  urlGet = environment.apiUrl + '/affecteduri';

  constructor(private http: HttpClient) { }

  getAffectedUri(id: number): Observable<AffectedUri> {
    return this.http.get<AffectedUri>(this.urlGet + '?id=' + id);
  }

  getAffectedUrisByReportedStackTrace(reportedStackTrace: ReportedStackTrace): Observable<AffectedUri[]> {
    return this.http.get<AffectedUri[]>(this.urlGet + '/listbyreportedstacktrace?idReportedStackTrace=' + reportedStackTrace.id);
  }

  createAffectedUri(
    reportedStackTraceId: number,
    uri: string,
    enabled: boolean,
    note: string
  ) {
    const affectedUri = {
      reportedStackTrace: { id: reportedStackTraceId },
      uri: uri,
      note: note,
      enabled: enabled
    };
    return this.http.post(this.urlGet, affectedUri);
  }

  updateAffectedUri(
    id: number,
    uri: string,
    enabled: boolean,
    note: string
  ) {
    const affectedUri = {
      id: id,
      uri: uri,
      note: note,
      enabled: enabled
    };
    return this.http.put(this.urlGet, affectedUri);
  }

  deleteAffectedUri(affectedUri: AffectedUri) {
    return this.http.delete(this.urlGet + '?id=' + affectedUri.id);
  }
}
