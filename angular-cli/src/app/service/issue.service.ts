import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Issue, IssueType, IssueTrackerConnection, AffectedUri, Revision, LogGroup } from '../model';
import { ReportedStackTrace } from '../model/ReportedStackTrace';

@Injectable()
export class IssueService {
  urlGet = environment.apiUrl + '/issue';

  constructor(private http: HttpClient) { }

  getIssue(id: number): Observable<Issue> {
    return this.http.get<Issue>(this.urlGet + '?id=' + id);
  }

  createIssue(
    issueTrackerConnectionId: number,
    identifier: string,
    title: string,
    openDate: Date,
    closeDate: Date,
    enabled: boolean,
    note: string,
    issueType: IssueType
  ) {
    const issue = {
      issueTrackerConnection: { id: issueTrackerConnectionId },
      identifier: identifier,
      title: title,
      openDate: openDate,
      closeDate: closeDate,
      enabled: enabled,
      note: note,
      issueType: issueType
    };
    return this.http.post(this.urlGet, issue);
  }

  updateIssue(
    id: number,
    issueTrackerConnectionId: number,
    identifier: string,
    title: string,
    openDate: Date,
    closeDate: Date,
    enabled: boolean,
    note: string,
    issueType: IssueType
  ) {
    const issue = {
      id: id,
      issueTrackerConnection: { id: issueTrackerConnectionId },
      identifier: identifier,
      title: title,
      openDate: openDate,
      closeDate: closeDate,
      enabled: enabled,
      note: note,
      issueType: issueType
    };
    return this.http.put(this.urlGet, issue);
  }

  deleteIssue(issue: Issue) {
    return this.http.delete(this.urlGet + '?id=' + issue.id);
  }

  getIssuesByIssueTrackerConnectionId(issueTrackerConnection: IssueTrackerConnection): Observable<Issue[]> {
    return this.http.get<Issue[]>(
      this.urlGet + '/listbyissuetrackerconnection?idIssueTrackerConnection=' + issueTrackerConnection.id
    );
  }

  getReportedStackTracesByIssue(issue: Issue): Observable<ReportedStackTrace[]> {
    return this.http.get<ReportedStackTrace[]>(
      this.urlGet + '/listreportedstacktraces?idIssue=' + issue.id
    );
  }

  getAffectedUrisByIssue(issue: Issue): Observable<AffectedUri[]> {
    return this.http.get<AffectedUri[]>(
      this.urlGet + '/listenabledaffecteduris?idIssue=' + issue.id
    );
  }

  getRevisionsByIssue (issue: Issue): Observable<Revision[]> {
    return this.http.get<Revision[]>(
      this.urlGet + '/listrevisions?idIssue=' + issue.id
    );
  }

  getLogGroupsByIssue (issue: Issue): Observable<LogGroup[]> {
    return this.http.get<LogGroup[]>(
      this.urlGet + '/listloggroups?idIssue=' + issue.id
    );
  }

  getIssueTrackerConnectionByIssue(issue: Issue): Observable<IssueTrackerConnection> {
    return this.http.get<IssueTrackerConnection>(
      this.urlGet + '/issuetrackerconnection?idIssue=' + issue.id
    );
  }
}
