import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { StudyService, IssueTrackerConnectionService, SourceConnectionService, GroupCorrelatorService } from '../../service';
import { Study } from '../../model';
import { SourceConnection } from '../../model/SourceConnection';
import { GroupCorrelator } from '../../model/GroupCorrelator';
import { IssueTrackerConnection } from '../../model/IssueTrackerConnection';

@Component({
  selector: 'app-studydetail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class StudyDetailComponent implements OnInit {
  study: Study;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private studyService: StudyService,
    private issueTrackerConnectionService: IssueTrackerConnectionService,
    private sourceConnectionService: SourceConnectionService,
    private groupCorrelatorService: GroupCorrelatorService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studyService.getStudy(params['id']).subscribe(
        dados => {
          this.study = dados;
        }
      );
    });
  }

  deleteIssueTrackerConnection(issueTrackerConnection: IssueTrackerConnection) {
    if (window.confirm('Are sure you want to delete this item ?')) {
      this.issueTrackerConnectionService.deleteIssueTrackerConnection(issueTrackerConnection).subscribe(
        res => { this.ngOnInit(); }
      );
    }
  }

  deleteSourceConnection(sourceConnection: SourceConnection) {
    if (window.confirm('Are sure you want to delete this item ?')) {
      this.sourceConnectionService.deleteSourceConnection(sourceConnection).subscribe(
        res => { this.ngOnInit(); }
      );
    }
  }

  deleteGroupCorrelator(grouCorrelator: GroupCorrelator) {
    if (window.confirm('Are sure you want to delete this item ?')) {
      this.groupCorrelatorService.deleteGroupCorrelator(grouCorrelator).subscribe(
        res => { this.ngOnInit(); }
      );
    }
  }

  relate(grouCorrelator: GroupCorrelator) {
    this.groupCorrelatorService.relate(grouCorrelator).subscribe(
      res => {
        this.router.navigate(['/groupcorrelator/listgrouprelationships/' + grouCorrelator.id]);
      }
    );
  }
}
