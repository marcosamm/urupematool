package br.ufrn.ase.urupema.dtos;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(of = {"name"})
public class CalledMethod implements Serializable, Comparable<CalledMethod> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String name;
	private int numberOfCalls;
	private boolean found;
	
	public CalledMethod(String name, int numberOfCalls, boolean found) {
		super();
		this.name = name;
		this.numberOfCalls = numberOfCalls;
		this.found = found;
	}

	@Override
	public int compareTo(CalledMethod arg0) {
		int cmp = numberOfCalls - arg0.getNumberOfCalls();
		
		if(cmp == 0) {
			cmp = name.compareTo(arg0.getName());
		}
		
		return cmp;
	}
}
