package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.naming.OperationNotSupportedException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.AffectedUri;
import br.ufrn.ase.urupema.domain.CalledMethodStats;
import br.ufrn.ase.urupema.domain.ChangedFile;
import br.ufrn.ase.urupema.domain.FileChangeType;
import br.ufrn.ase.urupema.domain.Issue;
import br.ufrn.ase.urupema.domain.IssueTrackerConnection;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.ReportedStackTrace;
import br.ufrn.ase.urupema.domain.Revision;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.Study;
import br.ufrn.ase.urupema.dtos.CalledMethod;
import br.ufrn.ase.urupema.dtos.StatsFindedFiles;
import br.ufrn.ase.urupema.dtos.StatsTopN;
import br.ufrn.ase.urupema.dtos.StudyResult;
import br.ufrn.ase.urupema.parser.StackTraceParser;
import br.ufrn.ase.urupema.repository.IssueTrackerConnectionRepository;
import br.ufrn.ase.urupema.repository.IssueTrackerRepository;
import br.ufrn.ase.urupema.repository.factory.IssueTrackerRepositoryFactory;
import lombok.extern.slf4j.Slf4j;

@Component
@Transactional
@Slf4j
public class IssueTrackerConnectionProcess {
	@Autowired
	private IssueTrackerRepositoryFactory issueTrackerRepositoryFactory;
	
	@Autowired
	private IssueTrackerConnectionRepository issueTrackerConnectionRepository;
	
	@Autowired
	private IssueTrackerSourceProcess issueTrackerSourceProcess;
	
	@Autowired
	private SourceConnectionProcess sourceConnectionProcess;
	
	@Autowired
	private LogGroupProcess logGroupProcess;
	
	@Autowired
	private LogProcess logProcess;
	
	public IssueTrackerConnection findById(Long id) throws IOException, InstantiationException, IllegalAccessException {
		IssueTrackerConnection itc = issueTrackerConnectionRepository.findById(id.longValue()).get();
		return itc;
	}
	
	public void save(IssueTrackerConnection issueTrackerConnection) throws InstantiationException, IllegalAccessException, IOException {
		issueTrackerConnection.setIssueTrackerSource(issueTrackerSourceProcess.findById(issueTrackerConnection.getIssueTrackerSource().getId()).get());
		issueTrackerConnection.getIssueTrackerSource().getIssueTrackerType().validateQueryProperties(issueTrackerConnection.getQueryProperties());
		issueTrackerConnectionRepository.save(issueTrackerConnection);
	}
	
	public List<IssueTrackerConnection> findAll(){
		return issueTrackerConnectionRepository.findAll();
	}
	
	public void delete(Long id) throws InstantiationException, IllegalAccessException, IOException {
		IssueTrackerConnection itc = findById(id);
		itc.getStudy().getIssueTrackerConnections().remove(itc);
		issueTrackerConnectionRepository.delete(itc);
	}
	
	public Study getStudy(Long idIssueTrackerConnection) throws InstantiationException, IllegalAccessException, IOException {
		IssueTrackerConnection itc = findById(idIssueTrackerConnection);
		itc.getStudy().getSourceConnections().size();
		return itc.getStudy();
	}
	
	public void importClosedIssuesWithStackTracesAndEnabledChangedFiles(Long idIssueTrackerConnection) throws InstantiationException, IllegalAccessException, IOException, OperationNotSupportedException {
		IssueTrackerConnection itc = findById(idIssueTrackerConnection);
		IssueTrackerRepository itr = issueTrackerRepositoryFactory.getIssueTrackerRepository(itc.getIssueTrackerSource());
		List<Issue> issuesWithStackTraceAndEnabledChangedFiles = itr.findClosedIssuesWithStackTrace(itc.getStartDate(), itc.getEndDate(), itc.getQueryProperties());
		System.out.println(issuesWithStackTraceAndEnabledChangedFiles.size() + " issues with Stack Trace");
		for (Iterator<Issue> iterator = issuesWithStackTraceAndEnabledChangedFiles.iterator(); iterator.hasNext();) {
			Issue issue = (Issue) iterator.next();
			Set<String> enabledChangedFile = issue.getEnabledChangedFiles(Arrays.asList(FileChangeType.MODIFIED, FileChangeType.RENAMED), Arrays.asList(".java"));
			if(enabledChangedFile.isEmpty()) {
				iterator.remove();
			}
		}
		System.out.println(issuesWithStackTraceAndEnabledChangedFiles.size() + " issues with Stack Trace and enabled changed file");
		Set<Issue> issues = new TreeSet<>(); 
		for(Issue issue : issuesWithStackTraceAndEnabledChangedFiles) {
			boolean existIss = false;
			for(Issue existIssue : itc.getIssues()) {
				if(issue.getIdentifier().equals(existIssue.getIdentifier())) {
					for(ReportedStackTrace reportedStackTrace : issue.getReportedStackTraces()) {
						boolean existRst = false;
						for(ReportedStackTrace existReportedStackTrace : existIssue.getReportedStackTraces()) {
							if(reportedStackTrace.getStackTrace().equals(existReportedStackTrace.getStackTrace())) {
								existRst = true;
								break;
							}
						}
						if(!existRst) {
							existIssue.addReportedStackTrace(reportedStackTrace);
						}
					}
					for(Revision revision : issue.getRevisions()) {
						//boolean existRev = false;
						for(Revision existRevision : existIssue.getRevisions()) {
							if(revision.getIdentifier().equals(existRevision.getIdentifier())) {
								for(ChangedFile changedFile : revision.getChangedFiles()) {
									boolean existCh = false;
									for(ChangedFile existChangedFile : existRevision.getChangedFiles()) {
										if(changedFile.getName().equals(existChangedFile.getName())) {
											existCh = true;
											break;
										}
									}
									if(!existCh) {
										existRevision.addChangedFile(changedFile);
									}
								}
								//existRev = true;
								break;
							}
						}
						/*if(!existRev) {
							existIssue.addRevision(revision);
						}*/
					}
					existIss = true;
					break;
				}
			}
			if(!existIss){
				issues.add(issue);
			}
		}
		if(!issues.isEmpty()) {
			for(Issue issue : issues) {
				if(issue.getEnabledReportedStackTraces(null).isEmpty()) {
					issue.setEnabled(false);
					issue.setNote(issue.getNote() + "\n=> No ReportedStackTrace enabled");
				} else {
					for(ReportedStackTrace reportedStackTrace : issue.getEnabledReportedStackTraces(null)) {
						if(reportedStackTrace.getStackTrace().split("\tat ").length < 5) {
							reportedStackTrace.setEnabled(false);
							reportedStackTrace.setNote(reportedStackTrace.getNote() + "\n=> Stack trace with less than five frames");
						}
					}
				}
				if(issue.getEnabledChangedFiles(null, null).isEmpty()) {
					issue.setEnabled(false);
					issue.setNote(issue.getNote() + "\n=> No ChangedFile enabled");
				}
			}
			itc.addIssues(issues);
		}
		save(itc);
	}
	
	public Set<String> getAffectedUrisByReportedStackTraces(Long idIssueTrackerConnection) throws InstantiationException, IllegalAccessException, IOException, OperationNotSupportedException {
		Set<String> affectedUris = new TreeSet<>();
		IssueTrackerConnection itc = findById(idIssueTrackerConnection);
		for(Issue issue : itc.getIssues()) {
			for(ReportedStackTrace reportedStackTrace : issue.getEnabledReportedStackTraces(null)) {
				for(AffectedUri aUri : reportedStackTrace.getEnabledAffectedUris()) {
					affectedUris.add(aUri.getUri());
				}
			}
		}
		return affectedUris;
	}
	
	public Set<String> findAffectedUrisByReportedStackTraces(Long idIssueTrackerConnection, Long idSourceConnection, String uriFragment) throws InstantiationException, IllegalAccessException, IOException, OperationNotSupportedException {
		IssueTrackerConnection itc = findById(idIssueTrackerConnection);
		SourceConnection sc = sourceConnectionProcess.findById(idSourceConnection);
		Set<String> affectedUris = new TreeSet<>();
		log.debug("Finding URIS");
		System.out.println(new Date());
		System.out.println("Finding URIS");
		for(Issue issue : itc.getIssues()) {
			log.debug(" - Issue " + issue.getIdentifier());
			System.out.println(" - Issue " + issue.getIdentifier());
			Set<ReportedStackTrace> reportedStackTraces = issue.getEnabledReportedStackTraces(null);
			if(!reportedStackTraces.isEmpty()) {
				for(ReportedStackTrace reportedStackTrace : reportedStackTraces) {
					Set<String> iUris = logProcess.getAffectedUrisByEquivalentStackTracesAndDateBetween(sc, reportedStackTrace.getStackTrace(), uriFragment);
					for(String uri : iUris) {
						AffectedUri affectedUri = new AffectedUri(uri, null);
						if(!reportedStackTrace.getAffectedUris().contains(affectedUri)) {
							reportedStackTrace.addAffectedUri(affectedUri);
						}
					}
					log.debug("    " + iUris.toString());
					System.out.println("    " + iUris.toString());
					affectedUris.addAll(iUris);
				}
			}
		}
		log.debug("URIS Found: " + affectedUris.size());
		for(String uri : affectedUris) {
			System.out.println(uri);
		}
		System.out.println(new Date());
		System.out.println("URIS Found: " + affectedUris.size());
		save(itc);
		return affectedUris;
	}
	
	public void matchIssueWithLogGroups(Long idIssueTrackerConnection, Long idSourceConnection) throws InstantiationException, IllegalAccessException, IOException {
		IssueTrackerConnection itc = findById(idIssueTrackerConnection);
		SourceConnection sc = sourceConnectionProcess.findById(idSourceConnection);
		log.debug("Matching issues");
		System.out.println(new Date() + " - Matching issues");
		for(Issue issue : itc.getIssues()) {
			log.debug(" - Issue " + issue.getIdentifier());
			Set<LogGroup> logGroups = new HashSet<>();
			Set<ReportedStackTrace> reportedStackTraces = issue.getEnabledReportedStackTraces(null);
			if(!reportedStackTraces.isEmpty()) {
				for(ReportedStackTrace reportedStackTrace : reportedStackTraces) {
					String stackTrace = reportedStackTrace.getStackTrace();
					List<LogGroup> lgs = logGroupProcess.findBySourceConnectionAndStacktrace(sc, stackTrace);
					if(lgs.isEmpty()) {
						String equivStackTrace = StackTraceParser.equivalentStackTrace(reportedStackTrace.getStackTrace());
						lgs = logGroupProcess.findBySourceConnectionAndStacktrace(sc, equivStackTrace);
					}
					if(lgs.isEmpty()) {
						lgs = logGroupProcess.findBySourceConnectionAndEquivalentStacktraceWithoutCauseByMessagesAndGeneratedNumbers(sc, reportedStackTrace.getStackTrace());
					}
					for(LogGroup lg : lgs) {
						LogGroup superGroup = lg;
						while(superGroup.getSuperGroup() != null) {
							superGroup = superGroup.getSuperGroup();
						}
						if(!logGroups.contains(superGroup)) {
							logGroups.add(superGroup);
						}
					}
				}
			}
			issue.addLogGroups(logGroups);
			log.debug("LogGroups Found: " + logGroups.size());
			System.out.print("\n" + issue.getIdentifier() + ": ");
			logGroups.stream().forEach(lg -> System.out.print(lg.getId() + ", "));
		}
		log.debug("Saving matches");
		System.out.println(new Date() + " - FINISH Matching issues");
		save(itc);
	}
	
	public StudyResult calculateTopNs(Long idIssueTrackerConnection, Long idSourceConnection, Set<String> fileExtensions, String packageSource, boolean checkPkgInStackTrace, int maxEnabledChangedFiles) throws InstantiationException, IllegalAccessException, OperationNotSupportedException, IOException  {
		List<StatsTopN> topNs = new ArrayList<>();
		
		String pkgInStackTrace = packageSource;
		if(!checkPkgInStackTrace) {
			pkgInStackTrace = null;
		}
		
		IssueTrackerConnection itc = findById(idIssueTrackerConnection);
		Set<FileChangeType> fileChangedTypes = new TreeSet<>(Arrays.asList(FileChangeType.MODIFIED, FileChangeType.RENAMED));
		log.debug("Calculate TopNs");
		for(Issue issue : itc.getIssues()) {
			log.debug(" - Issue " + issue.getIdentifier());
			boolean enabledChangedFilesReq = true;
			int numEnabledChangedFiles = issue.getEnabledChangedFiles(fileChangedTypes, fileExtensions).size();
			if(numEnabledChangedFiles < 1 || numEnabledChangedFiles > maxEnabledChangedFiles) {
				enabledChangedFilesReq = false;
			}
			if(issue.isEnabled() && enabledChangedFilesReq && !issue.getEnabledReportedStackTraces(pkgInStackTrace).isEmpty()) {
				for(LogGroup lg : issue.getLogGroups()) {
					if(lg.getSourceConnection().getId().equals(idSourceConnection)) {
						//Para garantir que sejam calculados os arquivos suspeitos, caso ainda não tenham sido
						lg.setSuspiciousFiles(logGroupProcess.findSuspiciousFilesByLogGroupId(lg.getId()));
						StatsTopN stn = new StatsTopN(issue, lg, fileChangedTypes, fileExtensions, packageSource);
						topNs.add(stn);
					}
				}
			}
		}
		
		List<StatsFindedFiles> ffs = new ArrayList<StatsFindedFiles>();
		for (StatsTopN statsTopN : topNs) {
			Set<String> findedFiles = statsTopN.sourceOnlyChangedFilesInTop10();
			for (String findedFile : findedFiles) {
				String changedFileNotes = statsTopN.getChangedFileNotes(findedFile);
				
				Map<String, CalledMethod> mapCalledMethodName = new HashMap<>();
				
				Set<CalledMethodStats> calledMethodsInSts = logGroupProcess.findCalledFileMethods(statsTopN.getLogGroupId(), findedFile);
				for(CalledMethodStats entry : calledMethodsInSts) {
					String methodName = entry.getQualifiedMethodName();
					methodName = methodName.substring(methodName.lastIndexOf('.') + 1);
					
					if(mapCalledMethodName.containsKey(methodName)) {
						CalledMethod cm = mapCalledMethodName.get(methodName);
						cm.setNumberOfCalls(cm.getNumberOfCalls() + entry.getCount().intValue());
					} else {
						CalledMethod cm = new CalledMethod(methodName, entry.getCount().intValue(), changedFileNotes.contains(methodName));
						mapCalledMethodName.put(methodName, cm);
					}
				}
				
				List<CalledMethod> calledMethods = new ArrayList<>(mapCalledMethodName.values());
				Collections.sort(calledMethods);
				
				StatsFindedFiles sff = new StatsFindedFiles(
						statsTopN.getIssueIdentifier(), 
						statsTopN.getLogGroupId(),
						findedFile,
						changedFileNotes, 
						calledMethods);
				ffs.add(sff);
			}
		}
		StudyResult str = new StudyResult(topNs, ffs);
		str.print();
		return str;
	}
	
	
	
	public void findAffectedUrisAgroupAndMatchIssueWithLogGroups(Long idIssueTrackerConnection, Long idSourceConnection, String uriFragment) throws OperationNotSupportedException, InstantiationException, IllegalAccessException, IOException  {
		Set<String> affectedUris = findAffectedUrisByReportedStackTraces(idIssueTrackerConnection, idSourceConnection, uriFragment);
		logProcess.agroup(idSourceConnection, affectedUris);
		matchIssueWithLogGroups(idIssueTrackerConnection, idSourceConnection);
	}
	
	public void agroup(Long idIssueTrackerConnection, Long idSourceConnection) throws OperationNotSupportedException, InstantiationException, IllegalAccessException, IOException  {
		IssueTrackerConnection itc = findById(idIssueTrackerConnection);
		Set<String> affectedUris = new TreeSet<>();
		for(Issue issue : itc.getIssues()) {
			for(ReportedStackTrace reportedStackTrace : issue.getEnabledReportedStackTraces(null)) {
				for(AffectedUri aUri : reportedStackTrace.getEnabledAffectedUris()) {
					affectedUris.add(aUri.getUri());
				}
			}
		}
		logProcess.agroup(idSourceConnection, affectedUris);
	}
}
