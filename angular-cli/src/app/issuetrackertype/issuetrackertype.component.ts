import { Component, OnInit } from '@angular/core';

import { IssueTrackerTypeService } from '../service';
import { IssueTrackerType } from '../model';

@Component({
  selector: 'app-issuetrackertype',
  templateUrl: './issuetrackertype.component.html',
  styleUrls: ['./issuetrackertype.component.css']
})
export class IssueTrackerTypeComponent implements OnInit {
  issueTrackerTypes: IssueTrackerType[];

  constructor(private issueTrackerTypeService: IssueTrackerTypeService) { }

  ngOnInit() {
    this.issueTrackerTypeService.getIssueTrackerTypes().subscribe(
      issueTrackerTypes => this.issueTrackerTypes = issueTrackerTypes
    );
  }
}
