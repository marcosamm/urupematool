package br.ufrn.ase.urupema.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractTraceElement implements TraceElement {
	private String fileName;
	private Integer lineNumber;
	private Integer qtdDescendents;
	
	@Override
	public String getQualifiedFileName() {
		String qFileName = null;
		
		if(fileName != null){
			qFileName = getSignature().substring(0, getSignature().lastIndexOf("."));
			if(fileName.equals("Native Method") || fileName.equals("Unknown Source") || fileName.equals("<generated>")){
				int i = qFileName.indexOf("$");
				if(i > 0) {
					if(qFileName.contains("$Proxy")) {
						qFileName = qFileName.substring(0, i) + "Proxy";
					} else {
						qFileName = qFileName.substring(0, i);
					}
				}
				qFileName = qFileName.replace("000", "");
				qFileName = qFileName.replace(".", "/")+".java";
			}else {
				qFileName = qFileName.substring(0, qFileName.lastIndexOf(".")+1).replace(".", "/") + fileName;
			}
		}
		
		return qFileName;
	}
	
	public List<TraceElement> getTraceElement(String signature) {
		List<TraceElement> traceElements = new ArrayList<>();
		
		if(getSignature().equals(signature)){
			traceElements.add(this);
		}
		for(TraceElement te : getChildren()) {
			traceElements.addAll(te.getTraceElement(signature));
		}
		
		return traceElements;
	}
	
	public boolean contains(TraceElement traceElement) {
		boolean contains = true;
		
		if(getQtdDescendents() < traceElement.getQtdDescendents()) {
			//Não pode conter alguem maior que ele
			contains = false;
		} else {
			List<TraceElement> tes = getTraceElement(traceElement.getSignature());
			if(!tes.isEmpty()){
				for(TraceElement child : traceElement.getChildren()) {
					boolean containsChild = false;
					for(TraceElement te : tes) {
						if(te.contains(child)) {
							containsChild = true;
							break;
						}
					}
					if(!containsChild) {
						contains = false;
						break;
					}
				}
			}else {
				contains = false;
			}
		}
		
		return contains;
	}
	
	@Override
	public String toString() {
		return toString("");
	}
	
	public String toString(String prefix) {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getSignature());
		if(this.getFileName() != null){;
			sb.append("(").append(this.getFileName());
			if(this.getLineNumber() != null){
				sb.append(":").append(this.getLineNumber());
			}
			sb.append(")");
		}
		sb.append("\n");
		for(TraceElement c : getChildren()) {
			String pre = prefix+"\t";
			sb.append(pre+c.toString(pre));
		}
		return sb.toString();
	}
	
	@Override
	public Integer getHeight(){
		int hChildren = 0;
		for(TraceElement c : getChildren()) {
			hChildren = Math.max(hChildren, c.getHeight());
		}
		return hChildren+1;
	}
	
	@Override
	public Integer getQtdDescendents() {
		if(qtdDescendents == null) {
			qtdDescendents = getChildren().size();
			for(TraceElement c : getChildren()) {
				qtdDescendents += c.getQtdDescendents();
			}
		}
		return qtdDescendents;
	}
	
	public Map<String, Integer> getQualifiedFileNamesAndMinTopFrameDist() {
		Map<String, Integer> mapQfnMtfd = new HashMap<>();
		String qName = this.getQualifiedFileName();
		if(qName != null){
			mapQfnMtfd.put(qName, getTopFrameDist());
		}
		for(TraceElement c : getChildren()) {
			for(Map.Entry<String, Integer> entry : c.getQualifiedFileNamesAndMinTopFrameDist().entrySet()) {
				if(mapQfnMtfd.containsKey(entry.getKey())) {
					if(mapQfnMtfd.get(entry.getKey()) > entry.getValue()) {
						mapQfnMtfd.put(entry.getKey(), entry.getValue());
					}
				}else {
					mapQfnMtfd.put(entry.getKey(), entry.getValue());
				}
			}
		}
		return mapQfnMtfd;
	}
	
	@Override
	public List<String> getOrderedQualifiedFileNames() {
		List<String> orderedQualifiedFileNames = new ArrayList<>();
		for(TraceElement c : getChildren()) {
			orderedQualifiedFileNames.addAll(c.getOrderedQualifiedFileNames());
		}
		String qName = this.getQualifiedFileName();
		if(qName != null){
			orderedQualifiedFileNames.add(qName);
		}
		return orderedQualifiedFileNames;
	}
}
