package br.ufrn.ase.urupema.elasticsearch.domain;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.ufrn.ase.urupema.domain.AbstractWebExcecutionTrace;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.TraceElement;
import br.ufrn.ase.urupema.domain.TraceElementImpl;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import lombok.Getter;

@Getter
public class EsErrorLog extends AbstractWebExcecutionTrace implements WebExecutionTrace {
	private EsGenericLog basicLog;
	private String exceptionMessage;
	private String stackTrace;
	
	private List<StackTraceElement> stackTraceElements;
	private List<TraceElement> traceElements;
	
	public EsErrorLog(EsGenericLog basicLog, String exceptionMessage, List<StackTraceElement> stackTraceElements, String stackTrace) {
		this.basicLog = basicLog;
		this.exceptionMessage = exceptionMessage;
		this.stackTraceElements = stackTraceElements;
		this.stackTrace = stackTrace;
	}

	@Override
	public String getRequestURL() {
		return basicLog.getRequestURL();
	}
	
	@Override
	public String getIdentifier() {
		return basicLog.getIdentifier();
	}
	
	@Override
	public Date getDate() {
		return basicLog.getDate();
	}

	@Override
	public LogType getLogType() {
		return LogType.ERROR;
	}
	
	@Override
	public Map<String, String> getMoreInformation() {
		return basicLog.getMoreInformation();
	}

	@Override
	public List<TraceElement> getTraceElements() {
		if(traceElements == null) {
			TraceElement tElements = TraceElementImpl.toTraceElement(stackTraceElements);
			if(tElements != null) {
				traceElements = Arrays.asList(tElements);
			} else {
				traceElements = Arrays.asList();
			}
		}
		return traceElements;
	}
}
