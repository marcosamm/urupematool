package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.AffectedUri;
import br.ufrn.ase.urupema.process.AffectedUriProcess;
import br.ufrn.ase.urupema.process.ReportedStackTraceProcess;


@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class AffectedUriResource {	
	@Autowired
	AffectedUriProcess process;
	
	@Autowired
	ReportedStackTraceProcess reportedStackTraceProcess;
	
	@GetMapping("/affecteduri")
	public AffectedUri findById(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findById(id);
	}
	
	@PostMapping("/affecteduri")
	public void put(@RequestBody AffectedUri affectedUri) throws IOException {
		process.save(affectedUri);
	}
	
	@PutMapping("/affecteduri")
	public void update(@RequestBody AffectedUri affectedUri) throws IOException, InstantiationException, IllegalAccessException {
		AffectedUri a = findById(affectedUri.getId());
		BeanUtils.copyProperties(affectedUri, a, "reportedStackTrace");
		process.save(a);
	}
	
	@DeleteMapping("/affecteduri")
	public void delete(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		process.delete(id);
	}
	
	@GetMapping("/affecteduri/listbyreportedstacktrace")
	public List<AffectedUri> findByReportedStackTrace(@RequestParam("idReportedStackTrace") Long id) throws InstantiationException, IllegalAccessException, IOException {
		return process.findByReportedStackTrace(reportedStackTraceProcess.findById(id));
	}
}
