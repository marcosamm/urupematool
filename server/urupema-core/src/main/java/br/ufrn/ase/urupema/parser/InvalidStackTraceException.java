package br.ufrn.ase.urupema.parser;

public class InvalidStackTraceException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public InvalidStackTraceException(String msg) {
		super(msg);
	}
}
