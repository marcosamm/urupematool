package br.ufrn.ase.urupema.elasticsearch.parser;

import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import br.ufrn.ase.urupema.elasticsearch.domain.EsErrorLog;
import br.ufrn.ase.urupema.parser.StackTraceParser;

public class EsErrorLogJsonParser {
	public static final String STACKTRACE_FIELD_NAME = "stacktrace";
	public static final String MESSAGE_FIELD_NAME = "message";
	
	
	EsGenericLogJsonParser genericLogJsonParser = new EsGenericLogJsonParser();
	private StackTraceParser stackTraceParser;
	
	public EsErrorLogJsonParser() {
		stackTraceParser = new StackTraceParser();
	}
	
	public EsErrorLogJsonParser(StackTraceParser stackTraceParser) {
		this.stackTraceParser = stackTraceParser;
	}
	
	public EsErrorLogJsonParser(String regexSignature) {
		stackTraceParser = new StackTraceParser(regexSignature);
	}
	
	public EsErrorLogJsonParser(String regexSignature, String regexIgnoreSignature) {
		stackTraceParser = new StackTraceParser(regexSignature, regexIgnoreSignature);
	}
	
	public EsErrorLog parse (JsonNode node) {
		JsonNode source = node.get("_source");
		String stackTraceLogString = source.get(STACKTRACE_FIELD_NAME) != null ? source.get(STACKTRACE_FIELD_NAME).asText() : null;
		String exceptionMessage = source.get(MESSAGE_FIELD_NAME) != null ? source.get(MESSAGE_FIELD_NAME).asText() : null;
		
		return new EsErrorLog(genericLogJsonParser.parse(node), exceptionMessage,
				stackTraceParser.parse(stackTraceLogString), stackTraceLogString);
	}
	
	public EsErrorLog parse (Map<String, Object> node) {
		@SuppressWarnings("unchecked")
		Map<String, Object> source = (Map<String, Object>) node.get("_source");
		String stackTraceLogString = source.get(STACKTRACE_FIELD_NAME) != null ? source.get(STACKTRACE_FIELD_NAME).toString() : null;
		String exceptionMessage = source.get(MESSAGE_FIELD_NAME) != null ? source.get(MESSAGE_FIELD_NAME).toString() : null;
		
		return new EsErrorLog(genericLogJsonParser.parse(node), exceptionMessage, stackTraceParser.parse(stackTraceLogString), stackTraceLogString);
	}
}