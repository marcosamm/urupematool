package br.ufrn.ase.urupema.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(of = "identifier")
public class UserStats implements Comparable<UserStats>{
	private String identifier;
	private Long count;
	
	public UserStats(String identifier, Long count) {
		this.identifier = identifier;
		this.count = count;
	}
	
	public void addCount(Long count){
		this.count += count;
	}

	@Override
	public int compareTo(UserStats o) {
		int ret = o.getCount().compareTo(this.count);
		
		if(ret == 0) {
			ret = o.getIdentifier().compareTo(this.identifier);
		}
		
		return ret;
	}
}
