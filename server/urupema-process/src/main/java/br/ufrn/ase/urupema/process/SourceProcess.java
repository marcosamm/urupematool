package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.Source;
import br.ufrn.ase.urupema.repository.SourceRepository;

@Component
public class SourceProcess {
	@Autowired
	SourceRepository sourceRepository;
	
	@Autowired
	SourceTypeProcess sourceTypeProcess;
	
	public Optional<Source> findById(Long id) throws IOException, InstantiationException, IllegalAccessException {
		Optional<Source> s = sourceRepository.findById(id);
		if(s.isPresent()) {
			s.get().setSourceType(sourceTypeProcess.findById(s.get().getSourceTypeId()));
		}
		return s;
	}
	
	public void save(Source source) {
		sourceRepository.save(source);
	}
	
	public List<Source> findAll() throws InstantiationException, IllegalAccessException, IOException{
		List<Source> srcs = sourceRepository.findAll();
		for (Source s : srcs) {
			s.setSourceType(sourceTypeProcess.findById(s.getSourceTypeId()));
		}
		return srcs;
	}
	
	public void delete(Source source) {
		sourceRepository.delete(source);
	}
}
