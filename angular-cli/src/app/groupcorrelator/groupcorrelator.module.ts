import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import {TableModule} from 'primeng/table';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
//import { library } from '@fortawesome/fontawesome-svg-core';
import {
  fas, faSearch
} from '@fortawesome/free-solid-svg-icons';

//library.add(faSearch);

import { routes } from './routes';

import { GroupCorrelatorDetailComponent } from './detail/detail.component';
import { ListGroupRelationshipsComponent } from './listgrouprelationships/listgrouprelationships.component';
import { GroupCorrelatorService, GroupRelationshipService } from '../service';
import { GroupCorrelatorCreateComponent } from './create/create.component';
import { GroupRelationshipComponent } from './grouprelationship/grouprelationship.component';
import { PipesModule } from '../pipes/pipes.module';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    FontAwesomeModule,
    TableModule,
    PipesModule
  ],
  declarations: [
    GroupCorrelatorCreateComponent,
    GroupCorrelatorDetailComponent,
    ListGroupRelationshipsComponent,
    GroupRelationshipComponent
],
  providers: [
    GroupCorrelatorService,
    GroupRelationshipService
  ]
})
export class GroupcorrelatorModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
  }
}
