/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * Scenario.class
 * 30/10/2017
 */
package br.ufrn.ase.urupema.perfminer.domain;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import br.ufrn.ase.urupema.domain.AbstractWebExcecutionTrace;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.TraceElement;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Scenario extends AbstractWebExcecutionTrace implements WebExecutionTrace{
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private String name;

	@Column
	private Date date;

	@Column(name = "thread_id")
	private long threadId;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "execution_id")
	private Execution execution;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Node root;
	
	@Column(name = "request_url", columnDefinition = "text")
	private String requestURL;

	@Column(name = "request_parameters", columnDefinition = "text")
	private String requestParameters;
	
	@Transient
	private transient String exceptionMessage;

	@Override
	public List<TraceElement> getTraceElements() {
		return Arrays.asList(root);
	}

	@Override
	public String getIdentifier() {
		return ""+getId();
	}
	
	@Override
	public String getExceptionMessage() {
		if(exceptionMessage == null) {
			exceptionMessage = root.getExceptionIncludingChildren();
		}
		return exceptionMessage;
	}
	
	@Override
	public Map<String, String> getMoreInformation() {
		Map<String, String> map = super.getMoreInformation();
		map.put("name", getName());
		map.put("requestParameters", getRequestParameters());
		return map;
	}

	@Override
	public LogType getLogType() {
		return LogType.EXECUTION;
	}

	@Override
	public String getStackTrace() {
		return null;
	}
}
