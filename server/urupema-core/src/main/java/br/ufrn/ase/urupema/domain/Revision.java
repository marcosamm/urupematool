package br.ufrn.ase.urupema.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@SequenceGenerator(name = "ID_REVISION_SEQUENCE", sequenceName = "revision_id_sequence", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Getter
@Setter
@ToString(of={"id", "identifier", "date"})
public class Revision implements Serializable, Comparable<Revision> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_REVISION_SEQUENCE")
	private Long id;
	
	@Column(nullable = false)
	private String identifier;
	
	private String comment;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	@JsonIgnore
	@ManyToMany(mappedBy="revisions")
	private Set<Issue> issues = new TreeSet<>();
	
	@OneToMany(mappedBy = "revision", fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval = true)
	private Set<ChangedFile> changedFiles = new TreeSet<>();
	
	public Revision() {
	}
	
	public Revision(String identifier, Date date, String comment) {
		super();
		this.identifier = identifier;
		this.date = date;
		this.comment = comment;
	}
	
	@Override
	public int compareTo(Revision arg0) {
		int ret = date.compareTo(arg0.getDate());
		if(ret == 0) {
			ret = identifier.compareTo(arg0.getIdentifier());
		}
		return ret;
	}
	
	public void addChangedFile(ChangedFile changedFile) {
		this.changedFiles.add(changedFile);
		changedFile.setRevision(this);
	}
	
	public void addChangedFiles(Collection<ChangedFile> changedFiles) {
		for (ChangedFile changedFile : changedFiles) {
			addChangedFile(changedFile);
		}
	}
	
	public void removeChangedFile(ChangedFile changedFile) {
		this.changedFiles.remove(changedFile);
		changedFile.setRevision(null);
	}
	
	public void removeChangedFiles(Collection<ChangedFile> changedFiles) {
		for (ChangedFile changedFile : changedFiles) {
			removeChangedFile(changedFile);
		}
	}
}
