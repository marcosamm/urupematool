import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IssueService, AffectedUriService } from '../../service';
import { Issue, AffectedUri, ReportedStackTrace } from '../../model';
import { ReportedStackTraceService } from '../../service/reportedstacktrace.service';

@Component({
  selector: 'app-affecteduricreate',
  templateUrl: './affecteduricreate.component.html',
  styleUrls: ['./affecteduricreate.component.css']
})
export class AffectedUriCreateComponent implements OnInit {
  affectedUriForm: FormGroup;
  issue: Issue;
  reportedStackTrace: ReportedStackTrace;
  affectedUri: AffectedUri;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private affectedUriService: AffectedUriService,
    private issueService: IssueService,
    private reportedStackTraceService: ReportedStackTraceService,
    private fb: FormBuilder
  ) {
    this.affectedUriForm = this.fb.group({
      uri: ['', [Validators.required, Validators.minLength(5)]],
      enabled: [false, []],
      note: ['', []],
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.issueService.getIssue(params['idIssue']).subscribe(
        issue => {
          this.issue = issue;
          this.issueService.getIssueTrackerConnectionByIssue(this.issue).subscribe(issueTrackerConnection => {
            this.issue.issueTrackerConnection = issueTrackerConnection;
          });
        }
      );
      if (params['idReportedStackTrace'] !== undefined) {
        this.reportedStackTraceService.getReportedStackTrace(params['idReportedStackTrace']).subscribe(
          reportedStackTrace => {
            this.reportedStackTrace = reportedStackTrace;
          }
        );
      }
      if (params['idAffectedUri'] !== undefined) {
        this.affectedUriService.getAffectedUri(params['idAffectedUri']).subscribe(
          affectedUri => {
            this.affectedUri = affectedUri;
            this.affectedUriForm.get('uri').setValue(this.affectedUri.uri);
            this.affectedUriForm.get('enabled').setValue(this.affectedUri.enabled);
            this.affectedUriForm.get('note').setValue(this.affectedUri.note);
          }
        );
      }
    });
  }

  create() {
    this.affectedUriService.createAffectedUri(
      this.reportedStackTrace.id,
      this.affectedUriForm.get('uri').value,
      this.affectedUriForm.get('enabled').value,
      this.affectedUriForm.get('note').value
    ).subscribe(
      result => {
        this.router.navigate(['/issuetrackerconnection/issuedetail/' + + this.issue.issueTrackerConnection.id + '/' + this.issue.id]);
      }
    );
  }

  update() {
    this.affectedUriService.updateAffectedUri(
      this.affectedUri.id,
      this.affectedUriForm.get('uri').value,
      this.affectedUriForm.get('enabled').value,
      this.affectedUriForm.get('note').value
    ).subscribe(
      result => {
        this.router.navigate(['/issuetrackerconnection/issuedetail/' + + this.issue.issueTrackerConnection.id + '/' + this.issue.id]);
      }
    );
  }
}
