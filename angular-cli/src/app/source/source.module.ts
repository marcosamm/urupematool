import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
//import { library } from '@fortawesome/fontawesome-svg-core';
import {
  fas, faPencilAlt, faTrash
} from '@fortawesome/free-solid-svg-icons';
//library.add(faPencilAlt, faTrash);

import { routes } from './routes';

import { SourceListComponent } from './list/list.component';
import { SourceCreateComponent } from './create/create.component';
import { SourceService } from '../service';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    FontAwesomeModule,
    TableModule
  ],
  declarations: [
    SourceListComponent,
    SourceCreateComponent
  ],
  providers: [
    SourceService
  ]
})
export class SourceModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
  }
 }
