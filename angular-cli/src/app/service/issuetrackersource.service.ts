import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { IssueTrackerSource } from '../model';

@Injectable()
export class IssueTrackerSourceService {
  urlGet = environment.apiUrl + '/issuetrackersource';

  constructor(private http: HttpClient) { }

  getIssueTrackerSource(id: number): Observable<IssueTrackerSource> {
    return this.http.get<IssueTrackerSource>(this.urlGet + '?id=' + id);
  }

  getIssueTrackerSources(): Observable<IssueTrackerSource[]> {
    return this.http.get<IssueTrackerSource[]>(this.urlGet + '/list');
  }

  createIssueTrackerSource(issueTrackerTypeId: string, name: string, connectionProperties: Map<string, string>) {
    const issueTrackerSource = {
      issueTrackerTypeId: issueTrackerTypeId,
      name: name,
      connectionProperties: this.convMap(connectionProperties)
    };
    console.log(issueTrackerSource);
    return this.http.post(this.urlGet, issueTrackerSource);
  }

  updateIssueTrackerSource(id: number, issueTrackerTypeId: string, name: string, connectionProperties: Map<string, string>) {
    const issueTrackerSource = {
      id: id,
      issueTrackerTypeId: issueTrackerTypeId,
      name: name,
      connectionProperties: this.convMap(connectionProperties)
    };
    return this.http.put(this.urlGet, issueTrackerSource);
  }

  deleteIssueTrackerSource(issueTrackerSource: IssueTrackerSource) {
    return this.http.delete(this.urlGet + '?id=' + issueTrackerSource.id);
  }

  convMap(map: Map<any, any>): {} {
    const convMap = {};
    map.forEach((val, key) => {
      convMap[key] = val;
    });
    return convMap;
  }
}
