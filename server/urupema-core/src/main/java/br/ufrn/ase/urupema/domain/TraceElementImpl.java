package br.ufrn.ase.urupema.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TraceElementImpl extends AbstractTraceElement implements TraceElement {
	
	private String signature;
	private TraceElementImpl parent;
	private List<TraceElementImpl> children = new ArrayList<>();
	private Integer topFrameDist;
	
	public TraceElementImpl() {
	}
	
	public TraceElementImpl(String signature) {
		this.signature = signature;
	}
	
	public void addChild(TraceElementImpl traceElement) {
		traceElement.setParent(this);
		children.add(traceElement);
	}
	
	public static TraceElement toTraceElement(List<StackTraceElement> stackTraceElements) {
		TraceElementImpl traceElement = null;
		if(stackTraceElements != null && !stackTraceElements.isEmpty()) {
			TraceElementImpl parent = toTraceElement(stackTraceElements.get(stackTraceElements.size()-1));
			parent.setTopFrameDist(stackTraceElements.size()-1);
			traceElement = parent;
			for(int i = stackTraceElements.size()-2; i >= 0; i--) {
				TraceElementImpl child = toTraceElement(stackTraceElements.get(i));
				parent.addChild(child);
				//TODO Ver a questão dos "... XX more" (Caused By) - talvez tenha que mudar o parser
				child.setTopFrameDist(parent.getTopFrameDist()-1);
				parent = child;
			}
		}
		return traceElement;
	}
	
	private static TraceElementImpl toTraceElement(StackTraceElement ste) {
		TraceElementImpl te= new TraceElementImpl();
		String signature = ste.getClassName() + "." + ste.getMethodName();
		//TODO Isso tem que ser feito no método de comparação e não alterando a assinatura do método, comentando código abaixo:
		//Verifica se o método é um construtor e ignora, pois o PerfMiner não guarda essa informação no banco
		//if(!ste.getMethodName().equals("<init>") && !ste.getMethodName().equals("<clinit>")) {
		//	signature += "."+ste.getMethodName();
		//}
		te.setSignature(signature);
		te.setFileName(ste.getFileName());
		te.setLineNumber(ste.getLineNumber());
		return te;
	}
}
