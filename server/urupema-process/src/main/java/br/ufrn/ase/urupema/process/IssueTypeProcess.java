package br.ufrn.ase.urupema.process;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.IssueType;

@Component
public class IssueTypeProcess {
	public Collection<IssueType> findAll() {
		return Arrays.asList(IssueType.values());
	}
}
