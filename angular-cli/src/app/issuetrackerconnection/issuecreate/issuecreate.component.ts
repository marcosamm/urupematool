import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IssueTrackerConnectionService, IssueService, IssueTypeService, } from '../../service';
import { IssueTrackerConnection, Issue, IssueType } from '../../model';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-issuecreate',
  templateUrl: './issuecreate.component.html',
  styleUrls: ['./issuecreate.component.css']
})
export class IssueCreateComponent implements OnInit {
  issueForm: FormGroup;
  issueTrackerConnection: IssueTrackerConnection;
  issue: Issue;
  issueTypes: IssueType [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private issueTrackerConnectionService: IssueTrackerConnectionService,
    private issueService: IssueService,
    private issueTypeService: IssueTypeService,
    private fb: FormBuilder
  ) {
    this.issueForm = this.fb.group({
      identifier: ['', [Validators.required, Validators.minLength(2)]],
      title: ['', [Validators.required, Validators.minLength(5)]],
      openDate: ['', [Validators.required]],
      closeDate: ['', [Validators.required]],
      issueType: [IssueType.BUG, [Validators.required]],
      note: [''],
      enabled: [false]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.issueTrackerConnectionService.getIssueTrackerConnection(params['idIssueTrackerConnection']).subscribe(
        issueTrackerConnection => {
          this.issueTrackerConnection = issueTrackerConnection;
          this.issueTrackerConnectionService.getStudy(this.issueTrackerConnection).subscribe(
            study => this.issueTrackerConnection.study = study
          );
        }
      );
      if (params['idIssue'] !== undefined) {
        this.issueService.getIssue(params['idIssue']).subscribe(
          issue => {
            this.issue = issue;
            this.issueForm.get('identifier').setValue(this.issue.identifier);
            this.issueForm.get('title').setValue(this.issue.title);
            this.issueForm.get('openDate').setValue(formatDate(this.issue.openDate, 'yyyy-MM-ddTHH:mm:ss', 'pt_BR', 'GMT-3'));
            this.issueForm.get('closeDate').setValue(formatDate(this.issue.closeDate, 'yyyy-MM-ddTHH:mm:ss', 'pt_BR', 'GMT-3'));
            this.issueForm.get('issueType').setValue(this.issue.issueType);
            this.issueForm.get('note').setValue(this.issue.note);
            this.issueForm.get('enabled').setValue(this.issue.enabled);
          }
        );
      }
      this.issueTypeService.getIssueTypes().subscribe(issueTypes => this.issueTypes = issueTypes);
    });
  }

  create() {
    this.issueService.createIssue(
      this.issueTrackerConnection.id,
      this.issueForm.get('identifier').value,
      this.issueForm.get('title').value,
      new Date(this.issueForm.get('openDate').value),
      new Date(this.issueForm.get('closeDate').value),
      this.issueForm.get('enabled').value,
      this.issueForm.get('note').value,
      this.issueForm.get('issueType').value
    ).subscribe(
      result => {
        this.router.navigate(
          ['/issuetrackerconnection/listissues/' + this.issueTrackerConnection.study.id + '/' + this.issueTrackerConnection.id]
        );
      }
    );
  }

  update() {
    this.issueService.updateIssue(
      this.issue.id,
      this.issueTrackerConnection.id,
      this.issueForm.get('identifier').value,
      this.issueForm.get('title').value,
      new Date(this.issueForm.get('openDate').value),
      new Date(this.issueForm.get('closeDate').value),
      this.issueForm.get('enabled').value,
      this.issueForm.get('note').value,
      this.issueForm.get('issueType').value
    ).subscribe(
      result => {
        this.router.navigate(
          ['/issuetrackerconnection/listissues/' + this.issueTrackerConnection.study.id + '/' + this.issueTrackerConnection.id]
        );
      }
    );
  }
}
