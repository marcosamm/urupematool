import { UriStats } from '.';

export interface TimeStats {
    date: Date;
    count: number;
    urisStats: UriStats[];
}
