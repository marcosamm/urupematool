import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IssueService, ChangedFileService, RevisionService, FileChangeTypeService } from '../../service';
import { Issue, ChangedFile, FileChangeType, Revision } from '../../model';

@Component({
  selector: 'app-changedfilecreate',
  templateUrl: './changedfilecreate.component.html',
  styleUrls: ['./changedfilecreate.component.css']
})
export class ChangedFileCreateComponent implements OnInit {
  changedFileForm: FormGroup;
  issue: Issue;
  revision: Revision;
  changedFile: ChangedFile;
  fileChangeTypes: FileChangeType[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private changedFileService: ChangedFileService,
    private issueService: IssueService,
    private revisionService: RevisionService,
    private fileChangeTypeService: FileChangeTypeService,
    private fb: FormBuilder
  ) {
    this.changedFileForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(10)]],
      note: [''],
      enabled: [false],
      fileChangeType: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.fileChangeTypeService.getFileChangeTypes().subscribe(fileChangeTypes => this.fileChangeTypes = fileChangeTypes);
      this.issueService.getIssue(params['idIssue']).subscribe(
        issue => { 
          this.issue = issue;
          this.issueService.getIssueTrackerConnectionByIssue(this.issue).subscribe(issueTrackerConnection => {
            this.issue.issueTrackerConnection = issueTrackerConnection;
          });
        }
      );
      this.revisionService.getRevision(params['idRevision']).subscribe(
        revision => { 
          this.revision = revision;
        }
      );
      if (params['idChangedFile'] !== undefined) {
        this.changedFileService.getChangedFile(params['idChangedFile']).subscribe(
          changedFile => {
            this.changedFile = changedFile;
            this.changedFileForm.get('name').setValue(this.changedFile.name);
            this.changedFileForm.get('note').setValue(this.changedFile.note);
            this.changedFileForm.get('enabled').setValue(this.changedFile.enabled);
            this.changedFileForm.get('fileChangeType').setValue(this.changedFile.fileChangeType);
          }
        );
      }
    });
  }

  create() {
    this.changedFileService.createChangedFile(
      this.revision.id,
      this.changedFileForm.get('name').value,
      this.changedFileForm.get('note').value,
      this.changedFileForm.get('enabled').value,
      this.changedFileForm.get('fileChangeType').value
    ).subscribe(
      result => { this.router.navigate(['/issuetrackerconnection/issuedetail/' + + this.issue.issueTrackerConnection.id + '/'+ this.issue.id]); }
    );
  }

  update() {
    this.changedFileService.updateChangedFile(
      this.changedFile.id,
      this.revision.id,
      this.changedFileForm.get('name').value,
      this.changedFileForm.get('note').value,
      this.changedFileForm.get('enabled').value,
      this.changedFileForm.get('fileChangeType').value
    ).subscribe(
      result => { this.router.navigate(['/issuetrackerconnection/issuedetail/' + + this.issue.issueTrackerConnection.id + '/'+ this.issue.id]); }
    );
  }
}
