package br.ufrn.ase.urupema.extensible.issuetrackertype.repository;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.IssueTrackerType;
import br.ufrn.ase.urupema.extensible.issuetrackertype.IssueTrackerTypePlugin;

@Repository
public class IssueTrackerTypeRepository {
	Map<String, IssueTrackerType> mapIssueTrackerTypes;
	
	public IssueTrackerTypeRepository(@Value("${extensionPackages}") final String extensionPackages) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		mapIssueTrackerTypes = new TreeMap<>();
		
		ConfigurationBuilder builder = new ConfigurationBuilder();
		List<URL> urls = new ArrayList<>(ClasspathHelper.forPackage("br.ufrn.ase.urupema"));
		if(extensionPackages != null && !extensionPackages.isBlank()) {
			for(String pack : extensionPackages.split(",")) {
				if(!pack.isBlank()) {
					urls.addAll(ClasspathHelper.forPackage(pack.trim()));
				}
			}
		}
		builder.setUrls(urls);
		Reflections reflections = new Reflections(builder);
		Set<Class<? extends IssueTrackerTypePlugin>> classes = reflections.getSubTypesOf(IssueTrackerTypePlugin.class);
		for (Class<? extends IssueTrackerTypePlugin> class1 : classes) {
			if(!Modifier.isAbstract(class1.getModifiers()) && !class1.isInterface()) {
				IssueTrackerTypePlugin inst = class1.getConstructor().newInstance();
				if(!mapIssueTrackerTypes.containsKey(inst.getId())) {
					mapIssueTrackerTypes.put(inst.getId(), inst);
				} else {
					throw new InstantiationError("Duplicate ID found for IssueTrackerType");
				}
			}
		}
	}
	
	public IssueTrackerTypePlugin findById(String id) throws InstantiationException, IllegalAccessException{
		return (IssueTrackerTypePlugin) mapIssueTrackerTypes.get(id);
	}
	
	public Collection<IssueTrackerType> findAll() throws InstantiationException, IllegalAccessException{
		return mapIssueTrackerTypes.values();
	}
}
