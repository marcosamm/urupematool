import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SourceConnectionService, LogGroupService } from '../../service';
import { SourceConnection } from '../../model/SourceConnection';
import { SuspiciousFile } from '../../model/SuspiciousFile';
import {TreeNode} from 'primeng/api';


@Component({
  selector: 'app-suspiciousfilesallgroups',
  templateUrl: './suspiciousfilesallgroups.component.html',
  styleUrls: ['./suspiciousfilesallgroups.component.css']
})
export class SuspiciousFilesAllGroupsComponent implements OnInit {
  filesTree: TreeNode[];
  selected: TreeNode;

  sourceConnection: SourceConnection;
  suspiciousFiles: SuspiciousFile [];

  constructor(
    private route: ActivatedRoute,
    private sourceConnectionService: SourceConnectionService
  ) { }

  reducePath = (nodes: TreeNode[], path: string) => {
    const split = path.split('/');

    // 2.1
    if (split.length === 1) {
        return [
            ...nodes,
            {
                label: split[0],
                icon: 'fa-file-o'
            }
        ];
    }

    // 2.2
    if (nodes.findIndex(n => n.label === split[0]) === -1) {
        return [
            ...nodes,
            {
                label: split[0],
                icon: 'fa-folder',
                children: this.reducePath([], split.slice(1).join('/'))
            }
        ];
    }

    // 2.3
    return nodes.map(n => {
        if (n.label !== split[0]) {
            return n;
        }

        return Object.assign({}, n, {
            children: this.reducePath(n.children, split.slice(1).join('/'))
        });
    });
  }

  fillPathAndCountFiles(treeNode: TreeNode, prefix: string) {
    if (prefix !== '') {
      treeNode.data = prefix + '/' + treeNode.label;
    } else {
      treeNode.data = treeNode.label;
    }

    let count = 0;

    if (treeNode.icon === 'fa-file-o') {
      count = 1;
    }

    if (treeNode.children !== undefined) {
      treeNode.children.forEach(child => {
        count += this.fillPathAndCountFiles(child, treeNode.data);
      });
    }

    if (count > 0 && treeNode.icon !== 'fa-file-o') {
      treeNode.label += ' (' + count + ')';
    }

    treeNode['count'] = count;

    return count;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.sourceConnectionService.getSourceConnection(params['idSourceConnection']).subscribe(
        sourceConnection => {
          this.sourceConnection = sourceConnection;
          this.sourceConnectionService.getSuspiciousFilesForAllLogGroups(sourceConnection).subscribe(
            suspicousFiles => {
              this.suspiciousFiles = suspicousFiles;
              const fileNames = this.suspiciousFiles.map(suspiciousFile => suspiciousFile.name);
              const fileNamesUniques = Array.from(new Set(fileNames)).sort((a, b) =>  (a > b ? 1 : -1));
              const filesTree = fileNamesUniques.reduce(this.reducePath, []);

              filesTree.forEach(element => {
                this.fillPathAndCountFiles(element, '');
              });

              this.filesTree = filesTree;
            }
          );
        }
      );
    });
  }
}
