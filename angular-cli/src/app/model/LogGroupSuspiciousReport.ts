export interface LogGroupSuspiciousReport {
    title: string;
    description: string;
    affectedURIs: string;
    suspiciousFiles: string;
    stackTraceSamples: string[];
}
