package br.ufrn.ase.urupema.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@SequenceGenerator(name = "ID_SOURCE_SEQUENCE", sequenceName = "source_id_sequence", allocationSize = 1)
@Getter
@Setter
@ToString(of= {"id", "name"})
public class Source implements Serializable, Comparable<Source> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_SOURCE_SEQUENCE")
	private Long id;
	
	private String sourceTypeId;
	
	private transient SourceType sourceType;
	
	@Column(nullable = false, unique=true)
	private String name;
	
	private String urlConnection;
	
	private String userName;
	
	private String password;
	
	private String accessIndexName;
	private String accessDocumentType;
	
	private String errorIndexName;
	private String errorDocumentType;
	
	private String executionIndexName;
	private String executionDocumentType;

	@Override
	public int compareTo(Source o) {
		return name.compareTo(o.getName());
	}
	
	public void setSourceType(SourceType sourceType){
		this.sourceType = sourceType;
		if(sourceType != null) {
			this.sourceTypeId = sourceType.getId();
		}
	}
}
