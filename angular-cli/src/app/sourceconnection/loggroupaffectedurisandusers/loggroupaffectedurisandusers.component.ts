import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Table } from 'primeng/table';
import { SourceConnectionService, LogGroupService } from '../../service';
import { SourceConnection } from '../../model/SourceConnection';
import { LogGroup } from '../../model/LogGroup';
import { UriStats } from '../../model';


@Component({
  selector: 'app-loggroupaffectedurisandusers',
  templateUrl: './loggroupaffectedurisandusers.component.html',
  styleUrls: ['./loggroupaffectedurisandusers.component.css']
})
export class LogGroupAffectedUrisAndUsersComponent implements OnInit {
  @ViewChild('dt', {static: false}) tableRef: Table;
  sourceConnection: SourceConnection;
  logGroup: LogGroup;
  urisStats: UriStats[];

  constructor(
    private route: ActivatedRoute,
    private logGroupService: LogGroupService,
    private sourceConnectionService: SourceConnectionService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.sourceConnectionService.getSourceConnection(params['idSourceConnection']).subscribe(
        sourceConnection => {
          this.sourceConnection = sourceConnection;
          this.logGroupService.getLogGroup(params['idLogGroup']).subscribe(
            logGroup => {
              this.logGroup = logGroup;
            }
          );
          this.logGroupService.getAffectedUrisByLogGroupId(params['idLogGroup']).subscribe(
            urisStats => {
              this.urisStats = urisStats;
            }
          );
        }
      );
    });
  }
}
