package br.ufrn.ase.urupema.extensible.correlator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.domain.GroupCorrelator;
import br.ufrn.ase.urupema.domain.GroupCorrelatorType;
import br.ufrn.ase.urupema.domain.GroupRelationship;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.repository.WebExecutionRepository;
import br.ufrn.ase.urupema.repository.factory.LogRepositoryFactory;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public abstract class AbstractGroupCorrelatorCoveredBy implements GroupCorrelatorTypePlugin {
	protected LogRepositoryFactory logRepositoryFactory;
	protected WebExecutionRepository originRepository;
	protected WebExecutionRepository destinyRepository;
	protected Set<GroupAggregatorType> compatibleOriginAggregatorTypes;
	protected Set<GroupAggregatorType> compatibleDestinyAggregatorTypes;

	@Override
	public int compareTo(GroupCorrelatorType o) {
		return getId().compareTo(o.getId());
	}
	
	@Override
	public List<GroupRelationship> correlate(GroupCorrelator groupCorrelator) throws OperationNotSupportedException, IOException {
		originRepository = (WebExecutionRepository) logRepositoryFactory.getLogRepository(groupCorrelator.getOrigin());
		destinyRepository = (WebExecutionRepository) logRepositoryFactory.getLogRepository(groupCorrelator.getDestiny());
		List<GroupRelationship> relationships = new ArrayList<>();
		
		log.debug("groupCorrelator.id = "+groupCorrelator.getId());
		log.debug("groupCorrelator.sourceConnectionOrigin.id = "+groupCorrelator.getOrigin().getId());
		log.debug("originLogGroups.size() = "+groupCorrelator.getOrigin().getLogGroups().size());
		log.debug("groupCorrelator.sourceConnectionDestiny.id = "+groupCorrelator.getDestiny().getId());
		log.debug("destinyLogGroups.size() = "+groupCorrelator.getDestiny().getLogGroups().size());
		
		for (LogGroup originLogGroup : groupCorrelator.getOrigin().getLogGroups()) {
			for(LogGroup destinyLogGroup : groupCorrelator.getDestiny().getLogGroups()) {
				if(originLogGroup.getLabel().equals(destinyLogGroup.getLabel())) {
					log.debug(originLogGroup.getLabel() + ": " + originLogGroup.getId() + " x " + destinyLogGroup.getId());
					if(!groupCorrelator.existsRelashionship(originLogGroup, destinyLogGroup)) {
						log.debug("Não existe relacionamento cadastrado entre os grupos. Procurando relacionamento... ");
						relationships.addAll(correlate(originLogGroup, destinyLogGroup));
					}else {
						log.debug("Já existe relacionamento cadastrado entre os grupos. Nada a ser feito!");
					}
				}
			}
		}
		
		return relationships;
	}
	
	List<GroupRelationship> correlate(LogGroup origin, LogGroup destiny) throws IOException {
		List<GroupRelationship> relationships = new ArrayList<>();
		
		Map<String, WebExecutionTrace> cache = new HashMap<>();
		for (LogGroup originSubGroup : origin.getSubGroups()) {
			Integer qtdTraceElementsOrigin = Integer.parseInt(originSubGroup.getMoreInformation().get("numTraceElements"));
			if(qtdTraceElementsOrigin > 0) {
				GroupRelationship relationship = correlate(originSubGroup, destiny.getSubGroups(), cache);
				if(relationship != null) {
					relationships.add(relationship);
				}
			}
		}
		
		return relationships;
	}
	
	abstract GroupRelationship correlate(LogGroup group, Collection<LogGroup> groups, Map<String, WebExecutionTrace> cache) throws IOException;
}