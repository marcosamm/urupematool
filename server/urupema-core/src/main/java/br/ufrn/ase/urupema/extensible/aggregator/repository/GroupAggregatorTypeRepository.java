package br.ufrn.ase.urupema.extensible.aggregator.repository;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.extensible.aggregator.GroupAggregatorTypePlugin;

@Repository
public class GroupAggregatorTypeRepository {
	Map<String, GroupAggregatorType> mapGroupAggregator;
	
	public GroupAggregatorTypeRepository(@Value("${extensionPackages}") final String extensionPackages, @Value("${excludedClasses}") final String excludedClasses)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException {
		mapGroupAggregator = new TreeMap<>();
		
		List<String> excludedClassesList = Arrays.asList(excludedClasses.split(","));
		
		ConfigurationBuilder builder = new ConfigurationBuilder();
		List<URL> urls = new ArrayList<>(ClasspathHelper.forPackage("br.ufrn.ase.urupema"));
		if(extensionPackages != null && !extensionPackages.isBlank()) {
			for(String pack : extensionPackages.split(",")) {
				if(!pack.isBlank()) {
					urls.addAll(ClasspathHelper.forPackage(pack.trim()));
				}
			}
		}
		builder.setUrls(urls);
		Reflections reflections = new Reflections(builder);    
		Set<Class<? extends GroupAggregatorTypePlugin>> classes = reflections.getSubTypesOf(GroupAggregatorTypePlugin.class);
		for (Class<? extends GroupAggregatorTypePlugin> class1 : classes) {
			if(!Modifier.isAbstract(class1.getModifiers()) && !class1.isInterface() && !excludedClassesList.contains(class1.getName())) {
				GroupAggregatorTypePlugin inst = class1.getConstructor().newInstance();
				mapGroupAggregator.put(inst.getId(), inst);
			}
		}
	}
	
	public GroupAggregatorTypePlugin findById(String id) throws InstantiationException, IllegalAccessException{
		return (GroupAggregatorTypePlugin) mapGroupAggregator.get(id);
	}
	
	public Collection<GroupAggregatorType> findAll() throws InstantiationException, IllegalAccessException{
		return mapGroupAggregator.values();
	}
}
