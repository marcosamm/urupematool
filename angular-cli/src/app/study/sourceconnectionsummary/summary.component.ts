import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { StudyService } from '../../service';
import { SourceConnection } from '../../model/SourceConnection';
import { Study } from '../../model';

@Component({
  selector: 'app-sourceconnectionsummary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SourceConnectionSummaryComponent implements OnInit {
  study: Study;
  sourceConnectionMap: Map<number, SourceConnection> = new Map();

  sourceConnectionCols: any[] = [];
  cols: any[];
  rows: any[];
  rowTotal: any;

  constructor(
    private route: ActivatedRoute,
    private studyService: StudyService
  ) { }

  ngOnInit() {
    this.cols = [
      { field: 'uri', header: 'URI' }
    ];
    this.route.params.subscribe(params => {
      this.studyService.getStudy(params['id']).subscribe(
        study => {
          this.study = study;
          for (const sc of study.sourceConnections) {
            this.sourceConnectionMap.set(sc.id, sc);
          }
          this.findDist(study.id);
        }
      );
    });
  }

  findDist(id: number) {
    this.studyService.getSourceConnectionSummary(this.study).subscribe(
      dados => {
        this.rows = [];
        this.rowTotal = {uri: 'TOTAL'};
        Object.keys(dados).forEach(
          uri => {
            const row = {uri: uri};
            Object.keys(dados[uri]).forEach(
              scsKey => {
                const sourceConnectionSummary = dados[uri][scsKey];
                if (!this.sourceConnectionCols.find(
                  x => x.field === sourceConnectionSummary.idSourceConnection)
                ) {
                  this.sourceConnectionCols.push(
                    {
                      field: sourceConnectionSummary.idSourceConnection,
                      header: this.sourceConnectionMap.get(sourceConnectionSummary.idSourceConnection).name
                    }
                  );
                }

                const nomFieldNumElements = sourceConnectionSummary.idSourceConnection + '_numElements';
                const nomFieldNumGroups = sourceConnectionSummary.idSourceConnection + '_numGroups';
                if (!this.cols.find(x => x.field === nomFieldNumElements)) {
                  this.cols.push({ field: nomFieldNumElements, header: 'Nº Elements' });
                }
                if (!this.cols.find(x => x.field === nomFieldNumGroups)) {
                  this.cols.push({ field: nomFieldNumGroups, header: 'Nº Groups' });
                }

                row[nomFieldNumElements] = sourceConnectionSummary.logsCount;
                if (this.rowTotal[nomFieldNumElements] === undefined) {
                  this.rowTotal[nomFieldNumElements] = 0;
                }
                this.rowTotal[nomFieldNumElements] = this.rowTotal[nomFieldNumElements] + row[nomFieldNumElements];

                row[nomFieldNumGroups] = sourceConnectionSummary.groupsCount;
                if (this.rowTotal[nomFieldNumGroups] === undefined) {
                  this.rowTotal[nomFieldNumGroups] = 0;
                }
                this.rowTotal[nomFieldNumGroups] = this.rowTotal[nomFieldNumGroups] + row[nomFieldNumGroups];
              }
            );
            this.rows.push(row);
          }
        );
      }
    );
  }
}
