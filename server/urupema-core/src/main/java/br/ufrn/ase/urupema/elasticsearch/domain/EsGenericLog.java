package br.ufrn.ase.urupema.elasticsearch.domain;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import lombok.Getter;

@Getter
public class EsGenericLog {
	private String id;
	private Date date;
	private String url;
	private String userLogin;
		
	private Map<String, String> moreInformation = new TreeMap<>();
	
	public EsGenericLog(String id, Date date, String url, String userLogin) {
		super();
		this.id = id;
		this.date = date;
		this.url = url;
		this.userLogin = userLogin;
	}
	
	public String getRequestURL() {
		return url;
	}
	
	public String getRequestURI () {
		int indexStart = getRequestURL().indexOf("/", 8);//First '/' after 'http://' ou 'https://'
		int indexEnd = getRequestURL().indexOf("?");
		if(indexEnd < 0) {
			indexEnd = getRequestURL().length();
		}
		return getRequestURL().substring(indexStart, indexEnd);
	}
	
	public String getIdentifier() {
		return id;
	}
	
	public Map<String, String> getMoreInformation() {
		if(userLogin != null) {
			moreInformation.put("userLogin", userLogin);
		}
		return moreInformation;
	}
}
