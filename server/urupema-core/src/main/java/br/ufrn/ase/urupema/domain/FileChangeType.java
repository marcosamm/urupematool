package br.ufrn.ase.urupema.domain;

public enum FileChangeType {
	ADDED,
	MODIFIED,
	DELETED,
	RENAMED
}
