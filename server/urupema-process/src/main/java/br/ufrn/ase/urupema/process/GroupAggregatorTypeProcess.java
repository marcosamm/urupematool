package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.extensible.aggregator.repository.GroupAggregatorTypeRepository;

@Component
public class GroupAggregatorTypeProcess {
	@Autowired
	private GroupAggregatorTypeRepository repository;
	
	public GroupAggregatorType findById(String id) throws IOException, InstantiationException, IllegalAccessException {
		return repository.findById(id);
	}
	
	public Collection<GroupAggregatorType> findAll() throws InstantiationException, IllegalAccessException{
		return repository.findAll();
	}
}
