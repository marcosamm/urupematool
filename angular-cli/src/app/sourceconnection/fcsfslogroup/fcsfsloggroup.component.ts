import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SourceConnectionService, LogGroupService } from '../../service';
import { SourceConnection } from '../../model/SourceConnection';
import { LogGroup } from '../../model';

@Component({
  selector: 'app-fcsfsloggroup',
  templateUrl: './fcsfsloggroup.component.html',
  styleUrls: ['./fcsfsloggroup.component.css']
})
export class FCSFsLogGroupComponent implements OnInit {
  sourceConnection: SourceConnection;
  logGroup: LogGroup;

  constructor(
    private route: ActivatedRoute,
    private logGroupService: LogGroupService,
    private sourceConnectionService: SourceConnectionService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.findLogGroup(params['idLogGroup']);
      this.sourceConnectionService.getSourceConnection(params['idSourceConnection']).subscribe(
        sourceConnection => this.sourceConnection = sourceConnection
      );
    });
  }

  findLogGroup(idLogGroup: number) {
    this.logGroupService.getLogGroup(idLogGroup).subscribe(
      logGroup => {
        this.logGroup = logGroup;
        this.logGroupService.getMapFCSFsByLogGroup(logGroup).subscribe(
          mapFCSFs => {
            this.logGroup.mapFCSFs = mapFCSFs;
          }
        );
      }
    );
  }
}
