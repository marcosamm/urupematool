package br.ufrn.ase.urupema.extensible.aggregator;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.extensible.aggregator.repository.GroupAggregatorTypeRepository;
import br.ufrn.ase.urupema.repository.WebExecutionRepository;
import br.ufrn.ase.urupema.repository.factory.LogRepositoryFactory;

@Component
public class GroupAggregatorTypeFactory {
	@Autowired
	private LogRepositoryFactory repositoryFactory;
	
	@Autowired
	private GroupAggregatorTypeRepository groupAggregatorRepository;
	
	public GroupAggregatorTypePlugin getInstance(SourceConnection sourceConnection) throws OperationNotSupportedException, InstantiationException, IllegalAccessException {
		WebExecutionRepository repository = null;
		
		switch(sourceConnection.getLogType()) {
			case ERROR:
				repository = repositoryFactory.getErrorLogRepository(sourceConnection.getSource());
				break;
			case EXECUTION:
				repository = repositoryFactory.getScenarioRepository(sourceConnection.getSource());
				break;
			default:
				break;
		}
		
		GroupAggregatorTypePlugin ga = groupAggregatorRepository.findById(sourceConnection.getGroupAggregatorTypeId());
		ga.setLogRepository(repository);
		return ga;
	}
}
