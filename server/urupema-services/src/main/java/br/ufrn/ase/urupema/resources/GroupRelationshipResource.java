/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.GroupRelationship;
import br.ufrn.ase.urupema.process.GroupCorrelatorProcess;
import br.ufrn.ase.urupema.process.GroupRelationshipProcess;


//@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class GroupRelationshipResource {
	@Autowired
	GroupRelationshipProcess process;
	
	@Autowired
	GroupCorrelatorProcess groupCorrelatorProcess;
	
	@GetMapping("/grouprelationship/listbygroupcorrelator")
	public List<GroupRelationship> findByGroupCorrelator(@RequestParam("idGroupCorrelator") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findByGroupCorrelator(groupCorrelatorProcess.findById(id));
	}
	
	@GetMapping("/grouprelationship/detail")
	public GroupRelationship findById(@RequestParam("id") Long id) throws IOException {
		return process.findById(id);
	}
}
