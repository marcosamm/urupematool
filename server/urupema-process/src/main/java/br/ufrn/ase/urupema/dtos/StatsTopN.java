package br.ufrn.ase.urupema.dtos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import br.ufrn.ase.urupema.domain.FileChangeType;
import br.ufrn.ase.urupema.domain.Issue;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.SuspiciousFile;
import lombok.Getter;

public class StatsTopN {
	private Issue issue;
	
	private LogGroup logGroup;
	
	private Set<FileChangeType> fileChangedTypes;
	
	private Set<String> changedFileNames;
	
	private Set<String> fileExtensions;
	
	@Getter
	private String sourcePackage;
	
	public StatsTopN() {
		
	}
	
	public StatsTopN(Issue issue, LogGroup logGroup, Set<FileChangeType> fileChangedTypes, Set<String> fileExtensions, String sourcePackage) {
		super();
		this.issue = issue;
		this.logGroup = logGroup;
		this.fileChangedTypes = fileChangedTypes;
		this.fileExtensions = fileExtensions;
		this.sourcePackage = sourcePackage;
	}
	
	public String getIssueIdentifier() {
		return issue.getIdentifier();
	}
	
	public Long getLogGroupId() {
		return logGroup.getId();
	}
	
	public int getLogGroupElementsCount() {
		return logGroup.getElementsCount();
	}
	
	public Set<String> getChangedFiles(){
		String pkgStart = sourcePackage.replace(".", "/");
		if(changedFileNames == null) {
			changedFileNames = issue.getEnabledChangedFiles(fileChangedTypes, fileExtensions).stream()
			.map(cf -> cf.substring(cf.indexOf(pkgStart)))
			.collect(Collectors.toSet());
		}
		return changedFileNames;
	}
	
	private Set<String> changedFilesInTopN(int n, String pkg) {
		List<SuspiciousFile> suspFiles = new ArrayList<>(logGroup.getSuspiciousFiles());
		String pkgStart = pkg.replace(".", "/");
		Set<String> changedAndSuspiciousFiles = new TreeSet<>(getChangedFiles());
		List<String> topNSuspiciousFiles = suspFiles.stream()
				.filter(sf -> sf.getName().startsWith(pkgStart))
				.filter(sf -> sf.getName().contains("."))
				.filter(sf -> fileExtensions.contains(
					sf.getName().substring(sf.getName().lastIndexOf(".")))
				)
				.sorted(Comparator.reverseOrder())
				.limit(n)
				.map(sf -> sf.getName())
				.collect(Collectors.toList());
		changedAndSuspiciousFiles.retainAll(topNSuspiciousFiles);
		return changedAndSuspiciousFiles;
	}
	
	public Set<String> changedFilesInTopN(int n) {
		return changedFilesInTopN(n, "");
	}
	
	public Set<String> changedFilesInTop1() {
		return changedFilesInTopN(1);
	}
	
	public Set<String> changedFilesInTop3() {
		return changedFilesInTopN(3);
	}
	
	public Set<String> changedFilesInTop5() {
		return changedFilesInTopN(5);
	}
	
	public Set<String> changedFilesInTop10() {
		return changedFilesInTopN(10);
	}
	
	private int numChangedFilesInTopN(int n, String pkg) {
		return changedFilesInTopN(n, pkg).size();
	}
	
	public int numChangedFilesInTopN(int n) {
		return numChangedFilesInTopN(n, "");
	}
	
	public int getNumChangedFilesInTop1() {
		return changedFilesInTop1().size();
	}
	
	public int getNumChangedFilesInTop3() {
		return changedFilesInTop3().size();
	}
	
	public int getNumChangedFilesInTop5() {
		return changedFilesInTop5().size();
	}
	
	public int getNumChangedFilesInTop10() {
		return changedFilesInTop10().size();
	}
	
	private List<SuspiciousFile> getTop(int k){
		List<SuspiciousFile> suspFiles = new ArrayList<>(logGroup.getSuspiciousFiles());
		return suspFiles.stream()
				.sorted(Comparator.reverseOrder())
				.limit(k)
				.collect(Collectors.toList());
	}
	
	public List<SuspiciousFile> getTop10(){
		return getTop(10);
	}
	
	public Set<String> sourceOnlyChangedFilesInTopN(int n) {
		return changedFilesInTopN(n, sourcePackage);
	}
	
	public int numSourceOnlyChangedFilesInTopN(int n) {
		return changedFilesInTopN(n, sourcePackage).size();
	}
	
	public Set<String> sourceOnlyChangedFilesInTop1() {
		return sourceOnlyChangedFilesInTopN(1);
	}
	
	public Set<String> sourceOnlyChangedFilesInTop3() {
		return sourceOnlyChangedFilesInTopN(3);
	}
	
	public Set<String> sourceOnlyChangedFilesInTop5() {
		return sourceOnlyChangedFilesInTopN(5);
	}
	
	public Set<String> sourceOnlyChangedFilesInTop10() {
		return sourceOnlyChangedFilesInTopN(10);
	}
	
	public int getNumSourceOnlyChangedFilesInTop1() {
		return numSourceOnlyChangedFilesInTopN(1);
	}
	
	public int getNumSourceOnlyChangedFilesInTop3() {
		return numSourceOnlyChangedFilesInTopN(3);
	}
	
	public int getNumSourceOnlyChangedFilesInTop5() {
		return numSourceOnlyChangedFilesInTopN(5);
	}
	
	public int getNumSourceOnlyChangedFilesInTop10() {
		return numSourceOnlyChangedFilesInTopN(10);
	}
	
	private List<SuspiciousFile> getSourceOnlyTop(int k){
		List<SuspiciousFile> suspFiles = new ArrayList<>(logGroup.getSuspiciousFiles());
		String pkgStart = sourcePackage.replace(".", "/");
		return suspFiles.stream()
				.filter(sf -> sf.getName().startsWith(pkgStart))
				.sorted(Comparator.reverseOrder())
				.limit(k)
				.collect(Collectors.toList());
	}
	
	public List<SuspiciousFile> getSourceOnlyTop10(){
		return getSourceOnlyTop(10);
	}
	
	static float getAvePrecision(Collection<String> changedFiles, List<String> suspiciousFiles) {
		float avp = 0f;
		
		int truePositives = 0;
		int predictedPositives = 0;
		for(String sf : suspiciousFiles) {
			predictedPositives++;
			if(changedFiles.contains(sf)) {
				truePositives++;
				avp += truePositives / (float) predictedPositives;
			}
		}
		
		return avp / (float) changedFiles.size();
	}
	
	float getAveragePrecision(Collection<String> changedFiles, List<SuspiciousFile> suspiciousFiles) {
		return getAvePrecision(changedFiles, suspiciousFiles.stream().map(sf -> sf.getName()).collect(Collectors.toList()));
	}
	
	public float getAveragePrecision(int k) {
		return getAveragePrecision(getChangedFiles(), getTop(k));
	}
	
	public float getSourceOnlyAveragePrecision(int k) {
		return getAveragePrecision(getChangedFiles(), getSourceOnlyTop(k));
	}
	
	public String getChangedFileNotes(String changedFileName) {
		return issue.getChangedFileNotes(changedFileName).stream().collect( Collectors.joining("\n"));
	}
}
