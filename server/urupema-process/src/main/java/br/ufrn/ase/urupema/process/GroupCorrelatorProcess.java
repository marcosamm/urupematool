package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.List;

import javax.naming.OperationNotSupportedException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.GroupCorrelator;
import br.ufrn.ase.urupema.domain.GroupRelationship;
import br.ufrn.ase.urupema.repository.GroupCorrelatorRepository;

@Component
@Transactional
public class GroupCorrelatorProcess {
	@Autowired
	private GroupCorrelatorRepository groupCorrelatorRepository;
	
	@Autowired
	private GroupCorrelatorTypeProcess groupCorrelatorTypeProcess;
	
	public GroupCorrelator findById(Long id) throws IOException, InstantiationException, IllegalAccessException {
		GroupCorrelator gc = groupCorrelatorRepository.findById(id).get();
		if(gc != null) {
			gc.setGroupCorrelatorType(groupCorrelatorTypeProcess.findById(gc.getGroupCorrelatorTypeId()));
		}
		return gc;
	}
	
	public void save(GroupCorrelator groupCorrelator) {
		groupCorrelatorRepository.save(groupCorrelator);
		
	}
	
	public List<GroupCorrelator> findAll(){
		return groupCorrelatorRepository.findAll();
	}
	
	public void delete(Long id) throws InstantiationException, IllegalAccessException, IOException {
		GroupCorrelator gc = findById(id);
		gc.getStudy().getGroupCorrelators().remove(gc);
		groupCorrelatorRepository.delete(gc);
	}
	
	public List<GroupRelationship> correlate(Long idGroupCorrelator) throws InstantiationException, IllegalAccessException, OperationNotSupportedException, IOException{
		GroupCorrelator groupCorrelator = findById(idGroupCorrelator);
		List<GroupRelationship> relationships = groupCorrelatorTypeProcess.findById(groupCorrelator.getGroupCorrelatorTypeId()).correlate(groupCorrelator);
		groupCorrelator.addGroupRelationships(relationships);
		groupCorrelatorRepository.save(groupCorrelator);
		return relationships;
	}
}
