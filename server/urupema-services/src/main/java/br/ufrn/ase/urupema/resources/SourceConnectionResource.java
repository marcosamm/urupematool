/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.Source;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.Study;
import br.ufrn.ase.urupema.domain.SuspiciousFile;
import br.ufrn.ase.urupema.process.SourceConnectionProcess;
import br.ufrn.ase.urupema.process.StudyProcess;


@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class SourceConnectionResource {
	@Autowired
	StudyProcess studyProcess;
	
	@Autowired
	SourceConnectionProcess process;
	
	@GetMapping("/sourceconnection")
	public SourceConnection findById(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findById(id);
	}
	
	@PostMapping("/sourceconnection")
	public void put(@RequestBody SourceConnection sourceConnection) throws IOException {
		process.save(sourceConnection);
	}
	
	@PutMapping("/sourceconnection")
	public void update(@RequestBody SourceConnection sourceConnection) throws IOException, InstantiationException, IllegalAccessException {
		SourceConnection s = findById(sourceConnection.getId());
		//Copy properties ignoring logGroups
		BeanUtils.copyProperties(sourceConnection, s, "id", "logGroups", "study", "groupAggregatorType", "suspiciousFilesFinderType");
		process.save(s);
	}
	
	@DeleteMapping("/sourceconnection")
	public void delete(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		process.delete(id);
	}
	
	@GetMapping("/sourceconnection/logDistByNumTraceElements")
	public Map<String, Map<Integer, Integer>> getLogsDistByNumTraceElements(@RequestParam("idSourceConnection") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.getDistrElementsByNumTraceElements(id);
	}
	
	@GetMapping("/sourceconnection/study")
	public Study getStudy(@RequestParam("idSourceConnection") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.getStudy(id);
	}	
	
	@GetMapping("/sourceconnection/source")
	public Source getSource(@RequestParam("idSourceConnection") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.getSource(id);
	}
	
	@GetMapping("/sourceconnection/suspiciousFilesForAllLogGroups")
	public List<SuspiciousFile> getSuspiciousFilesForAllLogGroups(@RequestParam("idSourceConnection") Long id) throws IOException, InstantiationException, IllegalAccessException, OperationNotSupportedException {
		return process.getSuspiciousFilesForAllLogGroups(id);
	}
}
