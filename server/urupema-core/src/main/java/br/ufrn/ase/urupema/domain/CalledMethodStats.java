package br.ufrn.ase.urupema.domain;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(of = "qualifiedMethodName")
public class CalledMethodStats implements Comparable<CalledMethodStats>{
	private String qualifiedMethodName;
	private Long count;
	private Set<LineNumberStats> lineNumbersStats;
	
	public CalledMethodStats(String qualifiedMethodName, Long count) {
		this.qualifiedMethodName = qualifiedMethodName;
		this.count = count;
		this.lineNumbersStats = new TreeSet<>();
	}
	
	public void addCount(Long count){
		this.count += count;
	}
	
	public void addLineNumberStats(LineNumberStats lineNumberStats) {
		LineNumberStats exist = this.lineNumbersStats.stream()
				.filter(element -> element.equals(lineNumberStats))
				.findAny()
				.orElse(null);
		
		if(exist == null) {
			this.lineNumbersStats.add(lineNumberStats);
		} else {
			exist.addCount(lineNumberStats.getCount());
		}
		count += lineNumberStats.getCount();
	}
	
	public void addLineNumberStats(Collection<LineNumberStats> lineNumbersStats) {
		for (LineNumberStats lineNumberStats : lineNumbersStats) {
			addLineNumberStats(lineNumberStats);
		}
	}

	@Override
	public int compareTo(CalledMethodStats o) {
		int ret = o.getCount().compareTo(this.count);
		
		if(ret == 0) {
			ret = o.getQualifiedMethodName().compareTo(this.qualifiedMethodName);
		}
		
		return ret;
	}
}
