package br.ufrn.ase.urupema;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import({UrupemaCoreApplication.class})
@Configuration
public class UrupemaServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(UrupemaCoreApplication.class, args);
	}
}
