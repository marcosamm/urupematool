import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { StudyService } from '../../service';
import { Study } from '../../model';
import { GroupCorrelator } from '../../model/GroupCorrelator';

@Component({
  selector: 'app-sourceconnectionsummary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class GroupCorrelatorSummaryComponent implements OnInit {
  study: Study;
  groupCorrelatorMap: Map<number, GroupCorrelator> = new Map();

  groupCorrelatorCols: any[] = [];
  cols: any[];
  rows: any[];
  rowTotal: any;
  gcSelecteds: number[] = [];

  constructor(
    private route: ActivatedRoute,
    private studyService: StudyService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studyService.getStudy(params['id']).subscribe(
        study => {
          this.study = study;
          for (const gc of study.groupCorrelators) {
            this.groupCorrelatorMap.set(gc.id, gc);
            this.gcSelecteds.push(gc.id);
          }
          this.findDist(study.id);
        }
      );
    });
  }

  findDist(id: number) {
    this.groupCorrelatorCols = [];
    this.cols = [
      { field: 'uri', header: 'URI' }
    ];
    this.studyService.getGroupCorrelatorSummary(this.study).subscribe(
      dados => {
        this.rows = [];
        this.rowTotal = {uri: 'TOTAL'};
        Object.keys(dados).forEach(
          uri => {
            const row = {uri: uri};
            const bothRow: any = {};
            Object.keys(dados[uri]).forEach(
              gcKey => {
                const gcSummary = dados[uri][gcKey];
                if (this.gcIsSelected(Number(gcSummary.idGroupCorrelator))) {
                  if (!this.groupCorrelatorCols.find(
                    x => x.field === gcSummary.idGroupCorrelator)
                  ) {
                    this.groupCorrelatorCols.push(
                      {
                        field: gcSummary.idGroupCorrelator,
                        header: this.groupCorrelatorMap.get(gcSummary.idGroupCorrelator).name
                      }
                    );
                  }

                  const nomFieldNumElementsOrigin = gcSummary.idGroupCorrelator + '_numElementsOrigin';
                  if (!this.cols.find(x => x.field === nomFieldNumElementsOrigin)) {
                    this.cols.push({ field: nomFieldNumElementsOrigin, header: 'Nº Elements' });
                  }
                  const nomFieldNumGroupsOrigin = gcSummary.idGroupCorrelator + '_numGroupsOrigin';
                  if (!this.cols.find(x => x.field === nomFieldNumGroupsOrigin)) {
                    this.cols.push({ field: nomFieldNumGroupsOrigin, header: 'Nº Groups' });
                  }
                  const nomFieldNumElementsDestiny = gcSummary.idGroupCorrelator + '_numElementsDestiny';
                  if (!this.cols.find(x => x.field === nomFieldNumElementsDestiny)) {
                    this.cols.push({ field: nomFieldNumElementsDestiny, header: 'Nº Elements' });
                  }
                  const nomFieldNumGroupsDestiny = gcSummary.idGroupCorrelator + '_numGroupsDestiny';
                  if (!this.cols.find(x => x.field === nomFieldNumGroupsDestiny)) {
                    this.cols.push({ field: nomFieldNumGroupsDestiny, header: 'Nº Groups' });
                  }

                  row[nomFieldNumElementsOrigin] = gcSummary.idsLogsOrigin.length;
                  if (this.rowTotal[nomFieldNumElementsOrigin] === undefined) {
                    this.rowTotal[nomFieldNumElementsOrigin] = 0;
                  }
                  this.rowTotal[nomFieldNumElementsOrigin] = this.rowTotal[nomFieldNumElementsOrigin] + row[nomFieldNumElementsOrigin];

                  row[nomFieldNumGroupsOrigin] = gcSummary.idsGroupsOrigin.length;
                  if (this.rowTotal[nomFieldNumGroupsOrigin] === undefined) {
                    this.rowTotal[nomFieldNumGroupsOrigin] = 0;
                  }
                  this.rowTotal[nomFieldNumGroupsOrigin] = this.rowTotal[nomFieldNumGroupsOrigin] + row[nomFieldNumGroupsOrigin];

                  row[nomFieldNumElementsDestiny] = gcSummary.idsLogsDestiny.length;
                  if (this.rowTotal[nomFieldNumElementsDestiny] === undefined) {
                    this.rowTotal[nomFieldNumElementsDestiny] = 0;
                  }
                  this.rowTotal[nomFieldNumElementsDestiny] = this.rowTotal[nomFieldNumElementsDestiny] + row[nomFieldNumElementsDestiny];

                  row[nomFieldNumGroupsDestiny] = gcSummary.idsGroupsDestiny.length;
                  if (this.rowTotal[nomFieldNumGroupsDestiny] === undefined) {
                    this.rowTotal[nomFieldNumGroupsDestiny] = 0;
                  }
                  this.rowTotal[nomFieldNumGroupsDestiny] = this.rowTotal[nomFieldNumGroupsDestiny] + row[nomFieldNumGroupsDestiny];

                  if ('idsLogsOrigin' in bothRow) {
                    bothRow['idsLogsOrigin'] = this.retains(bothRow['idsLogsOrigin'], gcSummary.idsLogsOrigin);
                    bothRow['idsGroupsOrigin'] = this.retains(bothRow['idsGroupsOrigin'], gcSummary.idsGroupsOrigin);
                    bothRow['idsLogsDestiny'] = this.retains(bothRow['idsLogsDestiny'], gcSummary.idsLogsDestiny);
                    bothRow['idsGroupsDestiny'] = this.retains(bothRow['idsGroupsDestiny'], gcSummary.idsGroupsDestiny);
                  } else {
                    bothRow['idsLogsOrigin'] = gcSummary.idsLogsOrigin;
                    bothRow['idsGroupsOrigin'] = gcSummary.idsGroupsOrigin;
                    bothRow['idsLogsDestiny'] = gcSummary.idsLogsDestiny;
                    bothRow['idsGroupsDestiny'] = gcSummary.idsGroupsDestiny;
                  }
                }
              }
            );

            row['both_numElementsOrigin'] = bothRow['idsLogsOrigin'].length;
            row['both_numGroupsOrigin'] = bothRow['idsGroupsOrigin'].length;
            row['both_numElementsDestiny'] = bothRow['idsLogsDestiny'].length;
            row['both_numGroupsDestiny'] = bothRow['idsGroupsDestiny'].length;

            if ('both_numElementsOrigin' in this.rowTotal) {
              this.rowTotal['both_numElementsOrigin'] += row['both_numElementsOrigin'];
              this.rowTotal['both_numGroupsOrigin'] += row['both_numGroupsOrigin'];
              this.rowTotal['both_numElementsDestiny'] += row['both_numElementsDestiny'];
              this.rowTotal['both_numGroupsDestiny'] += row['both_numGroupsDestiny'];
            } else {
              this.rowTotal['both_numElementsOrigin'] = row['both_numElementsOrigin'];
              this.rowTotal['both_numGroupsOrigin'] = row['both_numGroupsOrigin'];
              this.rowTotal['both_numElementsDestiny'] = row['both_numElementsDestiny'];
              this.rowTotal['both_numGroupsDestiny'] = row['both_numGroupsDestiny'];
            }
            this.rows.push(row);
          }
        );
        this.cols.push({ field: 'both_numElementsOrigin', header: 'Nº Elements' });
        this.cols.push({ field: 'both_numGroupsOrigin', header: 'Nº Groups' });
        this.cols.push({ field: 'both_numElementsDestiny', header: 'Nº Elements' });
        this.cols.push({ field: 'both_numGroupsDestiny', header: 'Nº Groups' });
      }
    );
  }

  getGroupsCorrelators(): GroupCorrelator [] {
    return Array.from(this.groupCorrelatorMap.values());
  }

  retains(array1: any[], array2: any[]): any[] {
    return array1.filter(x => array2.find(y => x === y));
  }

  gcIsSelected (gcId: number) {
    return this.gcSelecteds.find( gck => gck === gcId);
  }

  toogleGcSelected (gcId: number) {
    if (this.gcIsSelected(gcId)) {
      this.gcSelecteds = this.gcSelecteds.filter(obj => obj !== gcId);
    } else {
      this.gcSelecteds.push(gcId);
    }
    this.rows = undefined;
    this.findDist(this.study.id);
  }
}
