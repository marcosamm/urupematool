package br.ufrn.ase.urupema.extensible.correlator;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.domain.GroupRelationship;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.extensible.aggregator.GroupAggregatorByUriAndStacktrace;
import br.ufrn.ase.urupema.extensible.aggregator.GroupAggregatorByUriAndSubtrace;
import br.ufrn.ase.urupema.parser.StackTraceParser;
import br.ufrn.ase.urupema.repository.ErrorLogRepository;
import br.ufrn.ase.urupema.repository.WebExecutionRepository;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class GroupCorrelatorCoveredBy extends AbstractGroupCorrelatorCoveredBy {
	
	@Override
	public String getId() {
		return "urupema:covered_by";
	}
	
	@Override
	public String getName() {
		return "Covered By";
	}

	@Override
	public String getDescription() {
		return "group x is covered by related groups";
	}
	
	GroupRelationship correlate(LogGroup group, Collection<LogGroup> groups, Map<String, WebExecutionTrace> cache) throws IOException {
		GroupRelationship relationship = null;
		
		//Pega o representante do grupo de origem
		WebExecutionTrace reprOriginGroup = findWebExecutionTraceRpresentant(group, originRepository, cache);
		log.debug("***["+reprOriginGroup.getIdentifier()+"] x " + groups.size() + " representants");
		int i = 1;
		//Percorre os grupos de destino pegando seus representantes e comparando-os com o representante do grupo de origem
		for(LogGroup eqvGrp : groups) {
			String idRepresentantGrp = eqvGrp.getRepresentantId();
			Integer qtdTraceElementsOrigin = getNumTraceElements(group);
			Integer qtdTraceElementsDestiny = getNumTraceElements(eqvGrp);
			String [] originSignatures = group.getLabel().split("\n");
			if(qtdTraceElementsOrigin <= qtdTraceElementsDestiny && containsAll(eqvGrp.getLabel(), originSignatures)) {
				if(originSignatures.length == 1 || findWebExecutionTraceRpresentant(eqvGrp, destinyRepository, cache).contains(reprOriginGroup.getTraceElements())) {
					log.debug((i++) + " **** "+reprOriginGroup.getIdentifier()+" COVERED_BY "+ idRepresentantGrp);
					if(relationship == null) {
						relationship = new GroupRelationship();
						relationship.setGroup(group);
					}
					relationship.getRelateds().add(eqvGrp);
				}else {
					log.debug((i++) + " **** "+reprOriginGroup.getIdentifier()+" NOT_COVERED_BY "+ idRepresentantGrp);
				}
			}
		}
		
		return relationship;
	}
	
	private WebExecutionTrace findWebExecutionTraceRpresentant(LogGroup group, WebExecutionRepository repository, Map<String, WebExecutionTrace> cache) throws IOException {
		String reprOriginGroupKey = group.getSourceConnection().getId()+"."+group.getRepresentantId();
		WebExecutionTrace reprOriginGroup = cache.get(reprOriginGroupKey);
		if(reprOriginGroup == null) {
			if(group.getSourceConnection().getLogType() == LogType.ERROR) {
				StackTraceParser stp = new StackTraceParser(group.getSourceConnection().getRegexSignature(), group.getSourceConnection().getRegexIgnoreSignature());
				reprOriginGroup = ((ErrorLogRepository)repository).findById(group.getRepresentantId(), stp).get();
				
			}else {
				reprOriginGroup = repository.findById(group.getRepresentantId()).get();
			}
			cache.put(reprOriginGroupKey, reprOriginGroup);
		}
		return reprOriginGroup;
	}
	
	private int getNumTraceElements(LogGroup group) {
		int numTraceElements = 0;
		
		if(group.getMoreInformation().containsKey("numTraceElements")) {
			numTraceElements = Integer.parseInt(group.getMoreInformation().get("numTraceElements"));
		}else {
			numTraceElements = group.getLabel().split("\n").length;
		}
		
		return numTraceElements;
	}
	
	private boolean containsAll(String s, String [] strings) {
		boolean ret = true;
		
		for(String s1 : strings) {
			if(!s.contains(s1)) {
				ret = false;
				break;
			}
		}
		
		return ret;
	}

	@Override
	public Set<GroupAggregatorType> getCompatibleOriginAggregatorTypes() {
		if(compatibleOriginAggregatorTypes == null) {
			compatibleOriginAggregatorTypes = new TreeSet<>();
			compatibleOriginAggregatorTypes.add(new GroupAggregatorByUriAndStacktrace());
			compatibleOriginAggregatorTypes.add(new GroupAggregatorByUriAndSubtrace());
		}
		return compatibleOriginAggregatorTypes;
	}

	@Override
	public Set<GroupAggregatorType> getCompatibleDestinyAggregatorTypes() {
		if(compatibleDestinyAggregatorTypes == null) {
			compatibleDestinyAggregatorTypes = new TreeSet<>();
			compatibleDestinyAggregatorTypes.add(new GroupAggregatorByUriAndStacktrace());
		}
		return compatibleDestinyAggregatorTypes;
	}
}