import { IssueTrackerType } from "./IssueTrackerType";

export interface IssueTrackerSource {
    id: number;
    name: string;
    issueTrackerTypeId: string;
    issueTrackerType: IssueTrackerType;
    connectionProperties: Map<string, string>;
}
