package br.ufrn.ase.urupema.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.ChangedFile;
import br.ufrn.ase.urupema.domain.Revision;

@Repository
public interface ChangedFileRepository extends JpaRepository<ChangedFile, Long>{
	public List<ChangedFile> findByRevision(Revision revision);
}
