package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.GroupCorrelatorType;
import br.ufrn.ase.urupema.extensible.correlator.repository.GroupCorrelatorTypeRepository;

@Component
public class GroupCorrelatorTypeProcess {
	@Autowired
	private GroupCorrelatorTypeRepository repository;
	
	public GroupCorrelatorType findById(String id) throws IOException, InstantiationException, IllegalAccessException {
		return repository.findById(id);
	}
	
	public Collection<GroupCorrelatorType> findAll() throws InstantiationException, IllegalAccessException{
		return repository.findAll();
	}
}
