package br.ufrn.ase.urupema.process;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.AffectedUri;
import br.ufrn.ase.urupema.domain.ReportedStackTrace;
import br.ufrn.ase.urupema.repository.AffectedUriRepository;

@Component
@Transactional
public class AffectedUriProcess {
	@Autowired
	private AffectedUriRepository repository;
	
	public AffectedUri findById(Long id) {
		return repository.findById(id).get();
	}
	
	public void save(AffectedUri affectedUri) {
		repository.save(affectedUri);
	}
	
	public void delete(Long id) {
		AffectedUri a = findById(id);
		a.getReportedStackTrace().removeAffectedUri(a);
		repository.delete(a);
	}
	
	public List<AffectedUri> findByReportedStackTrace(ReportedStackTrace reportedStackTrace){
		return repository.findByReportedStackTrace(reportedStackTrace);
	}
}
