/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.GroupCorrelatorType;
import br.ufrn.ase.urupema.process.GroupCorrelatorTypeProcess;


//@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class GroupCorrelatorTypeResource {
	@Autowired
	private GroupCorrelatorTypeProcess process;
	
	@GetMapping("/groupcorrelatortype/list")
	public Collection<GroupCorrelatorType> findAll() throws IOException, InstantiationException, IllegalAccessException {
		return process.findAll();
	}
	
	@GetMapping("/groupcorrelatortype/detail")
	public GroupCorrelatorType findById(@RequestParam("id") String id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findById(id);
	}
}
