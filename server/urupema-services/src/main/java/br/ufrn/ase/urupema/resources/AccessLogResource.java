/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.UriStats;
import br.ufrn.ase.urupema.process.AccessLogProcess;
import br.ufrn.ase.urupema.process.SourceConnectionProcess;


//@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class AccessLogResource {
	@Autowired
	SourceConnectionProcess sourceConnectionProcess;
	
	@Autowired
	AccessLogProcess accessLogProcess;
	
	@GetMapping("/access/count")
	public Set<? extends UriStats> countAccessesGroupedByUri(
			@RequestParam("idSourceConnection") Long idSourceConnection
			) throws JsonParseException, JsonMappingException, IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		SourceConnection sc = sourceConnectionProcess.findById(idSourceConnection);
		return accessLogProcess.countAccessesGroupedByUri(sc);
	}
}
