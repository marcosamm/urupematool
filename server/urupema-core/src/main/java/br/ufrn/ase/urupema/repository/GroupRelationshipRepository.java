package br.ufrn.ase.urupema.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.GroupCorrelator;
import br.ufrn.ase.urupema.domain.GroupRelationship;

@Repository
public interface GroupRelationshipRepository extends JpaRepository<GroupRelationship, Long>{
	public List<GroupRelationship> findByGroupCorrelator(GroupCorrelator grouCorrelator);
}
