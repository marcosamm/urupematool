package br.ufrn.ase.urupema.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.LogGroup;

@Repository
public class LogGroupRepositoryImpl implements LogGroupRepositoryCustom {
	@Autowired
	EntityManager em;

	@Override
	public List<LogGroup> findByLabelLikeMultipleStrings(String label) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
	    CriteriaQuery<LogGroup> cq = cb.createQuery(LogGroup.class);
	 
	    Root<LogGroup> book = cq.from(LogGroup.class);
	    List<Predicate> predicates = new ArrayList<>();
	    predicates.add(cb.like(book.get("label"), label));
	    cq.where(predicates.toArray(new Predicate[0]));
	 
	    return em.createQuery(cq).getResultList();
	}

}
