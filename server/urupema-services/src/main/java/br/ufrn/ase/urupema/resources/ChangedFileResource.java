package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.ChangedFile;
import br.ufrn.ase.urupema.process.ChangedFileProcess;
import br.ufrn.ase.urupema.process.RevisionProcess;


@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class ChangedFileResource {	
	@Autowired
	ChangedFileProcess process;
	
	@Autowired
	RevisionProcess revisionProcess;
	
	@GetMapping("/changedfile")
	public ChangedFile findById(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findById(id);
	}
	
	@PostMapping("/changedfile")
	public void put(@RequestBody ChangedFile changedFile) throws IOException {
		process.save(changedFile);
	}
	
	@PutMapping("/changedfile")
	public void update(@RequestBody ChangedFile changedFile) throws IOException, InstantiationException, IllegalAccessException {
		ChangedFile c = findById(changedFile.getId());
		BeanUtils.copyProperties(changedFile, c);
		process.save(c);
	}
	
	@DeleteMapping("/changedfile")
	public void delete(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		process.delete(id);
	}
	
	@GetMapping("/changedfile/listbyrevision")
	public List<ChangedFile> findByRevision(@RequestParam("idRevision") Long id) throws InstantiationException, IllegalAccessException, IOException {
		return process.findByRevision(revisionProcess.findById(id));
	}
}
