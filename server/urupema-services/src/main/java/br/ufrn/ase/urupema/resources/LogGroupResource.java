/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.CalledMethodStats;
import br.ufrn.ase.urupema.domain.Issue;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.SuspiciousFile;
import br.ufrn.ase.urupema.domain.TimeStats;
import br.ufrn.ase.urupema.domain.UriStats;
import br.ufrn.ase.urupema.dtos.LogGroupSuspiciousReportDTO;
import br.ufrn.ase.urupema.process.LogGroupProcess;
import br.ufrn.ase.urupema.process.SourceConnectionProcess;


//@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class LogGroupResource {
	@Autowired
	LogGroupProcess process;
	
	@Autowired
	SourceConnectionProcess sourceConnectionProcess;
	
	@GetMapping("/loggroup/listbysourceconnection")
	public List<LogGroup> findBySourceConnection(@RequestParam("idSourceConnection") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findBySourceConnection(sourceConnectionProcess.findById(id));
	}
	
	@GetMapping("/loggroup/detail")
	public LogGroup findById(@RequestParam("id") Long id) throws IOException {
		return process.findById(id);
	}
	
	@GetMapping("/loggroup/suspiciousfilesbyloggroupid")
	public Set<SuspiciousFile> findSuspiciousFilesByLogGroupId(@RequestParam("idLogGroup") Long id) throws IOException, InstantiationException, IllegalAccessException, OperationNotSupportedException {
		return process.findSuspiciousFilesByLogGroupId(id);
	}
	
	@GetMapping("/loggroup/affecteduris")
	public Set<UriStats> findAffectedUrisById(@RequestParam("idLogGroup") Long id) throws IOException, OperationNotSupportedException {
		return process.findAffectedUrisById(id);
	}
	
	@GetMapping("/loggroup/logsovertime")
	public Set<TimeStats> findLogsOverTime(@RequestParam("idLogGroup") Long id) throws IOException, OperationNotSupportedException {
		return process.findLogsOverTime(id);
	}
	
	@GetMapping("/loggroup/calledfilemethods")
	public Set<CalledMethodStats> findCalledFileMethods(@RequestParam("idLogGroup") Long id, @RequestParam("fileName") String fileName) throws IOException, OperationNotSupportedException {
		return process.findCalledFileMethods(id, fileName);
	}
	
	@GetMapping("/loggroup/listbysourceconnectionandlabel")
	public List<LogGroup> findBySourceConnectionAndLabel(@RequestParam("idSourceConnection") Long id, @RequestParam("label") String label) throws IOException, InstantiationException, IllegalAccessException {
		return process.findBySourceConnectionAndLabel(sourceConnectionProcess.findById(id), label);
	}
	
	@GetMapping("/loggroup/listbysourceconnectionandstacktrace")
	public List<LogGroup> findBySourceConnectionAndStacktrace(@RequestParam("idSourceConnection") Long id, @RequestParam("stacktrace") String stacktrace) throws IOException, InstantiationException, IllegalAccessException {
		return process.findBySourceConnectionAndStacktrace(sourceConnectionProcess.findById(id), stacktrace);
	}
	
	@GetMapping("/loggroup/listsubgroupsbyloggroup")
	public Set<LogGroup> findSubGroupsByLogGroup(@RequestParam("idLogGroup") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findSubGroupsByLogGroupId(id);
	}
	
	@GetMapping("/loggroup/listmapfcsfsbyloggroup")
	public Map<String, Float> listMapFCSFs(@RequestParam("idLogGroup") Long id) throws IOException {
		return process.listMapFCSFs(id);
	}
	
	@GetMapping("/loggroup/sourceconnection")
	public SourceConnection findSourceConnectionByLogGroup(@RequestParam("idLogGroup") Long id) throws IOException {
		return process.findSourceConnectionByLogGroup(id);
	}
	
	@GetMapping("/loggroup/summary")
	public LogGroupSuspiciousReportDTO summary(@RequestParam("idLogGroup") Long id) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		return process.generateSummary(id);
	}
	
	@GetMapping("/loggroup/issues")
	public Set<Issue> listIssues(@RequestParam("idLogGroup") Long id) throws IOException, OperationNotSupportedException, InstantiationException, IllegalAccessException {
		return process.findIssuesByLogGroup(id);
	}
}
