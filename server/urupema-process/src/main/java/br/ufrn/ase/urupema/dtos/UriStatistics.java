package br.ufrn.ase.urupema.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UriStatistics {
	private String uri;
	private long executions;
	private long errors;
	private long manual;
	private long auto;
}
