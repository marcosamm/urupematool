export interface LineNumberStats {
  fileName: string;
  lineNumber: number;
  count: number;
}
