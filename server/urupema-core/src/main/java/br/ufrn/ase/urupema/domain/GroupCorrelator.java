package br.ufrn.ase.urupema.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

@Entity
@SequenceGenerator(name = "ID_GROUPCORRELATOR_SEQUENCE", sequenceName = "groupcorrelator_id_sequence", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Getter
@Setter
public class GroupCorrelator implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_GROUPCORRELATOR_SEQUENCE")
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	@JsonIgnore
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY, optional=false)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_groupcorrelator_study_id"))
	private Study study;
	
	@Column(nullable = false)
	private String groupCorrelatorTypeId;
	
	@Transient
	private GroupCorrelatorType groupCorrelatorType;
	
	@ManyToOne(fetch = FetchType.EAGER, optional=false)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_groupcorrelator_sourceconnection_origin_id"))
	private SourceConnection origin;
	
	@ManyToOne(fetch = FetchType.EAGER, optional=false)
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_groupcorrelator_sourceconnection_destiny_id"))
	private SourceConnection destiny;
	
	@JsonIgnore
	@OneToMany(mappedBy = "groupCorrelator", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
	private List<GroupRelationship> groupRelationships;
	
	public void addGroupRelationship(GroupRelationship groupRelationship) {
		if(getGroupRelationships() == null) {
			groupRelationships = new ArrayList<>();
		}
		groupRelationships.add(groupRelationship);
		groupRelationship.setGroupCorrelator(this);
	}
	
	public void addGroupRelationships(List<GroupRelationship> groupRelationships) {
		for (GroupRelationship groupRelationship : groupRelationships) {
			addGroupRelationship(groupRelationship);
		}
	}
	
	public boolean existsRelashionship(LogGroup originGroup, LogGroup destinyGroup) {
		boolean exist = false;
		
		for(GroupRelationship relationship : getGroupRelationships()) {
			if(relationship.existsRelashionship(originGroup, destinyGroup)) {
				exist = true;
				break;
			}
		}
		
		return exist;
	}
}
