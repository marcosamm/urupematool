package br.ufrn.ase.urupema.domain;

public enum PropertyType {
	STRING,
	BOOLEAN,
	INTEGER,
	LONG,
	FLOAT,
	DOUBLE
}
