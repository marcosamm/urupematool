import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IssueTrackerConnectionService, IssueService } from '../../service';
import { Issue, ReportedStackTrace } from '../../model';
import { ReportedStackTraceService } from '../../service/reportedstacktrace.service';

@Component({
  selector: 'app-reportedstacktracecreate',
  templateUrl: './reportedstacktracecreate.component.html',
  styleUrls: ['./reportedstacktracecreate.component.css']
})
export class ReportedStackTraceCreateComponent implements OnInit {
  reportedStackTraceForm: FormGroup;
  issue: Issue;
  reportedStackTrace: ReportedStackTrace;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private reportedStackTraceService: ReportedStackTraceService,
    private issueService: IssueService,
    private fb: FormBuilder
  ) {
    this.reportedStackTraceForm = this.fb.group({
      stackTrace: ['', [Validators.required, Validators.minLength(30)]],
      note: [''],
      enabled: [false]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.issueService.getIssue(params['idIssue']).subscribe(
        issue => { 
          this.issue = issue;
          this.issueService.getIssueTrackerConnectionByIssue(this.issue).subscribe(issueTrackerConnection => {
            this.issue.issueTrackerConnection = issueTrackerConnection;
          });
        }
      );
      if (params['idReportedStackTrace'] !== undefined) {
        this.reportedStackTraceService.getReportedStackTrace(params['idReportedStackTrace']).subscribe(
          reportedStackTrace => {
            this.reportedStackTrace = reportedStackTrace;
            this.reportedStackTraceForm.get('stackTrace').setValue(this.reportedStackTrace.stackTrace);
            this.reportedStackTraceForm.get('note').setValue(this.reportedStackTrace.note);
            this.reportedStackTraceForm.get('enabled').setValue(this.reportedStackTrace.enabled);
          }
        );
      }
    });
  }

  create() {
    this.reportedStackTraceService.createReportedStackTrace(
      this.issue.id,
      this.reportedStackTraceForm.get('stackTrace').value,
      this.reportedStackTraceForm.get('note').value,
      this.reportedStackTraceForm.get('enabled').value
    ).subscribe(
      result => { this.router.navigate(['/issuetrackerconnection/issuedetail/' + + this.issue.issueTrackerConnection.id + '/'+ this.issue.id]); }
    );
  }

  update() {
    this.reportedStackTraceService.updateReportedStackTrace(
      this.reportedStackTrace.id,
      this.issue.id,
      this.reportedStackTraceForm.get('stackTrace').value,
      this.reportedStackTraceForm.get('note').value,
      this.reportedStackTraceForm.get('enabled').value
    ).subscribe(
      result => { this.router.navigate(['/issuetrackerconnection/issuedetail/' + + this.issue.issueTrackerConnection.id + '/'+ this.issue.id]); }
    );
  }
}
