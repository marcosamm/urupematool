package br.ufrn.ase.urupema.extensible.aggregator;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.repository.LogRepository;
import br.ufrn.ase.urupema.repository.WebExecutionRepository;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class GroupAggregatorByStacktrace implements GroupAggregatorTypePlugin {
	public static String ID = "urupema:st";
	
	private LogRepository logRepository;
	private List<LogType> logTypes = Arrays.asList(LogType.ERROR, LogType.EXECUTION);
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getName() {
		return "Stack Trace";
	}

	@Override
	public String getDescription() {
		return "Creates a set of logs grouped by identical Stack Traces";
	}
	
	protected WebExecutionRepository getWebExecutionRepository() {
		return (WebExecutionRepository) logRepository;
	}
	
	protected WebExecutionTrace getWebExecutionTrace(String id, Map<String, WebExecutionTrace> webExecTraceCache) throws IOException {
		WebExecutionTrace webExecutionTrace = null;
		if(webExecTraceCache != null) {
			webExecutionTrace = webExecTraceCache.get(id);
		}
		if(webExecutionTrace == null) {
			webExecutionTrace = getWebExecutionRepository().findById(id).get();
			webExecTraceCache.put(id, webExecutionTrace);
		}
		return webExecutionTrace;
	}
	
	@Override
	public List<LogGroup> agroup(SourceConnection sourceConnection, Map<String, Object> params, Map<String, WebExecutionTrace> cache) throws IOException {
		log.debug("Starting agroup by Stack Trace Groups");
		@SuppressWarnings("unchecked")
		Set<String> uris = (Set<String>) params.get("uris");
		List<LogGroup> groups = getWebExecutionRepository().agroupByStackTrace(uris, sourceConnection.getStartDate(), sourceConnection.getEndDate());
		
		int qtdLogs = 0;
		for(LogGroup stackTraceGroup : groups) {
			/*if(sourceConnection.getLogType()!=LogType.ACCESS) {
				WebExecutionTrace representantStacktraceGroup = getWebExecutionTrace(stackTraceGroup.getRepresentantId(), cache);
				stackTraceGroup.getMoreInformation().put("numTraceElements", representantStacktraceGroup.getQtdTraceElements().toString());
			}*/
			stackTraceGroup.setGroupAggregatorTypeId(ID);
			qtdLogs += stackTraceGroup.getElementsId().size();
		}
		
		log.debug("Stack Trace Groups: " + groups.size() + " - subGroups: 0 - logs: " + qtdLogs);
		return groups;
	}

	@Override
	public int compareTo(GroupAggregatorType o) {
		return getId().compareTo(o.getId());
	}
}
