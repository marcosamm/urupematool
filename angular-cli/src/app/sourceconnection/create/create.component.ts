import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SourceService, SourceConnectionService, StudyService, GroupAggregatorTypeService, SuspiciousFilesFinderTypeService } from '../../service';
import { Study, Source, GroupAggregatorType, SuspiciousFilesFinderType } from '../../model';
import { LogType } from '../../model/LogType.enum';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-sourceconnectioncreate',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class SourceConnectionCreateComponent implements OnInit {
  sourceConnectionForm: FormGroup;
  id: number = undefined;
  study: Study = undefined;
  sources: Source [] = [];
  sourcesMap: Map<number, Source> = new Map();
  logTypes: LogType[] = [];
  startDate: Date;
  groupAggregatorTypes: GroupAggregatorType[] = [];
  groupAggregatorTypesMap: Map<string, GroupAggregatorType> = new Map();
  suspiciousFilesFinderTypes: SuspiciousFilesFinderType[] = [];
  suspiciousFilesFinderTypesMap: Map<string, SuspiciousFilesFinderType> = new Map();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private studyService: StudyService,
    private sourceService: SourceService,
    private sourceConnectionService: SourceConnectionService,
    private groupAggregatorTypeService: GroupAggregatorTypeService,
    private suspiciousFilesFinderTypeService: SuspiciousFilesFinderTypeService,
    private fb: FormBuilder
  ) {
    this.sourceConnectionForm = this.fb.group({
      sourceId: ['', Validators.required],
      name: ['', [Validators.required, Validators.minLength(5)]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
      logType: ['', [Validators.required]],
      groupAggregatorTypeId: ['', [Validators.required]],
      suspiciousFilesFinderTypeId: [''],
      regexSignature: [''],
      regexIgnoreSignature: [''],
      suspiciousFilesFilter: ['']
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studyService.getStudy(params['idStudy']).subscribe(
        study => this.study = study
      );
      this.sourceService.getSources().subscribe(
        sources => {
          this.sources = sources;
          for (const source of sources) {
            this.sourcesMap.set(source.id, source);
          }
        }
      );
      this.groupAggregatorTypeService.getGroupAggregatorTypes().subscribe(
        groupAggregatorTypes => {
          for (const groupAggregatorType of groupAggregatorTypes) {
            this.groupAggregatorTypesMap.set(groupAggregatorType.id, groupAggregatorType);
          }
        }
      );
      this.suspiciousFilesFinderTypeService.getSuspiciousFilesFinderTypes().subscribe(
        suspiciousFilesFinderTypes => {
          this.suspiciousFilesFinderTypes = suspiciousFilesFinderTypes;
          for (const suspiciousFilesFinderType of suspiciousFilesFinderTypes) {
            this.suspiciousFilesFinderTypesMap.set(suspiciousFilesFinderType.id, suspiciousFilesFinderType);
          }
        }
      );
      if (params['id'] !== undefined) {
        this.sourceConnectionService.getSourceConnection(params['id']).subscribe(
          sourceConnection => {
            this.id = sourceConnection.id;
            this.sourceConnectionForm.get('sourceId').setValue(sourceConnection.source.id);
            this.sourceConnectionForm.get('name').setValue(sourceConnection.name);
            this.sourceConnectionForm.get('startDate').setValue(
              formatDate(sourceConnection.startDate, 'yyyy-MM-ddTHH:mm:ss', 'pt_BR', 'GMT-3')
            );
            this.sourceConnectionForm.get('endDate').setValue(
              formatDate(sourceConnection.endDate, 'yyyy-MM-ddTHH:mm:ss', 'pt_BR', 'GMT-3')
            );
            this.sourceConnectionForm.get('logType').setValue(sourceConnection.logType);
            this.sourceConnectionForm.get('groupAggregatorTypeId').setValue(sourceConnection.groupAggregatorTypeId);
            this.sourceConnectionForm.get('regexSignature').setValue(sourceConnection.regexSignature);
            this.sourceConnectionForm.get('regexIgnoreSignature').setValue(sourceConnection.regexIgnoreSignature);
            this.sourceConnectionForm.get('suspiciousFilesFinderTypeId').setValue(sourceConnection.suspiciousFilesFinderTypeId);
            this.sourceConnectionForm.get('suspiciousFilesFilter').setValue(sourceConnection.suspiciousFilesFilter);

            this.fillLogTypes(sourceConnection.source.id);
            this.fillAggragatorTypes(sourceConnection.logType);
          }
        );
      }
    });
  }

  create() {
    this.sourceConnectionService.createSourceConnection(
      this.study.id,
      this.sourceConnectionForm.get('sourceId').value,
      this.sourceConnectionForm.get('name').value,
      new Date(this.sourceConnectionForm.get('startDate').value),
      new Date(this.sourceConnectionForm.get('endDate').value),
      this.sourceConnectionForm.get('logType').value,
      this.sourceConnectionForm.get('groupAggregatorTypeId').value,
      this.sourceConnectionForm.get('suspiciousFilesFinderTypeId').value,
      this.sourceConnectionForm.get('regexSignature').value,
      this.sourceConnectionForm.get('regexIgnoreSignature').value,
      this.sourceConnectionForm.get('suspiciousFilesFilter').value
    ).subscribe(
      result => { this.router.navigate(['/study/detail/' + this.study.id]); }
    );
  }

  update() {
    this.sourceConnectionService.updateSourceConnection(
      this.id,
      this.study.id,
      this.sourceConnectionForm.get('sourceId').value,
      this.sourceConnectionForm.get('name').value,
      new Date(this.sourceConnectionForm.get('startDate').value),
      new Date(this.sourceConnectionForm.get('endDate').value),
      this.sourceConnectionForm.get('logType').value,
      this.sourceConnectionForm.get('groupAggregatorTypeId').value,
      this.sourceConnectionForm.get('suspiciousFilesFinderTypeId').value,
      this.sourceConnectionForm.get('regexSignature').value,
      this.sourceConnectionForm.get('regexIgnoreSignature').value,
      this.sourceConnectionForm.get('suspiciousFilesFilter').value
    ).subscribe(
      result => { this.router.navigate(['/study/detail/' + this.study.id]); }
    );
  }

  fillLogTypes(sourceId) {
    this.logTypes = this.sourcesMap.get(Number(sourceId)).sourceType.logTypes;
  }

  fillAggragatorTypes(logType: LogType) {
    this.groupAggregatorTypes = [];
    this.groupAggregatorTypesMap.forEach((value: GroupAggregatorType, key: string) => {
      if (value.logTypes.find(x => x === logType)) {
        this.groupAggregatorTypes.push(value);
      }
    });
  }
}
