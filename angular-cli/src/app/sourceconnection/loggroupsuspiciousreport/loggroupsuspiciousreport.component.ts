import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LogGroupService } from '../../service';
import { LogGroupSuspiciousReport } from '../../model/LogGroupSuspiciousReport';


@Component({
  selector: 'app-loggroupsuspiciousreport',
  templateUrl: './loggroupsuspiciousreport.component.html',
  styleUrls: ['./loggroupsuspiciousreport.component.css']
})
export class LogGroupSuspiciousReportComponent implements OnInit {
  logGroupSuspiciousReport: LogGroupSuspiciousReport;

  constructor(
    private route: ActivatedRoute,
    private logGroupService: LogGroupService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.logGroupService.getLogGroupSuspiciousReport(params['idLogGroup']).subscribe(
        logGroupSuspiciousReport => {
          this.logGroupSuspiciousReport = logGroupSuspiciousReport;
        }
      );
    });
  }
}
