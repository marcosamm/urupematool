package br.ufrn.ase.urupema.extensible.aggregator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.parser.StackTraceParser;
import br.ufrn.ase.urupema.repository.ErrorLogRepository;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupAggregatorByUriAndSubtrace extends GroupAggregatorByUriAndStacktrace implements GroupAggregatorTypePlugin {
	public static String ID = "urupema:uri_subst";
	
	private List<LogType> logTypes = Arrays.asList(LogType.ERROR);
	
	private int minFrames = 0;
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getName() {
		return "URI and SubTrace";
	}

	@Override
	public String getDescription() {
		return "Group by URI (superGroup) and subgroup (subGroups) by subset of stack trace";
	}
	
	private ErrorLogRepository getErrorLogRepository() {
		return (ErrorLogRepository) getWebExecutionRepository();
	}
	
	protected LogGroup agroup(SourceConnection sourceConnection, String uri) throws IOException {		
		LogGroup group = new LogGroup(uri, sourceConnection);
		group.setTypeDescription("URI");
		group.setGroupAggregatorTypeId(ID);
		Set<LogGroup> subtraceGroups = new HashSet<>();
		
		Map<String, WebExecutionTrace> logsCache = new HashMap<>();
		
		LogGroup uriGroup = super.agroup(sourceConnection, uri);
		for(LogGroup stackTraceGroup : uriGroup.getSubGroups()) {
			WebExecutionTrace representantStacktraceGroup = getErrorLogRepository()
					.findById(stackTraceGroup.getRepresentantId(), 
						new StackTraceParser(
							sourceConnection.getRegexSignature(), 
							sourceConnection.getRegexIgnoreSignature()
						)
					)
					.get();
			logsCache.put(representantStacktraceGroup.getIdentifier(), representantStacktraceGroup);
			if(representantStacktraceGroup.getQtdTraceElements() >= minFrames) {
				boolean equivalent = false;
				for(LogGroup subtraceGroup : subtraceGroups) {
					WebExecutionTrace representantSubtraceGroup = logsCache.get(subtraceGroup.getRepresentantId());
					if(group.getFirstOccurrenceDate() == null || stackTraceGroup.getFirstOccurrenceDate().before(group.getFirstOccurrenceDate())) {
						group.setFirstOccurrenceDate(stackTraceGroup.getFirstOccurrenceDate());
					}
					if(group.getLastOccurrenceDate() == null || stackTraceGroup.getLastOccurrenceDate().after(group.getLastOccurrenceDate())) {
						group.setLastOccurrenceDate(stackTraceGroup.getLastOccurrenceDate());
					}
					if(representantStacktraceGroup.equivalent(representantSubtraceGroup)) {
						equivalent = true;
						if(stackTraceGroup.getFirstOccurrenceDate().before(subtraceGroup.getFirstOccurrenceDate())) {
							subtraceGroup.setFirstOccurrenceDate(stackTraceGroup.getFirstOccurrenceDate());
							subtraceGroup.setRepresentantId(stackTraceGroup.getRepresentantId());
						}
						if(stackTraceGroup.getLastOccurrenceDate().after(subtraceGroup.getLastOccurrenceDate())) {
							subtraceGroup.setLastOccurrenceDate(stackTraceGroup.getLastOccurrenceDate());
						}
						subtraceGroup.addSubGroup(stackTraceGroup);
						break;
					}
				}
				if(!equivalent) {
					logsCache.put(representantStacktraceGroup.getIdentifier(), representantStacktraceGroup);
					LogGroup subtraceGroup = new LogGroup(representantStacktraceGroup.getTraceElementsAsString(), sourceConnection);
					subtraceGroup.setRepresentantId(representantStacktraceGroup.getIdentifier());
					subtraceGroup.setFirstOccurrenceDate(stackTraceGroup.getFirstOccurrenceDate());
					subtraceGroup.setLastOccurrenceDate(stackTraceGroup.getLastOccurrenceDate());
					subtraceGroup.setTypeDescription("SUBTRACE");
					subtraceGroup.setGroupAggregatorTypeId(ID);
					subtraceGroup.getMoreInformation().put("numTraceElements", representantStacktraceGroup.getQtdTraceElements().toString());
					subtraceGroup.addSubGroup(stackTraceGroup);
					subtraceGroup.updateElementsCount();
					
					subtraceGroups.add(subtraceGroup);
				}
			}
		}
		group.addAllSubGroups(subtraceGroups);
		//group.setSourceConnection(sourceConnection);
		return group;
	}

	@Override
	public int compareTo(GroupAggregatorType o) {
		return getId().compareTo(o.getId());
	}

	@Override
	public List<LogGroup> agroup(SourceConnection sourceConnection, Map<String, Object> params, Map<String, WebExecutionTrace> cache) throws IOException {
		@SuppressWarnings("unchecked")
		Set<String> uris = (Set<String>) params.get("uris");
		List<LogGroup> groups = new ArrayList<>();
		for(String uri : uris) {
			groups.add(agroup(sourceConnection, uri));
		}
		return groups;
	}
}
