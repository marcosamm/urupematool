import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SourceConnection } from '../../model/SourceConnection';
import { SourceConnectionService } from '../../service';
import { Source } from '../../model';

@Component({
  selector: 'app-sourceconnectiondetail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class SourceConnectionDetailComponent implements OnInit {
  @Input() sourceConnection: SourceConnection;

  constructor(
    private sourceConnectionService: SourceConnectionService
  ) { }

  ngOnInit() {
  }

  getSource(): Source {
    if (this.sourceConnection.source === undefined) {
      this.sourceConnectionService.getSource(this.sourceConnection).subscribe(
        source => {
          this.sourceConnection.source = source;
        }
      );
    }
    return this.sourceConnection.source;
  }
}
