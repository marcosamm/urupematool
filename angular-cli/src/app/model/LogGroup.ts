import { SourceConnection } from './SourceConnection';

export interface LogGroup {
    id: number;
    label: string;
    typeDescription: string;
    firstOccurrenceDate: Date;
    lastOccurrenceDate: Date;
    representantId: string;
    elementsId: string[];
    elementsCount: number;
    superGroup: LogGroup;
    subGroups: LogGroup[];
    subGroupsCount: number;
    moreInformation: Map<string, string>;
    sourceConnection: SourceConnection;
    mapFCSFs: Map<string, number>;
    mapFcsfsCount: number;
}
