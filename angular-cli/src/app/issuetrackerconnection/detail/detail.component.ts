import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IssueTrackerConnection } from '../../model';

@Component({
  selector: 'app-issuetrackerconnectiondetail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class IssueTrackerConnectionDetailComponent implements OnInit {
  @Input() issueTrackerConnection: IssueTrackerConnection;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }
}
