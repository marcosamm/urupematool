
import { Routes } from '@angular/router';

import { IssueTrackerSourceListComponent } from './list/list.component';
import { IssueTrackerSourceCreateComponent } from './create/create.component';

/**
 * Define the routes
 */
export const routes: Routes = [
    { path: 'issuetrackersource', component: IssueTrackerSourceListComponent},
    { path: 'issuetrackersource/list', component: IssueTrackerSourceListComponent},
    { path: 'issuetrackersource/create', component: IssueTrackerSourceCreateComponent},
    { path: 'issuetrackersource/create/:id', component: IssueTrackerSourceCreateComponent}
];