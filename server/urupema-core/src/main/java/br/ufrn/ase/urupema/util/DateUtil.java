package br.ufrn.ase.urupema.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class DateUtil {
	public static LocalDate toLocalDate(Date dateToConvert) {
		return Instant.ofEpochMilli(dateToConvert.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	}
	public static Set<LocalDate> daysInRange(Date start, Date end) {
		return daysInRange(toLocalDate(start), toLocalDate(end));
	}
	public static Set<LocalDate> daysInRange(LocalDate start, LocalDate end) {
		Set<LocalDate> result = new HashSet<LocalDate>();
		while (start.isBefore(end) || start.isEqual(end)) {
			result.add(start);
			start = start.plusDays(1);
		}
		return result;
	}
	
	public static String toBrDateString(Date date) {
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:SS");
		return sdf.format(date);
	}
}
