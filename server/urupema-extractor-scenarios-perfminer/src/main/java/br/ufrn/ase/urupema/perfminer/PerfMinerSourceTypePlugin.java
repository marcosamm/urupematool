package br.ufrn.ase.urupema.perfminer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.naming.OperationNotSupportedException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.Source;
import br.ufrn.ase.urupema.extensible.sourcetype.AbstractSourceTypePlugin;
import br.ufrn.ase.urupema.extensible.sourcetype.SourceTypePlugin;
import br.ufrn.ase.urupema.perfminer.repository.ScenarioPerfMinerRepository;
import br.ufrn.ase.urupema.repository.ScenarioRepository;


public class PerfMinerSourceTypePlugin extends AbstractSourceTypePlugin implements SourceTypePlugin {
	private Map<Long, ScenarioRepository> cache = new HashMap<>();
	
	public PerfMinerSourceTypePlugin() {
		super();
		logTypes.addAll(Arrays.asList(LogType.EXECUTION));
	}
	
	@Override
	public String getId() {
		return "perfminer";
	}

	@Override
	public String getName() {
		return "PerfMiner Source Type";
	}
	
	private DataSource getDataSource(String driverClassName, String url, String username, String password) {
        HikariConfig dataSourceConfig = new HikariConfig();
        dataSourceConfig.setDriverClassName(driverClassName);
        dataSourceConfig.setJdbcUrl(url);
        dataSourceConfig.setUsername(username);
        dataSourceConfig.setPassword(password);
        return new HikariDataSource(dataSourceConfig);
    }
	
    private LocalContainerEntityManagerFactoryBean getEntityManagerFactory(DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan("br.ufrn.ase.urupema.perfminer.*");
        Properties jpaProperties = new Properties();
        jpaProperties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        jpaProperties.put("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
        jpaProperties.put("hibernate.temp.use_jdbc_metadata_defaults", false);
        entityManagerFactoryBean.setJpaProperties(jpaProperties);
        entityManagerFactoryBean.afterPropertiesSet();
        return entityManagerFactoryBean;
    }

	@Override
	public ScenarioRepository getScenarioRepository(Source sourceConnection) throws OperationNotSupportedException {
		if(!cache.containsKey(sourceConnection.getId())) {
			DataSource ds = getDataSource("org.postgresql.Driver", sourceConnection.getUrlConnection(), sourceConnection.getUserName(), sourceConnection.getPassword());
			EntityManagerFactory emf = getEntityManagerFactory(ds).getObject();
			
			JpaRepositoryFactory factory = new JpaRepositoryFactory(emf.createEntityManager());
			cache.put(sourceConnection.getId() ,factory.getRepository(ScenarioPerfMinerRepository.class));
		}
		
		return cache.get(sourceConnection.getId());
	}
}
