package br.ufrn.ase.urupema.domain;

import java.time.LocalDate;
import java.util.Set;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(of = "date")
public class TimeStats implements Comparable<TimeStats>{
	private LocalDate date;
	private Long count;
	private Set<UriStats> urisStats;
	
	public TimeStats(LocalDate date, Long count, Set<UriStats> urisStats) {
		this.date = date;
		this.count = count;
		this.urisStats = urisStats;
	}

	@Override
	public int compareTo(TimeStats o) {
		return this.date.compareTo(o.getDate());
	}
}
