package br.ufrn.ase.urupema.resources;

import java.io.IOException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.Revision;
import br.ufrn.ase.urupema.process.IssueProcess;
import br.ufrn.ase.urupema.process.RevisionProcess;


@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class RevisionResource {	
	@Autowired
	RevisionProcess process;
	
	@Autowired
	IssueProcess issueProcess;
	
	@GetMapping("/revision")
	public Revision findById(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findById(id);
	}
	
	@PostMapping("/revision/{issueId}")
	public void put(@RequestBody Revision revision, @PathVariable long issueId) throws IOException {
		process.add(revision, issueId);
	}
	
	@PutMapping("/revision")
	public void update(@RequestBody Revision revision) throws IOException, InstantiationException, IllegalAccessException {
		Revision i = findById(revision.getId());
		//Copy properties ignoring logGroups
		BeanUtils.copyProperties(revision, i, "issues", "changedFiles");
		process.save(i);
	}
	
	@DeleteMapping("/revision")
	public void delete(@RequestParam("idIssue") Long idRevision, @RequestParam("idRevision") Long idIssue) throws IOException, InstantiationException, IllegalAccessException {
		process.delete(idRevision, idIssue);
	}
}
