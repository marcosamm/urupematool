package br.ufrn.ase.urupema.extensible.sourcetype;

import java.util.Set;
import java.util.TreeSet;

import javax.naming.OperationNotSupportedException;

import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.Source;
import br.ufrn.ase.urupema.domain.SourceType;
import br.ufrn.ase.urupema.repository.AccessLogRepository;
import br.ufrn.ase.urupema.repository.ErrorLogRepository;
import br.ufrn.ase.urupema.repository.ScenarioRepository;
import lombok.Getter;

@Getter
public abstract class AbstractSourceTypePlugin implements SourceTypePlugin {
	protected Set<LogType> logTypes = new TreeSet<>();
	
	public Set<LogType> getLogTypes() {
		return logTypes;
	}

	@Override
	public AccessLogRepository getAccessLogRepository(Source source) throws OperationNotSupportedException {
		throw new OperationNotSupportedException("Access Log not supported");
	}

	@Override
	public ErrorLogRepository getErrorLogRepository(Source source) throws OperationNotSupportedException {
		throw new OperationNotSupportedException("Error Log not supported");
	}

	@Override
	public ScenarioRepository getScenarioRepository(Source source) throws OperationNotSupportedException {
		throw new OperationNotSupportedException("Scenario Log not supported");
	}

	@Override
	public int compareTo(SourceType o) {
		return getName().compareTo(o.getName());
	}
}
