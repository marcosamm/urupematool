import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SourceService, SourceTypeService } from '../../service';
import { SourceType } from '../../model';

@Component({
  selector: 'app-studycreate',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class SourceCreateComponent implements OnInit {
  sourceForm: FormGroup;
  id: number = undefined;
  sourceTypes: SourceType[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: SourceService,
    private sourceTypeService: SourceTypeService,
    private fb: FormBuilder
  ) {
    this.sourceForm = this.fb.group({
      sourceTypeId: ['', Validators.required],
      name: ['', [Validators.required, Validators.minLength(5)]],
      urlConnection: ['', Validators.required],
      userName: [''],
      password: [''],
      accessIndexName: [''],
      accessDocumentType: [''],
      executionIndexName: [''],
      executionDocumentType: [''],
      errorIndexName: [''],
      errorDocumentType: ['']
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.sourceTypeService.getSourceTypes().subscribe(sourceTypes => this.sourceTypes = sourceTypes );
      if (params['id'] !== undefined) {
        this.service.getSource(params['id']).subscribe(
          source => {
            this.id = source.id;
            this.sourceForm.get('sourceTypeId').setValue(source.sourceTypeId);
            this.sourceForm.get('name').setValue(source.name);
            this.sourceForm.get('urlConnection').setValue(source.urlConnection);
            this.sourceForm.get('userName').setValue(source.userName);
            this.sourceForm.get('password').setValue(source.password);
            this.sourceForm.get('accessIndexName').setValue(source.accessIndexName);
            this.sourceForm.get('accessDocumentType').setValue(source.accessDocumentType);
            this.sourceForm.get('executionIndexName').setValue(source.executionIndexName);
            this.sourceForm.get('executionDocumentType').setValue(source.executionDocumentType);
            this.sourceForm.get('errorIndexName').setValue(source.errorIndexName);
            this.sourceForm.get('errorDocumentType').setValue(source.errorDocumentType);
          }
        );
      }
    });
  }

  create() {
    this.service.createSource(
      this.sourceForm.get('sourceTypeId').value,
      this.sourceForm.get('name').value,
      this.sourceForm.get('urlConnection').value,
      this.sourceForm.get('userName').value,
      this.sourceForm.get('password').value,
      this.sourceForm.get('accessIndexName').value,
      this.sourceForm.get('accessDocumentType').value,
      this.sourceForm.get('executionIndexName').value,
      this.sourceForm.get('executionDocumentType').value,
      this.sourceForm.get('errorIndexName').value,
      this.sourceForm.get('errorDocumentType').value
    ).subscribe(
      result => { this.router.navigate(['/source/list']); }
    );
  }

  update() {
    this.service.updateSource(
      this.id,
      this.sourceForm.get('sourceTypeId').value,
      this.sourceForm.get('name').value,
      this.sourceForm.get('urlConnection').value,
      this.sourceForm.get('userName').value,
      this.sourceForm.get('password').value,
      this.sourceForm.get('accessIndexName').value,
      this.sourceForm.get('accessDocumentType').value,
      this.sourceForm.get('executionIndexName').value,
      this.sourceForm.get('executionDocumentType').value,
      this.sourceForm.get('errorIndexName').value,
      this.sourceForm.get('errorDocumentType').value
    ).subscribe(
      result => { this.router.navigate(['/source/list']); }
    );
  }
}
