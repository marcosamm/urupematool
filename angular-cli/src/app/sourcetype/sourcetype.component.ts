import { Component, OnInit } from '@angular/core';

import { SourceTypeService } from '../service';
import { SourceType } from '../model';

@Component({
  selector: 'app-sourcetype',
  templateUrl: './sourcetype.component.html',
  styleUrls: ['./sourcetype.component.css']
})
export class SourceTypeComponent implements OnInit {
  sourceTypes: SourceType[];

  constructor(private sourceTypeService: SourceTypeService) { }

  ngOnInit() {
    this.sourceTypeService.getSourceTypes().subscribe(
      sourceTypes => this.sourceTypes = sourceTypes
    );
  }
}
