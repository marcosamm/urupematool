package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.UriStats;
import br.ufrn.ase.urupema.repository.factory.LogRepositoryFactory;

@Component
public class AccessLogProcess {
	@Autowired
	private LogRepositoryFactory repositoryFactory;
	
	public Set<? extends UriStats> countAccessesGroupedByUri(SourceConnection sourceConnection) throws IOException, OperationNotSupportedException {
		return repositoryFactory.getAccessLogRepository(sourceConnection.getSource()).countGroupedByUri(sourceConnection.getStartDate(), sourceConnection.getEndDate());
	}
}
