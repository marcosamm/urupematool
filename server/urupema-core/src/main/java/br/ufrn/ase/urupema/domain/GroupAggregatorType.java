package br.ufrn.ase.urupema.domain;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface GroupAggregatorType extends Comparable<GroupAggregatorType>{
	public String getId();
	public String getName();
	public String getDescription();
	public List<LogType> getLogTypes();
	public List<LogGroup> agroup(SourceConnection sourceConnection, Map<String, Object> params,
			Map<String, WebExecutionTrace> cache) throws IOException;
}
