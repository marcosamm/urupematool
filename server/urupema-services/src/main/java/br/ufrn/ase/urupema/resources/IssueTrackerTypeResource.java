/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.IssueTrackerType;
import br.ufrn.ase.urupema.process.IssueTrackerTypeProcess;

@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class IssueTrackerTypeResource {
	@Autowired
	private IssueTrackerTypeProcess process;
	
	@GetMapping("/issuetrackertype/list")
	public Collection<IssueTrackerType> findAll() throws IOException, InstantiationException, IllegalAccessException {
		return process.findAll();
	}
	
	@GetMapping("/issuetrackertype/detail")
	public IssueTrackerType findById(@RequestParam("id") String id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findById(id);
	}
}
