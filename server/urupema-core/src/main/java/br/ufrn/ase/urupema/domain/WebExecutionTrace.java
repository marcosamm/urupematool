package br.ufrn.ase.urupema.domain;

import java.util.List;
import java.util.Map;

public interface WebExecutionTrace extends Log {
	public List<TraceElement> getTraceElements();
	public boolean contains(TraceElement traceElement);
	public boolean contains(List<TraceElement> traceElements);
	public boolean equivalent(WebExecutionTrace other);
	public String getExceptionMessage();
	public String getStackTrace();
	public String getTraceElementsAsString();
	public Integer getQtdTraceElements();
	public List<String> getOrderedQualifiedFileNames();
	Map<String, Integer> getQualifiedFileNamesAndMinTopFrameDist();
}
