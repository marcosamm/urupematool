package br.ufrn.ase.urupema.domain;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@SequenceGenerator(name = "ID_ISSUESOURCE_SEQUENCE", sequenceName = "issuesource_id_sequence", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Getter
@Setter
@ToString(of= {"id", "name"})
public class IssueTrackerSource implements Serializable, Comparable<IssueTrackerSource> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_ISSUESOURCE_SEQUENCE")
	private Long id;
	
	private String issueTrackerTypeId;
	
	private transient IssueTrackerType issueTrackerType;
	
	@Column(nullable = false, unique=true)
	private String name;
	
	@ElementCollection(fetch= FetchType.EAGER)
    @CollectionTable(name="issuesource_connectionproperty")
    @MapKeyColumn(name="property")
    @Column(name="value")
	private Map<String, String> connectionProperties = new TreeMap<>();

	@Override
	public int compareTo(IssueTrackerSource o) {
		return name.compareTo(o.getName());
	}
	
	public void setIssueTrackerType(IssueTrackerType issueTrackerType){
		this.issueTrackerType = issueTrackerType;
		if(issueTrackerType != null) {
			this.issueTrackerTypeId = issueTrackerType.getId();
		}
	}
}
