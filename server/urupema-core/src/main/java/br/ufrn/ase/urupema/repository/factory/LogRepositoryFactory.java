package br.ufrn.ase.urupema.repository.factory;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.ufrn.ase.urupema.domain.Source;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.extensible.sourcetype.repository.SourceTypeRepository;
import br.ufrn.ase.urupema.repository.AccessLogRepository;
import br.ufrn.ase.urupema.repository.ErrorLogRepository;
import br.ufrn.ase.urupema.repository.LogRepository;
import br.ufrn.ase.urupema.repository.ScenarioRepository;
import br.ufrn.ase.urupema.repository.WebExecutionRepository;

@Repository
public class LogRepositoryFactory {
	@Autowired
	private SourceTypeRepository sourceTypeRepository;
	
	public AccessLogRepository getAccessLogRepository(Source source) throws OperationNotSupportedException {
		try {
			return sourceTypeRepository.findById(source.getSourceTypeId()).getAccessLogRepository(source);
		} catch (IllegalAccessException | InstantiationException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	public ErrorLogRepository getErrorLogRepository(Source source) throws OperationNotSupportedException {
		try {
			return sourceTypeRepository.findById(source.getSourceTypeId()).getErrorLogRepository(source);
		} catch (IllegalAccessException | InstantiationException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	public ScenarioRepository getScenarioRepository(Source source) throws OperationNotSupportedException {
		try {
			return sourceTypeRepository.findById(source.getSourceTypeId()).getScenarioRepository(source);
		} catch (IllegalAccessException | InstantiationException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	@Deprecated
	public WebExecutionRepository getWebExecutionRepository(SourceConnection sourceConnection) throws OperationNotSupportedException {
		WebExecutionRepository repository;
		switch (sourceConnection.getLogType()) {
			case ERROR:
				repository = getErrorLogRepository(sourceConnection.getSource());
				break;
			case EXECUTION:
				repository = getScenarioRepository(sourceConnection.getSource());
				break;
			default:
				throw new OperationNotSupportedException("Access Log not supported");
		}
		return repository;
	}
	
	public LogRepository getLogRepository(SourceConnection sourceConnection) throws OperationNotSupportedException {
		LogRepository repository;
		switch (sourceConnection.getLogType()) {
			case ACCESS:
				repository = getAccessLogRepository(sourceConnection.getSource());
				break;
			case ERROR:
				repository = getErrorLogRepository(sourceConnection.getSource());
				break;
			case EXECUTION:
				repository = getScenarioRepository(sourceConnection.getSource());
				break;
			default:
				throw new OperationNotSupportedException("Access Log not supported");
		}
		return repository;
	}
}
