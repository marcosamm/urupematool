package br.ufrn.ase.urupema.domain;

public enum IssueType {
	BUG,
	SUPPORT,
	FEATURE
}
