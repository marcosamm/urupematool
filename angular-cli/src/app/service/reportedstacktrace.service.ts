import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { ReportedStackTrace } from '../model/ReportedStackTrace';

@Injectable()
export class ReportedStackTraceService {
  urlGet = environment.apiUrl + '/reportedstacktrace';

  constructor(private http: HttpClient) { }

  getReportedStackTrace(id: number): Observable<ReportedStackTrace> {
    return this.http.get<ReportedStackTrace>(this.urlGet + '?id=' + id);
  }

  createReportedStackTrace(
    issueId: number,
    stackTrace: string,
    note: string,
    enabled: boolean
  ) {
    const reportedStackTrace = {
      issue: { id: issueId },
      stackTrace: stackTrace,
      note: note,
      enabled: enabled
    };
    return this.http.post(this.urlGet, reportedStackTrace);
  }

  updateReportedStackTrace(
    id: number,
    issueId: number,
    stackTrace: string,
    note: string,
    enabled: boolean
  ) {
    const reportedStackTrace = {
      id: id,
      stackTrace: stackTrace,
      note: note,
      enabled: enabled
    };
    return this.http.put(this.urlGet, reportedStackTrace);
  }

  deleteReportedStackTrace(reportedStackTrace: ReportedStackTrace) {
    return this.http.delete(this.urlGet + '?id=' + reportedStackTrace.id);
  }
}
