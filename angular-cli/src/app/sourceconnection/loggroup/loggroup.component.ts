import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SourceConnectionService, LogGroupService } from '../../service';
import { LogGroup } from '../../model/LogGroup';
import { SourceConnection } from '../../model/SourceConnection';

@Component({
  selector: 'app-loggroup',
  templateUrl: './loggroup.component.html',
  styleUrls: ['./loggroup.component.css']
})
export class LogGroupComponent implements OnInit {
  sourceConnection: SourceConnection;
  logGroup: LogGroup;
  totalElemnts: number;
  totalSubgroups: number;

  constructor(
    private route: ActivatedRoute,
    private logGroupService: LogGroupService,
    private sourceConnectionService: SourceConnectionService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.findLogGroup(params['idLogGroup']);
      this.sourceConnectionService.getSourceConnection(params['idSourceConnection']).subscribe(
        sourceConnection => this.sourceConnection = sourceConnection
      );
    });
  }

  findLogGroup(idLogGroup: number) {
    this.logGroupService.getLogGroup(idLogGroup).subscribe(
      logGroup => {
        this.logGroup = logGroup;
        this.totalElemnts = 0;
        this.totalSubgroups = 0;
        this.logGroupService.getSubGroupsByLogGroup(logGroup).subscribe(
          subGroups => {
            this.logGroup.subGroups = subGroups;
            for (const lg of this.logGroup.subGroups) {
              this.totalElemnts += lg.elementsCount;
              this.totalSubgroups += lg.subGroupsCount;
            }
          }
        );
      }
    );
  }
}
