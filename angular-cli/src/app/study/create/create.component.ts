import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StudyService } from '../../service';

@Component({
  selector: 'app-studycreate',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class StudyCreateComponent implements OnInit {
  studyForm: FormGroup;
  id: number = undefined;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: StudyService,
    private fb: FormBuilder
  ) {
    this.studyForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        this.service.getStudy(params['id']).subscribe(
          study => {
            this.id = study.id;
            this.studyForm.get('name').setValue(study.name);
          }
        );
      }
    });
  }

  create() {
    this.service.createStudy(this.studyForm.get('name').value).subscribe(
      result => { this.router.navigate(['/study/list']); }
    );
  }

  update() {
    this.service.updateStudy(this.id, this.studyForm.get('name').value).subscribe(
      result => { this.router.navigate(['/study/list']); }
    );
  }
}
