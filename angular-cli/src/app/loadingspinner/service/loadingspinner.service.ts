import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { LoadingSpinnerState } from '../loadingspinner/loadingspinner.model';

@Injectable({
  providedIn: 'root'
})
export class LoadingSpinnerService {
  private loaderSubject = new Subject<LoadingSpinnerState>();
  loaderState = this.loaderSubject.asObservable();

  constructor() { }

  show() {
    this.loaderSubject.next(<LoadingSpinnerState>{ show: true });
  }

  hide() {
    this.loaderSubject.next(<LoadingSpinnerState>{ show: false });
  }
}

