/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ElasticScenarioExtractor.java 
 * 4 de nov de 2017
 */
package br.ufrn.ase.urupema.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import br.ufrn.ase.urupema.domain.Issue;

public interface IssueTrackerRepository {
	public List<Issue> findClosedIssuesWithStackTrace(Date startDate, Date endDate, Map<String, String> queryProperties);
}
