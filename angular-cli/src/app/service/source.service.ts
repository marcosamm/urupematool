import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Source } from '../model/Source';

@Injectable()
export class SourceService {
  urlGet = environment.apiUrl + '/source';

  constructor(private http: HttpClient) { }

  getSource(id: number): Observable<Source> {
    return this.http.get<Source>(this.urlGet + '?id=' + id);
  }

  getSources(): Observable<Source[]> {
    return this.http.get<Source[]>(this.urlGet + '/list');
  }

  createSource(sourceTypeId: string, name: string, urlConnection: string, userName: string, password: string, 
    accessIndexName: string, accessDocumentType: string, 
    executionIndexName: string, executionDocumentType: string, 
    errorIndexName: string, errorDocumentType: string) {
    const source = {
      sourceTypeId: sourceTypeId,
      name: name,
      urlConnection: urlConnection,
      userName: userName,
      password: password,
      accessIndexName: accessIndexName,
      accessDocumentType: accessDocumentType,
      executionIndexName: executionIndexName,
      executionDocumentType: executionDocumentType,
      errorIndexName: errorIndexName,
      errorDocumentType: errorDocumentType
    };
    return this.http.post(this.urlGet, source);
  }

  updateSource(id: number, sourceTypeId: string, name: string, urlConnection: string, userName: string, password: string, 
    accessIndexName: string, accessDocumentType: string, 
    executionIndexName: string, executionDocumentType: string, 
    errorIndexName: string, errorDocumentType: string) {
    const source = {
      id: id,
      name: name,
      sourceTypeId: sourceTypeId,
      urlConnection: urlConnection,
      userName: userName,
      password: password,
      accessIndexName: accessIndexName,
      accessDocumentType: accessDocumentType,
      executionIndexName: executionIndexName,
      executionDocumentType: executionDocumentType,
      errorIndexName: errorIndexName,
      errorDocumentType: errorDocumentType
    };
    return this.http.put(this.urlGet, source);
  }

  deleteSource(source: Source) {
    return this.http.delete(this.urlGet + '?id=' + source.id);
  }
}
