package br.ufrn.ase.urupema.extensible.aggregator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.ufrn.ase.urupema.domain.GroupAggregatorType;
import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.LogType;
import br.ufrn.ase.urupema.domain.SourceConnection;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.parser.StackTraceParser;
import br.ufrn.ase.urupema.repository.ErrorLogRepository;
import br.ufrn.ase.urupema.repository.LogRepository;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupAggregatorByUriAndSubtraceAndStacktrace implements GroupAggregatorTypePlugin {
	public static String ID = "urupema:st_subst_uri";
	
	private LogRepository logRepository;
	private List<LogType> logTypes = Arrays.asList(LogType.ERROR);
	
	private int minFrames = 0;
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getName() {
		return "Stack Trace, SubTrace and URI";
	}

	@Override
	public String getDescription() {
		return "Creates a set of logs grouped by URI (superGroup) and subgrouped by subtrace and stacktrace";
	}
	
	private ErrorLogRepository getErrorLogRepository() {
		return (ErrorLogRepository) logRepository;
	}
	
	protected LogGroup agroup(SourceConnection sourceConnection, String uri) throws IOException {
		LogGroup group = new LogGroup(uri, sourceConnection);
		group.setTypeDescription("URI, Subtrace and Stacktrace");
		group.setGroupAggregatorTypeId(ID);
		Set<LogGroup> subtraceGroups = new HashSet<>();
		
		Map<String, WebExecutionTrace> logsCache = new HashMap<>();
		
		LogGroup uriGroup = getErrorLogRepository().agroupByUriSubgroupByStackTrace(uri, sourceConnection.getStartDate(), sourceConnection.getEndDate());
		for(LogGroup stackTraceGroup : uriGroup.getSubGroups()) {
			WebExecutionTrace representantStacktraceGroup = getErrorLogRepository()
					.findById(stackTraceGroup.getRepresentantId(), 
						new StackTraceParser(
							sourceConnection.getRegexSignature(), 
							sourceConnection.getRegexIgnoreSignature()
						)
					)
					.get();
			logsCache.put(representantStacktraceGroup.getIdentifier(), representantStacktraceGroup);
			if (representantStacktraceGroup.getQtdTraceElements() >= minFrames) {
				boolean equivalent = false;
				for(LogGroup subtraceGroup : subtraceGroups) {
					WebExecutionTrace representantSubtraceGroup = logsCache.get(subtraceGroup.getRepresentantId());
					if(representantStacktraceGroup.equivalent(representantSubtraceGroup)) {
						equivalent = true;
						subtraceGroup.addElementsId(stackTraceGroup.getElementsId());
						if(stackTraceGroup.getFirstOccurrenceDate().before(subtraceGroup.getFirstOccurrenceDate())) {
							subtraceGroup.setFirstOccurrenceDate(stackTraceGroup.getFirstOccurrenceDate());
							subtraceGroup.setRepresentantId(stackTraceGroup.getRepresentantId());
						}
						if(stackTraceGroup.getLastOccurrenceDate().after(subtraceGroup.getLastOccurrenceDate())) {
							subtraceGroup.setLastOccurrenceDate(stackTraceGroup.getLastOccurrenceDate());
						}
						break;
					}
				}
				if (!equivalent) {
					logsCache.put(representantStacktraceGroup.getIdentifier(), representantStacktraceGroup);
					stackTraceGroup.setLabel(representantStacktraceGroup.getTraceElementsAsString());
					stackTraceGroup.getMoreInformation().put("numTraceElements", representantStacktraceGroup.getQtdTraceElements().toString());
					subtraceGroups.add(stackTraceGroup);
				}
			}
		}
		group.addAllSubGroups(subtraceGroups);
		//group.setSourceConnection(sourceConnection);
		return group;
	}

	@Override
	public int compareTo(GroupAggregatorType o) {
		return getId().compareTo(o.getId());
	}

	@Override
	public List<LogGroup> agroup(SourceConnection sourceConnection, Map<String, Object> params, Map<String, WebExecutionTrace> cache) throws IOException {
		@SuppressWarnings("unchecked")
		Set<String> uris = (Set<String>) params.get("uris");
		List<LogGroup> groups = new ArrayList<>();
		for(String uri : uris) {
			groups.add(agroup(sourceConnection, uri));
		}
		return groups;
	}
}
