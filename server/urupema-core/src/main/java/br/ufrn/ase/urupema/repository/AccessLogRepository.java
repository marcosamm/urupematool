/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ElasticScenarioExtractor.java 
 * 4 de nov de 2017
 */
package br.ufrn.ase.urupema.repository;

public interface AccessLogRepository extends LogRepository {
}
