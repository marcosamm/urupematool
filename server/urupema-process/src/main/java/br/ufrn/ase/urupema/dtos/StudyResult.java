package br.ufrn.ase.urupema.dtos;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import lombok.Getter;


public class StudyResult {
	
	@Getter
	private List<StatsTopN> statsTopNs;
	
	@Getter
	private List<StatsFindedFiles> statsFindedFiles;
	
	private Integer numBuggyFiles;
	
	private Set<String> distinctBuggyFiles;
	
	public StudyResult(List<StatsTopN> statsTopNs, List<StatsFindedFiles> statsFindedFiles) {
		this.statsTopNs = statsTopNs;
		this.statsFindedFiles = statsFindedFiles;
	}
	
	public int getNumBuggyFiles() {
		if(numBuggyFiles == null) {
			numBuggyFiles = 0;
			for (StatsTopN statsTopN : statsTopNs) {
				numBuggyFiles += statsTopN.getChangedFiles().size();
			}
		}
		return numBuggyFiles;
	}
	
	public int getNumDistinctBuggyFiles() {
		if(distinctBuggyFiles == null) {
			distinctBuggyFiles = new TreeSet<>();
			for (StatsTopN statsTopN : statsTopNs) {
				distinctBuggyFiles.addAll(statsTopN.getChangedFiles());
			}
		}
		return distinctBuggyFiles.size();
	}
	
	private int numChangedFilesInTopN(int n) {
		int numFilesInTopN = 0;
		for (StatsTopN statsTopN : statsTopNs) {
			numFilesInTopN += statsTopN.numChangedFilesInTopN(n);
		}
		return numFilesInTopN;
	}
	
	public int getNumDistinctIssues() {
		Set<String> issuesIdentifiers = new TreeSet<>();
		for (StatsTopN statsTopN : statsTopNs) {
			issuesIdentifiers.add(statsTopN.getIssueIdentifier());
		}
		return issuesIdentifiers.size();
	}
	
	public int getNumDistinctLogGroups() {
		Set<Long> idsLogGroups = new TreeSet<>();
		for (StatsTopN statsTopN : statsTopNs) {
			idsLogGroups.add(statsTopN.getLogGroupId());
		}
		return idsLogGroups.size();
	}
	
	public int getNumChangedFilesInTop1() {
		return numChangedFilesInTopN(1);
	}
	
	public int getNumChangedFilesInTop3() {
		return numChangedFilesInTopN(3);
	}
	
	public int getNumChangedFilesInTop5() {
		return numChangedFilesInTopN(5);
	}
	
	public int getNumChangedFilesInTop10() {
		return numChangedFilesInTopN(10);
	}
	
	public int numDistinctChangedFilesInTopN(int i) {
		Set<String> changedFilesInTopN = new TreeSet<>();
		for (StatsTopN statsTopN : statsTopNs) {
			changedFilesInTopN.addAll(statsTopN.changedFilesInTopN(i));
		}
		return changedFilesInTopN.size();
	}
	
	public int getNumDistinctChangedFilesInTop1() {
		return numDistinctChangedFilesInTopN(1);
	}
	
	public int getNumDistinctChangedFilesInTop3() {
		return numDistinctChangedFilesInTopN(3);
	}
	
	public int getNumDistinctChangedFilesInTop5() {
		return numDistinctChangedFilesInTopN(5);
	}
	
	public int getNumDistinctChangedFilesInTop10() {
		return numDistinctChangedFilesInTopN(10);
	}
	
	public float getkRecall(int k) {
		float kRecall = 0;
		for (StatsTopN statsTopN : statsTopNs) {
			float kRec = statsTopN.numChangedFilesInTopN(k) / (float) statsTopN.getChangedFiles().size();
			kRecall += kRec;
		}
		return kRecall / statsTopNs.size();
	}
	
	public float getkRecall1() {
		return getkRecall(1);
	}
	
	public float getkRecall3() {
		return getkRecall(3);
	}
	
	public float getkRecall5() {
		return getkRecall(5);
	}
	
	public float getkRecall10() {
		return getkRecall(10);
	}
	
	private int numSourceOnlyChangedFilesInTopN(int n) {
		int numFilesInTopN = 0;
		for (StatsTopN statsTopN : statsTopNs) {
			numFilesInTopN += statsTopN.numSourceOnlyChangedFilesInTopN(n);
		}
		return numFilesInTopN;
	}
	
	public int getNumSourceOnlyChangedFilesInTop1() {
		return numSourceOnlyChangedFilesInTopN(1);
	}
	
	public int getNumSourceOnlyChangedFilesInTop3() {
		return numSourceOnlyChangedFilesInTopN(3);
	}
	
	public int getNumSourceOnlyChangedFilesInTop5() {
		return numSourceOnlyChangedFilesInTopN(5);
	}
	
	public int getNumSourceOnlyChangedFilesInTop10() {
		return numSourceOnlyChangedFilesInTopN(10);
	}
	
	public int numDistinctSourceOnlyChangedFilesInTopN(int i) {
		Set<String> changedFilesInTopN = new TreeSet<>();
		for (StatsTopN statsTopN : statsTopNs) {
			changedFilesInTopN.addAll(statsTopN.sourceOnlyChangedFilesInTopN(i));
		}
		return changedFilesInTopN.size();
	}
	
	public int getNumDistinctSourceOnlyChangedFilesInTop1() {
		return numDistinctSourceOnlyChangedFilesInTopN(1);
	}
	
	public int getNumDistinctSourceOnlyChangedFilesInTop3() {
		return numDistinctSourceOnlyChangedFilesInTopN(3);
	}
	
	public int getNumDistinctSourceOnlyChangedFilesInTop5() {
		return numDistinctSourceOnlyChangedFilesInTopN(5);
	}
	
	public int getNumDistinctSourceOnlyChangedFilesInTop10() {
		return numDistinctSourceOnlyChangedFilesInTopN(10);
	}
	
	public float getkRecallSourceOnly(int k) {
		float kRecallSourceOnly = 0;
		for (StatsTopN statsTopN : statsTopNs) {
			float kRec = statsTopN.numSourceOnlyChangedFilesInTopN(k) / (float) statsTopN.getChangedFiles().size();
			kRecallSourceOnly += kRec;
		}
		return kRecallSourceOnly / statsTopNs.size();
	}
	
	public float getkRecallSourceOnly1() {
		return getkRecallSourceOnly(1);
	}
	
	public float getkRecallSourceOnly3() {
		return getkRecallSourceOnly(3);
	}
	
	public float getkRecallSourceOnly5() {
		return getkRecallSourceOnly(5);
	}
	
	public float getkRecallSourceOnly10() {
		return getkRecallSourceOnly(10);
	}
	
	private float getkPrecision(int k) {
		float kPrecision = 0;
		for (StatsTopN statsTopN : statsTopNs) {
			float kPrec = statsTopN.numChangedFilesInTopN(k) / (float) Math.min(k, statsTopN.getTop10().size());
			kPrecision += kPrec;
		}
		return kPrecision / statsTopNs.size();
	}
	
	public float getkPrecision1() {
		return getkPrecision(1);
	}
	
	public float getkPrecision3() {
		return getkPrecision(3);
	}
	
	public float getkPrecision5() {
		return getkPrecision(5);
	}
	
	public float getkPrecision10() {
		return getkPrecision(10);
	}
	
	private float getkPrecisionSourceOnly(int k) {
		float kPrecisionSourceOnly = 0;
		for (StatsTopN statsTopN : statsTopNs) {
			float kPrec = statsTopN.numSourceOnlyChangedFilesInTopN(k) / (float) Math.min(k, statsTopN.getTop10().size());
			kPrecisionSourceOnly += kPrec;
		}
		return kPrecisionSourceOnly / statsTopNs.size();
	}
	
	public float getkPrecisionSourceOnly1() {
		return getkPrecisionSourceOnly(1);
	}
	
	public float getkPrecisionSourceOnly3() {
		return getkPrecisionSourceOnly(3);
	}
	
	public float getkPrecisionSourceOnly5() {
		return getkPrecisionSourceOnly(5);
	}
	
	public float getkPrecisionSourceOnly10() {
		return getkPrecisionSourceOnly(10);
	}
	
	private int numIssuesLeastOneChangedFileTopN(int n) {
		Set<String> distinctIssues = new TreeSet<String>();
		for (StatsTopN statsTopN : statsTopNs) {
			int numChangedFilesInTopN = statsTopN.numChangedFilesInTopN(n);
			if(numChangedFilesInTopN > 0) {
				distinctIssues.add(statsTopN.getIssueIdentifier());
			}
		}
		return distinctIssues.size();
	}
	
	public int getNumIssuesLeastOneChangedFileTop1() {
		return numIssuesLeastOneChangedFileTopN(1);
	}
	
	public int getNumIssuesLeastOneChangedFileTop3() {
		return numIssuesLeastOneChangedFileTopN(3);
	}
	
	public int getNumIssuesLeastOneChangedFileTop5() {
		return numIssuesLeastOneChangedFileTopN(5);
	}
	
	public int getNumIssuesLeastOneChangedFileTop10() {
		return numIssuesLeastOneChangedFileTopN(10);
	}
	
	private int numPairLeastOneChangedFileTopN(int n) {
		int numIssuesInTopN = 0;
		for (StatsTopN statsTopN : statsTopNs) {
			int numChangedFilesInTopN = statsTopN.numChangedFilesInTopN(n);
			if(numChangedFilesInTopN > 0) {
				numIssuesInTopN ++;
			}
		}
		return numIssuesInTopN;
	}
	
	public int getNumPairsLeastOneChangedFileTop1() {
		return numPairLeastOneChangedFileTopN(1);
	}
	
	public int getNumPairsLeastOneChangedFileTop3() {
		return numPairLeastOneChangedFileTopN(3);
	}
	
	public int getNumPairsLeastOneChangedFileTop5() {
		return numPairLeastOneChangedFileTopN(5);
	}
	
	public int getNumPairsLeastOneChangedFileTop10() {
		return numPairLeastOneChangedFileTopN(10);
	}
	
	private float getRecallN(int n) {
		return numPairLeastOneChangedFileTopN(n) / (float) statsTopNs.size();
	}
	
	public float getRecallN1() {
		return getRecallN(1);
	}
	
	public float getRecallN3() {
		return getRecallN(3);
	}
	
	public float getRecallN5() {
		return getRecallN(5);
	}
	
	public float getRecallN10() {
		return getRecallN(10);
	}
	
	private int numIssuesLeastOneSourceOnlyChangedFileTopN(int n) {
		Set<String> distinctIssues = new TreeSet<String>();
		for (StatsTopN statsTopN : statsTopNs) {
			int numChangedFilesInTopN = statsTopN.numSourceOnlyChangedFilesInTopN(n);
			if(numChangedFilesInTopN > 0) {
				distinctIssues.add(statsTopN.getIssueIdentifier());
			}
		}
		return distinctIssues.size();
	}
	
	public int getNumIssuesLeastOneSourceOnlyChangedFileTop1() {
		return numIssuesLeastOneSourceOnlyChangedFileTopN(1);
	}
	
	public int getNumIssuesLeastOneSourceOnlyChangedFileTop3() {
		return numIssuesLeastOneSourceOnlyChangedFileTopN(3);
	}
	
	public int getNumIssuesLeastOneSourceOnlyChangedFileTop5() {
		return numIssuesLeastOneSourceOnlyChangedFileTopN(5);
	}
	
	public int getNumIssuesLeastOneSourceOnlyChangedFileTop10() {
		return numIssuesLeastOneSourceOnlyChangedFileTopN(10);
	}
	
	private int numPairsLeastOneSourceOnlyChangedFileTopN(int n) {
		int numIssuesInTopN = 0;
		for (StatsTopN statsTopN : statsTopNs) {
			int numChangedFilesInTopN = statsTopN.numSourceOnlyChangedFilesInTopN(n);
			if(numChangedFilesInTopN > 0) {
				numIssuesInTopN ++;
			}
		}
		return numIssuesInTopN;
	}
	
	public int getNumPairsLeastOneSourceOnlyChangedFileTop1() {
		return numPairsLeastOneSourceOnlyChangedFileTopN(1);
	}
	
	public int getNumPairsLeastOneSourceOnlyChangedFileTop3() {
		return numPairsLeastOneSourceOnlyChangedFileTopN(3);
	}
	
	public int getNumPairsLeastOneSourceOnlyChangedFileTop5() {
		return numPairsLeastOneSourceOnlyChangedFileTopN(5);
	}
	
	public int getNumPairsLeastOneSourceOnlyChangedFileTop10() {
		return numPairsLeastOneSourceOnlyChangedFileTopN(10);
	}
	
	private float getRecallNSourceOnly(int n) {
		return numPairsLeastOneSourceOnlyChangedFileTopN(n) / (float) statsTopNs.size();
	}
	
	public float getRecallNSourceOnly1() {
		return getRecallNSourceOnly(1);
	}
	
	public float getRecallNSourceOnly3() {
		return getRecallNSourceOnly(3);
	}
	
	public float getRecallNSourceOnly5() {
		return getRecallNSourceOnly(5);
	}
	
	public float getRecallNSourceOnly10() {
		return getRecallNSourceOnly(10);
	}
	
	private float getRecall(int n) {
		return numChangedFilesInTopN(n) / (float) getNumBuggyFiles();
	}
	
	public float getRecall1() {
		return getRecall(1);
	}
	
	public float getRecall3() {
		return getRecall(3);
	}
	
	public float getRecall5() {
		return getRecall(5);
	}
	
	public float getRecall10() {
		return getRecall(10);
	}
	
	private float getRecallSourceOnly(int n) {
		return numSourceOnlyChangedFilesInTopN(n) / (float) getNumBuggyFiles();
	}
	
	public float getRecallSourceOnly1() {
		return getRecallSourceOnly(1);
	}
	
	public float getRecallSourceOnly3() {
		return getRecallSourceOnly(3);
	}
	
	public float getRecallSourceOnly5() {
		return getRecallSourceOnly(5);
	}
	
	public float getRecallSourceOnly10() {
		return getRecallSourceOnly(10);
	}
	
	private int numSuspiciousFilesTopN(int n) {
		int numFilesInTopN = 0;
		for (StatsTopN statsTopN : statsTopNs) {
			numFilesInTopN += Math.min(n,statsTopN.getTop10().size());
		}
		return numFilesInTopN;
	}
	
	private float getPrecision(int n) {
		return numChangedFilesInTopN(n) / (float) numSuspiciousFilesTopN(n);
	}
	
	public float getPrecision1() {
		return getPrecision(1);
	}
	
	public float getPrecision3() {
		return getPrecision(3);
	}
	
	public float getPrecision5() {
		return getPrecision(5);
	}
	
	public float getPrecision10() {
		return getPrecision(10);
	}
	
	private int numSourceOnlySuspiciousFilesTopN(int n) {
		int numFilesInTopN = 0;
		for (StatsTopN statsTopN : statsTopNs) {
			numFilesInTopN += Math.min(n,statsTopN.getSourceOnlyTop10().size());
		}
		return numFilesInTopN;
	}
	
	private float getPrecisionSourceOnly(int n) {
		return numSourceOnlyChangedFilesInTopN(n) / (float) numSourceOnlySuspiciousFilesTopN(n);
	}
	
	public float getPrecisionSourceOnly1() {
		return getPrecisionSourceOnly(1);
	}
	
	public float getPrecisionSourceOnly3() {
		return getPrecisionSourceOnly(3);
	}
	
	public float getPrecisionSourceOnly5() {
		return getPrecisionSourceOnly(5);
	}
	
	public float getPrecisionSourceOnly10() {
		return getPrecisionSourceOnly(10);
	}
	
	private float getMeanAveragePrecision(int k) {
		float map = 0f;
		for (StatsTopN statsTopN : statsTopNs) {
			map += statsTopN.getAveragePrecision(k);
		}
		return map / (float) statsTopNs.size();
	}
	
	public float getMeanAveragePrecision1() {
		return getMeanAveragePrecision(1);
	}
	
	public float getMeanAveragePrecision3() {
		return getMeanAveragePrecision(3);
	}
	
	public float getMeanAveragePrecision5() {
		return getMeanAveragePrecision(5);
	}
	
	public float getMeanAveragePrecision10() {
		return getMeanAveragePrecision(10);
	}
	
	private float getMeanAveragePrecisionSourceOnly(int k) {
		float map = 0f;
		for (StatsTopN statsTopN : statsTopNs) {
			map += statsTopN.getSourceOnlyAveragePrecision(k);
		}
		return map / (float) statsTopNs.size();
	}
	
	public float getMeanAveragePrecisionSourceOnly1() {
		return getMeanAveragePrecisionSourceOnly(1);
	}
	
	public float getMeanAveragePrecisionSourceOnly3() {
		return getMeanAveragePrecisionSourceOnly(3);
	}
	
	public float getMeanAveragePrecisionSourceOnly5() {
		return getMeanAveragePrecisionSourceOnly(5);
	}
	
	public float getMeanAveragePrecisionSourceOnly10() {
		return getMeanAveragePrecisionSourceOnly(10);
	}
	
	public void print() {
		System.out.println("\n\n\n");
		System.out.println("Issue\tLG Id\tCF\tT1\tT3\tT5\tT10\tTS1\tTS3\tTS5\tTS10");
		for(StatsTopN stn : statsTopNs) {
			System.out.println(stn.getIssueIdentifier() + "\t" + stn.getLogGroupId() + "\t" + stn.getChangedFiles().size() + "\t" 
					+  stn.getNumChangedFilesInTop1() + "\t" + stn.getNumChangedFilesInTop3() + "\t" + stn.getNumChangedFilesInTop5() + "\t" + stn.getNumChangedFilesInTop10() + "\t" +
					+  stn.getNumSourceOnlyChangedFilesInTop1() + "\t" + stn.getNumSourceOnlyChangedFilesInTop3() + "\t" + stn.getNumSourceOnlyChangedFilesInTop5() + "\t" + stn.getNumSourceOnlyChangedFilesInTop10()
			);
		}
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println("\t\t" + getNumBuggyFiles() + "\t" 
				+ getNumChangedFilesInTop1() + "\t" + getNumChangedFilesInTop3() + "\t" + getNumChangedFilesInTop5() + "\t" + getNumChangedFilesInTop10() + "\t"
				+ getNumSourceOnlyChangedFilesInTop1() + "\t" + getNumSourceOnlyChangedFilesInTop3() + "\t" + getNumSourceOnlyChangedFilesInTop5() + "\t" + getNumSourceOnlyChangedFilesInTop10()
		);
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println("\t\t" + getNumDistinctBuggyFiles() + "\t" 
				+ getNumDistinctChangedFilesInTop1() + "\t" + getNumDistinctChangedFilesInTop3() + "\t" + getNumDistinctChangedFilesInTop5() + "\t" + getNumDistinctChangedFilesInTop10() + "\t"
				+ getNumDistinctSourceOnlyChangedFilesInTop1() + "\t" + getNumDistinctSourceOnlyChangedFilesInTop3() + "\t" + getNumDistinctSourceOnlyChangedFilesInTop5() + "\t" + getNumDistinctSourceOnlyChangedFilesInTop10()
		);
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println(statsTopNs.size() + "p\t\t\t" 
				+ getNumPairsLeastOneChangedFileTop1() + "\t" + getNumPairsLeastOneChangedFileTop3() + "\t" + getNumPairsLeastOneChangedFileTop5() + "\t" + getNumPairsLeastOneChangedFileTop10() + "\t"
				+ getNumPairsLeastOneSourceOnlyChangedFileTop1() + "\t" + getNumPairsLeastOneSourceOnlyChangedFileTop3() + "\t" + getNumPairsLeastOneSourceOnlyChangedFileTop5() + "\t" + getNumPairsLeastOneSourceOnlyChangedFileTop10()
		);
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println(getNumDistinctIssues() + "i\t"+getNumDistinctLogGroups()+"g\t" 
				+ getNumIssuesLeastOneChangedFileTop1() + "\t" + getNumIssuesLeastOneChangedFileTop3() + "\t" + getNumIssuesLeastOneChangedFileTop5() + "\t" + getNumIssuesLeastOneChangedFileTop10() + "\t"
				+ getNumIssuesLeastOneSourceOnlyChangedFileTop1() + "\t" + getNumIssuesLeastOneSourceOnlyChangedFileTop3() + "\t" + getNumIssuesLeastOneSourceOnlyChangedFileTop5() + "\t" + getNumIssuesLeastOneSourceOnlyChangedFileTop10()
		);
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println("\tk-recall\t" 
				+ String.format("%.2f", getkRecall1()) + "\t" + String.format("%.2f", getkRecall3()) + "\t" + String.format("%.2f", getkRecall5()) + "\t" + String.format("%.2f", getkRecall10()) + "\t"
				+ String.format("%.2f", getkRecallSourceOnly1()) + "\t" + String.format("%.2f", getkRecallSourceOnly3()) + "\t" + String.format("%.2f", getkRecallSourceOnly5()) + "\t" + String.format("%.2f", getkRecallSourceOnly10())
		);
		System.out.println("\tk-precision?!\t" 
				+ String.format("%.2f", getkPrecision1()) + "\t" + String.format("%.2f", getkPrecision3()) + "\t" + String.format("%.2f", getkPrecision5()) + "\t" + String.format("%.2f", getkPrecision10()) + "\t"
				+ String.format("%.2f", getkPrecisionSourceOnly1()) + "\t" + String.format("%.2f", getkPrecisionSourceOnly3()) + "\t" + String.format("%.2f", getkPrecisionSourceOnly5()) + "\t" + String.format("%.2f", getkPrecisionSourceOnly10())
		);
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println("\trecall@N\t" 
				+ String.format("%.4f", getRecallN1()) + "\t" + String.format("%.4f", getRecallN3()) + "\t" + String.format("%.4f", getRecallN5()) + "\t" + String.format("%.4f", getRecallN10()) + "\t"
				+ String.format("%.4f", getRecallNSourceOnly1()) + "\t" + String.format("%.4f", getRecallNSourceOnly3()) + "\t" + String.format("%.4f", getRecallNSourceOnly5()) + "\t" + String.format("%.4f", getRecallNSourceOnly10())
		);
		System.out.println("\tMAP\t\t" 
				+ String.format("%.4f", getMeanAveragePrecision1()) + "\t" + String.format("%.4f", getMeanAveragePrecision3()) + "\t" + String.format("%.4f", getMeanAveragePrecision5()) + "\t" + String.format("%.4f", getMeanAveragePrecision10()) + "\t"
				+ String.format("%.4f", getMeanAveragePrecisionSourceOnly1()) + "\t" + String.format("%.4f", getMeanAveragePrecisionSourceOnly3()) + "\t" + String.format("%.4f", getMeanAveragePrecisionSourceOnly5()) + "\t" + String.format("%.4f", getMeanAveragePrecisionSourceOnly10())
		);
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println("\trecall\t\t" 
				+ String.format("%.2f", getRecall1()) + "\t" + String.format("%.2f", getRecall3()) + "\t" + String.format("%.2f", getRecall5()) + "\t" + String.format("%.2f", getRecall10()) + "\t"
				+ String.format("%.2f", getRecallSourceOnly1()) + "\t" + String.format("%.2f", getRecallSourceOnly3()) + "\t" + String.format("%.2f", getRecallSourceOnly5()) + "\t" + String.format("%.2f", getRecallSourceOnly10())
		);
		System.out.println("\tprecision\t" 
				+ String.format("%.2f", getPrecision1()) + "\t" + String.format("%.2f", getPrecision3()) + "\t" + String.format("%.2f", getPrecision5()) + "\t" + String.format("%.2f", getPrecision10()) + "\t"
				+ String.format("%.2f", getPrecisionSourceOnly1()) + "\t" + String.format("%.2f", getPrecisionSourceOnly3()) + "\t" + String.format("%.2f", getPrecisionSourceOnly5()) + "\t" + String.format("%.2f", getPrecisionSourceOnly10())
		);
	}
}
