package br.ufrn.ase.urupema.domain;

import java.util.Set;

public interface SourceType extends Comparable<SourceType>{
	public String getId();	
	public String getName();
	public Set<LogType> getLogTypes();
}
