import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceConnectionDetailComponent } from './detail.component';

describe('DetailComponent', () => {
  let component: SourceConnectionDetailComponent;
  let fixture: ComponentFixture<SourceConnectionDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceConnectionDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceConnectionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
