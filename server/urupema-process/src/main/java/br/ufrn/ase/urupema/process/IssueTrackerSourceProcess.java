package br.ufrn.ase.urupema.process;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufrn.ase.urupema.domain.IssueTrackerSource;
import br.ufrn.ase.urupema.repository.IssueTrackerSourceRepository;

@Component
public class IssueTrackerSourceProcess {
	@Autowired
	IssueTrackerSourceRepository issueTrackerSourceRepository;
	
	@Autowired
	IssueTrackerTypeProcess issueTrackerTypeProcess;
	
	public Optional<IssueTrackerSource> findById(Long id) throws IOException, InstantiationException, IllegalAccessException {
		Optional<IssueTrackerSource> s = issueTrackerSourceRepository.findById(id);
		if(s.isPresent()) {
			s.get().setIssueTrackerType(issueTrackerTypeProcess.findById(s.get().getIssueTrackerTypeId()));
		}
		return s;
	}
	
	public void save(IssueTrackerSource issueTrackerSource) throws InstantiationException, IllegalAccessException, IOException {
		issueTrackerSource.setIssueTrackerType(issueTrackerTypeProcess.findById(issueTrackerSource.getIssueTrackerTypeId()));
		issueTrackerSource.getIssueTrackerType().validateConnectionProperties(issueTrackerSource.getConnectionProperties());
		issueTrackerSourceRepository.save(issueTrackerSource);
	}
	
	public List<IssueTrackerSource> findAll() throws InstantiationException, IllegalAccessException, IOException{
		List<IssueTrackerSource> srcs = issueTrackerSourceRepository.findAll();
		for (IssueTrackerSource issueTrackerSource : srcs) {
			issueTrackerSource.setIssueTrackerType(issueTrackerTypeProcess.findById(issueTrackerSource.getIssueTrackerTypeId()));
		}
		return srcs;
	}
	
	public void delete(IssueTrackerSource issueTrackerSource) {
		issueTrackerSourceRepository.delete(issueTrackerSource);
	}
}
