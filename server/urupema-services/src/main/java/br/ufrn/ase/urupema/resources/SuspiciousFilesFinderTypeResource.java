/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioResource.class
 * 26/10/2017
 */
package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.SuspiciousFilesFinderType;
import br.ufrn.ase.urupema.process.SuspiciousFilesFinderTypeProcess;

@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class SuspiciousFilesFinderTypeResource {
	@Autowired
	private SuspiciousFilesFinderTypeProcess process;
	
	@GetMapping("/suspiciousfilesfindertype/list")
	public Collection<SuspiciousFilesFinderType> findAll() throws IOException, InstantiationException, IllegalAccessException {
		return process.findAll();
	}
	
	@GetMapping("/suspiciousfilesfindertype/detail")
	public SuspiciousFilesFinderType findById(@RequestParam("id") String id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findById(id);
	}
}
