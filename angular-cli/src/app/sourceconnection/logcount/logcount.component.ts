import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Table } from 'primeng/table';
import { SourceConnectionService, LogGroupService } from '../../service';
import { SourceConnection } from '../../model/SourceConnection';
import { UriStats } from '../../model/UriStats';

@Component({
  selector: 'app-logcount',
  templateUrl: './logcount.component.html',
  styleUrls: ['./logcount.component.css']
})
export class LogCountComponent implements OnInit {
  @ViewChild('dt', {static: false}) tableRef: Table;

  sourceConnection: SourceConnection;
  logCounts: UriStats[];
  selectedUris: string [] = [];
  totals: number = null;
  totalsUsers: number = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sourceConnectionService: SourceConnectionService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.sourceConnectionService.getSourceConnection(params['idSourceConnection']).subscribe(
        sourceConnection => {
          this.sourceConnection = sourceConnection;
          this.sourceConnectionService.getLogCount(this.sourceConnection).subscribe(
            logCounts => {
              this.logCounts = logCounts;
              this.totals = 0;
              this.totalsUsers = 0;
              for (const lc of this.logCounts) {
                this.totals += lc.count;
                this.totalsUsers += lc.usersCount;
              }
            }
          );
        }
      );
    });
  }

  agroup() {
    console.log(this.selectedUris);
    this.sourceConnectionService.agroup(this.sourceConnection, this.selectedUris).subscribe(
      ret => {
        this.router.navigate(['/sourceconnection/listloggroups/' + this.sourceConnection.id]);
      }
    );
  }

  isSelectedUri (uri: string) {
    return this.selectedUris.includes(uri);
  }

  toggleUri (uri: string) {
    const index = this.selectedUris.indexOf(uri);
    if (index > -1) {
      this.selectedUris.splice(index, 1);
    } else {
      this.selectedUris.push(uri);
    }
  }

  toggleAll () {
    if ( this.tableRef !== undefined && this.tableRef.filteredValue !== undefined) {
      this.tableRef.filteredValue.forEach(element => {
        this.toggleUri(element.uri);
      });
    } else {
      this.tableRef.value.forEach(element => {
        this.toggleUri(element.uri);
      });
    }
  }

  filteredUris(): number {
    let tot = this.logCounts.length;
    if ( this.tableRef !== undefined && this.tableRef.filteredValue !== undefined) {
      tot = this.tableRef.filteredValue.length;
    }
    return tot;
  }

  filteredCount(): number {
    let tot = this.totals;
    if ( this.tableRef !== undefined && this.tableRef.filteredValue !== undefined) {
      tot = 0;
      this.tableRef.filteredValue.forEach(element => {
        tot += element.count;
      });
    }
    return tot;
  }

  filteredUsers(): number {
    let tot = this.totalsUsers;
    if ( this.tableRef !== undefined && this.tableRef.filteredValue !== undefined) {
      tot = 0;
      this.tableRef.filteredValue.forEach(element => {
        tot += element.usersCount;
      });
    }
    return tot;
  }

  findTotalCallUsers(uriStats: UriStats): number {
    let total:number = 0;
    uriStats.usersStats.forEach(userStats => { total += userStats.count; })
    return total;
  }
}
