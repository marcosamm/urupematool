import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SourceConnectionService } from '../../service';
import { SourceConnection } from '../../model/SourceConnection';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-findlogs',
  templateUrl: './findlogs.component.html',
  styleUrls: ['./findlogs.component.css']
})
export class FindLogsComponent implements OnInit {
  findLogsForm: FormGroup;
  sourceConnection: SourceConnection;

  cols: any[];
  rows: any[];

  constructor(
    private route: ActivatedRoute,
    private sourceConnectionService: SourceConnectionService,
    private fb: FormBuilder
  ) {
    this.findLogsForm = this.fb.group({
      propertyName: ['id_registro_entrada', [Validators.required]],
      propertyValue: ['159042254', [Validators.required]],
    });
  }

  ngOnInit() {
    this.cols = [
      { field: 'id', header: 'id' },
      { field: 'uri', header: 'URI' },
      { field: 'logType', header: 'Log Type' },
      { field: 'date', header: 'Date' }
    ];
    this.route.params.subscribe(params => {
      if (params['propertyName'] !== undefined) {
        this.findLogsForm.get('propertyName').setValue(params['propertyName']);
      }
      if (params['propertyValue'] !== undefined) {
        this.findLogsForm.get('propertyValue').setValue(params['propertyValue']);
      }
      this.sourceConnectionService.getSourceConnection(params['idSourceConnection']).subscribe(
        sourceConnection => {
          this.sourceConnection = sourceConnection;
          if (params['propertyName'] !== undefined && params['propertyValue'] !== undefined) {
            this.find();
          }
        }
      );

    });
  }

  find() {
    this.rows = [];
    this.sourceConnectionService.findLogsIdsByProperty(
      this.sourceConnection.id,
      this.findLogsForm.get('propertyName').value,
      this.findLogsForm.get('propertyValue').value,
    ).subscribe(
      logIds => {
        for (const id of logIds) {
          this.sourceConnectionService.getLog(this.sourceConnection.id, id).subscribe(
            log => {
              const row = {
                id: log.identifier,
                uri: log.requestURI,
                logType: log.logType,
                date: log.date
              };
              this.rows.push(row);
            }
          );
        }
      }
    );
  }
}
