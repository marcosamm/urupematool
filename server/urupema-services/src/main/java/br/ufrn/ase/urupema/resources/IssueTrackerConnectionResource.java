package br.ufrn.ase.urupema.resources;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.IssueTrackerConnection;
import br.ufrn.ase.urupema.domain.Study;
import br.ufrn.ase.urupema.dtos.StudyResult;
import br.ufrn.ase.urupema.process.IssueTrackerConnectionProcess;


@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class IssueTrackerConnectionResource {	
	@Autowired
	IssueTrackerConnectionProcess process;
	
	@GetMapping("/issuetrackerconnection")
	public IssueTrackerConnection findById(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findById(id);
	}
	
	@PostMapping("/issuetrackerconnection")
	public void put(@RequestBody IssueTrackerConnection issueTrackerConnection) throws IOException, InstantiationException, IllegalAccessException {
		process.save(issueTrackerConnection);
	}
	
	@PutMapping("/issuetrackerconnection")
	public void update(@RequestBody IssueTrackerConnection issueTrackerConnection) throws IOException, InstantiationException, IllegalAccessException {
		IssueTrackerConnection s = findById(issueTrackerConnection.getId());
		//Copy properties ignoring study, issues
		BeanUtils.copyProperties(issueTrackerConnection, s, "study", "issues");
		process.save(s);
	}
	
	@DeleteMapping("/issuetrackerconnection")
	public void delete(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		process.delete(id);
	}
	
	@GetMapping("/issuetrackerconnection/study")
	public Study getStudy(@RequestParam("idIssueTrackerConnection") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.getStudy(id);
	}
	
	@GetMapping("/issuetrackerconnection/importissues")
	public void importClosedIssuesWithStackTracesAndEnabledChangedFiles(@RequestParam("idIssueTrackerConnection") Long idIssueTrackerConnection) throws IOException, InstantiationException, IllegalAccessException, OperationNotSupportedException {
		process.importClosedIssuesWithStackTracesAndEnabledChangedFiles(idIssueTrackerConnection);
	}
	
	@GetMapping("/issuetrackerconnection/importaffecteduris")
	public Set<String> importAffectedUrisByReportedStackTraces(
			@RequestParam("idIssueTrackerConnection") Long idIssueTrackerConnection,
			@RequestParam("idSourceConnection") Long idSourceConnection,
			@RequestParam("uriFragment") String uriFragment
	) throws IOException, InstantiationException, IllegalAccessException, OperationNotSupportedException {
		return process.findAffectedUrisByReportedStackTraces(idIssueTrackerConnection, idSourceConnection, uriFragment);
	}
	
	@GetMapping("/issuetrackerconnection/listaffecteduris")
	public Set<String> findAffectedUrisByReportedStackTraces(@RequestParam("idIssueTrackerConnection") Long idIssueTrackerConnection) throws IOException, InstantiationException, IllegalAccessException, OperationNotSupportedException {
		return process.getAffectedUrisByReportedStackTraces(idIssueTrackerConnection);
	}
	
	@GetMapping("/issuetrackerconnection/agroup")
	public void agroup(@RequestParam("idIssueTrackerConnection") Long idIssueTrackerConnection, @RequestParam("idSourceConnection") Long idSourceConnection) throws IOException, InstantiationException, IllegalAccessException, OperationNotSupportedException {
		process.agroup(idIssueTrackerConnection, idSourceConnection);
	}
	
	@GetMapping("/issuetrackerconnection/match")
	public void match(@RequestParam("idIssueTrackerConnection") Long idIssueTrackerConnection, @RequestParam("idSourceConnection") Long idSourceConnection) throws IOException, InstantiationException, IllegalAccessException {
		process.matchIssueWithLogGroups(idIssueTrackerConnection, idSourceConnection);
	}
	
	@GetMapping("/issuetrackerconnection/agroupandmatch")
	public void agroupAndMatch(@RequestParam("idIssueTrackerConnection") Long idIssueTrackerConnection, @RequestParam("idSourceConnection") Long idSourceConnection) throws IOException, InstantiationException, IllegalAccessException, OperationNotSupportedException {
		process.agroup(idIssueTrackerConnection, idSourceConnection);
		process.matchIssueWithLogGroups(idIssueTrackerConnection, idSourceConnection);
	}
	
	@GetMapping("/issuetrackerconnection/findaffectedurisandagroupandmatch")
	public void findAffectedUrisAgroupAndMatchIssueWithLogGroups(
			@RequestParam("idIssueTrackerConnection") Long idIssueTrackerConnection, 
			@RequestParam("idSourceConnection") Long idSourceConnection, 
			@RequestParam("uriFragment") String uriFragment
	) throws IOException, InstantiationException, IllegalAccessException, OperationNotSupportedException {
		process.findAffectedUrisAgroupAndMatchIssueWithLogGroups(idIssueTrackerConnection, idSourceConnection, uriFragment);
	}
	
	@GetMapping("/issuetrackerconnection/calculateTopNs")
	public StudyResult calculateTopNs(
			@RequestParam("idIssueTrackerConnection") Long idIssueTrackerConnection, 
			@RequestParam("idSourceConnection") Long idSourceConnection, 
			@RequestParam("fileExtensions") List<String> fileExtensions, 
			@RequestParam("sourcePackage") String sourcePackage,
			@RequestParam("checkSourcePackageInStackTrace") boolean checkSourcePackageInStackTrace,
			@RequestParam("maxEnabledChangedFiles") int maxEnabledChangedFiles) throws IOException, InstantiationException, IllegalAccessException, OperationNotSupportedException {
		return process.calculateTopNs(idIssueTrackerConnection, idSourceConnection, new TreeSet<>(fileExtensions), sourcePackage, checkSourcePackageInStackTrace, maxEnabledChangedFiles);
	}
}
