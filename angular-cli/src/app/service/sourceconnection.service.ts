import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { SourceConnection } from '../model/SourceConnection';
import { UriStats } from '../model/UriStats';
import { Log } from '../model/Log';
import { Study, Source, SuspiciousFile } from '../model';

@Injectable()
export class SourceConnectionService {
  urlGet = environment.apiUrl + '/sourceconnection';

  constructor(private http: HttpClient) { }

  getSourceConnection(id: number): Observable<SourceConnection> {
    return this.http.get<SourceConnection>(this.urlGet + '?id=' + id);
  }

  getLog(idSourceConnection: number, id: string): Observable<Log> {
    return this.http.get<Log>(this.urlGet + '/log/detail?idSourceConnection=' + idSourceConnection + '&id=' + id);
  }

  findLogsIdsByProperty(idSourceConnection: number, propertyName: string, propertyValue: string): Observable<string[]> {
    return this.http.get<string[]>(
      this.urlGet + '/log/idsbyproperty?idSourceConnection=' + idSourceConnection
      + '&propertyName=' + propertyName
      + '&propertyValue=' + propertyValue
    );
  }

  getLogCount(sourceConnection: SourceConnection): Observable<UriStats[]> {
    return this.http.get<UriStats[]>(this.urlGet + '/log/count?idSourceConnection=' + sourceConnection.id);
  }

  agroup(sourceConnection: SourceConnection, uris: string []): Observable<void> {
    const httpParams = new HttpParams();
    httpParams
      .append('idSourceConnection', sourceConnection.id.toString())
      .append('uris', JSON.stringify(uris));
    return this.http.post<void>(this.urlGet + '/log/agroup',
      {'idSourceConnection' : sourceConnection.id, 'uris' : uris})
    ;
  }

  createSourceConnection(
    studyId: number,
    sourceId: number,
    name: string,
    startDate: Date,
    endDate: Date,
    logType: string,
    groupAggregatorTypeId: string,
    suspiciousFilesFinderTypeId: string,
    regexSignature: string,
    regexIgnoreSignature: string,
    suspiciousFilesFilter: string
  ) {
    const sourceConnection = {
      study: { id: studyId },
      source: { id: sourceId },
      name: name,
      startDate: startDate,
      endDate: endDate,
      logType: logType,
      groupAggregatorTypeId: groupAggregatorTypeId,
      suspiciousFilesFinderTypeId: suspiciousFilesFinderTypeId,
      regexSignature: regexSignature,
      regexIgnoreSignature: regexIgnoreSignature,
      suspiciousFilesFilter: suspiciousFilesFilter
    };
    return this.http.post(this.urlGet, sourceConnection);
  }

  updateSourceConnection(
    id: number,
    studyId: number,
    sourceId: number,
    name: string,
    startDate: Date,
    endDate: Date,
    logType: string,
    groupAggregatorTypeId: string,
    suspiciousFilesFinderTypeId: string,
    regexSignature: string,
    regexIgnoreSignature: string,
    suspiciousFilesFilter: string
  ) {
    const sourceConnection = {
      id: id,
      study: { id: studyId },
      source: { id: sourceId },
      name: name,
      startDate: startDate,
      endDate: endDate,
      logType: logType,
      groupAggregatorTypeId: groupAggregatorTypeId,
      suspiciousFilesFinderTypeId: suspiciousFilesFinderTypeId,
      regexSignature: regexSignature,
      regexIgnoreSignature: regexIgnoreSignature,
      suspiciousFilesFilter: suspiciousFilesFilter
    };
    return this.http.put(this.urlGet, sourceConnection);
  }

  deleteSourceConnection(sourceConnection: SourceConnection) {
    return this.http.delete(this.urlGet + '?id=' + sourceConnection.id);
  }

  getLogDistByNumTraceElements(id: number) {
    return this.http.get<any[]>(this.urlGet + '/logDistByNumTraceElements?idSourceConnection=' + id);
  }

  getStudy(sourceConnection: SourceConnection): Observable<Study> {
    return this.http.get<Study>(this.urlGet + '/study?idSourceConnection=' + sourceConnection.id);
  }

  getSource(sourceConnection: SourceConnection): Observable<Source> {
    return this.http.get<Source>(this.urlGet + '/source?idSourceConnection=' + sourceConnection.id);
  }

  getSuspiciousFilesForAllLogGroups(sourceConnection: SourceConnection): Observable<SuspiciousFile[]> {
    return this.http.get<SuspiciousFile[]>(this.urlGet + '/suspiciousFilesForAllLogGroups?idSourceConnection=' + sourceConnection.id);
  }
}
