import { PropertyType } from './PropertyType.enum';

export interface Property {
    name: string;
    description: string;
    defaultValue: string;
    required: boolean;
    propertyType: PropertyType;
}
