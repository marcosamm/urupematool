import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SourceConnectionService, LogGroupService } from '../../service';
import { SourceConnection } from '../../model/SourceConnection';
import { LogGroup } from '../../model';
import { SelectableRowDblClick } from 'primeng/table';

@Component({
  selector: 'app-commonfcsfs',
  templateUrl: './commonfcsfs.component.html',
  styleUrls: ['./commonfcsfs.component.css']
})
export class CommonFCSFsComponent implements OnInit {
  sourceConnection: SourceConnection;
  logGroup: LogGroup;
  showLessThen: number;
  showPackage: string;

  constructor(
    private route: ActivatedRoute,
    private logGroupService: LogGroupService,
    private sourceConnectionService: SourceConnectionService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.showPackage = params['showPackage'];
      this.showLessThen = params['showLessThen'];
      this.showPackage = '';
      this.sourceConnectionService.getSourceConnection(params['idSourceConnection']).subscribe(
        sourceConnection => {
          this.sourceConnection = sourceConnection;
        }
      );
      this.logGroupService.getLogGroup(params['idLogGroup']).subscribe(
        logGroup => {
          this.logGroup = logGroup;
          this.logGroupService.getSubGroupsByLogGroup(logGroup).subscribe(
            subGroups => {
              this.logGroup.subGroups = subGroups.sort((lg1, lg2) => lg1.id - lg2.id);
              this.logGroup.subGroups.forEach(subGroup => {
                this.logGroupService.getMapFCSFsByLogGroup(subGroup).subscribe(map => {
                  subGroup.mapFCSFs = map;
                });
              });
            }
          );
        }
      );
    });
  }

  findCommonFCSFs(logGroupA: LogGroup, logGroupB: LogGroup): any [] {
    const commonFCSF = [];
    if (logGroupA.id !== logGroupB.id) {
      console.log('lgA.id=' + logGroupA.id + '; lgB.id=' + logGroupB.id);
      Object.keys(logGroupA.mapFCSFs).forEach(keyA => {
        Object.keys(logGroupB.mapFCSFs).forEach(keyB => {
          if (keyA === keyB) {
            console.log('     ==> rsA=' + logGroupA.mapFCSFs[keyA] + '; rsB=' + logGroupA.mapFCSFs[keyB] + '; keyA=' + keyA);
            commonFCSF.push(
              {
                fcsf: keyA,
                relativeSupportLogGroupA: logGroupA.mapFCSFs[keyA],
                relativeSupportLogGroupB: logGroupB.mapFCSFs[keyB]
              }
            );
          }
        });
      });
    }
    return commonFCSF;
  }

  isSelected(commonFCSFs: any []): boolean {
    let selected = false;
    if (this.showLessThen !== undefined && commonFCSFs.length < this.showLessThen){
      selected = true;
    }
    if (this.showPackage !== undefined){
      commonFCSFs.forEach(commonFCSF => {
        const indexOf = commonFCSF.fcsf.indexOf(this.showPackage);
        if (indexOf > -1) {
          selected = true;
        }
      });
    }
    return selected;
  }
}
