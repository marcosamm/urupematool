import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SourceConnectionService, LogGroupService, IssueService } from '../../service';
import { SourceConnection } from '../../model/SourceConnection';
import { LogGroup } from '../../model/LogGroup';
import { TimeStats } from '../../model';
import { UIChart } from 'primeng/chart';


@Component({
  selector: 'app-logsovertime',
  templateUrl: './logsovertime.component.html',
  styleUrls: ['./logsovertime.component.css']
})
export class LogsOverTimeComponent implements OnInit {
  chartLoaded = false;
  @ViewChild('chart', {static: false}) chart: UIChart;
  sourceConnection: SourceConnection;
  logGroup: LogGroup;
  cols: any[];
  rows: TimeStats[];
  chartData: any;
  chartOptions: any;
  maxCount: number;

  constructor(
    private route: ActivatedRoute,
    private logGroupService: LogGroupService,
    private sourceConnectionService: SourceConnectionService,
    private issueService: IssueService
  ) { }

  ngOnInit() {
    this.cols = [
      { field: 'date', header: 'Date' },
      { field: 'count', header: 'Count' }
    ];
    this.maxCount = 0;
    this.route.params.subscribe(params => {
      this.sourceConnectionService.getSourceConnection(params['idSourceConnection']).subscribe(
        sourceConnection => {
          this.sourceConnection = sourceConnection;
          this.logGroupService.getLogGroup(params['idLogGroup']).subscribe(
            logGroup => {
              this.logGroup = logGroup;
            }
          );

          this.logGroupService.getLogsOverTimeByLogGroupId(params['idLogGroup']).subscribe(
            async timesStats => {
              const labelsLogs: Date[] = [];
              const dataLogs = [];

              this.rows = timesStats;
              this.rows.forEach(element => {
                labelsLogs.push(element.date);
                dataLogs.push(element.count);
                this.maxCount = Math.max(this.maxCount, element.count);
              });
              this.maxCount = Math.round(1.2 * this.maxCount);

              const dataSets: any [] = [
                {
                  label: 'Logs',
                  data: dataLogs,
                  fill: false,
                  lineTension: 0,
                  backgroundColor: '#42A5F5',
                  borderColor: '#1E88E5',
                }
              ];

              this.chartData = {
                labels: labelsLogs,
                datasets: dataSets
              };
              this.chartOptions = {
                scales: {
                  xAxes: [{
                    distribution: 'linear',
                    time: {
                      unit: 'day',
                      displayFormats: {
                        day: 'MMM D'
                      }
                    },
                    stacked: true,
                  }],
                  yAxes: [{
                    ticks: {
                      beginAtZero: true,
                      max: this.maxCount
                    },
                    stacked: true
                  }]
                }
              };

              this.fillRevisions();
              this.chartLoaded = true;

            }
          );
        }
      );
    });
  }

  fillRevisions() {
    this.logGroupService.getIssues(this.logGroup).subscribe(issues => {
      issues.forEach(async issue => {
        let dataRevisions = [];
        this.chartData.labels.forEach(date => dataRevisions.push(0));
        const revisions = await this.issueService.getRevisionsByIssue(issue).toPromise();
          revisions.forEach(
            revision => {
              const revisionDateOnly = revision.date.toString().substr(0, 10);
              const minDateLogs = this.chartData.labels[0];
              const maxDateLogs = this.chartData.labels[this.chartData.labels.length - 1];
              if (revisionDateOnly < minDateLogs) {
                console.log("Tem que incluir antes: " + revisionDateOnly + " to " + minDateLogs);
                const minDate = new Date(minDateLogs);
                minDate.setDate(minDate.getDate() - 1);
                const beforeDates = this.getDaysArray(new Date(revisionDateOnly), minDate);
                const beforeLogs = [];
                beforeDates.forEach(date => {
                  beforeLogs.push(0);
                });
                this.chartData.labels = beforeDates.copyWithin(0, beforeDates.length).concat(this.chartData.labels);
                this.chartData.datasets[0].data = beforeLogs.concat(this.chartData.datasets[0].data);
                dataRevisions = beforeLogs.copyWithin(0, beforeLogs.length).concat(dataRevisions);
              } else if (revisionDateOnly > maxDateLogs) {
                const maxDate = new Date(maxDateLogs);
                maxDate.setDate(maxDate.getDate() + 1);
                const afterDates = this.getDaysArray(maxDate, new Date(revisionDateOnly));
                const afterLogs = [];
                afterDates.forEach(date => {
                  afterLogs.push(0);
                });
                this.chartData.labels = this.chartData.labels.concat(afterDates);
                this.chartData.datasets[0].data = this.chartData.datasets[0].data.concat(afterLogs);
                dataRevisions = dataRevisions.concat(afterLogs);
              }
              this.chartData.labels.forEach((date, index) => {
                if (date.toString() === revisionDateOnly) {
                  dataRevisions[index] = this.maxCount;
                }
              });
            }
          );
        this.chartData.datasets.push(
          {
            label: 'Revisions of Issue #' + issue.id,
            data: dataRevisions,
            backgroundColor: '#FF8181',
            borderColor: '#FF8181',
            barThickness: 2
          }
        );
        this.chart.reinit();
      });
    });
  }

  getDaysArray (start: Date, end: Date): any [] {
    const arr = [];
    const dt = start;
    for (; dt <= end; dt.setDate(dt.getDate() + 1)) {
        arr.push(new Date(dt).toISOString().substr(0, 10));
    }
    return arr;
  }
}
