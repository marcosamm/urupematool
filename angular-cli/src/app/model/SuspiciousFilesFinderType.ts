
export interface SuspiciousFilesFinderType {
    id: string;
    name: string;
    description: string;
}
