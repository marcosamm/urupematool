/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ElasticScenarioExtractor.java 
 * 4 de nov de 2017
 */
package br.ufrn.ase.urupema.elasticsearch.repository;

import static javax.xml.bind.DatatypeConverter.parseDateTime;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.ufrn.ase.urupema.domain.LogGroup;
import br.ufrn.ase.urupema.domain.SimpleWebExecutionTrace;
import br.ufrn.ase.urupema.domain.TimeStats;
import br.ufrn.ase.urupema.domain.TraceElement;
import br.ufrn.ase.urupema.domain.UriStats;
import br.ufrn.ase.urupema.domain.WebExecutionTrace;
import br.ufrn.ase.urupema.elasticsearch.parser.EsErrorLogJsonParser;
import br.ufrn.ase.urupema.elasticsearch.parser.EsGenericLogJsonParser;
import br.ufrn.ase.urupema.parser.StackTraceParser;
import br.ufrn.ase.urupema.repository.ErrorLogRepository;
import br.ufrn.ase.urupema.util.UrlUtil;


public class ElasticSearchErrorLogRepository extends AbstractLogRepository implements ErrorLogRepository {
	
	@Override
	public Set<UriStats> countGroupedByUri(Date start, Date end) throws IOException {
		return getUrisCount(start, end);
	}
	
	@Override
	public Set<UriStats> countGroupedByUri(Set<String> ids) throws IOException {
		return getUrisCount(ids);
	}
	
	@Override
	public Set<TimeStats> logsOverTime(Set<String> ids) throws IOException {
		return super.logsOverTime(ids);
	}
	
	private List<WebExecutionTrace> getByQuery(String query, EsErrorLogJsonParser errorJsonParser) throws IOException {
		List<WebExecutionTrace> logs = new ArrayList<>();
		
		String response = queryElasticSearch(query);
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"));
		JsonNode rootNode = mapper.readTree(response);
		JsonNode hitsNode = null;
		if(rootNode.get("responses") == null) {
			hitsNode = rootNode.get("hits").get("hits");
		}else {
			hitsNode = rootNode.get("responses").get(0).get("hits").get("hits");
		} 
		Iterator<JsonNode> i = hitsNode.elements();
		while(i.hasNext()) {
			logs.add(errorJsonParser.parse(i.next()));
		}
		
		return logs;	
	}
	
	@Override
	public Optional<WebExecutionTrace> findById(String id) throws IOException {
		return findById(id, new StackTraceParser());
	}
	
	@Override
	public Optional<WebExecutionTrace> findById(String id, StackTraceParser stackTraceParser)
			throws IOException {
		Optional<WebExecutionTrace> ret = Optional.empty();
		String query = "{\"query\": {\"match\" : {\""+EsGenericLogJsonParser.ID_FIELD_NAME+"\" : \""+id+"\"}}}";
		List<WebExecutionTrace> list = getByQuery(query, new EsErrorLogJsonParser(stackTraceParser));
		if(!list.isEmpty()) {
			ret = Optional.of(list.get(0));
		}
		return ret;
	}
	
	@Override
	public List<String> findIdsByDateBetween(Date start, Date end) throws IOException {
		String query = "{\"size\": 65000, \"query\": {\"range\":{\""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\":{\"gte\":"+start.getTime()+",\"lte\":"+end.getTime()+",\"format\":\"epoch_millis\"}}}, \"_source\": [\""+EsGenericLogJsonParser.ID_FIELD_NAME+"\"]}";
		return findIds(query);
	}
	
	@Override
	public List<String> findIdsByUriAndDateBetween(String uri, Date start, Date end) throws IOException {
		String query = "{\"size\": 65000, \"query\": {\"bool\": {\"must\": {\"wildcard\" : { \""+EsGenericLogJsonParser.URL_FIELD_NAME+".raw\" : \"*"+uri+"*\" }},\"filter\": {\"range\":{\""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\":{\"gte\":"+start.getTime()+",\"lte\":"+end.getTime()+",\"format\":\"epoch_millis\"}}}}}, \"_source\": [\""+EsGenericLogJsonParser.ID_FIELD_NAME+"\"]}";
		return findIds(query);
	}
	
	@Override
	public LogGroup agroupByUriSubgroupByStackTrace(String uri, Date start, Date end) throws IOException {
		LogGroup list = new LogGroup(uri);
		list.setTypeDescription("URI");
		Set<LogGroup> subGroups = new HashSet<>();
		
		
		String query = "{\"query\": {\"bool\": {\"must\": {\"wildcard\" : { \""+EsGenericLogJsonParser.URL_FIELD_NAME+".raw\" : \"*"+uri+"*\" }},\"filter\": {\"range\":{\""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\":{\"gte\":"+start.getTime()+",\"lte\":"+end.getTime()+",\"format\":\"epoch_millis\"}}}}},\"size\": 0, \"aggs\" : {\"stacktraces\": {\"terms\": {\"field\": \""+EsErrorLogJsonParser.STACKTRACE_FIELD_NAME+".raw\",\"size\": 65000},\"aggs\" : {\"ids\": {\"top_hits\": {\"_source\": {\"includes\": [\""+EsGenericLogJsonParser.ID_FIELD_NAME+"\", \""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\"]}, \"sort\": [{\""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\": {\"order\": \"asc\"}}] }}}}}}";
		String response = queryElasticSearch(query);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode = mapper.readTree(response);
		JsonNode backets = rootNode.get("aggregations").get("stacktraces").get("buckets"); 
		Iterator<JsonNode> b = backets.elements();
		while(b.hasNext()) {
			JsonNode bucket = b.next();
			String stackTrace = bucket.get("key").asText();
			if(stackTrace.contains("\u0000")) {
				stackTrace = stackTrace.replaceAll("\u0000", "");
			}
			LogGroup subGroup = new LogGroup(stackTrace);
			subGroup.setTypeDescription("Stack trace");
			JsonNode hitsNode = bucket.get("ids").get("hits").get("hits");
			Iterator<JsonNode> i = hitsNode.elements();
			while(i.hasNext()) {
				JsonNode n = i.next();
				subGroup.addElementId(n.get(EsGenericLogJsonParser.ID_FIELD_NAME).asText());
				Date date = parseDateTime(n.get("_source").get(EsGenericLogJsonParser.DATETIME_FIELD_NAME).asText()).getTime();
				if(subGroup.getFirstOccurrenceDate() == null || date.before(subGroup.getFirstOccurrenceDate())) {
					subGroup.setRepresentantId(n.get(EsGenericLogJsonParser.ID_FIELD_NAME).asText());
					subGroup.setFirstOccurrenceDate(date);
				}
				if(subGroup.getLastOccurrenceDate() == null || date.after(subGroup.getLastOccurrenceDate())) {
					subGroup.setLastOccurrenceDate(date);
				}
			}
			subGroups.add(subGroup);
			list.addElementsId(subGroup.getElementsId());
		}
		
		list.setSubGroups(subGroups);
		
		return list;
	}
	
	@Override
	public List<LogGroup> agroupByStackTrace(Set<String> uris, Date start, Date end) throws IOException {
		List<LogGroup> stackTraceGroups = new ArrayList<>();
		if(uris.isEmpty()) {
			throw new IllegalArgumentException("uris is empty");
		}
		
		StringBuilder should = new StringBuilder();
		should.append("\"should\": [");
		for (Iterator<String> iterator = uris.iterator(); iterator.hasNext();) {
			String uri = iterator.next();
			should.append("{\"wildcard\" : { \""+EsGenericLogJsonParser.URL_FIELD_NAME+".raw\" : \"*"+uri+"*\" } }");
			if(iterator.hasNext()) {
				should.append(",");
			}
		}
		should.append("]}");
		String query = "{\"query\": {\"bool\": {\"must\": [ {\"bool\": {"+should.toString()+"},{\"range\":{\""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\":{\"gte\":"+start.getTime()+",\"lte\":"+end.getTime()+",\"format\":\"epoch_millis\"}}}]}},\"size\": 0, \"aggs\" : {\"stacktraces\": {\"terms\": {\"field\": \""+EsErrorLogJsonParser.STACKTRACE_FIELD_NAME+".raw\",\"size\": 65000},\"aggs\" : {\"ids\": {\"top_hits\": {\"_source\": {\"includes\": [\""+EsGenericLogJsonParser.ID_FIELD_NAME+"\", \""+EsGenericLogJsonParser.URL_FIELD_NAME+"\", \""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\"]}, \"sort\": [{\""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\": {\"order\": \"asc\"}}] }}}}}}";
		String response = queryElasticSearch(query);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode = mapper.readTree(response);
		JsonNode backets = rootNode.get("aggregations").get("stacktraces").get("buckets");
		System.out.println("Total de grupos retornados: " + backets.size());
		Iterator<JsonNode> b = backets.elements();
		while(b.hasNext()) {
			JsonNode bucket = b.next();
			String stackTrace = bucket.get("key").asText();
			if(stackTrace.contains("\u0000")) {
				stackTrace = stackTrace.replaceAll("\u0000", "");
			}
			LogGroup subGroup = new LogGroup(stackTrace);
			subGroup.setTypeDescription("Stack trace");
			JsonNode hitsNode = bucket.get("ids").get("hits").get("hits");
			Iterator<JsonNode> i = hitsNode.elements();
			while(i.hasNext()) {
				JsonNode n = i.next();
				subGroup.addElementId(n.get(EsGenericLogJsonParser.ID_FIELD_NAME).asText());
				Date date = parseDateTime(n.get("_source").get(EsGenericLogJsonParser.DATETIME_FIELD_NAME).asText()).getTime();
				if(subGroup.getFirstOccurrenceDate() == null || date.before(subGroup.getFirstOccurrenceDate())) {
					subGroup.setRepresentantId(n.get(EsGenericLogJsonParser.ID_FIELD_NAME).asText());
					subGroup.setFirstOccurrenceDate(date);
				}
				if(subGroup.getLastOccurrenceDate() == null || date.after(subGroup.getLastOccurrenceDate())) {
					subGroup.setLastOccurrenceDate(date);
				}
			}
			stackTraceGroups.add(subGroup);
		}
		
		return stackTraceGroups;
	}
	
	@Override
	public List<LogGroup> agroupByStackTraceSubgroupByUri(Set<String> uris, Date start, Date end) throws IOException {
		if(uris.isEmpty()) {
			throw new IllegalArgumentException("uris is empty");
		}
		
		StringBuilder should = new StringBuilder();
		should.append("\"should\": [");
		for (Iterator<String> iterator = uris.iterator(); iterator.hasNext();) {
			String uri = iterator.next();
			should.append("{\"wildcard\" : { \""+EsGenericLogJsonParser.URL_FIELD_NAME+".raw\" : \"*"+uri+"*\" } }");
			if(iterator.hasNext()) {
				should.append(",");
			}
		}
		should.append("]}");
		String query = "{\"query\": {\"bool\": {\"must\": [ {\"bool\": {"+should.toString()+"},{\"range\":{\""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\":{\"gte\":"+start.getTime()+",\"lte\":"+end.getTime()+",\"format\":\"epoch_millis\"}}}]}},\"size\": 0, \"aggs\" : {\"stacktraces\": {\"terms\": {\"field\": \""+EsErrorLogJsonParser.STACKTRACE_FIELD_NAME+".raw\",\"size\": 65000},\"aggs\" : {\"ids\": {\"top_hits\": {\"_source\": {\"includes\": [\""+EsGenericLogJsonParser.ID_FIELD_NAME+"\", \""+EsGenericLogJsonParser.URL_FIELD_NAME+"\", \""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\"]}, \"sort\": [{\""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\": {\"order\": \"asc\"}}] }}}}}}";
		return agroupByStackTraceSubgroupByUriFromQuery(query);
	}
	
	private List<LogGroup> agroupByStackTraceSubgroupByUriFromQuery(String query) throws IOException {
		List<LogGroup> stackTraceGroups = new ArrayList<>();
		
		if(query == null || query.isEmpty()) {
			throw new IllegalArgumentException("query is empty");
		}
		
		String response = queryElasticSearch(query);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode = mapper.readTree(response);
		JsonNode backets = rootNode.get("aggregations").get("stacktraces").get("buckets"); 
		Iterator<JsonNode> b = backets.elements();
		while(b.hasNext()) {
			Map<String, LogGroup> uriMap = new HashMap<>();
			JsonNode bucket = b.next();
			String stackTrace = bucket.get("key").asText();
			if(stackTrace.contains("\u0000")) {
				stackTrace = stackTrace.replaceAll("\u0000", "");
			}
			LogGroup stackTraceGroup = new LogGroup(stackTrace);
			stackTraceGroup.setTypeDescription("Stack trace");
			JsonNode hitsNode = bucket.get("ids").get("hits").get("hits");
			Iterator<JsonNode> i = hitsNode.elements();
			while(i.hasNext()) {
				JsonNode node = i.next();
				String elementId = node.get(EsGenericLogJsonParser.ID_FIELD_NAME).asText();
				Date date = parseDateTime(node.get("_source").get(EsGenericLogJsonParser.DATETIME_FIELD_NAME).asText()).getTime();
				if(stackTraceGroup.getFirstOccurrenceDate() == null || date.before(stackTraceGroup.getFirstOccurrenceDate())) {
					stackTraceGroup.setRepresentantId(elementId);
					stackTraceGroup.setFirstOccurrenceDate(date);
				}
				if(stackTraceGroup.getLastOccurrenceDate() == null || date.after(stackTraceGroup.getLastOccurrenceDate())) {
					stackTraceGroup.setLastOccurrenceDate(date);
				}
				
				String uri = UrlUtil.getUri(node.get("_source").get(EsGenericLogJsonParser.URL_FIELD_NAME).asText());
				stackTraceGroup.addElementId(elementId);
				if(!uriMap.containsKey(uri)) {
					LogGroup uriSubGroup = new LogGroup(uri);
					uriMap.put(uri, uriSubGroup);
					stackTraceGroup.addSubGroup(uriSubGroup);
				}
				LogGroup uriSubGroup = uriMap.get(uri);
				uriSubGroup.setTypeDescription("URI");
				uriSubGroup.addElementId(elementId);
				if(uriSubGroup.getFirstOccurrenceDate() == null || date.before(uriSubGroup.getFirstOccurrenceDate())) {
					uriSubGroup.setFirstOccurrenceDate(date);
					uriSubGroup.setRepresentantId(elementId);
				}
				if(uriSubGroup.getLastOccurrenceDate() == null || date.after(uriSubGroup.getLastOccurrenceDate())) {
					uriSubGroup.setLastOccurrenceDate(date);
				}
			}
			stackTraceGroups.add(stackTraceGroup);
		}
		
		return stackTraceGroups;
	}
	
	public List<LogGroup> agroupByStackTracesSubgroupByUriFromStacktraceAndDateBetween(String stackTrace, Date start, Date end,  String uriFragment) throws IOException {
		StackTraceParser stp = new StackTraceParser();
		String stackTrackEquivWithoutMessages = StackTraceParser.equivalentStackTrace(StackTraceParser.stackTraceWithoutMessages(stackTrace));
		Set<String> strings = new TreeSet<>();
		SimpleWebExecutionTrace swet = stp.toSimpleError(stackTrackEquivWithoutMessages);
		TraceElement te = null;
		if(!swet.getTraceElements().isEmpty()) {
			te = swet.getTraceElements().get(0);
		}
		while(te != null) {
			String s = te.getSignature().replace("000", "*");
			if(te.getLineNumber() != null && te.getLineNumber() > 0) {
				s += "\\\\(" + te.getFileName() + "\\\\:" + te.getLineNumber() + "\\\\)";
			}
			strings.add(s);
			if(!te.getChildren().isEmpty()) {
				te = te.getChildren().get(0);
			}else {
				te = null;
			}
		}
		for(String jsfExpression : StackTraceParser.getJsfExpresions(stackTrace)) {
			String scapedJsfExpression = scapeWithBackslash(jsfExpression);
			strings.add(scapedJsfExpression);
		}
		for(String exceptionsLIne : StackTraceParser.getLinesWithExceptions(stackTrackEquivWithoutMessages)) {
			exceptionsLIne.replace(":", "");
			for(String exLn : Arrays.asList(exceptionsLIne.split(" "))) {
				strings.add(scapeWithBackslash(exLn));
			}
		}
		StringBuilder must = new StringBuilder();
		for (Iterator<String> iterator = strings.iterator(); iterator.hasNext();) {
			String stackTraceFragment = iterator.next();
			must.append(
					"            {\n" + 
					"              \"query_string\": {\n" + 
					"                \"query\": \""+EsErrorLogJsonParser.STACKTRACE_FIELD_NAME+" : *"+stackTraceFragment+"*\"\n" + 
					"              }\n" + 
					"            }"
			);
			if(iterator.hasNext()) {
				must.append(",");
			}
			must.append("\n");
		}
		
		if(uriFragment != null && !uriFragment.isEmpty()) {
			must.append(",\n            {\"wildcard\" : { \""+EsGenericLogJsonParser.URL_FIELD_NAME+".raw\" : \"*"+scapeWithBackslash(uriFragment)+"*\" } }\n");
		}
		
		String query = "{\n" + 
				"  \"query\": {\n" + 
				"    \"filtered\": {\n" + 
				"      \"query\": {\n" + 
				"        \"bool\": {\n" + 
				"          \"must\": [\n" + 
								must.toString() + 
				"          ]\n" + 
				"        }\n" + 
				"      },\n" + 
				"      \"filter\": {\n" + 
				"        \"bool\": {\n" + 
				"          \"must\": [\n" + 
				"            {\n" + 
				"              \"range\": {\n" + 
				"                \""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\": {\n" + 
				"                  \"gte\": "+start.getTime()+",\n" + 
				"                  \"lte\": "+end.getTime()+",\n" + 
				"                  \"format\": \"epoch_millis\"\n" + 
				"                }\n" + 
				"              }\n" + 
				"            }\n" + 
				"          ]\n" + 
				"        }\n" + 
				"      }\n" + 
				"    }\n" + 
				"  },\n" + 
				"  \"aggs\": {\n" + 
				"    \"stacktraces\": {\n" + 
				"      \"terms\": {\n" + 
				"        \"field\": \""+EsErrorLogJsonParser.STACKTRACE_FIELD_NAME+".raw\",\n" + 
				"        \"size\": 65000\n" + 
				"      },\n" + 
				"      \"aggs\": {\n" + 
				"        \"ids\": {\n" + 
				"          \"top_hits\": {\n" + 
				"            \"_source\": {\n" + 
				"              \"includes\": [\n" + 
				"                \""+EsGenericLogJsonParser.ID_FIELD_NAME+"\",\n" + 
				"                \""+EsGenericLogJsonParser.URL_FIELD_NAME+"\",\n" + 
				"                \""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\"\n" + 
				"              ]\n" + 
				"            },\n" + 
				"            \"sort\": [\n" + 
				"              {\n" + 
				"                \""+EsGenericLogJsonParser.DATETIME_FIELD_NAME+"\": {\n" + 
				"                  \"order\": \"asc\"\n" + 
				"                }\n" + 
				"              }\n" + 
				"            ]\n" + 
				"          }\n" + 
				"        }\n" + 
				"      }\n" + 
				"    }\n" + 
				"  }\n" + 
				"}";
		List<LogGroup> lgs = agroupByStackTraceSubgroupByUriFromQuery(query);
		//System.out.println("stackTrackEquivWithoutMessages\n\n" + stackTrackEquivWithoutMessages + "\n\n");
		for (Iterator<LogGroup> iterator = lgs.iterator(); iterator.hasNext();) {
			LogGroup lg = iterator.next();
			String lgStackTrackEquivWithoutMessages = StackTraceParser.equivalentStackTrace(StackTraceParser.stackTraceWithoutMessages(lg.getLabel()));
			//System.out.println("lgStackTrackEquivWithoutMessages\n\n" + lgStackTrackEquivWithoutMessages + "\n\n");
			if(!lgStackTrackEquivWithoutMessages.contains(stackTrackEquivWithoutMessages) && !stackTrackEquivWithoutMessages.contains(lgStackTrackEquivWithoutMessages)) {
				SimpleWebExecutionTrace swetLg = stp.toSimpleError(lgStackTrackEquivWithoutMessages); 
				if(!swetLg.contains(swet.getTraceElements())) {
					iterator.remove();
				}
			}
		}
		return lgs;
	}
	
	@Override
	public Set<String> findAffectedUrisByEquivalentStackTracesAndDateBetween(String stackTrace, Date start, Date end, String uriFragment) throws IOException {
		Set<String> uris = new TreeSet<>();
		for(LogGroup lgSt : agroupByStackTracesSubgroupByUriFromStacktraceAndDateBetween(stackTrace, start, end, uriFragment)){
			for(LogGroup lgUri : lgSt.getSubGroups()) {
				uris.add(lgUri.getLabel());
			}
		}
		return uris;
	}
	
	public static String scapeWithBackslash(String string) {
		String st = string;
		st = st.replace("{", "\\\\{").replace("}", "\\\\}");
		st = st.replace(":", "\\\\:");
		st = st.replace("!", "\\\\!");
		st = st.replace("+", "\\\\+");
		st = st.replace("-", "\\\\-");
		st = st.replace("=", "\\\\=");
		st = st.replace("&", "\\\\&");
		st = st.replace("|", "\\\\|");
		st = st.replace("(", "\\\\(").replace(")", "\\\\)");
		st = st.replace("[", "\\\\[").replace("]", "\\\\]");
		st = st.replace("^", "\\\\^");
		st = st.replace("~", "\\\\~");
		st = st.replace("*", "\\\\*");
		st = st.replace("?", "\\\\?");
		st = st.replace("/", "\\\\/");
		st = st.replace(">", "?").replace("<", "?");
		st = st.replace(" ", "\\\\ ");
		return st;
	}
}
