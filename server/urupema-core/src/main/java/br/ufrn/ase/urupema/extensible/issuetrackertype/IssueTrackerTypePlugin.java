package br.ufrn.ase.urupema.extensible.issuetrackertype;

import java.util.Map;

import br.ufrn.ase.urupema.domain.IssueTrackerType;
import br.ufrn.ase.urupema.repository.IssueTrackerRepository;

public interface IssueTrackerTypePlugin extends IssueTrackerType {
	public IssueTrackerRepository getIssueTrackerRepository(Map<String, String> connectionProperties);
}
