package br.ufrn.ase.urupema.resources;

import java.io.IOException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.ase.urupema.domain.ReportedStackTrace;
import br.ufrn.ase.urupema.process.ReportedStackTraceProcess;


@RestController
@CrossOrigin(origins = "http://localhost:4200") // allow access or application form other adresses
public class ReportedStackTraceResource {	
	@Autowired
	ReportedStackTraceProcess process;
	
	@GetMapping("/reportedstacktrace")
	public ReportedStackTrace findById(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		return process.findById(id);
	}
	
	@PostMapping("/reportedstacktrace")
	public void put(@RequestBody ReportedStackTrace reportedStackTrace) throws IOException {
		process.save(reportedStackTrace);
	}
	
	@PutMapping("/reportedstacktrace")
	public void update(@RequestBody ReportedStackTrace reportedStackTrace) throws IOException, InstantiationException, IllegalAccessException {
		ReportedStackTrace i = findById(reportedStackTrace.getId());
		//Copy properties ignoring logGroups
		BeanUtils.copyProperties(reportedStackTrace, i, "affectedUris", "issue");
		process.save(i);
	}
	
	@DeleteMapping("/reportedstacktrace")
	public void delete(@RequestParam("id") Long id) throws IOException, InstantiationException, IllegalAccessException {
		process.delete(id);
	}
}
