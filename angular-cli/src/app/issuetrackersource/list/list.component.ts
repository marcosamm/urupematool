import { Component, OnInit } from '@angular/core';

import { IssueTrackerSourceService } from '../../service';
import { IssueTrackerSource } from '../../model';

@Component({
  selector: 'app-source',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class IssueTrackerSourceListComponent implements OnInit {
  issueTrackerSources: IssueTrackerSource[] = [];

  constructor(private issueTrackerSourceService: IssueTrackerSourceService) { }

  ngOnInit() {
    this.issueTrackerSourceService.getIssueTrackerSources().subscribe(issueTrackerSources => this.issueTrackerSources = issueTrackerSources);
  }

  deleteIssueTrackerSource(issueTrackerSource: IssueTrackerSource) {
    if (window.confirm('Are sure you want to delete this item ?')) {
      this.issueTrackerSourceService.deleteIssueTrackerSource(issueTrackerSource).subscribe(
        res => { this.ngOnInit(); }
      );
    }
  }
}
