import { LineNumberStats } from './LineNumberStats';

export interface CalledMethodStats {
  qualifiedMethodName: string;
  count: number;
  lineNumbersStats: LineNumberStats[];
}
