import { Component, OnInit, Input } from '@angular/core';
import { GroupCorrelator } from '../../model/GroupCorrelator';

@Component({
  selector: 'app-groupcorrelatordetail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class GroupCorrelatorDetailComponent implements OnInit {
  @Input() groupCorrelator: GroupCorrelator;

  constructor() { }

  ngOnInit() {
  }

}
