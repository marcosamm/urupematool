package br.ufrn.ase.urupema.domain;

import lombok.Getter;

@Getter
public enum LogType {
	ACCESS("Access"),
	ERROR("Error"),
	EXECUTION("Execution");

	private final String label;

	private LogType(String label) {
		this.label = label;
	}
}
